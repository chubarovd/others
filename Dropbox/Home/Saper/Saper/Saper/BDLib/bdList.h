template <typename T> class BdList {
public :
	static const int MAX_SIZE = 1000;
	int size;
	T arr[MAX_SIZE];
	int errorCode;
public :
	int getErrorCode();
	int getSize();
	void setValue(int index, T value);
	void add(T value);
	void add(int index, T value);
	void remove(int index);
	void clear();
	int removeValue(T value);
	T getValue(int index);
	int findValue(T value);
	BdList();
};

template <class T> void BdList<T>::clear() {
    size = 0;
    errorCode = 1;
    T tmp;
    for (int i = 0; i < MAX_SIZE; i++) {
        arr[i] = tmp;
    }
}

template <typename T> int BdList<T>::getErrorCode() {
	return errorCode;
}

template <typename T> int BdList<T>::getSize() {
	return size;
}

template <typename T> void BdList<T>::setValue(int index, T value) {
	if (0 <= index && index < size) {
		arr[index] = value;
		errorCode = 0;
	} else {
		errorCode = -1;
	}
}

template <typename T> void BdList<T>::add(int index, T value) {
	if (0 <= index && index <= size && size < MAX_SIZE) {
		for (int i = size; i > index; i--) {
			arr[i] = arr[i-1];
		}
		arr[index] = value;
		size ++;
		errorCode = 0;
	} else {
		errorCode = -1;
	}
}

template <typename T> void BdList<T>::add(T value) {
	add(size, value);
}

template <typename T> void BdList<T>::remove(int index) {
	if (0 <= index && index < size) {
		for (int i = index; i < size-1; i++) {
			arr[i] = arr[i+1];
		}
		size --;
		errorCode = 0;
	} else {
		errorCode = -1;
	}
}

template <typename T> int BdList<T>::removeValue(T value) {
	for (int i = 0; i < size; i++) {
		if(arr[i] == value) {
			for (int j = i; j < size-1; j++) {
				arr[j] = arr[j+1];
			}
			size--;
			errorCode = 0;
			return i;
		}
	}
	errorCode = -1;
	return -1;
}

template <typename T> T BdList<T>::getValue(int index) {
	if(0 <= index && index < size) {
		errorCode = 0;
		return arr[index];
	} else {
		errorCode = -1;
	}

}

template <typename T> int BdList<T>::findValue(T value) {
    for(int i = 0; i < size; i++) {
        if(arr[i] == value) {
            errorCode = 0;
            return i;
        }
    }
    errorCode = 0;
    return -1;
}

template <typename T>BdList<T>::BdList() {
	size = 0;
	errorCode = 0;
}

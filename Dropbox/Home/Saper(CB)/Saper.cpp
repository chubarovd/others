// Saper.cpp : Defines the entry point for the console application.
//

#include "./lib/BdLib.cpp"
#include "./lib/gameSaper.cpp"

class Form : public BdComponentListener {
public :
	BdWindow window;

	BdButton play_b;
	BdButton empty_b;
	BdButton exit_b;

	void onClick(BdComponent* v, int x, int y);

	Form();
};

int main() {
	Form form;
	form.window.show();
	return 0;
}

Form::Form() : window(400, 600),
					play_b(40, 50, 320, 60, "play", true),
					empty_b(40, 160, 320, 60, "empty", true),
					exit_b(40, 270, 320, 60, "exit", true) {
	window.container.addChild(&play_b);
	window.container.addChild(&empty_b);
	window.container.addChild(&exit_b);

	play_b.addListener(this);
	empty_b.addListener(this);
	exit_b.addListener(this);
}

void Form::onClick(BdComponent* v, int x, int y) {
    if(v == &play_b) {   // Starts the game
        startGame();
    }
    if(v == &empty_b) {  // I don't know what this button doing...

    }
    if(v == &exit_b) {   // Exit from the game
        txDisableAutoPause();
        exit(0);
    }
}

#include "..\TXLib.h"

int angle = 0;

int MA[3][3] = {1, 0, 0,
                1, 0, 0,
                1, 1, 0};
int MB[3][3] = {0, 1, 0,
                1, 1, 0,
                0, 1, 0};
int MC[3][3] = {0, 0, 1,
                1, 0, 1,
                0, 1, 1};
int MPX[3][3] = {1, 0, 0,
                 0, cos(angle), -sin(angle),
                 0, sin(angle), cos(angle)};
int MPZ[3][3] = {cos(angle), -sin(angle), 0,
                 sin(angle), cos(angle), 0,
                 0, 0, 1};


double dist = 50;
double xSize = 400;
double ySize = 200;

int xnum = 300;
int ynum = 50;
int znum = 500;



double Matrix(double x0, double y0, double z0,
              double x1, double y1, double z1,
              double x2, double y2, double z2) {
    return (x0*y1*z2) + (y0*x2*z1) + (x1*z0*y2) - (z0*y1*x2) - (x0*y2*z1) - (x1*y0*z2);
}

double MatrixP(char napr) {
    int MP[3][3];
    for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
            MP[i][j] = 0;
        }
    }
    if(napr == 'x') {
        for(int n = 0; n < 3; n++) {
            for(int i = 0; i < 3; i++) {
                for(int j = 0; j < 3; j++) {
                    MP[i][j] += MA[i][n]*MPX[n][j];
                }
            }
        }
    } else if(napr == 'y') {

    } else if(napr == 'z') {
        for(int n = 0; n < 3; n++) {
            for(int i = 0; i < 3; i++) {
                for(int j = 0; j < 3; j++) {
                    MP[i][j] += MC[i][n]*MPZ[n][j];
                }
            }
        }
    }
}

double A = Matrix(1, 0, 0,
                  1, 0, 0,
                  1, 1, 0);
double B = Matrix(0, 1, 0,
                  1, 1, 0,
                  0, 1, 0);
double C = Matrix(0, 0, 1,
                  1, 0, 1,
                  0, 1, 1);

void draw(double x, double y, double z, double x1, double y1, double z1) {
//    double a = x / (xSize/2);
//    double b = y / (ySize/2);
//    double c = (z + dist) / dist;
//
//    double screenX = xSize/2 + xSize/2 * a/c + 200;
//    double screenY = ySize/2 - ySize/2 * b/c + 200;
//
////--------------------------------------------
//
//    double a1 = x1 / (xSize/2);
//    double b1 = y1 / (ySize/2);
//    double c1 = (z1 + dist) / dist;
//
//    double screenX1 = xSize/2 + xSize/2 * a1/c1 + 200;
//    double screenY1 = ySize/2 - ySize/2 * b1/c1 + 200;

    double a = x - xnum;
    double b = y - ynum;
    double c = z - znum;

    double t = (-1)*(A*x + B*y + C*z)/(A*a + B*b + C*c);
    x = x + a*t + 100;
    y = y + b*t + 100;

    double a1 = x1 - xnum;
    double b1 = y1 - ynum;
    double c1 = z1 - znum;

    double t1 = (-1)*(A*x1 + B*y1 + C*z1)/(A*a1 + B*a1 + C*c1);
    x1 = x1 + a1*t1 + 100;
    y1 = y1 + b1*t1 + 100;

    txSetColor(TX_BLACK);
//    printf("%.0lf %.0lf %.0lf %.0lf\n", x, y, x1, y1);
    txLine(x, y, x1, y1);
}

int main() {
    txCreateWindow(900, 700);
    txSetColor(TX_WHITE);
    txClear();

    while(!GetAsyncKeyState(VK_ESCAPE)) {
        if(GetAsyncKeyState(VK_DOWN)) {
//            ynum -= 30;
            angle += 10;
            A = MatrixP('x');
        } else {

        }
        if(GetAsyncKeyState(VK_UP)) {
//            ynum += 30;
            angle -= 10;
            A = MatrixP('x');
        } else {

        }
        if(GetAsyncKeyState(VK_RIGHT)) {
//            xnum -= 30;
            angle += 10;
            B = MatrixP('z');
        } else {

        }
        if(GetAsyncKeyState(VK_LEFT)) {
//            xnum += 30;
            angle -= 10;
            B = MatrixP('z');
        } else {

        }

        draw(1, 1, 1, 200, 1, 1);
        draw(200, 1, 1, 100, 100*sqrt(3), 1);
        draw(100, 100*sqrt(3), 1, 1, 1, 1);
        draw(100, 100*sqrt(3)/3, 100*sqrt(6)/3, 1, 1, 1);
        draw(100, 100*sqrt(3)/3, 100*sqrt(6)/3, 200, 1, 1);
        draw(100, 100*sqrt(3)/3, 100*sqrt(6)/3, 100, 100*sqrt(3), 1);
        txSetColor(TX_WHITE);
        txSleep(10);
        txClear();
    }
}




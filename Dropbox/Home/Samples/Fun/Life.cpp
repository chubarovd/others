#include "../../BdLibFromSVN/BdLib.cpp"
#include "../../BdLibFromSVN/BdTimer.cpp"

const int ONE_CELL = 20;
const int ONE_YEAR_IN_MS = 500;
const int WINDOW_X = 800;
const int WINDOW_Y = 600;

class Organism;

BdList<Organism*> organisms;
int area[WINDOW_X/ONE_CELL - 1][WINDOW_Y/ONE_CELL - 1];

class Organism : public BdComponent {
public:
    enum Lifeway {PREDATOR, HERBIVORE, OMNIVORE, PLANT};
    Lifeway lw;

    int ages;

    void draw(Surface* s);
    void process(TxEvent* event);
    void randomCords();

    Organism();
    Organism(Lifeway lw_);
};

class LifeProcess : public BdTimerListener{
public:
    BdWindow window;
    BdTimer timer;

    void addOrganism(Organism* org);
    void removeOrganism(Organism* org);
    void onTick(BdTimer* timer);

    LifeProcess();
};

int main() {
    srand(time(NULL));
    LifeProcess lp;
    lp.window.show();
}

//-------------------------------------------------------------------
//---------------------------Organism--------------------------------
//-------------------------------------------------------------------

Organism::Organism() : BdComponent() {
    randomCords();
    lw = (Lifeway)(rand() % 4);
    ages = 0;
}

Organism::Organism(Lifeway lw_) : BdComponent() {
    randomCords();
    lw = lw_;
    ages = 0;
}

void Organism::draw(Surface* s) {
    s->setFillColor(TX_TRANSPARENT);
    switch(lw) {
    case PREDATOR :
        s->setColor(RGB(227, 38, 54), 2);
        s->rectangle(0, 0, ONE_CELL, ONE_CELL);
        break;
    case HERBIVORE :
        s->setColor(RGB(251, 236, 93), 2);
        s->circle(ONE_CELL/2, ONE_CELL/2, ONE_CELL/2);
        break;
    case OMNIVORE :
        s->setColor(RGB(0, 127, 255), 2);
        s->line(0, 0, ONE_CELL, 0);
        s->line(ONE_CELL, 0, ONE_CELL/2, ONE_CELL);
        s->line(ONE_CELL/2, ONE_CELL, 0, 0);
        break;
    case PLANT :
        s->setColor(RGB(141, 182, 0), 2);
        s->line(ONE_CELL/2, 0, ONE_CELL, ONE_CELL);
        s->line(ONE_CELL, ONE_CELL, 0, ONE_CELL);
        s->line(0, ONE_CELL, ONE_CELL/2, 0);
        break;
    }
}

void Organism::process(TxEvent* event) {
    int napr = rand() % 5;
    switch(napr) {
    case 0 : // do nothing
        break;
    case 1 : // move UP if it possible
        if(area[x/ONE_CELL][y/ONE_CELL - 1] == 0 && y/ONE_CELL >= 0) {
            area[x/ONE_CELL][y/ONE_CELL] = 0;
            y -= ONE_CELL;
            area[x/ONE_CELL][y/ONE_CELL] = 1;
        }
        break;
    case 2 : // move DOWN if it possible
        if(area[x/ONE_CELL][y/ONE_CELL + 1] == 0 && y/ONE_CELL  < (WINDOW_Y/ONE_CELL)) {
            area[x/ONE_CELL][y/ONE_CELL] = 0;
            y += ONE_CELL;
            area[x/ONE_CELL][y/ONE_CELL] = 1;
        }
        break;
    case 3 : // move LEFT if it possible
        if(area[x/ONE_CELL - 1][y/ONE_CELL] == 0 && x/ONE_CELL >= 0) {
            area[x/ONE_CELL][y/ONE_CELL] = 0;
            x -= ONE_CELL;
            area[x/ONE_CELL][y/ONE_CELL] = 1;
        }
        break;
    case 4 : // move RIGHT if it possible
        if(area[x/ONE_CELL + 1][y/ONE_CELL] == 0 && x/ONE_CELL < (WINDOW_X/ONE_CELL - 1)) {
            area[x/ONE_CELL][y/ONE_CELL] = 0;
            x += ONE_CELL;
            area[x/ONE_CELL][y/ONE_CELL] = 1;
        }
        break;
    }
}

void Organism::randomCords() {
    x = (rand() % (WINDOW_X/ONE_CELL - 1))*ONE_CELL;
    for(int i = 0; i < organisms.getSize() && area[x/ONE_CELL][i] != 0; i++) {
        area[x/ONE_CELL][i] = 0;
        x = (rand() % (WINDOW_X/ONE_CELL - 1))*ONE_CELL;
        area[x/ONE_CELL][i] = 1;
        break;
    }
    y = (rand() % (WINDOW_Y/ONE_CELL - 1))*ONE_CELL;
    for(int i = 0; i < organisms.getSize() && area[i][y/ONE_CELL] != 0; i++) {
        area[i][y/ONE_CELL] = 0;
        y = (rand() % (WINDOW_Y/ONE_CELL - 1))*ONE_CELL;
        area[i][y/ONE_CELL] = 1;
        break;
    }
}

//-------------------------------------------------------------------
//--------------------------LifeProcess------------------------------
//-------------------------------------------------------------------

LifeProcess::LifeProcess() : window(WINDOW_X, WINDOW_Y + 10){
    for(int i = 0; i < WINDOW_X/ONE_CELL - 1; i++) {
        for(int j = 0; j < WINDOW_Y/ONE_CELL - 1; j++) {
            area[i][j] = 0;
        }
    }

    addOrganism(new Organism(Organism::PREDATOR));
    addOrganism(new Organism(Organism::PREDATOR));
    addOrganism(new Organism(Organism::HERBIVORE));
    addOrganism(new Organism(Organism::HERBIVORE));
    addOrganism(new Organism(Organism::OMNIVORE));
    addOrganism(new Organism(Organism::OMNIVORE));
    addOrganism(new Organism(Organism::PLANT));
    addOrganism(new Organism(Organism::PLANT));

    window.container.addChild(&timer);
    timer.setPeriodMillis(ONE_YEAR_IN_MS);
    timer.addListener(this);
    timer.start();
}

void LifeProcess::addOrganism(Organism* org) {
    window.container.addChild(org);
    organisms.add(org);
}

void LifeProcess::removeOrganism(Organism* org) {
    window.container.removeChild(org);
    organisms.removeValue(org);
}

void LifeProcess::onTick(BdTimer* timer) {
    for(int i = 0; i < WINDOW_X/ONE_CELL - 1; i++) {
        for(int j = 0; j < WINDOW_Y/ONE_CELL - 1; j++) {
            /*if(area[i][j] != 0) */txSetColor(TX_BLACK, 2);
//            else if(area[i][j] == 0) txSetColor(TX_TRANSPARENT);
//            txRectangle(i*ONE_CELL, j*ONE_CELL, (i + 1)*ONE_CELL, (j + 1)*ONE_CELL);
        }
    }
}

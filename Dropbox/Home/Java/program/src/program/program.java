package program;

import java.io.File;
import java.util.Scanner;

class program {
	static int MAX = 101;

	int n, ans;
	char[][] g = new char [MAX][MAX];
	static enum Colors{W, G, B};
	Colors[][] color = new Colors [MAX][MAX];
	
	static int dr[] = {-1, 0, 1, 0};
	static int dc[] = {0, 1, 0, -1};

	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(new File("input.txt"));
		int n;
		n = sc.nextInt();
		int[][] arr = new int[n][n];
		for(int i = 0; i < n; i++) {
			for(int j = 0; j < n; j++) {
				arr[i][j] = sc.nextInt();
			}
		}
	}
}

class point_t {
	int r;
    int c;
    point_t(int u_, int v_) {
		r = u_;
		c = v_;
	}
}
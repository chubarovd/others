package attempts;

import javax.swing.JOptionPane;
import java.util.Scanner;

public class Attempts {
    public static void main(String[] args) {
//		int a, b;
//		String a_char = JOptionPane.showInputDialog("Enter first value:");
//		a = Integer.parseInt(a_char);
//		String b_char = JOptionPane.showInputDialog("Enter second value:");
//		b = Integer.parseInt(b_char);
//		X.SetNumbers(a, b);
//		System.out.println(X.ReturnSumOfNumbers());
		Scanner sc = new Scanner(System.in);
		if(sc.hasNextInt()) {
			System.out.println(sc.nextInt());
		} else if(sc.hasNextFloat()) {
			System.out.println(sc.nextFloat());
		} else if(sc.hasNext()) {
			System.out.println(sc.next());
		}
    }

}
class X {
	private static int a;
	private static int b;
	private static boolean numbersSetted = false;
	public static void SetNumbers(int a_, int b_) {
		a = a_;
		b = b_;
		numbersSetted = true;
	}
	
	public static int ReturnSumOfNumbers() {
		if(numbersSetted) {
			System.out.println(a + " " + b);
			return (a+b);
		}
		System.out.println("Error! \n Numbers don't specified.");
		return 0;
	}
}
package jsocket;

import java.net.*;
import java.io.*;
import java.util.Scanner;

public class Jsocket {
	public static void main(String[] args) {	
		int port = 2215;
		String address = "localhost";
		Scanner sc = new Scanner(System.in);
		if(sc.hasNext() && "server".equals(sc.next())) {
			try {
				ServerSocket s = new ServerSocket(port);
				s.bind(s.getLocalSocketAddress());
				System.out.println("Waiting for a client...");
				s.setSoTimeout(1000);
				Socket cs = s.accept();
				System.out.println("Got a client :");
				InputStream sin = cs.getInputStream();
				OutputStream sout = cs.getOutputStream();
				//^---------v
				DataInputStream in = new DataInputStream(sin);
				DataOutputStream out = new DataOutputStream(sout);

				String line;
				while(true) {
					line = in.readUTF(); // ожидаем пока клиент пришлет строку текста.
					System.out.println("Client sent me this line : " + line);
					System.out.println("I'm sending it back...");
					out.writeUTF(line); // отсылаем клиенту обратно ту самую строку текста.
					out.flush(); // заставляем поток закончить передачу данных.
					System.out.println("Waiting for the next line...");
					System.out.println();
				}
			} catch (IOException ex) {
				System.out.println("Unexpected error...");
			}
		} else if(sc.hasNext() && "client".equals(sc.next())) {
			try {
				Socket cs = new Socket("localhost", port);
				cs.connect();
				
				InputStream sin = cs.getInputStream();
				OutputStream sout = cs.getOutputStream();
				//^---------v
				DataInputStream in = new DataInputStream(sin);
				DataOutputStream out = new DataOutputStream(sout);

				String line;
				while(true) {
					Scanner client_sc = new Scanner(System.in);
					if(client_sc.hasNext()) {
						line = client_sc.next();
						out.writeUTF(line); // отсылаем введенную строку текста серверу.
						out.flush();
					}
				}
			} catch (IOException ex) {
				System.out.println("Unexpected error...");
			}
		}
	}
}

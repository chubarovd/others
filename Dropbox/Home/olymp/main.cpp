#include <algorithm>
#include <math.h>
#include <stdlib.h>
#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <vector>

int a[10][10];
int dx[] = {-1, 0, 1, 0};
int dy[] = {0, -1, 0, 1};

int x_, y_;

int main() {
	for(int i = 0; i < 10; i++) {
		a[i][i] = 0;
	}
	f(7, 7);
}

void f(int x, int y) {
	if(a[x][y] == 0) {
		a[x][y] = 1;
		x_ = x;
		y_ = y;
		for(int i = 0; i < 4; i++) {
			f(x + dx[i], y + dy[i]);
		}
	} else {
		printf("%d %d", x_, y_);
		return;
	}
}
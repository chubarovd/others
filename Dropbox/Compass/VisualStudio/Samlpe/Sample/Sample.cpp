// Sample.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int main()
{
	int array[4] = {1, 2, 3, 4};
	int new_array[4] = {4, 3, 2, 1};

	*new_array = *array;

	for(int i = 0; i < 4; i++) { 
		printf("%d  ", new_array[i]);
	}
	PauseAfterProgram();
}


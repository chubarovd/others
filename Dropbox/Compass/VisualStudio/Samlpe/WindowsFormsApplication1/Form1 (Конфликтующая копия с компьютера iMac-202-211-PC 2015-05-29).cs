﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1 {
    public partial class Form1 : Form {
        int a, b;
        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            button1.Click += new EventHandler(this.button1_Click);
            button2.Click += new EventHandler(this.button2_Click);
            button3.Click += new EventHandler(this.button3_Click);
            button4.Click += new EventHandler(this.button4_Click);
        }

        private void button1_Click(object sender, EventArgs e) {
            label1.SetText();
        }

        private void button2_Click(object sender, EventArgs e) {
            
        }

        private void button3_Click(object sender, EventArgs e) {
            
        }

        private void button4_Click(object sender, EventArgs e) {
            
        }

        private void label1_Click(object sender, EventArgs e) {

        }
    }
}

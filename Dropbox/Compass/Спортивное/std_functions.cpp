#include <algorithm>
#include <math.h>
#include <stdlib.h>
#include <cstdlib>
#include <stdio.h>
#include <iostream>

using namespace std;

int main() {
    const int SIZE = 7;
    int arr[SIZE];
    for(int i = 0; i < 7; i++) {
        scanf("%d", &arr[i]);
    }
    printf("\nsource: ");
    for(int i = 0; i < 7; i++) {
        printf("%d ", arr[i]);
    }
    printf("\nsort: ");
    sort(arr, arr + SIZE);
    for(int i = 0; i < 7; i++) {
        printf("%d ", arr[i]);
    }
    printf("\nreverse: ");
    reverse(arr, arr + SIZE);
    for(int i = 0; i < 7; i++) {
        printf("%d ", arr[i]);
    }
}

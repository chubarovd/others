#include <stdlib.h>
#include <iostream>
#include <cstdlib>
#include <math.h>
#include <stdio.h>
#include <algorithm>
#include <vector>
#include <string>
#include <map>

using namespace std;

const int MAX = 251;

int n;
int a[MAX][MAX];
int dp[MAX][MAX];
int inf = 10^7;

struct Edge {
    int numb;
    int ancestor;
    int child;
};

int rec(int i, int j) {
    if(dp[i][j] != inf) {
        return dp[i][j];
    }
    if(i == 0 && j > 0) {
        return dp[i][j] = rec(i, j - 1) + a[i][j];
    } else if(j == 0 && i > 0) {
        return dp[i][j] = rec(i - 1, j) + a[i][j];
    } else if(i > 0 && j > 0) {
        int r1 = rec(i - 1, j);
        int r2 = rec(i, j - 1);
        if(r1 <= r2) {
            return dp[i][j] = r1 + a[i][j];
        } else {
            return dp[i][j] = r2 + a[i][j];
        }
    }
}

void dp_prov(int i, int j) {
    ans[i][j] = '#';
    if(i == 0 && j == 0) {
        return;
    } else if(i == 0 && j > 0) {
        dp_prov(i, j - 1);
    } else if(j == 0 && i > 0) {
        dp_prov(i - 1, j);
    } else if(min(dp[i][j - 1], dp[i - 1][j]) == dp[i][j - 1]) {
        dp_prov(i, j - 1);
    } else {
        dp_prov(i - 1, j);
    }
}

int main() {
    freopen("1553input.txt", "r", stdin);

    int n, m;
    cin >> n >> m;


    for(int i = 0; i < m; i++) {

    }
    dp[0][0] = a[0][0];

    rec(n - 1, n - 1);
}

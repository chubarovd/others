#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>

using namespace std;

struct Strings {
    char* m[1001];
    int size;

    bool findStr(char* str, bool print) {
        for(int i = 0; i < size; i++) {
            //cout << str << " " << m[i] << endl;
            cout << strcmp(str, m[i]) << endl;
            if(strcmp(str, m[i]) == 0) {
                if(print) cout << "YES" << endl;
                return true;
            }
        }
        if(print) cout << "NO" << endl;
        return false;
    }

    void addStr(char* str) {
        strcpy(str, m[size]);
        size++;
    }

    Strings() : size(0) {};
};

int main(){
    freopen("input.txt", "r", stdin);

    Strings s;

    char oper;
    char str[10];
    while(1) {
        cin >> oper;
        if(oper == '+') {
            cin >> str;
            if(!s.findStr(str, false)) s.addStr(str);
            continue;
        }

        if(oper == '?') {
            cin >> str;
            s.findStr(str, true);
            continue;
        }

        if(oper == '#') return 0;
    }
}

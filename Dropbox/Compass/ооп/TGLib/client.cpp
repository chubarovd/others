#include "TXLib.h"
#include "tglib.h"


using namespace std;

//preved-medved protocol implementation for a single client

int main(int argc, char **argv)
{
    TGLib_start();

    TGLPort client;

    assert(client.connect("localhost", 2302, -1));
    printf("connected\n");

    char buf[100];
    memset(buf, 0, sizeof(buf));
    size_t left = 0;
    client.setMessRecvTimeout(1);
	while(true) {
        printf("tik\n");
		assert(client.sendMess("preved", strlen("preved") +1, -1));
		printf("sent preved\n");
		printf("receiving\n");
		if(client.recvMess(buf, 100, &left) > 0) {
        printf("received: %s\n", buf);
    }
}

    TGLib_end();
    return 0;
}

#include "BDLib.cpp"
#include "BdMessageList.cpp"
#include "BdTextField.cpp"
#include "BdScroller.cpp"
#include "BdListComponent.cpp"
#include "BdTimer.cpp"
#include "tglib.h"

class Form : public BdComponentListener, BdTimerListener {
public:

    BdWindow window;
    BdMessageList ml;
    BdTextField text;
    BdButton send;
    BdButton recv;
    BdScroller sc;


    BdTimer timer;
    //netWork//
    TGLPort myPort;
    bool Connect;
    //netWork//

    Form();
    void onClick(BdComponent* v, int x, int y);
    void onSelectionChanged (BdListComponent* l);
    void onTick(BdTimer* timer);
    void show();
};

void Form::show() {
    window.show();
}

int main() {
    Form form;
    form.window.show();
    TGLib_end();
    return 0;
}

Form::Form() : window (700, 600), ml(0, 0, 681, 481, BdMessageList::CHAT), text(100, 430, 503),
               send(600, 430, 100, 70, "send", true), recv(0, 430, 100, 70, "recv", true), sc(0, 0, 700, 430) {
    sc.setInnerComponent(&ml);
    window.container.addChild(&sc);
    window.container.addChild(&text);
    window.container.addChild(&send);
    send.addListener(this);
    //window.container.addChild(&recv);
    //recv.addListener(this);


    timer.addListener(this);
    timer.setPeriodMillis(100);
    timer.start();
    window.container.addChild(&timer);
    //network//
    Connect = false;
    //network//
}

void Form::onTick(BdTimer* timer) {
    if (!Connect) {
        if (myPort.connect("10.77.251.109", 666, -1)) {
            ml.setUserName("2");
            Connect = true;
            cout << "Connected или просто хрень какая-то\n";
        }
    }
    else {
        char buf[100] = {};
        size_t left;
        myPort.setMessRecvTimeout(10);
        if (myPort.recvMess(buf, 10, &left)) {
            ml.addMess(buf, BdMessageList::RECEIVED);
        }
    }
}

void Form::onClick(BdComponent* v, int x, int y) {
    if(v == &send) {
        char buf[100] = {};
        text.getText(buf);
        ml.addMess(buf, BdMessageList::SENDED);
        myPort.sendMess(buf, 100, 100);
    }
}

#include "bdLib.h"

class BdTabSetListener;

class BdTabSet : public BdContainer {
private:
    BdList<BdTabSetListener*> tabListeners;
    enum TabCrossStat {CROSS_UNDER_CURSOR, CROSS_NONE};
    TabCrossStat crossStatus[20];
    bool addButStatus;
    int selectedTabIndex;
    char text[20][20];

    static const double HEAD_SIZE = 20;
    static const double FACTOR = 1.0 / 3;
    static const double DOUBLE_FACTOR = 2.0 / 3;
    static const double TAB_LENGTH = 150;
    static const double FACTOR_OF_TEXT_SIZE = 1.2;

public:
    BdTabSet(int x, int y, int width, int height);

    void draw(Surface* s);
    void process(TxEvent* event);
    void addTab(BdComponent* _component, char* _text);
    void removeTab(int index);

    void addTabSetListener(BdTabSetListener* ts);
    void removeTabSetListener(BdTabSetListener* ts);

    int getSelectedTabIndex();
    void getSelectedTabName(char* text_);
    void getTabName(int index, char* text_);
};

class BdTabSetListener {
public:
    virtual void onTabChanged(BdTabSet* tabSet) = 0;
};



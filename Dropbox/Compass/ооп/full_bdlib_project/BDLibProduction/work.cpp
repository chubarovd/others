#include "BDLib.cpp"
#include "BdListComponent.cpp"
#include "BdProgressBar.cpp"
#include "BdTimer.cpp"

class Form_t : public BdComponentListener, BdListComponentListener, BdTimerListener//, BdMenuListener
{
public:
    BdWindow window;
    BdButton start;
    BdButton stop;
    BdLabel title;
    BdProgressBar progressbar;
    BdTimer timer;
    BdListItem item1;
    BdListItem item2;
    BdListModel model;
    BdListComponent list;

    //Popup
    BdMenu m;
    BdMenuItem i1;

    void onClick (BdComponent* v, int x, int y);
    void onSelectionChanged (BdListComponent* l);
    void onTick(BdTimer* timer);
    void show ();

    Form_t ();
};

int main ()
{
    Form_t form;
    form.show();
    return 0;
}

void Form_t::show()
{
    window.show();
}

Form_t::Form_t () : window (750, 650),
    start (25, 275, 150, 150, "START", false),
    stop (575, 275, 150, 150, "STOP", false),
    title (100, 25, 500, 100, "����� ��� ��������"),
    progressbar (25, 450, 700, 175),
    item1 ("�����������������"),
    item2 ("�������������������"),
    model (),
    list (25, 150, 700, 100, true),
    //popup
    m(0, 0, 200, 50),
    i1("�������")
{
    model.addItem (item1);
    model.addItem (item2);
    list.setModel(model);

    window.container.addChild(&timer);
    window.container.addChild(&start);
    window.container.addChild(&stop);
    window.container.addChild(&title);
    window.container.addChild(&list);
    window.container.addChild(&progressbar);
    start.addListener(this);
    stop.addListener(this);
    list.addListener(this);

    timer.setPeriodMillis(100);
    timer.addListener(this);
    timer.stop();
    progressbar.setMin(0);
    progressbar.setMax(100);

    m.addItem(&i1);
    m.build();

    start.setPopupComponent(&m);
}

void Form_t::onClick(BdComponent* v, int x, int y)
{
    if (v == &start)
    {
        progressbar.setPerforming(true);
        start.setEnabled(false);
        stop.setEnabled(true);
        list.setEnabled(false);
        timer.start();
    }
    if (v == &stop)
    {
        progressbar.setPerforming(false);
        start.setEnabled(true);
        stop.setEnabled(false);
        list.setEnabled(true);
        timer.stop();
    }
}

void Form_t::onSelectionChanged(BdListComponent* l)
{
    start.setEnabled(true);
    char str [100];
    int itemCount;
    itemCount = l -> getSelectedItem ();
    model.getTextItem(str, itemCount);
    title.setText (str);
    if (str[0] == '�')
    {
        progressbar.setType(INDETERMINATE);
    }
    else
    {
        progressbar.setType(DETERMINATE);
        progressbar.setProgress(progressbar.getMin());
    }
}

void Form_t::onTick(BdTimer* timer)
{
    progressbar.setProgress(progressbar.getProgress() + 1);
    if (progressbar.getProgress() == progressbar.getMax()) {
        timer->stop();
    }
}

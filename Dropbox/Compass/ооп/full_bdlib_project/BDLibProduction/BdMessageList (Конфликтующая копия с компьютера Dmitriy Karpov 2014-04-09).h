#include "BdLib.h"

class BdMessageList : public BdComponent {
public:
    enum Type {RECEIVED, SENDED};
private:
    static const int SIZE = 1000;

    time_t rawtime;
    struct tm * timeinfo;
    char time_[20];
    char times_[SIZE][100];

    static const int INTERVAL = 10;

    double width_of_font;
    double height_of_font;
    double lenght_of_side_before_triangle;

    int startHeight;

    Type types[SIZE];
    char name[100];
    char receiveName[100];

    char message[SIZE][100];
    int count_;
    bool multiline(char* line);
    int multilinesCount;
public:
    BdMessageList(int x_, int y_, int w_, int h_);

    void draw(Surface* s);
    void process(TxEvent* event);
    void addMess(char* mess, Type type);
    void deleteMess(int index);
    void setUserName(char* name_);
};

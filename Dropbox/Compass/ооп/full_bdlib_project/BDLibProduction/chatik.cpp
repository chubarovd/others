#include "BdLib.cpp"
#include "BdTimer.cpp"
#include "BdTextField.cpp"
#include "BdMessageList.cpp"
#include "BdScroller.cpp"
#include "tglib.h"

bool connect_ = false;
size_t left = 0;

class Form : public BdComponentListener, BdTimerListener {
private:
    static const int SIZE = 100;

    enum messageTypes {SEND_MESS, CALL_OF_USER_LIST, USER_READY, NONE};
    messageTypes mType;

    TGLPort p;

    char buf[SIZE];
    char ip[SIZE];
    char name[SIZE];
public:
    BdWindow window;

    BdTimer counter;
    BdTextField stext;
    BdTextField iptext;
    BdButton send;
    BdButton connect;
    BdMessageList messList;
    BdScroller sc;
    BdLabel nameL;
    BdTextField nameText;

    void onClick (BdComponent* v, int x, int y);
    void onTick(BdTimer* timer);

    void sendMessage(char* str);

    Form();
};

int main() {
    Form form;
    form.window.show();
}

Form::Form() : window (700, 500),
                messList(0, 0, 681, 481),
                sc(0, 0, 700, 430),
                iptext(3, 3, 500),
                stext(5, 433, 600),
                nameText(203, 73, 500),
                nameL(0, 70, 200, 70, "UserName:"),
                send(600, 430, 100, 70, "send", false),
                connect(500, 0, 200, 70, "connect", true) {
    sc.setInnerComponent(&messList);
    window.container.addChild(&iptext);
    window.container.addChild(&nameText);
    window.container.addChild(&nameL);
    window.container.addChild(&connect);
    connect.addListener(this);
}

void Form::onClick (BdComponent* v, int x, int y) {
    if(v == &connect) {
        if(iptext.getSymbolCount() > 0 && nameText.getSymbolCount() > 0) {
            iptext.getText(ip);
            window.container.addChild(&sc);
            if(p.connect(ip, 2215, -1)) {
                nameText.getText(name);
                p.setMessRecvTimeout(0);
                p.sendMess(name, strlen(name) + 1, -1);
                connect_ = true;

                window.container.addChild(&send);
                send.addListener(this);
                send.setEnabled(true);
                window.container.addChild(&stext);
                window.container.addChild(&counter);
                counter.setPeriodMillis(1);
                counter.addListener(this);
                counter.start();

                window.container.removeChild(&iptext);
                window.container.removeChild(&nameText);
                window.container.removeChild(&nameL);
                connect.removeListener(this);
                window.container.removeChild(&connect);
            }
        }
    }
    if(v == &send) {
        stext.getText(buf);
        sendMessage(buf);
    }
}

void Form::onTick(BdTimer* timer) {
    if(connect_) {
        int messLen = p.recvMess(buf, SIZE, &left);
        if(messLen > 0) {
            buf[messLen] = 0;
            messList.addMess(buf, BdMessageList::RECEIVED);
        }
    }
}

void Form::sendMessage(char* str) {
    char output[SIZE];
    int type = str[0] - '0';
    mType = (messageTypes)type;
    for(int i = 1; i < strlen(str); i++) {
        output[i - 1] = str[i];
    }
    output[strlen(str) - 1] = 0;

    if(p.sendMess(str, strlen(str) + 1, -1)) {
        messList.addMess(output, BdMessageList::SENDED);
    }
}


#include <stdio.h>
#include "BdSpinner.h"
#include <stdio.h>
#include <stdlib.h>

void BdSpinner::setValue(int value) {
    assert(value <= MAX_VALUE && value >= 0);
    this->value = value;
    for (int i = 0; i < listeners.getSize(); i++) {
        listeners.getValue(i)->onChange(this);
    }
}
int BdSpinner::getValue() {
    return value;
}
void BdSpinner::setStep(int step) {
    this->step = step;
}
void BdSpinner::addSpinnerListener(BdSpinnerListener *spl) {
    listeners.add(spl);
}
void BdSpinner::removeSpinnerListener(BdSpinnerListener *spl) {
    listeners.removeValue(spl);
}
void BdSpinner::onClick(BdComponent* comp, int x_, int y_) {
    if (comp == &butPlus) {
        setValue(value+step);
    }
    if (comp == &butMinus) {
         setValue(value-step);
    }
    char str[10];
    sprintf(str, "%d", value);
    text.setText(str);
}
void BdSpinner::textInserted(BdTextField* field, int pos, int count) {
    char str[10];
    field->getText(str);
    setValue(atof(str));
}
void BdSpinner::textDeleted(BdTextField* field, int pos, int count) {
    char str[10];
    field->getText(str);
    setValue(atof(str));
}
BdSpinner:: BdSpinner (int x_, int y_, double width_, int value, int step, bool status) :
    BdContainer(x_, y_, width_, HEIGHT),
    text(0, 0, width_-24),
    butMinus(width_-24, HEIGHT/2, 24, HEIGHT/2, "v", status),
    butPlus(width_-24, 0, 24, HEIGHT/2, "^", status)
    {
    addChild(&text);
    addChild(&butPlus);
    addChild(&butMinus);
    setValue(value);
    setStep(step);
    butPlus.addListener(this);
    butMinus.addListener(this);
    text.listeners.add(this);
}
/*
void BdSpinner::draw(Surface* s) {
    BdContainer::draw(s);
    s->setFillColor(TX_TRANSPARENT);
    s->rectangle(0, 0, width, height);
}
*/

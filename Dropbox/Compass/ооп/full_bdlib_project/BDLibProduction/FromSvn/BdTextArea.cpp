#include "BdText.h"

BdTextArea::BdTextArea(int x, int y, int width, int height, BdContainer* container) : BdComponent(x, y, width, height, container) {
    borderColor = TX_BLACK;
    backgroundColor = TX_GRAY;
    curLen = 0;
    listenerCount = 0;
    thin = 1;
    onFocus = false;
    userCanEditText = true;
    curPos = 0;
    text[0] = 0;
    text[1] = 0;
    full = false;
}

BdTextArea::BdTextArea() {
    borderColor = TX_BLACK;
    backgroundColor = TX_GRAY;
    thin = 1;
    curLen = 2;
    curPos = 0;
    text[0] = 0;
    text[1] = 0;
    listenerCount = 0;
    onFocus = false;
    userCanEditText = true;
    full = false;
}

void strclear(char *f, int length) {
    for (int i = 0; i < length; i++) {
        f[i] = 0;
    }
}

bool BdTextArea::LineDesc::ifMouseOn(POINT pos) {
    return (pos.y >= startY && pos.y <= startY + h) ? true : false;
}

void BdTextArea::draw(Surface* s) {
    s->setFillColor(backgroundColor);
    s->setColor(borderColor, thin);
    s->rectangle(0, 0, width, height);
    s->setColor(TX_BLACK);
    char tempStr[1000] = {0};
    SIZE tmp = txGetTextExtent("a");
    int i = 0;
    int str = 1;
    int tempPos = 0;
    SIZE size;
    lines.clear();
    symbol = txGetTextExtent("a").cx;
    while (true) {
        size = txGetTextExtent(tempStr);
        if (i == curPos && GetTickCount() % 1000 <= 500 && onFocus) {
            s->rectangle(5 + size.cx, tmp.cy * (str - 1) + 5 - 2,
                         5 + size.cx + 1, tmp.cy * (str - 1) + 5 - 2 + tmp.cy + 4);
        }
        if (text[i] == 0) {
            LineDesc tempLineDesc = {x + 5, y + tmp.cy * (str - 1) + 5, size.cx, tmp.cy, i - strlen(tempStr), strlen(tempStr)};
            lines.add(tempLineDesc);
            s->drawText(5, tmp.cy * (str - 1) + 5,
                        5 + size.cx, tmp.cy * (str - 1) + 5 + tmp.cy, tempStr, DT_VCENTER);
            strclear(tempStr, strlen(tempStr));
            tempPos = 0;
            break;
        } else if (size.cx + tmp.cx > width - 10 || text[i] == '\n') {
            LineDesc tempLineDesc = {x + 5, y + tmp.cy * (str - 1) + 5, size.cx, tmp.cy, i - strlen(tempStr), strlen(tempStr)};
            lines.add(tempLineDesc);
            s->drawText(5, tmp.cy * (str - 1) + 5, 5 + size.cx, tmp.cy * (str - 1) + 5 + tmp.cy, tempStr, DT_VCENTER);
            strclear(tempStr, strlen(tempStr));
            tempPos = 0;
            if ((str + 1) * tmp.cy > height - 10) {
                full = true;
            }
            str++;
        } else {
            tempStr[tempPos++] = text[i];
        }
        i++;
    }
    if (onFocus) {
        txLine(txMouseX(), txMouseY() - 10, txMouseX(), txMouseY() + 10);
    }

}
void BdTextArea::setEditable(bool b) {
    this->userCanEditText = b;
}
void BdTextArea::setText(const char* text2, int len) {
    strcpy(text, text2);
    curPos = len;
    curLen = len;
}
int BdTextArea::getText(char *dest, int destLen) {
    strncpy(dest, text, destLen);
    return curLen - destLen;
}

void BdTextArea::insertText(char *text, int len, int pos) {
    curPos = pos;
    for (int j = 0; j < len; j++) {
        for (int i = curLen; i > curPos; i--) {
            this->text[i] = this->text[i - 1];
        }
        this->text[curPos] = text[j];
        curLen++;
        curPos++;
    }
}

void BdTextArea::removeText(int pos, int count) {
    count--;
    curPos = pos;
    if (pos + count < curLen) {
        curLen -= count;
        for (int i = pos; i < curLen; i++) {
            text[i] = text[i + 1 + count];
        }
        text[curLen] = 0;
    } else {
        curLen = pos;
        text[curLen] = 0;
    }
}

/*void saveText(FILE *file) {
    file = fopen("save.txt", "w");
    fprintf(file, "%s\n", text);
}*/
void BdTextArea::process(TxEvent* e) {
    if (userCanEditText) {
        if (e->type == TxEvent::TX_MOUSE_RELEASE) {
            if(mouseOn(e)) {
                onFocus = true;
                thin = 2;
                int i;
                LineDesc tempLineDesc;
                for (i = 0; i < lines.getSize(); i++) {
                    tempLineDesc = lines.getValue(i);
                    if (tempLineDesc.ifMouseOn(e->pos)) {
                        //printf("FOUND!!!!!!!!!!!! i = %d\n", i);
                        break;
                    }
                }
                if (e->pos.x < tempLineDesc.startX)
                    curPos = tempLineDesc.startPos;
                else if (e->pos.x > tempLineDesc.startX + tempLineDesc.w)
                    curPos = tempLineDesc.startPos + tempLineDesc.charCount;
                else {
                    //printf ("startPos = %d, e.pos.x = %d, symbol = %d, e->pos.x) / symbol = %d\n", tempLineDesc.startPos, (e->pos.x - x), symbol, (e->pos.x - x) / symbol);
                    curPos = tempLineDesc.startPos + (e->pos.x - x) / symbol;
                }
            } else {
                onFocus = false;
                thin = 1;
            }
        }
        if (e->code == VK_LEFT && curPos >= 1 && e->type != 0 && e->type != 6) curPos--;
        if (e->code == VK_RIGHT && curLen > curPos + 1 && e->type != 0 && e->type != 6) curPos++;
        if (e->code == VK_DELETE && e->type == 4) {
            if (curPos < curLen) {
                curLen--;
                for (int i = curPos; i < curLen; i++) {
                    text[i] = text[i + 1];
                }
                text[curLen] = 0;
            }
        }
        if (e->type == TxEvent::TX_CHAR && onFocus) {
            if (e->code == VK_RETURN && !full) {

                for (int i = curLen; i > curPos; i--) {
                    text[i] = text[i - 1];
                }
                text[curPos] = 10;
                curLen++;
                curPos++;
            } else if (e->code == VK_BACK) {
                if (curPos) {
                    curLen--;
                    for (int i = curPos - 1; i < curLen; i++) {
                        text[i] = text[i + 1];
                    }
                    text[curLen] = 0;
                    curPos--;
                }
                full = false;
            } else if (e->code == VK_TAB) {
                for (int i = curLen + 3; i > curPos; i--) {
                    text[i] = text[i - 1];
                }
                text[curPos] = VK_SPACE;
                text[curPos + 1] = VK_SPACE;
                text[curPos + 2] = VK_SPACE;
                text[curPos + 3] = VK_SPACE;
                curLen+=4;
                curPos+=4;
            } else if (!full) {
                for (int i = curLen; i > curPos; i--) {
                    text[i] = text[i - 1];
                }
                text[curPos] = e->code;
                curLen++;
                curPos++;
            }
        }
    }
}

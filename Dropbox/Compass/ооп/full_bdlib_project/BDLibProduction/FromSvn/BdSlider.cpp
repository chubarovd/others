#include "BdSlider.h"
BdSlider::BdSlider() {}
BdSlider :: BdSlider (int x_, int y_, int width_, int height_, double min_, double max_, SliderType type_, bool drawDigits_) : BdComponent (x_, y_, width_, height_) {
    x = x_;
    y = y_;
    type = type_;
    min = min_;
    max = max_;
    drawDigits = drawDigits_;
    now_value = min + (max - min) / 2;
}

void BdSlider :: draw(Surface* s)
{
    if (type == S_GORISONTAL)
    {
        double d1 = width / 30;
        double d2 = height / 10;
        double percent = (now_value - min) / (max - min);
        double xS = percent * (width  - 2 * INDENTX);
        s->setColor(TX_BLUE);
        s->setFillColor(TX_BLUE);
        s->line( INDENTX, height / 2,  width - INDENTX, height / 2);
        s->rectangle(xS - d1 + INDENTX, height / 2 - d2, xS + d1 + INDENTX, height / 2 + d2);
        s->setColor(TX_RED);
        s->line (INDENTX, height * 0.4, INDENTX, height * 0.6);
        s->line (width - INDENTX, height * 0.4, width - INDENTX, height * 0.6);

        if (drawDigits == true)
        {
        char numberAsStr[10];
        sprintf(numberAsStr, "%.0lf", min);
        s->setColor(TX_BLACK);
        s->drawText(0, 0.5 * height, 4 * INDENTX, height, numberAsStr);
        sprintf(numberAsStr, "%.0lf", max);
        s->drawText(width - 5 * INDENTX, 0.5 * height, width, height, numberAsStr);
        sprintf(numberAsStr, "%.0lf", now_value);
        s->drawText(xS - d1 + INDENTX - 100, height / 2 - d2, xS + d1 + 1 * INDENTX + 100, height / 2 - d2 - 40 , numberAsStr);
        }
    }
    if (type == S_VERTICAL)
    {
        double d1 = height / 30;
        double d2 = width / 10;
        double percent = (now_value - min) / (max - min);
        double xS = percent * (height  - 2 * INDENTY);
        s->setColor(TX_BLUE);
        s->setFillColor(TX_BLUE);
        s->line(width / 2, INDENTY, width / 2, height - INDENTY);
        s->rectangle(width / 2 - d2, xS - d1 + INDENTY, width / 2 + d2, xS + d1 + INDENTY);
        s->setColor(TX_RED);
        s->line (width * 0.4, INDENTY, width * 0.6, INDENTY);
        s->line (width * 0.4, height - INDENTY, width * 0.6, height - INDENTY);

        if (drawDigits == true)
        {
        char numberAsStr[100];
        sprintf(numberAsStr, "%.0lf", min);
        s->setColor(TX_BLACK);
        s->drawText(0.5 * width, 0, width, 4 * INDENTY, numberAsStr);
        sprintf(numberAsStr, "%.0lf", max);
        s->drawText(0.5 * width, height - 4 * INDENTY, width, height, numberAsStr);
        sprintf(numberAsStr, "%.0lf", now_value);
        s->drawText(width / 2 - d2 - 20, xS - d1 + INDENTY - 10, width / 2 + d2 + 100, xS + d1 + INDENTY + 10, numberAsStr);
        //s->drawText(200, 200, 300, 300, numberAsStr);
        }
        return;

    }

}

void BdSlider :: process(TxEvent* event)
{
    if (type == S_GORISONTAL)
    {
        if (state == BT_PRESSED && event->type != TxEvent::TX_MOUSE_RELEASE)
        {
            setValue(min + (event->pos.x - x - INDENTX) * (max - min) / (width - 2 * INDENTX));
            return;
        }
        if (control(event) == 1)
        {
            if (event->type == TxEvent::TX_MOUSE_MOVED && state == BT_NORMAL) {
                state = BT_UNDER_CURSOR;
            }

            if (event->type == TxEvent::TX_MOUSE_PRESS) {
                state = BT_PRESSED;
            }

            if (event->type == TxEvent::TX_MOUSE_MOVED && state == BT_PRESSED)
            {
                setValue(min + (event->pos.x - x - INDENTX) * (max - min) / (width - 2 * INDENTX));
            }
            if (event->type == TxEvent::TX_MOUSE_RELEASE && state == BT_PRESSED)
            {
                state = BT_UNDER_CURSOR;
            }

        } else {
            state = BT_NORMAL;
        }
    }

    if (type == S_VERTICAL)
    {
        if (state == BT_PRESSED && event->type != TxEvent::TX_MOUSE_RELEASE)
        {
            setValue(min + (event->pos.y - y - INDENTY) * (max - min) / (height - 2 * INDENTY));
            return;
        }
        if (control(event) == 1)
        {
            if (event->type == TxEvent::TX_MOUSE_MOVED && state == BT_NORMAL) {
                state = BT_UNDER_CURSOR;
            }

            if (event->type == TxEvent::TX_MOUSE_PRESS) {
                state = BT_PRESSED;
            }

            if (event->type == TxEvent::TX_MOUSE_MOVED && state == BT_PRESSED)
            {
                setValue(min + (event->pos.y - y - INDENTY) * (max - min) / (height - 2 * INDENTY));
            }
            if (event->type == TxEvent::TX_MOUSE_RELEASE && state == BT_PRESSED)
            {
                state = BT_UNDER_CURSOR;
            }

        } else {
            state = BT_NORMAL;
        }
    }

}

int BdSlider :: getMin() {
    return(this->min);
}

void BdSlider :: setMin(int min) {
    this->min = min;
}

int BdSlider :: getMax() {
    return(this->max);
}

void BdSlider :: setMax(int max) {
    this->max = max;
}

int BdSlider :: getValue() {
    return (this->now_value);
}

void BdSlider :: setValue(int newValue) {
        if (newValue > getMax())
        {
            newValue = getMax();
        }
        if (newValue < min)
        {
            newValue  = min;
        }
        this->now_value = newValue;
        for(int i = 0; i < sliderlisteners.getSize(); i++) {
                sliderlisteners.getValue(i) -> onChange(this);
        }
}


void BdSlider :: addSliderListener(BdSliderListener* slider)  {
    sliderlisteners.add(slider);
}

void BdSlider :: removeSliderListener(BdSliderListener* slider) {
    sliderlisteners.removeValue(slider);
}

bool BdSlider :: control(TxEvent* event1)
{
    if (type == S_GORISONTAL)
    {
        double d1 = width / 30;
        double d2 = height / 10;
        double percent = (now_value - min) / (max - min);
        double xS = percent * (width  - 2 * INDENTX);
        if (xS - d1 + INDENTX + x < event1->pos.x && event1->pos.x < xS + d1 + INDENTX + x && height / 2 - d2 + y < event1->pos.y && event1->pos.y  < height / 2 + d2 + y)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    if (type == S_VERTICAL)
    {
        double d1 = height / 30;
        double d2 = width / 10;
        double percent = (now_value - min) / (max - min);
        double xS = percent * (height  - 2 * INDENTY);
        if (width / 2 - d2 + x < event1->pos.x && event1->pos.x < width / 2 + d2 + x && xS - d1 + INDENTY + y < event1->pos.y && event1->pos.y  <xS + d1 + INDENTY + y)
        {
        return 1;
        }
        else
        {
        return 0;
        }
    }

}

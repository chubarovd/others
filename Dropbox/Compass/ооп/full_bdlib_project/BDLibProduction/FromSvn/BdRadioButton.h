#include "BdLib.h"

class BdRadioButtonListener;

class BdRadioButton : public BdComponent {
protected:
    char text[101];
public:
    BdList<BdRadioButtonListener*> rbListeners;
    bool selected;

    BdRadioButton(int x_, int y_, int width_, int height_, const char* _text);

    void draw(Surface* s);
//    void process(TxEvent* event);

    void addRBListener(BdRadioButtonListener* rbl);
    void removeRBListener(BdRadioButtonListener* rbl);
};

class BdRadioGroup : public BdComponentListener {
private:
    BdComponent* selectedButton;
    BdList<BdRadioButton*> Buttons[101];
public:
    BdRadioGroup();

    void onClick(BdComponent* b, int _x, int _y);

    void addRadBut(BdRadioButton* _button);
    void removeRadBut(BdRadioButton* _button);
};

class BdRadioButtonListener {
public:
    virtual void onChoosing(BdRadioButton* b) = 0;
};








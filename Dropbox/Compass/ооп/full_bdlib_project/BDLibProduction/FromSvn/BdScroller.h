#ifndef BDSCROLLER_H
#define BDSCROLLER_H
#include "BdLib.h"

class BdScrollView;

class BdScrollSlider : public BdComponent {
public:
    enum Type {Horisontal, Vertical};
private:
    enum MouseStatus {None, nextButtonOver, nextButtonClick, backButtonOver, backButtonClick, scrollOver, scrollClick};

    int buttonScroll;

    MouseStatus mouseStatus;

    BdScrollView *innerScrollView;

    double mouseClickX;
    double mouseClickY;

    enum Enable {enable, disenable};
    double minCount, maxCount;
    double Count;
    double sliderSize;
    double sliderArgument;
    Type type;

public:
    BdScrollSlider ();
    BdScrollSlider (int _x, int _y, int _width, int _height, double _min, double _max, Type _type, BdScrollView *_innerScrollView);
    double getCount();
    void process(TxEvent *event);
    void draw(Surface *surface);
    void refresh(BdComponent *_innerComponent);
};

class BdScrollView : public BdComponent {
private:
    int startX, startY;
public:
    BdComponent *innerComponent;
    void setStartX(int _startX);
    void setStartY(int _startY);
    BdScrollView ();
    BdScrollView (int _x, int _y, int _width, int _height);
    void process(TxEvent *event);
    void draw(Surface *surface);

    void setInnerComponent (BdComponent *_innerComponent);
};

class BdScroller : public BdContainer {
private:
    const int ARGUMENT = 20;
    bool isShowDownSlider;
    BdScrollSlider downSlider;
    bool isShowRightSlider;
    BdScrollSlider rightSlider;
    BdScrollView scrollView;
public:
    void setStartX(int _startX);
    void setStartY(int _startY);
    BdScroller (int _x, int _y, int _width, int _height);
    BdScroller (int _x, int _y, int _width, int _height, int _startX, int _startY);

    void setInnerComponent (BdComponent *_innerComponent);
};

#endif // BDSCROLLER_H

#ifndef BDPOPUPMENU_H
#define BDPOPUPMENU_H

class BdMenu;

class BdMenuItem : public BdComponent {
private:
    static const int MAXSIZE = 100;
    char itemtext[MAXSIZE];
    BdMenu *tab;
public:
    BdMenuItem(int _x, int _y, int _width, int _height, char* _itemtext);
    BdMenuItem(char* _itemtext);
    BdMenuItem();
    bool active;
    char* getText();
    void setTab(BdMenu *_tab);
    void activate();
    void build();

    void destroy();
    virtual void draw(Surface* s);
    virtual void process(TxEvent* event);

    BdMenu *father;
};

class BdMenu : public BdComponent {
private:
    BdList<BdMenuItem*> items;
public:
    BdMenu(int _x, int _y, int _weigth, int _height);
    BdMenu();
    bool active;

    int getItemsSize();
    void addItem(BdMenuItem*);
    void build();
    void destroy();

    virtual void draw(Surface* s);
    virtual void process(TxEvent* event);

    BdMenuItem *father;
};

#endif // BDPOPUPMENU_H

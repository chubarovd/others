#ifndef BDSPINNER_H
#define BDSPINNER_H
#include "BdLib.h"
#include "BdText.h"

class BdSpinnerListener;

class BdSpinner : public BdContainer, public BdComponentListener, public BdTextListener {
public:
    static const int MAX_VALUE = 1000, HEIGHT = 70, MAX_SIZE = 50;
private :
    BdButton butPlus, butMinus;
    BdTextField text;
    BdList <BdSpinnerListener*> listeners;
    int value, step;
public:
    void setValue(int value);
    int getValue();
    void setStep(int step);
    void onClick(BdComponent* comp, int x, int y);
    void addSpinnerListener(BdSpinnerListener *spl);
    void removeSpinnerListener(BdSpinnerListener *spl);
    void textInserted(BdTextField* field, int pos, int count);
    void textDeleted(BdTextField* field, int pos, int count);
    BdSpinner(int x_, int y_, double width_, int value, int step, bool status);
    //void draw(Surface* s);
};

class BdSpinnerListener{
public:
    virtual void onChange(BdSpinner* spinner)=0;
};

#endif

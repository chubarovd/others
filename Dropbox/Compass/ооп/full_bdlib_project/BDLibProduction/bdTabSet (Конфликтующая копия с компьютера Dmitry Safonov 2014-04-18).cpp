#include "bdTabSet.h"

BdTabSet::BdTabSet(int x, int y, int width, int height) : BdContainer(x, y, width, height) {
    selectedTabIndex = 0;
    addButStatus = false;
}

void BdTabSet::draw(Surface* s) {
    s->setColor(RGB(100, 100, 100), 2);
    s->line(x, HEAD_SIZE, width, HEAD_SIZE);
    for(int i = 0; i < children.getSize(); i++) {
        if(i == selectedTabIndex) {
            s->setColor(RGB(0, 0, 0));
            s->setFillColor(RGB(255, 255, 255));
            s->rectangle(TAB_LENGTH*i - 1, -1, TAB_LENGTH*(i + 1), HEAD_SIZE);
            s->selectFont("Franklin Gothic", false, false);
            s->drawText(TAB_LENGTH*i - 1, 0, TAB_LENGTH*(i + 1) - HEAD_SIZE, HEAD_SIZE, text[i]);
            s->setColor(RGB(255, 255, 255), 3);
            s->line(TAB_LENGTH*i, HEAD_SIZE - 1, TAB_LENGTH*(i + 1) - 3, HEAD_SIZE - 1);
        } else {
            s->setColor(RGB(70, 70, 70));
            s->setFillColor(RGB(190, 190, 190));
            s->rectangle(TAB_LENGTH*i, -1, TAB_LENGTH*(i + 1), HEAD_SIZE);
            s->selectFont("Franklin Gothic", false, false);
            s->drawText(TAB_LENGTH*i, 0, TAB_LENGTH*(i + 1) - HEAD_SIZE, HEAD_SIZE, text[i]);
        }
        switch(crossStatus[i]) {
            case CROSS_NONE :
                s->setColor(RGB(205, 143, 143), 2);
                s->line(TAB_LENGTH*(i + 1) - DOUBLE_FACTOR*HEAD_SIZE, FACTOR*HEAD_SIZE, TAB_LENGTH*(i + 1) - FACTOR*HEAD_SIZE, DOUBLE_FACTOR*HEAD_SIZE);
                s->line(TAB_LENGTH*(i + 1) - DOUBLE_FACTOR*HEAD_SIZE, DOUBLE_FACTOR*HEAD_SIZE, TAB_LENGTH*(i + 1) - FACTOR*HEAD_SIZE, FACTOR*HEAD_SIZE);
                break;
            case CROSS_UNDER_CURSOR :
                s->setColor(RGB(205, 0, 0), 2);
                s->line(TAB_LENGTH*(i + 1) - DOUBLE_FACTOR*HEAD_SIZE, FACTOR*HEAD_SIZE, TAB_LENGTH*(i + 1) - FACTOR*HEAD_SIZE, DOUBLE_FACTOR*HEAD_SIZE);
                s->line(TAB_LENGTH*(i + 1) - DOUBLE_FACTOR*HEAD_SIZE, DOUBLE_FACTOR*HEAD_SIZE, TAB_LENGTH*(i + 1) - FACTOR*HEAD_SIZE, FACTOR*HEAD_SIZE);
                break;
        }
//        switch(addButStatus) {
//            case true :
//                txSetColor(RGB(171, 212, 0), 4);
//                txLine(TAB_LENGTH*children.getSize() + HEAD_SIZE/8, 2*HEAD_SIZE/5, TAB_LENGTH*children.getSize() + 3*HEAD_SIZE/5, 2*HEAD_SIZE/5);
//                txLine(TAB_LENGTH*children.getSize() + HEAD_SIZE/3, HEAD_SIZE/5, TAB_LENGTH*children.getSize() + HEAD_SIZE, 3*HEAD_SIZE/5);
//                break;
//            case false :
//                txSetColor(RGB(141, 182, 0), 4);
//                txLine(TAB_LENGTH*children.getSize() + HEAD_SIZE/8, 2*HEAD_SIZE/5, TAB_LENGTH*children.getSize() + 3*HEAD_SIZE/5, 2*HEAD_SIZE/5);
//                txLine(TAB_LENGTH*children.getSize() + HEAD_SIZE/3, HEAD_SIZE/5, TAB_LENGTH*children.getSize() + HEAD_SIZE, 3*HEAD_SIZE/5);
//                break;
//        }
    }
    s->translate(0, HEAD_SIZE);
    children.getValue(selectedTabIndex)->draw(s);
    s->translate(0, 0 - HEAD_SIZE);
}

void BdTabSet::process(TxEvent* event) {
    for(int i = 0; i < children.getSize(); i++) {
        if(event->pos.x > TAB_LENGTH*i && event->pos.x < TAB_LENGTH*(i + 1) - HEAD_SIZE &&
            event->pos.y > 0 && event->pos.y < HEAD_SIZE) {
            if(event->type == TxEvent::TX_MOUSE_PRESS) {
                int oldIndex = selectedTabIndex;
                selectedTabIndex = i;
                if (oldIndex != selectedTabIndex) {
                    for(int j = 0; j < tabListeners.getSize(); j++) {
                        tabListeners.getValue(j)->onTabChanged(this);
                    }
                }
            }
        }

        if(event->pos.x > TAB_LENGTH*(i + 1) - HEAD_SIZE && event->pos.x < TAB_LENGTH*(i + 1) &&
            event->pos.y > 0 && event->pos.y < HEAD_SIZE) {
            crossStatus[i] = CROSS_UNDER_CURSOR;
            if(event->type == TxEvent::TX_MOUSE_PRESS) {
                removeTab(i);
                break;
            }
        } else {
            crossStatus[i] = CROSS_NONE;
        }
        event->pos.y -= HEAD_SIZE;
        if(event->type == TxEvent::TX_NONE) {
            children.getValue(i)->process(event);
        } else {
            children.getValue(selectedTabIndex)->process(event);
        }
        event->pos.y += HEAD_SIZE;
    }
    if(children.getSize() <= 0) {
        txSetColor(TX_BLACK);
        txSetFillColor(TX_WHITE);
        txClear();
        txDrawText(width/4, height, 600, 400, "tabs ended. sorry:((");
        exit(-1);
    }
}

void BdTabSet::addTab(BdComponent* _component, const char* _text) {
    children.add(_component);
    _component->setParent(this);
    strcpy(text[children.getSize() - 1], _text);
    crossStatus[children.getSize() - 1] = CROSS_NONE;
}

void BdTabSet::removeTab(int index) {
    children.getValue(index)->setParent(NULL);
    children.remove(index);
    if(index < selectedTabIndex) {
        selectedTabIndex--;
    } else if (index == selectedTabIndex) {
        if(selectedTabIndex == children.getSize()) {
            selectedTabIndex--;
        } else if(selectedTabIndex < children.getSize()) {
            selectedTabIndex = index;
        }
    } else if(index > selectedTabIndex) {
        selectedTabIndex;
    }
    for (int a = 0; a < children.getSize(); a++) {
        if(a == index) {
            for (int b = a; b < children.getSize(); b++) {
                strcpy(text[b], text[b + 1]);
                crossStatus[b] = crossStatus[b + 1];
            }
        }
    }
}

int BdTabSet::getSelectedTabIndex() {
    return selectedTabIndex;
}

void BdTabSet::addTabSetListener(BdTabSetListener* ts) {
    tabListeners.add(ts);
}

void BdTabSet::removeTabSetListener(BdTabSetListener* ts) {
    tabListeners.removeValue(ts);
}


#ifndef BDTEXT_H
#define BDTEXT_H
#include "BdLib.h"
//----------------------------------------------------------------------
//-----------------------------BdTextField------------------------------
//----------------------------------------------------------------------
class BdTextListener;
//------------------------CURSOR----------------------------------------
class BdCursor {
public:
	int x;
	int y;
	int w;
	int h;
};
//-----------------------TEXT FIELD-------------------------------------
class BdTextField: public BdComponent {
private:
    static const int FIELD_HEIGHT = 70;
    static const int MAX_SIZE = 50;
    char text[MAX_SIZE];
    char temp[MAX_SIZE];
	int symbolCount;

    bool mousePressed;
    bool controlPressed;
    bool isLetter(TxEvent* event);
	int pos;
	void typeSymbol(TxEvent* event);
	void deleteSymbol(TxEvent* event); //works like backspace

	char highlight[50];
	bool highlighting;
    bool highlightingEnded;

    int pos0;
    void deleteHighlight();
	void getHighlight();


public:
    bool active;

	void getText(char str[]);
	void setText(char str[]);
	int getSymbolCount();

    COLORREF backGroundColor;
	COLORREF lineColor;
    COLORREF highlightColor;
	COLORREF inactiveLineColor;

	void getCursorPos();

	BdCursor curs;

	void draw(Surface* s);
	void drawCursor(Surface* s);

	void process(TxEvent* event);
	bool onClick(TxEvent* event);//returns true if mouse clicks inside the textfield

	BdList<BdTextListener*> listeners;
	void addTextListener(BdTextListener* listener);

	BdTextField(int x_, int y_, int width_);
};
//--------------------------------------------------------------------------------
//---------------------------------------BdTextArea-------------------------------
//--------------------------------------------------------------------------------
class BdTextArea;

class BdTextAreaListener {
public:
    virtual void textInserted(BdTextArea* area, int pos, int len) = 0;
    virtual void textRemoved(BdTextArea* area, int pos, int len) = 0;
};

class BdTextArea : public BdComponent {
public:
    char text[100500];
    int curLen;
    int curPos;
    COLORREF borderColor;
    COLORREF backgroundColor;
    int thin;
    int listenerCount;
    int symbol;
    BdList<BdTextAreaListener*> listeners;
    bool onFocus;
    bool userCanEditText;
    bool full;
    void draw(Surface *s);
    void process(TxEvent *e);
    void setEditable(bool b);
    void setText(const char* text2, int len);
    int getText(char *dest, int destLen);
    void insertText(char *text, int len, int pos);
    void removeText(int pos, int count);
    void addListener(BdTextAreaListener* listener) {
        listeners.add(listener);
	}
	struct LineDesc {
        int startX;
        int startY;
        int w;
        int h;
        int startPos;
        int charCount;
        bool ifMouseOn(POINT pos);
	};
	BdList<LineDesc> lines;
    BdTextArea();
	BdTextArea(int x, int y, int width, int height, BdContainer* container);
};
//----------------------------------------------------------------------
//------------------------------TEXT FIELD LISTENER---------------------
//----------------------------------------------------------------------
class BdTextListener {
public:
    virtual void textInserted(BdTextField* field, int pos, int count) = 0;
    virtual void textDeleted(BdTextField* field, int pos, int count) = 0;
};

#endif

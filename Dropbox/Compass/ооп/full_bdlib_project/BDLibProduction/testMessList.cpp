#include "BDLib.cpp"
#include "BdMessageList.cpp"
#include "BdTextField.cpp"
#include "BdScroller.cpp"

class Form : public BdComponentListener{
public:
    char buf[100];

    BdWindow window;
    BdMessageList ml;
    BdTextField text;
    BdButton send;
    BdButton recv;
    BdScroller sc;

    Form();
    void onClick(BdComponent* v, int x, int y);
};

int main() {
    Form form;
    form.window.show();
}

Form::Form() : window (700, 500), ml(0, 0, 680, 481), text(100, 430, 503),
                send(600, 430, 100, 70, "send", true), recv(0, 430, 100, 70, "recv", true), sc(0, 0, 700, 430, &ml) {
        window.container.addChild(&ml);
        window.container.addChild(&sc);
        window.container.addChild(&text);
        window.container.addChild(&send);
        send.addListener(this);
        window.container.addChild(&recv);
        recv.addListener(this);
    }

void Form::onClick(BdComponent* v, int x, int y) {
    if(v == &send) {
        text.getText(buf);
        ml.addMess(buf, BdMessageList::SENDED);
    }
    if(v == &recv) {
        text.getText(buf);
        ml.addMess(buf, BdMessageList::RECEIVED);
    }
}

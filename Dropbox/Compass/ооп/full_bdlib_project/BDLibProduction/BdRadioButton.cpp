#include "BdRadioButton.h"

BdRadioButton::BdRadioButton(int x_, int y_, int width_, int height_, const char* _text) :
                BdComponent(x_, y_, width_, height_) {
    strcpy(text, _text);
}

void BdRadioButton::draw(Surface* s) {
    switch(selected) {
        case true :
            s->setColor(RGB(180, 180, 180), 3);
            s->setFillColor(RGB(250, 250, 250));
            s->circle(height/2, height/2, height/4);
            s->setColor(RGB(100, 197, 90));
            s->setFillColor(RGB(100, 197, 90));
            s->circle(height/2, height/2, height/4 - height/10);

            s->setColor(RGB(0, 0, 0));
            s->selectFont("Franklin Gothic", height/1.5, height/3);
            s->drawText(height, 0, width, height, text);
            break;
        case false :
            s->setColor(RGB(180, 180, 180), 3);
            s->setFillColor(RGB(250, 250, 250));
            s->circle(height/2, height/2, height/4);

            s->setColor(RGB(0, 0, 0));
            s->selectFont("Franklin Gothic", height/1.5, height/3);
            s->drawText(height, 0, width, height, text);
            break;
    }
}

//void BdRadioButton::process(TxEvent* event) {
//
//}

void BdRadioButton::addRBListener(BdRadioButtonListener* rbl) {
    rbListeners.add(rbl);
}

void BdRadioButton::removeRBListener(BdRadioButtonListener* rbl) {
    rbListeners.removeValue(rbl);
}

BdRadioGroup::BdRadioGroup() {

}

void BdRadioGroup::onClick(BdComponent* b, int _x, int _y) {
    if(selectedButton != b) {
        for(int i = 0; i < Buttons->getSize(); i++) {
            if(Buttons->getValue(i) == b) {
                Buttons->getValue(i)->selected = true;
                for(int j = 0; j < Buttons->getValue(j)->rbListeners.getSize(); j++) {
                    Buttons->getValue(i)->rbListeners.getValue(j)->onChoosing(Buttons->getValue(i));
                }
            } else if (Buttons->getValue(i) != b) {
                Buttons->getValue(i)->selected = false;
            }
        }
        selectedButton = b;
    }
}

void BdRadioGroup::addRadBut(BdRadioButton* _button) {
    Buttons->add(_button);
    _button->addListener(this);
}

void BdRadioGroup::removeRadBut(BdRadioButton* _button) {
    _button->removeListener(this);
    Buttons->removeValue(_button);
}

#include "BDLib.cpp"
#include "BdListComponent.cpp"
#include "BdTextField.cpp"

char strr[100];

class Form_t : public BdListComponentListener, BdTextListener, BdComponentListener
{
public:
    BdWindow window;
    BdLabel title;
    BdListItem item1;
    BdListItem item2;
    BdListModel model;
    BdListComponent list;
    BdTextField field;
    BdButton addButton;

    void show ();

    void onSelectionChanged(BdListComponent* l);
    void onClick (BdComponent* v, int x, int y);
    void textInserted(BdTextField* field, int pos, int count);
    void textDeleted(BdTextField* field, int pos, int count);

    Form_t ();
};

int main ()
{
    Form_t form;
    form.show();
    return 0;
}

void Form_t::show()
{
    window.show();
}

Form_t::Form_t () : window (750, 650),
    title (100, 25, 500, 100, "����� ��� ��������"),
    addButton (475 ,350, 150, 80, "��������", true),
    field (25, 350, 400),
    item1 ("�����������������"),
    item2 ("�������������������"),
    model (),
    list (25, 150, 700, 190, true)
{
    model.addItem (item1);
    model.addItem (item2);
    list.setModel(model);

    window.container.addChild(&title);
    window.container.addChild(&list);
    window.container.addChild(&addButton);
    window.container.addChild(&field);
    list.addListener(this);
    field.listeners.add(this);
    addButton.addListener(this);
}


void Form_t::onSelectionChanged(BdListComponent* l)
{
    char str [100];
    int itemCount;
    itemCount = l -> getSelectedItem ();
    model.getTextItem(str, itemCount);
    title.setText (str);
}

void Form_t::onClick(BdComponent* v, int x, int y)
{
    if (v == &addButton)
    {
        field.getText(strr);
        BdListItem addItem (strr);
        model.addItem(addItem);
        list.setModel(model);
    }
}

void Form_t::textInserted(BdTextField* field, int pos, int count)
{

}

void Form_t::textDeleted(BdTextField* field, int pos, int count)
{

}

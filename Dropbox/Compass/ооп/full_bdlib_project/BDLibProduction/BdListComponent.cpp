#include "BdListComponent.h"

COLORREF MY_GRAY = RGB (90, 90, 90);
///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////BdListItem/////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
BdListItem::BdListItem ()
{

}

BdListItem::BdListItem (char* text_)
{
    int i = 0;
    while (text_[i] != 0)
    {
        itemText[i] = text_[i];
        i++;
    }
    itemText[i] = 0;
}

void BdListItem::getText(char* str)
{
    int i = 0;
    while (itemText[i] != 0)
    {
        str[i] = itemText[i];
        i++;
    }
    str[i] = 0;
}

///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////BdListModel////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
BdListModel::BdListModel ()
{

}

void BdListModel::addItem (BdListItem item)
{
    items.add(item);
}

BdList<BdListItem>* BdListModel::getItems ()
{
    return &items;
}

void BdListModel::getTextItem (char* str, int itemIndex)
{
    items.getValue(itemIndex).getText(str);
}


///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////BdListComponent////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
BdListComponent::BdListComponent (int x_, int y_, int width_, int height_, bool status_) : BdComponent (x_, y_, width_, height_, status_)
{
    firstVisibleItem = 0;
    countVisibleItem = (height_ - 2 * SDVIG_BUTTON_HEIGHT) / ITEM_HEIGHT;
    selectedItem = -1;
}

void BdListComponent::setModel(BdListModel model_)
{
    model = model_;
}

int BdListComponent::getSelectedItem ()
{
    return selectedItem;
}

void BdListComponent::setSelectedItem(int i)
{
    selectedItem = i;
}

void BdListComponent::draw(Surface* s)
{
    s -> setFillColor(TX_GRAY);
    s -> rectangle (0, 0, width, height);
    s -> setColor (TX_BLACK);
    s -> setFillColor(TX_WHITE);
    s -> rectangle (0, 0, width, SDVIG_BUTTON_HEIGHT);
    s -> rectangle (0, height - SDVIG_BUTTON_HEIGHT, width, height);
    s -> setColor (TX_BLACK, 5);
    if (firstVisibleItem == 0)
    {
        s -> line (width/2 - 20, 0 + height - SDVIG_BUTTON_HEIGHT + 3, width/2, height - 3);
        s -> line (width/2 + 20, 0 + height - SDVIG_BUTTON_HEIGHT + 3, width/2, height - 3);
    } else if (firstVisibleItem + 1 == model.getItems()->getSize())
    {
        s -> line (width/2 - 20, SDVIG_BUTTON_HEIGHT - 3, width/2, 3);
        s -> line (width/2 + 20, SDVIG_BUTTON_HEIGHT - 3, width/2, 3);
    }
    else
    {
        s -> line (width/2 - 20, SDVIG_BUTTON_HEIGHT - 3, width/2, 3);
        s -> line (width/2 + 20, SDVIG_BUTTON_HEIGHT - 3, width/2, 3);
        s -> line (width/2 - 20, 0 + height - SDVIG_BUTTON_HEIGHT + 3, width/2, height - 3);
        s -> line (width/2 + 20, 0 + height - SDVIG_BUTTON_HEIGHT + 3, width/2, height - 3);
    }

    for (int i = 0; (i < countVisibleItem) && (i + firstVisibleItem < model.getItems()->getSize()); i++)
    {
        if (i + firstVisibleItem == getSelectedItem())
        {
            if (isEnabled() == true)
            {
                s -> setFillColor (TX_BLACK);
                s -> setColor (TX_WHITE);
            }
            else
            {
                s -> setFillColor (MY_GRAY);
                s -> setColor (TX_WHITE);
            }
        }
        else
        {
            if (isEnabled() == true)
            {
                s -> setFillColor (TX_WHITE);
                s -> setColor (TX_BLACK);
            }
            else
            {
                s -> setFillColor (TX_GRAY);
                s -> setColor (TX_WHITE);
            }
        }
        s -> rectangle (0, SDVIG_BUTTON_HEIGHT + ITEM_HEIGHT * i, width, SDVIG_BUTTON_HEIGHT + ITEM_HEIGHT + ITEM_HEIGHT*i);
        char str[100];
        model.getTextItem(str, i + firstVisibleItem);
        s -> drawText (0, SDVIG_BUTTON_HEIGHT + ITEM_HEIGHT * i, width,  SDVIG_BUTTON_HEIGHT + ITEM_HEIGHT + ITEM_HEIGHT*i, str);
    }
}

bool BdListComponent::isEventForItem(TxEvent* event, int itemIndex)
{
    RECT rect = {x,SDVIG_BUTTON_HEIGHT + y + ITEM_HEIGHT * itemIndex, x + width,SDVIG_BUTTON_HEIGHT + y + ITEM_HEIGHT + ITEM_HEIGHT*itemIndex};
    POINT pt = event->pos;
    if (In(pt, rect))
    {
        return true;
    }
    else
    {
        return false;
    }
}

BdListModel* BdListComponent::getModel()
{
    return &model;
}


void BdListComponent::process(TxEvent* event)
{
    if (isEnabled() == true)
    {
        if ((event-> type) == (TxEvent::TX_MOUSE_PRESS) && event->code == 1)
        {
            RECT upButton = {x, y, x + width, y + SDVIG_BUTTON_HEIGHT};
            RECT downButton = {x, height + y - SDVIG_BUTTON_HEIGHT, x + width, y + height};
            POINT pt = event->pos;
            if ((In(pt, upButton)) && (firstVisibleItem >= 1)) {
                firstVisibleItem--;
            }
            else if ((In(pt, downButton)) && (firstVisibleItem + 1 < model.getItems()->getSize())) {
                    firstVisibleItem++;

                }
                else
                {
                    for (int i = 0; (i + firstVisibleItem < model.getItems()->getSize()) && (i < countVisibleItem); i++)
                    {
                        if (isEventForItem(event, i))
                        {
                            setSelectedItem(i + firstVisibleItem);
                            for (int j = 0; j < listeners.getSize(); j++)
                            {
                                listeners.getValue(j)->onSelectionChanged(this);
                            }
                        }
                    }
                }
        }
    }
}

void BdListComponent::addListener (BdListComponentListener* listener)
{
    listeners.add(listener);
}

void BdListComponent::removeListener (BdListComponentListener* listener)
{
    listeners.removeValue(listener);
}

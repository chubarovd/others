#include "BdLib.h"

class BdTimerListener;

class BdTimer: public BdComponent {
enum work {ON,OFF};
private:
    int period;
    int time;
    int working;
    BdList<BdTimerListener*> bdTimerListeners;
public:
    BdTimer();
    void start();
    void stop();
    void setPeriodMillis(int periodMillis);
    void draw (Surface* s);
    void process (TxEvent* event);
    void addListener(BdTimerListener* listener);
    void removeListener(BdTimerListener* listener);
};

class BdTimerListener {
public:
    virtual void onTick(BdTimer* timer) = 0;
};

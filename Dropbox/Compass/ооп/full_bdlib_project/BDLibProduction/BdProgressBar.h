#include "BdLib.h"

enum BdProgressBarType{DETERMINATE, INDETERMINATE};
const int REFRESH_TIME = 25;
class BdProgressBar: public BdComponent{
private:
    BdProgressBarType type;
    int progress;
	double min;
	double max;
	int percentCount;
	bool performing;
	int roundX;
	int roundStep;
	int time;
public:
	BdProgressBar(int x_, int y_, int width_, int height_);
	void draw(Surface* s);
	void process(TxEvent* event);
	void setType(BdProgressBarType type);
	void setProgress(int p);
	int getProgress();
	void setMin(int min);
	int getMin();
	void setMax(int max);
	int getMax();
	void setPerforming(bool performing);
	bool getPerforming();
};


#include "BDLib.cpp"
#include "BdMessageList.cpp"
#include "BdTextField.cpp"
#include "BdScroller.cpp"
#include "BdListComponent.cpp"
#include "BdTimer.cpp"
#include "tglib.h"

class Form : public BdComponentListener, BdTimerListener {
public:
    BdWindow window;
    BdMessageList ml;
    BdTextField text;
    BdScroller sc;


    BdTimer timer;
    //netWork//
    TGLServerPort myServerPort;
    TGLPort myPorts[100];
    int countPorts;
    //netWork//

    Form();
    void onClick(BdComponent* v, int x, int y);
    void onSelectionChanged (BdListComponent* l);
    void onTick(BdTimer* timer);
    void show();
};

void Form::show() {
    window.show();
}

int main() {
    Form form;
    form.window.show();
    TGLib_end();
    return 0;
}

Form::Form() : window (700, 500), ml(0, 0, 681, 481), text(100, 430, 503),
               sc(0, 0, 700, 430) {
    sc.setInnerComponent(&ml);
    window.container.addChild(&sc);
    window.container.addChild(&text);

    timer.addListener(this);
    timer.setPeriodMillis(100);
    timer.start();
    window.container.addChild(&timer);

    //network//
    myServerPort.bind("localhost", 1024);
    countPorts = 0;
    //network//
}

void Form::onTick(BdTimer* timer) {
    if (myServerPort.accept(&myPorts[countPorts], 1)) {
        printf("port conected %d", countPorts + 1);
        countPorts++;
    }
    for (int i = 0; i < countPorts; ++i) {
        char buf[100] = {};
        size_t left;
        myPorts[i].setMessRecvTimeout(10);
        if (myPorts[i].recvMess(buf, 100, &left)) {
            ml.addMess(buf, BdMessageList::RECEIVED);
            for (int j = 0; j < countPorts; ++j) {
                if (i != j) {
                    myPorts[j].sendMess(buf, strlen(buf), 100);
                }
            }
        }
    }
}

void Form::onClick(BdComponent* v, int x, int y) {

}

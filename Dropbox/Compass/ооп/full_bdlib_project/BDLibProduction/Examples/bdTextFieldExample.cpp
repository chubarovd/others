#include "bdTextField.h"
#include "bdTextField.cpp"
#include "stdio.h"

#ifdef __TX_MAC_OS__
int screenW = 1920;
int screenH = 1080;

#elsenW = 1024;
int screenH = 768;

#endif

class BdCountForm: public BdTextFieldListener {
public:
    void symbolTyped(BdTextField* field);
    void symbolDeleted(BdTextField* field);
    void savedToBuffer(BdTextField* field);
    void insertedFromBuffer(BdTextField* field);
    void onClick(BdTextField* field);
    void highlightDeleted(BdTextField* field);

    BdLabel label;
    BdTextField field;
    BdPanel* formPanel;
    int count;
    BdCountForm(BdPanel* panel);
};

BdCountForm:: BdCountForm(BdPanel* panel): label(200, 400, 50, 30, "0"), field(200, 100, 400, 30, TX_WHITE, TX_BLACK, TX_GRAY, TX_GRAY, 7) {
    count = 0;
    formPanel = panel;
    panel->children.add(&field);
    panel->children.add(&label);
    field.listeners.add(this);
}

void BdCountForm:: symbolTyped(BdTextField* field) {
    count = field->symbolCount;
    sprintf(label.text, "%d", count);
}
void BdCountForm:: symbolDeleted(BdTextField* field) {
    count = field->symbolCount;
    sprintf(label.text, "%d", count);
}
void BdCountForm:: onClick(BdTextField* field) {
}
void BdCountForm:: highlightDeleted(BdTextField* field) {
    count = field->symbolCount;
    sprintf(label.text, "%d", count);
}

int main() {
	BdWindow w(800, 800);

    BdCountForm form(&(w.component));

    w.show();

	return 0;
}

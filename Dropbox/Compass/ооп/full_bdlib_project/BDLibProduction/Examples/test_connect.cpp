#include "BDLib.cpp"
#include "BdListComponent.cpp"
#include "BdTimer.cpp"
#include "tglib.h"

class Form_t : public BdComponentListener, BdTimerListener
{
public:
    BdWindow window;
    BdButton btServer;
    BdButton btClient;
    BdLabel lbStatus;
    BdTimer timer;
    bool serverStarted;
    bool needConnect;
    bool connectionEstablished;
    TGLServerPort serverPort;
    TGLPort clientPort;

    void onClick (BdComponent* source, int x, int y);
    void onSelectionChanged (BdListComponent* l);
    void show ();
    void onTick(BdTimer* timer);

    Form_t ();
};

int main ()
{
    Form_t form;
    form.show();
    TGLib_end();
    return 0;
}

void Form_t::show()
{
    window.show();
}

Form_t::Form_t () : window (750, 650),
    btServer (100, 100, 150, 150, "Server", BdButton::disable),
    btClient (300, 100, 150, 150, "Client", BdButton::disable),
    lbStatus (100, 10, 400, 50, "�������� �����")
{

    window.container.addChild(&btServer);
    window.container.addChild(&btClient);
    window.container.addChild(&lbStatus);
    btServer.addListener(this);
    btClient.addListener(this);
    timer.addListener(this);
    timer.setPeriodMillis(100);
    timer.start();
    window.container.addChild(&timer);
    serverStarted = false;
    needConnect = false;
    connectionEstablished = false;
}

void Form_t::onTick(BdTimer* timer) {
    if (connectionEstablished) {
        timer->stop();
        return;
    }
    if (serverStarted) {
        printf("Try to accept\n");
        if (serverPort.accept(&clientPort, 1)) {
            lbStatus.setText("Connection accepted!");
            connectionEstablished = true;
        }
    } else if (needConnect) {
        printf("try to connect...\n");
        clientPort.connect("localhost", 10001, 10);
        lbStatus.setText("Connected!");
        printf("connected\n");
        connectionEstablished = true;
    }
}

void Form_t::onClick(BdComponent* source, int x, int y)
{
    btServer.status = BdButton::disable;
    btClient.status = BdButton::disable;
    if (source == &btServer)
    {
        serverPort.bind("localhost", 10001);
        serverStarted = true;
        lbStatus.setText("Wait for connnection...");
    }

    if (source == &btClient)
    {
        needConnect = true;
        lbStatus.setText("Connecting...");
    }
}

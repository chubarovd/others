#include "BDLib.cpp"
#include "bdRadioButton.cpp"

class Form : public BdComponentListener {
private:
    BdWindow window;

    BdLabel al;
    BdLabel bl;

    BdButton btTaras;
    BdButton btAndrey;
    BdButton btClose;
    BdButton btRevival;

    BdRadioGroup group;
        BdRadioButton rbtField;
        BdRadioButton rbtFielda;
public:
    bool andreyOnScreen;
    bool tarasOnScreen;
    bool revival;
    bool labela;
    bool labelb;

    Form() : window (1000, 650), btTaras(80, 400, 120, 50, "Taras", BdButton::enable), btAndrey(400, 400, 120, 50, "Andrey", BdButton::enable),
                                    btClose(560, 0, 34, 16, "x", BdButton::enable), btRevival (100, 200, 400, 50, "Revival", BdButton::disable),
                                    al(650, 70, 180, 30, "selected: Fielda"), bl(650, 70, 180, 30, "selected: Fieldb"),
                                    rbtField(650, 10, 150, 30, "Fielda"), rbtFielda(650, 40, 150, 30, "Fieldb"), group() {

//====================================================

        group.addRadBut(&rbtField);
        group.addRadBut(&rbtFielda);

        window.container.addChild(&rbtField);
        window.container.addChild(&rbtFielda);

        rbtField.addListener(this);
        rbtFielda.addListener(this);

//====================================================

        window.container.addChild(&btTaras);
        window.container.addChild(&btClose);
        window.container.addChild(&btRevival);
        btTaras.addListener(this);
        btClose.addListener(this);

        andreyOnScreen = false;
        tarasOnScreen = true;
        revival = false;
        labela = false;
        labelb = false;
    };

    void onClick(BdComponent* b, int _x, int _y) {
        if(b == &btTaras){
            if (andreyOnScreen == false) {
                window.container.addChild(&btAndrey);
                btAndrey.status = BdButton::enable;
                btAndrey.addListener(this);

                btRevival.removeListener(this);
                btRevival.status = BdButton::disable;
                andreyOnScreen = true;
            } else {
                window.container.removeChild(&btAndrey);
                btAndrey.removeListener(this);
                btAndrey.status = BdButton::disable;
                andreyOnScreen = false;
            }
        }
        if(b == &btClose) {
            txDisableAutoPause();
            exit(-1);
        }
        if(b == &btAndrey) {
            if(tarasOnScreen == true) {
                window.container.removeChild(&btTaras);
                btTaras.removeListener(this);
                btTaras.status = BdButton::disable;

                btRevival.addListener(this);
                btRevival.status = BdButton::enable;
                tarasOnScreen = false;
            }
        }
        if(b == &btRevival) {
            if(tarasOnScreen == false) {
                window.container.addChild(&btTaras);
                btTaras.addListener(this);
                btTaras.status = BdButton::enable;
                tarasOnScreen = true;
            }
            btRevival.removeListener(this);
            btRevival.status = BdButton::disable;
        }
        if(b == &rbtField) {
            if(labela == false && labelb == true) {
                window.container.removeChild(&bl);
                window.container.addChild(&al);
                labela = true;
                labelb = false;
            }
            if(labela == false && labelb == false) {
                window.container.addChild(&al);
                labela = true;
            }
        }
        if(b == &rbtFielda) {
            if(labelb == false && labela == true) {
                window.container.removeChild(&al);
                window.container.addChild(&bl);
                labelb = true;
                labela = false;
            }
            if(labelb == false && labela == false) {
                window.container.addChild(&bl);
                labelb = true;
            }
        }
    }

    void show () {
        window.show();
    }
};

int main() {
    Form form;
    form.show();
}















#include "..\BdLib.cpp"
#include "..\BdTextField.cpp"
#include "stdio.h"

class BdCountForm: public BdTextListener {
public:
    void textInserted(BdTextField* field, int pos, int count);
    void textDeleted(BdTextField* field, int pos, int count);

    BdLabel label;
    BdTextField field;
    BdContainer* formContainer;
    int count;
    BdCountForm(BdContainer* container);
};

BdCountForm:: BdCountForm(BdContainer* container): label(200, 400, 60, 40, "0"), field(200, 100, 1100, 70, TX_WHITE, TX_BLACK, TX_GRAY, TX_GRAY, 21) {
    count = 0;
    formContainer= container;
    formContainer->addChild(&field);
    formContainer->addChild(&label);
    field.listeners.add(this);
}

void BdCountForm:: textInserted(BdTextField* field, int pos, int count) {
    count = field->getSymbolCount();
    char temp[100];
    sprintf(temp, "%d", count);
    label.setText(temp);
}
void BdCountForm:: textDeleted(BdTextField* field, int pos, int count) {
    count = field->getSymbolCount();
    char temp[100];
    sprintf(temp, "%d", count);
    label.setText(temp);
}

int main() {
	BdWindow w(1400, 800);

    BdCountForm form(&(w.container));

    w.show();

	return 0;
}

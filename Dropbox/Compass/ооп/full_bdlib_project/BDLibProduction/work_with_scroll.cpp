#include "BdLib.cpp"
#include "BdListComponent.cpp"
#include "BdScroller.cpp"

class Form_t : public BdComponentListener
{
public:
    BdWindow window;
    BdButton button;
    BdButton start;
    BdContainer cont;
    BdScroller sc;

    void onClick (BdComponent* v, int x, int y);
    void onSelectionChanged (BdListComponent* l);
    void show ();

    Form_t ();
};

int main ()
{
    Form_t form;
    form.show();
    return 0;
}

void Form_t::show()
{
    window.show();
}

Form_t::Form_t () : window (750, 650),
    button (0, 0, 500, 500, "BUTTON", true),
    start (0, 0, 150, 150, "Start", true),
    cont(0, 0, 1000, 3000),
    sc(100, 100, 400, 400, &cont)
{
    window.container.addChild(&sc);
    cont.addChild(&button);

    button.addListener(this);
}

void Form_t::onClick(BdComponent* v, int x, int y) {
    if(v == &button) {
        printf("I was clicked!!\n");
    }
}



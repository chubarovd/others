#include "C:\Users\admin\Dropbox\Compass\���\full_bdlib_project\BDLibProduction\BdLib.cpp"
#include "C:\Users\admin\Dropbox\Compass\���\full_bdlib_project\BDLibProduction\tglib.h"
#include <iostream>

static const int MAX_PLAYERS = 10;
static const int LIVES_COUNT = 3;
static const int myPort = 5151;
//static const char* host = "10.77.251.111";
static const char* host = "localhost";
static const int BLUE = 1, RED = 2;
static const int OFFSET = 1;
static const int SPEED = 1;

class Whizbang{
public:
    int time;
    int x;

    Whizbang(int x_, int time_) {
        x = x_;
        time = time_;
    }
    Whizbang() {
        x = 0;
        time = 0;
    }
};

class Player {
public:
    char name[100];
    int color;
    int lives;
    int x;
    bool isPlaying;

    TGLPort port;
    BdList <Whizbang> whizbangs;

    Player(int x_) {
        x = x_;
        lives = LIVES_COUNT;
        isPlaying = false;
    }
    Player() {
        lives = LIVES_COUNT;
        isPlaying = false;
    }
};

void copyIntToChar(int &i, char *str, int a);
void makePlayersArr(char *str, BdList<Player> players, int i);
bool refresh(BdList<Player> *players);

int main() {
    BdList <Player> players;
    TGLServerPort serverPort;
    bool aff = serverPort.bind(host, myPort);
    while(!aff) aff = serverPort.bind(host, myPort);
    while(!GetAsyncKeyState(VK_ESCAPE)) {
        Player p(rand()%1000);
        while(serverPort.accept(&p.port, 10)) {
            players.add(p);
            Player p(rand()%1000);
        }
        for(int i=0; i < players.getSize(); i++) {
            Player p = players.getValue(i);
            char str[100];
            size_t bytesLeft = 0;
            if(p.port.recvMess(str, 1000, &bytesLeft) > 0) {
                assert(bytesLeft == 0);
                char tmp[100];
                int j=0;
                while(str[j] != '~') {
                    tmp[j] = str[j];
                    j++;
                }
                j++;
                int command = atoi(tmp);
                printf("command=%d\n", command);
                if(command == 0) {
                    char s[1000];
                    s[0] = '1';
                    s[1] = '~';
                    if(players.getSize() < MAX_PLAYERS) s[2] = '0';
                    else s[2] = '1';
                    makePlayersArr(s, players, 3);
                    bool tmp = p.port.sendMess(s, 1000, 10);
                    while(!tmp) tmp = p.port.sendMess(s, 1000, 10);
                }
                if(command == 2) {
                    char c = str[j++];
                    p.color = atoi(&c);
                    assert(str[j] == '~');
                    j++;
                    char tmp[100];
                    int k = 0;
                    while(str[j] != '\t') {
                        tmp[k] = str[j];
                        j++;
                        k++;
                    }
                    strcpy(p.name, tmp);
                    p.isPlaying = true;
                    players.setValue(i, p);
                    char s[1000];
                    s[0] = '3';
                    s[1] = '~';
                    s[2] = '0';
                    makePlayersArr(s, players ,3);
                    bool a = p.port.sendMess(s, 1000, 10);
                    while(!a) a = p.port.sendMess(s, 1000, 10);
                }
                if(command == 5 || command == 6) {
                    if(command == 5) {
                        if(str[j] == 'L') p.x -= OFFSET;
                        else {
                            assert(str[j] == 'R');
                            p.x += OFFSET;
                        }
                    }
                    if(command == 6) {
                        Whizbang w(p.x, GetTickCount());
                        p.whizbangs.add(w);
                    }
                    players.setValue(i, p);
                    char s[1000];
                    s[0] = '4';
                    s[1] = '~';
                    refresh(&players);
                    makePlayersArr(s, players, 2);
                    bool tmp = p.port.sendMess(s, 1000, 10);
                    while(!tmp) tmp = p.port.sendMess(s, 1000, 10);
                }
            }
            if(refresh(&players)) {
                char s[1000];
                s[0] = '4';
                s[1] = '~';
                makePlayersArr(s, players, 2);
                bool tmp = p.port.sendMess(s, 1000, 10);
                while(!tmp) tmp = p.port.sendMess(s, 1000, 10);
            }
        }
    }
}
void makePlayersArr(char *str, BdList<Player> players, int i) {
    int count = 0;
    while(count < players.getSize()) {
        str[i++] = '<';
        Player p = players.getValue(count);
        int j = i;
        for(i; i < strlen(p.name);i++) {
            str[i] = p.name[i-j];
        }
        str[++i] = '~';
        i++;
        copyIntToChar(i, str, p.x);
        str[i++] = '~';
        copyIntToChar(i, str, p.lives);
        str[i++] = '~';
        copyIntToChar(i, str, p.color);
        str[i++] = '~';
        str[i++] = '{';
        for(int j = 0; j < p.whizbangs.getSize(); j++) {
            Whizbang w = p.whizbangs.getValue(j);
            copyIntToChar(i, str, w.x);
            str[i++] = '~';
            copyIntToChar(i, str, w.time);
            str[i++] = ';';
        }
        str[i++] = '}';
        if(p.isPlaying) str[i++] = '0';
        else str[i++] = '1';
        str[i++] = '>';
        count ++;
    }
}
void copyIntToChar(int &i, char *str, int a) {
    char *tmp;
    sprintf(tmp, "%d", a);
    int digitCount = 0;
    while(a > 0) {
        a /= 10;
        digitCount ++;
    }
    for(int j = i; j < i+digitCount; j++) {
        str[j] = tmp[j-i];
    }
    i += digitCount;
}
bool refresh(BdList<Player> *players) {
   bool res = false;
   for(int i=0; i < players->getSize(); i++) {
        Player p1 = players->getValue(i);
        for(int j=0; j < players->getSize(); i++) {
            if(i != j) {
                Player p2 = players->getValue(j);
                for(int k=0; k < p1.whizbangs.getSize(); k++) {
                    Whizbang w = p1.whizbangs.getValue(k);
                    int py = 50;
                    if(p2.color == RED) py = 950;
                    int wy = 950 - w.time*SPEED;
                    if(p1.color == BLUE) wy = 50 + w.time*SPEED;
                    int A = (p2.x - w.x)*(p2.x - w.x) + (py - wy)*(py - wy);
                    if(A < 55) {
                        p2.lives --;
                        if(p2.lives == 0) p2.isPlaying = false;
                        p1.whizbangs.remove(p1.whizbangs.getSize()-1);
                        res = true;
                    }
                }
            }
        }
   }
   return res;
}

#include "..\BDLibProduction\BdLib.cpp"
#include "..\BDLibProduction\tglib.h"
#include "..\BDLibProduction\BdTextField.cpp"
#include "..\BDLibProduction\BdTimer.cpp"
#include "iostream"

static const int myPort = 5151;
//static const char* host = "10.77.251.111";
static char* host = "192.168.1.100";
static const int BLUE = 1, RED = 2;
static const int SPEED = 1;
class Whizbang{
public:
    int time;
    int x;

    Whizbang(int x_, int time_) {
        x = x_;
        time = time_;
    }
    Whizbang() {
        x = 0;
        time = 0;
    }
};

class Player : public BdComponent{
public:
    char name[100];
    int lives;
    bool mine;
    int color;
    TGLPort port;
    BdList <Whizbang> whizbangs;

    void draw(Surface *s);

    Player() {
        mine = false;
    }
};

class Proc : public BdComponentListener, public BdTimerListener, public BdComponent{
public:
    BdLabel label;
    BdButton butRed, butBlue, butYes, butNo;
    BdTextField text;
    BdTimer timer;
    BdList<Player> players;
    TGLPort port;
    char myName[100];
    int myColor;
    BdContainer *container;

    void onClick(BdComponent *comp, int x, int y);
    void onTick(BdTimer* _timer);
    void draw(Surface *s);

    Proc(BdContainer *container_) : label(400, 100, 200, 50, "aza"), butYes(400, 500, 50, 50, "Yes", false),
                                    butNo(550, 500, 50, 50, "No", false), butBlue(400, 500, 50, 50, "Blue", false),
                                    butRed(550, 500, 50, 50, "Red", false), text(400, 500, 200){
        container = container_;
        container->addChild(this);
        text.addListener(this);
        butBlue.addListener(this);
        butNo.addListener(this);
        butRed.addListener(this);
        butYes.addListener(this);
        label.addListener(this);
        timer.setPeriodMillis(10);
    }
};

void readPlayersArr(char *str, BdList<Player> *players, int i);
void charToInt(char *str, int beg, int end, int &res);

int main() {
    BdWindow window(1300, 600);
    Proc prog(&window.container);
    bool connection = prog.port.connect(host, myPort, 10);
    while(!connection) connection = prog.port.connect(host, myPort, 10);
    prog.port.sendMess("0", 0, 10);
    prog.timer.start();
    window.show();
}
void readPlayersArr(char *str, BdList<Player> *players, int i) {
    while(str[i++] == '<') {
        Player p;
        int j = i;
        while(str[i] != '~') {
            p.name[i-j] = str[i];
            i++;
        }
        i++;
        j = i;
        while(str[i] != '~') i++;
        charToInt(str, j, i-1, p.x);
        i++;
        j = i;
        while(str[i] != '~') i++;
        charToInt(str, j, i-1, p.lives);
        i++;
        j = i;
        while(str[i] != '~') i++;
        charToInt(str, j, i-1, p.color);
        i++;
        if(p.color = BLUE) p.y = 950;
        else p.y = 50;
        assert(str[i] == '{');
        i++;
        while(str[i] != '}') {
            Whizbang w;
            j = i;
            while(str[i] != '~') i++;
            charToInt(str, j, i-1, w.x);
            i++;
            j = i;
            while(str[i] != ';') i++;
            charToInt(str, j, i-1, w.time);
            i++;
            p.whizbangs.add(w);
        }
        i++;
        if(atoi((const char*)str[i]) == 0) p.setEnabled(true);
        else p.setEnabled(false);
        i++;
        assert(str[i] == '>');
        i++;
        players->add(p);
    }
}
void charToInt(char *str, int beg, int end, int &res) {
    char tmp[100];
    for(int i=beg; i < end+1; i++) {
        tmp[i-beg] = str[i];
    }
    res = atoi(tmp);
}
void Proc::onClick(BdComponent *comp, int x, int y) {
    if(comp == &butYes) {
        container->removeChild(&label);
        container->removeChild(&butYes);
        container->removeChild(&butNo);
    }
    if(comp == &butNo) {
        txDisableAutoPause();
        exit(-1);
    }
    if(comp == &butRed) {
        myColor = RED;
        container->removeChild(&butRed);
        container->removeChild(&butBlue);
        label.setText("Write your name");
        container->addChild(&text);
    }
    if(comp == &butBlue) {
        myColor = BLUE;
        container->removeChild(&butRed);
        container->removeChild(&butBlue);
        label.setText("Write your name");
        container->addChild(&text);
    }
    if(comp == &text) {
        text.getText(myName);
        while(myName[text.getSymbolCount()-1] != '\t') text.getText(myName);
        container->removeChild(&text);
    }
}
void Proc::onTick(BdTimer* _timer) {
    if(&timer == _timer) {
        char str[1000];
        size_t bytesLeft = 0;
        if(port.recvMess(str, 1000, &bytesLeft) > 0) {
            assert(bytesLeft == 0);
            int i = 0;
            int command;
            while(str[i] != '~') i++;
            i++;
            charToInt(str, 0, i-2, command);
            if(command == 1) {
                readPlayersArr(str, &players, i+1);
                if(atoi((const char*)str[i++]) == 1) {
                    label.setText("You can't play. Do yo want remain?");
                    container->addChild(&label);
                    container->addChild(&butYes);
                    container->addChild(&butNo);
                }else {
                    label.setText("Choose command color");
                    container->addChild(&label);
                    container->addChild(&butRed);
                    container->addChild(&butBlue);
                    char s[1000];
                    s[0] = '2';
                    s[1] = '~';
                    s[2] = myColor + '0';
                    s[3] = '~';
                    for(int k = 0; k < strlen(myName); k++) {
                        s[k+4] = myName[k];
                    }
                    bool a = port.sendMess(s, 1000, 10);
                    while(!a) a = port.sendMess(s, 1000, 10);
                }
            }
            if(command == 3) {
                readPlayersArr(str, &players, i+1);
                if(atoi((const char*)str[i++]) == 0) {
                    label.setText("Will you play?");
                    container->addChild(&label);
                    container->addChild(&butYes);
                    container->addChild(&butNo);
                }else {
                    label.setText("You can't play. Do yo want remain?");
                    container->addChild(&label);
                    container->addChild(&butYes);
                    container->addChild(&butNo);
                }
            }
            if(command == 4) {
                readPlayersArr(str, &players, i);
            }
        }
        if(GetAsyncKeyState(VK_LEFT)) {
            char s[1000];
            s[0] = '5';
            s[1] = '~';
            s[2] = 'L';
            bool a = port.sendMess(s, 1000, 10);
            while(!a) a = port.sendMess(s, 1000, 10);
        }
        if(GetAsyncKeyState(VK_RIGHT)) {
            char s[1000];
            s[0] = '5';
            s[1] = '~';
            s[2] = 'R';
            bool a = port.sendMess(s, 1000, 10);
            while(!a) a = port.sendMess(s, 1000, 10);
        }
        if(GetAsyncKeyState(VK_SPACE)) {
            char s[1000];
            s[0] = '6';
            bool a = port.sendMess(s, 1000, 10);
            while(!a) a = port.sendMess(s, 1000, 10);
        }
    }
}
void Proc::draw(Surface *s) {
    s->setColor(TX_BLACK);
    s->line(0, 100, 1300, 100);
    s->line(0, 500, 1300, 500);
    s->line(1000, 0, 1000, 600);
    s->line(1000, 300, 1300, 300);
    s->textOut(1000, 0, "RED TEAM");
    s->textOut(1000, 500, "BLUE TEAM");
    int blueCount = 0, redCount = 0;
    for(int i=0; i < players.getSize(); i++) {
        Player p = players.getValue(i);
        if(p.color == BLUE) {
            blueCount ++;
            BdLabel label(1000, 300+blueCount*20, 300, 20, p.name);
        }else {
            redCount ++;
            BdLabel label(1000, 100+redCount*20, 300, 20, p.name);
        }
        if(p.name == myName) {
            p.mine = true;
        }
        p.draw(s);
    }
}
void Player::draw(Surface *s) {
    if(isEnabled()) {
        s->setColor(TX_BLACK);
        s->setFillColor(TX_WHITE);
        s->circle(x, y, 50);
        if(color == BLUE) {
            if(lives >= 3) {
                s->setFillColor(RGB(0, 0, 64));
                s->circle(x, y, 50);
            }
            if(lives >= 2) {
                s->setColor(RGB(0, 0, 160));
                s->setFillColor(RGB(0, 0, 160));
                s->circle(x, y, 45);
            }
            if(lives >= 1) {
                s->setColor(RGB(0, 0, 255));
                s->setFillColor(RGB(0, 0, 255));
                s->circle(x, y, 40);
            }
        }else {
            if(lives >= 3) {
                s->setFillColor(RGB(64, 0, 0));
                s->circle(x, y, 50);
            }
            if(lives >= 2) {
                s->setColor(RGB(160, 0, 0));
                s->setFillColor(RGB(160, 0, 0));
                s->circle(x, y, 45);
            }
            if(lives >= 1) {
                s->setColor(RGB(255, 0, 0));
                s->setFillColor(RGB(255, 0, 0));
                s->circle(x, y, 40);
            }
        }
        if(mine) {
            s->setColor(RGB(0, 255, 0));
            s->setFillColor(RGB(0, 255, 0));
        }else {
            s->setColor(RGB(255, 255, 0));
            s->setFillColor(RGB(255, 255, 0));
        }
        s->circle(x, y, 35);
        for(int i=0; i < whizbangs.getSize(); i++) {
            Whizbang w = whizbangs.getValue(i);
            int y = 0;
            if(color == BLUE) y = 50 + w.time*SPEED;
            else y = 950 - w.time*SPEED;
            s->circle(w.x, y, 5);
        }
    }
}

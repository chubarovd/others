#include "BdMessageList.h"

BdMessageList::BdMessageList(int x_, int y_, int w_, int h_) : BdComponent(x_, y_, w_, h_) {
    count_ = 0;
    startHeight = h_;
    multilinesCount = 0;
    strcpy(name, "you");
}

void BdMessageList::draw(Surface* s) {
    s->selectFont("Lucida Console", false, false);
    width_of_font = s->getTextExtentX("m");
    height_of_font = s->getTextExtentY("m");
    lenght_of_side_before_triangle = 2*height_of_font/3.0;
    s->setColor(TX_BLACK);
    s->setFillColor(RGB(240, 240 ,240));
    s->rectangle(0, 0, width, height);
    if(count_ == 0) {
        s->selectFont("Lucida Console", false, false);
        s->setColor(RGB(159, 159, 159));
        s->drawText((x + width)/4, 30, 3*(x + width)/4, 60, "� ��� ��� ��� ���������");
    }
    for(int i = 0; i < count_; i++) {
        s->selectFont("Lucida Console", height_of_font, width_of_font);
        lines = multiline(message[i]);
        if(types[i] == SENDED) {
            s->setColor(RGB(174, 233, 81));
            s->setFillColor(RGB(174, 233, 81));
            POINT m[6] = {{x + width - INTERVAL - (strlen(message[i]) + 1)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + multilinesCount*height_of_font},
                          {x + width - 2*INTERVAL, i*height_of_font + (i + 1)*INTERVAL + multilinesCount*height_of_font},
                          {x + width - 2*INTERVAL, i*height_of_font + (i + 1)*INTERVAL + lenght_of_side_before_triangle + multilinesCount*height_of_font},
                          {x + width - INTERVAL,   i*height_of_font + (i + 1)*INTERVAL + height_of_font + multilinesCount*height_of_font},
                          {x + width - INTERVAL - (strlen(message[i]) + 1)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + height_of_font + multilinesCount*height_of_font},
                          {x + width - INTERVAL - (strlen(message[i]) + 1)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + multilinesCount*height_of_font}};
            s->polygon(m, 6);
            s->setColor(RGB(57, 57, 57));
            s->drawText(x + width - INTERVAL - (strlen(message[i]) + 2)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + multilinesCount*height_of_font,
                        x + width - INTERVAL, i*height_of_font + (i + 1)*INTERVAL + height_of_font + multilinesCount*height_of_font, message[i]);

            s->setColor(RGB(160, 160, 160));
            s->selectFont("Lucida Console", 13, 6);
            s->drawText(x + width - 6*INTERVAL - (strlen(message[i]) + 2)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + multilinesCount*height_of_font,
                        x + width - INTERVAL - (strlen(message[i]) + 2)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + height_of_font + multilinesCount*height_of_font, times_[i]);
        } else if (types[i] == RECEIVED) {
            s->setColor(RGB(219, 219, 219));
            s->setFillColor(RGB(219, 219, 219));
            POINT m[6] = {{x + INTERVAL + (strlen(message[i]) + 1)*width_of_font, i*height_of_font + (i + 1)*INTERVAL},
                          {x + 2*INTERVAL, i*height_of_font + (i + 1)*INTERVAL},
                          {x + 2*INTERVAL, i*height_of_font + (i + 1)*INTERVAL + lenght_of_side_before_triangle},
                          {x + INTERVAL,   i*height_of_font + (i + 1)*INTERVAL + height_of_font},
                          {x + INTERVAL + (strlen(message[i]) + 1)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + height_of_font},
                          {x + INTERVAL + (strlen(message[i]) + 1)*width_of_font, i*height_of_font + (i + 1)*INTERVAL}};
            s->polygon(m, 6);
            s->setColor(RGB(77, 77, 77));
            s->drawText(x + INTERVAL, i*height_of_font + (i + 1)*INTERVAL,
                    x + INTERVAL + (strlen(message[i]) + 2)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + height_of_font, message[i]);

            s->setColor(RGB(160, 160, 160));
            s->selectFont("Lucida Console", 13, 6);
            s->drawText(x + INTERVAL + (strlen(message[i]) + 2)*width_of_font, i*height_of_font + (i + 1)*INTERVAL,
                        x + 6*INTERVAL + (strlen(message[i]) + 2)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + height_of_font, times_[i]);
        }
    }
}

void BdMessageList::process(TxEvent* event) {
    for(int i = 0; i < count_; i++) {
        POINT pos = event->pos;
        if(types[i] == SENDED) {
            RECT polygon = {x + width - INTERVAL - (strlen(message[i]) + 2)*width_of_font, i*height_of_font + (i + 1)*INTERVAL,
                        x + width - INTERVAL, i*height_of_font + (i + 1)*INTERVAL + height_of_font};
            if(In(pos, polygon) && event->code == 2 && event->type == TxEvent::TX_MOUSE_PRESS) {
                //��� ����� � �����o� ������������ ������� ���������,
            }

        }
        if(types[i] == RECEIVED) {
            RECT polygon = {x + INTERVAL, i*height_of_font + (i + 1)*INTERVAL,
                        x + INTERVAL + (strlen(message[i]) + 2)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + height_of_font};
            if(In(pos, polygon) && event->code == 2 && event->type == TxEvent::TX_MOUSE_PRESS) {
                //��� ����� � �����o� ������������ ������� ���������,
            }
        }
    }
}

int BdMessageList::multiline(char* line) {
    int multilines_ = 0;
    if(strlen(line)*width_of_font > 3*x/5.0) {
        multilinesCount += (strlen(line)*width_of_font)/(3*x/5.0);
        multilines_ += (strlen(line)*width_of_font)/(3*x/5.0);
        if((strlen(line)*width_of_font)%(3*x/5.0) > 0) {
            multilinesCount++;
            multilines_++;
        }
        return multilines_;
    }
    return 0;
}

void BdMessageList::addMess(char* mess, Type type) {
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    sprintf(time_, "%d:%d", timeinfo->tm_hour, timeinfo->tm_min);
    if(strlen(mess) > 0) {
        strcpy(times_[count_], time_);
        strcpy(message[count_], mess);
        types[count_] = type;
        count_++;
    }
    if(count_*height_of_font + (count_ + 1)*INTERVAL >= y + height) {
        height += height_of_font + INTERVAL;
    }
}

void BdMessageList::deleteMess(int index) {
    for (int i = index; i < count_; i++) {
        strcpy(message[i], message[i + 1]);
        types[i] = types[i + 1];
    }
    count_--;
    if((height - (height_of_font + INTERVAL)) > startHeight) {
        height -= height_of_font + INTERVAL;
    } else {
        height = startHeight;
    }
}

void BdMessageList::setUserName(char* name_) {
    strcpy(name, name_);
}

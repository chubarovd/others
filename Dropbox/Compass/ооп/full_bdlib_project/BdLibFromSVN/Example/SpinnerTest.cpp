#include "../BdSpinner.cpp"
#include "../BdTextField.cpp"
#include "../BdLib.cpp"

class Form : BdSpinnerListener{
    BdButton butTest;
    BdSpinner spinner;
public:
    Form(BdContainer *container, int x, int y, int width, double value, double step, bool status) : spinner(x, y, width, value, step, status), butTest(x, y + 150, value, value, "test", status) {
        container->addChild(&butTest);
		container->addChild(&spinner);
		spinner.addSpinnerListener(this);
	}

	void onChange(BdSpinner *spinner);
};
int main() {
    BdWindow window(500, 500);
    Form f(&window.container, 0, 0, 111, 0, 1, true);
    window.show();
}
void Form::onChange(BdSpinner *spinner) {
    butTest.width = spinner->getValue();
    butTest.height = spinner->getValue();
}

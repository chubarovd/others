#include "BdLib.h"

class BdTextArea;

class BdTextAreaListener {
public:
    virtual void textInserted(BdTextArea* area, int pos, int len) = 0;
    virtual void textRemoved(BdTextArea* area, int pos, int len) = 0;
};

class BdTextArea : public BdComponent {
public:
    char text[100500];
    int curLen;
    int curPos;
    COLORREF borderColor;
    COLORREF backgroundColor;
    int thin;
    int listenerCount;
    int symbol;
    BdList<BdTextAreaListener*> listeners;
    bool onFocus;
    bool userCanEditText;
    bool full;
    void draw(Surface *s);
    void process(TxEvent *e);
    void setEditable(bool b);
    void setText(const char* text2, int len);
    int getText(char *dest, int destLen);
    void insertText(char *text, int len, int pos);
    void removeText(int pos, int count);
    void addListener(BdTextAreaListener* listener) {
        listeners.add(listener);
	}
	struct LineDesc {
        int startX;
        int startY;
        int w;
        int h;
        int startPos;
        int charCount;
        bool ifMouseOn(POINT pos);
	};
	BdList<LineDesc> lines;
    BdTextArea();
	BdTextArea(int x, int y, int width, int height, BdContainer* container);
};

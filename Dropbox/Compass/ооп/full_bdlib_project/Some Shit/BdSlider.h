#include "BdLib.h"
#include "BdLib.cpp"

enum SliderState {BT_NORMAL, BT_UNDER_CURSOR, BT_PRESSED};
enum SliderType {S_VERTICAL, S_GORISONTAL};

class BdSlider;

class BdSliderListener
{
public:
    virtual void onChange (BdSlider* s) = 0;
};


class BdSlider : public BdComponent{
private:
    int x;
    int y;
protected:
    BdList<BdSliderListener*> sliderlisteners;
    SliderState state;
    static const int INDENTX = 10;
    static const int INDENTY = 30;
    SliderType type;
    double min;
    double now_value;
    bool drawDigits;
public:
    double max;
    void draw(Surface* s);
    void process(TxEvent* event);
    int getMin();
    void setMin (int min);
    int getMax();
    void setMax(int max);
    int getValue();
    void setValue(int newValue);
    bool control(TxEvent* event1);

    void addSliderListener(BdSliderListener* s);
    void removeSliderListener(BdSliderListener* s);

    BdSlider ();
    BdSlider (int x_, int y_, int width_, int height_, double min_, double max_, SliderType type_, bool drawDigits_);
};

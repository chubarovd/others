#include "BdLib.h"

class BdRadioButton : public BdComponent {
protected:
    char text[101];
public:
    bool selected;

    BdRadioButton(int x_, int y_, int width_, int height_, const char* _text);

    void draw(Surface* s);

//    void addListener(BdComponentListener* _listener);
//    void removeListener(BdComponentListener* _listener);

//    bool getSelected();
//    bool setSelected(bool _selected);
};

class BdRadioGroup : public BdComponentListener {
private:
    BdComponent* selectedButton;
    BdList<BdRadioButton*> Buttons[101];
public:
    BdRadioGroup();

    void onClick(BdComponent* b, int _x, int _y);

    void addRadBut(BdRadioButton* _button);
    void removeRadBut(BdRadioButton* _button);
};









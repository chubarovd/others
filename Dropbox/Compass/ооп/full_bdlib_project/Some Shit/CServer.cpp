#include "BdLib.cpp"
#include "BdTimer.cpp"
#include "BdTabSet.cpp"
#include "tglib.h"

bool activate = false;
size_t left = 0;

class Form : public BdComponentListener, BdTimerListener, BdTabSetListener {
private:
    static const int SIZE = 100;

    enum messageTypes {SEND_TEXT, GET_USER_LIST, USER_LIST, READY, DISCONNECT, RENAME, SEND_PRIVATE_TEXT, RECV_PRIVATE_TEXT, NONE};
    messageTypes mType;

    bool someoneLeft;
    bool someoneConnect;

    TGLServerPort sp;
    TGLPort p[SIZE];
    bool recipients[SIZE];
    char names[SIZE][SIZE];
    int portsCount;

    char buf[SIZE];
public:
    BdWindow window;

    BdTimer counter;

    void onClick (BdComponent* v, int x, int y);
    void onTick(BdTimer* timer);
    void onTabChanged(BdTabSet* tabSet);

    void messageProcessing(char* str, int index);
    void deleteUser(int ind);
    void sendUserList(int ind);

    Form();
};

int main() {
    Form form;
    form.window.show();
}

Form::Form() : window(500, 600) {
    sp.bind("0.0.0.0", 2215);
    window.container.addChild(&counter);
    counter.setPeriodMillis(1);
    counter.addListener(this);
    counter.start();
    portsCount = 0;
    mType = NONE;
}

void Form::onClick (BdComponent* v, int x, int y) {

}

void Form::onTick(BdTimer* timer) {
    if(sp.accept(&p[portsCount], 1)) {
        p[portsCount].setMessRecvTimeout(1);
        recipients[portsCount] = false;
        int messLen = p[portsCount].recvMess(buf, SIZE, &left);
        if(messLen > 0) {
            buf[messLen] = 0;
            strcpy(names[portsCount], buf);
        }
        printf("CONNECTIG <%s> INTO %d\n", names[portsCount], portsCount);
        portsCount++;

        char send[SIZE];
        sprintf(send, "2");
        for(int i = 0; i < portsCount; i++) {
            strcat(send, names[i]);
            strcat(send, ";");
        }
        if(p[portsCount - 1].sendMess(send, strlen(send) + 1, -1)) {
            printf("SEND USER LIST TO %s\n", names[portsCount - 1]);
            recipients[portsCount - 1] = true;
        } else {
            printf("USER LIST HAS NOT BEEN SENT TO <%s>\n", names[portsCount - 1]);
        }
        for(int i = 0; i < portsCount - 1; i++) {
            recipients[i] = false;
            if(p[i].sendMess("1", 2, -1)) {
                if(p[i].sendMess(send, strlen(send) + 1, -1)) {
                    printf("SEND USER LIST TO %s\n", names[portsCount - 1]);
                    recipients[i] = true;
                } else {
                    printf("USER LIST HAS NOT BEEN SENT TO <%s>\n", names[portsCount - 1]);
                }
            } else {
                printf("USER LIST HAS NOT BEEN SENT TO <%s>\n", names[portsCount - 1]);
            }
        }
    }
    for(int i = 0; i < portsCount; i++) {
        int messLen = p[i].recvMess(buf, SIZE, &left);
        if(messLen > 0) {
            buf[messLen] = 0;
            messageProcessing(buf, i);
        }
    }
}

void Form::onTabChanged(BdTabSet* tabset) {

}

void Form::messageProcessing(char* str, int index) {
    char input[SIZE] = {};
    char recv[SIZE] = {};
    int type = str[0] - '0';
    mType = (messageTypes)type;

    for(int i = 1; i < strlen(str); i++) {
        input[i - 1] = str[i];
    }
    printf("TYPE: %d ~~ MESS: %s ~~ FROM <%s>\n", mType, input, names[index]);
    switch(mType) {
        case SEND_TEXT :        //complete
            for(int i = 0; i < portsCount; i++) {
                if(i != index && recipients[i] == true) {
                    sprintf(recv, "0%s: %s", names[index], input);
                    p[i].sendMess(recv, strlen(recv) + 1, 30);
                    printf("SEND: %s TO <%s>\n", input, names[i]);
                }
            }
        break;
        case GET_USER_LIST :
            sendUserList(index);
        break;
        case READY :        //complete
            printf("%s READY\n", names[index]);
            recipients[index] = true;
        break;
        case DISCONNECT :
            deleteUser(index);
            for(int i = 0; i < portsCount; i++) {
                recipients[i] = false;
                p[i].sendMess("1", 2, -1);
                sendUserList(i);
            }
        break;
        case RENAME :

        break;
    }
}

void Form::deleteUser(int ind) {
    for(int j = ind; j < portsCount - 1; j++) {
        p[j] = p[j + 1];
        recipients[j] = recipients[j + 1];
        strcpy(names[j], names[j + 1]);
    }
    portsCount--;
}

void Form::sendUserList(int ind) {
    recipients[ind] = false;
    char send[SIZE];
    sprintf(send, "2");
    for(int i = 0; i < portsCount; i++) {
        strcat(send, names[i]);
        strcat(send, ";");
    }
    if(p[ind].sendMess(send, strlen(send) + 1, -1)) {
        printf("SEND USER LIST TO %s\n", names[ind]);
        recipients[ind] = true;
    } else {
        printf("USER LIST HAS NOT BEEN SENT TO <%s>\n", names[ind]);
    }
}



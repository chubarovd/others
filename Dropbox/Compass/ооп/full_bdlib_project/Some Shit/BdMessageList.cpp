#include "BdMessageList.h"

BdMessageList::BdMessageList(int x_, int y_, int w_, int h_, TypeOfMessList type) : BdComponent(x_, y_, w_, h_) {
    count_ = 0;
    items_count = 0;
    startHeight = h_;
    multilinesCount = 0;
    commonType = type;
}

void BdMessageList::draw(Surface* s) {
    s->selectFont("Lucida Console", false, false);
    width_of_font = s->getTextExtentX("m");
    height_of_font = s->getTextExtentY("m");
    lenght_of_side_before_triangle = 2*height_of_font/3.0;
    s->setColor(TX_BLACK);
    s->setFillColor(RGB(240, 240 ,240));
    s->rectangle(0, 0, width, height);
    if(commonType == CHAT) {
        if(count_ == 0) {
            s->selectFont("Lucida Console", false, false);
            s->setColor(RGB(159, 159, 159));
            s->drawText((x + width)/4, 30, 3*(x + width)/4, 60, "� ��� ��� ��� ���������");
        }
        for(int i = 0; i < count_; i++) {
            s->selectFont("Lucida Console", height_of_font, width_of_font);
            if(types[i] == SENDED) {
                if(multiline(message[i])) {


                } else {
                    s->setColor(RGB(174, 233, 81));
                    s->setFillColor(RGB(174, 233, 81));
                    POINT m[6] = {{width - INTERVAL - (strlen(message[i]) + 1)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + multilinesCount*height_of_font},
                                  {width - 2*INTERVAL, i*height_of_font + (i + 1)*INTERVAL + multilinesCount*height_of_font},
                                  {width - 2*INTERVAL, i*height_of_font + (i + 1)*INTERVAL + lenght_of_side_before_triangle + multilinesCount*height_of_font},
                                  {width - INTERVAL,   i*height_of_font + (i + 1)*INTERVAL + height_of_font + multilinesCount*height_of_font},
                                  {width - INTERVAL - (strlen(message[i]) + 1)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + height_of_font + multilinesCount*height_of_font},
                                  {width - INTERVAL - (strlen(message[i]) + 1)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + multilinesCount*height_of_font}};
                    s->polygon(m, 6);
                    s->setColor(RGB(57, 57, 57));
                    s->drawText(width - INTERVAL - (strlen(message[i]) + 2)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + multilinesCount*height_of_font,
                                width - INTERVAL, i*height_of_font + (i + 1)*INTERVAL + height_of_font + multilinesCount*height_of_font, message[i]);
                }
                s->setColor(RGB(160, 160, 160));
                s->selectFont("Lucida Console", 13, 6);
                s->drawText(width - 6*INTERVAL - (strlen(message[i]) + 2)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + multilinesCount*height_of_font,
                            width - INTERVAL - (strlen(message[i]) + 2)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + height_of_font + multilinesCount*height_of_font, times_[i]);
            } else if(types[i] == RECEIVED) {
                if(multiline(message[i])) {

                } else {
                    s->setColor(RGB(219, 219, 219));
                    s->setFillColor(RGB(219, 219, 219));
                    POINT m[6] = {{INTERVAL + (strlen(message[i]) + 1)*width_of_font, i*height_of_font + (i + 1)*INTERVAL},
                                  {2*INTERVAL, i*height_of_font + (i + 1)*INTERVAL},
                                  {2*INTERVAL, i*height_of_font + (i + 1)*INTERVAL + lenght_of_side_before_triangle},
                                  {INTERVAL,   i*height_of_font + (i + 1)*INTERVAL + height_of_font},
                                  {INTERVAL + (strlen(message[i]) + 1)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + height_of_font},
                                  {INTERVAL + (strlen(message[i]) + 1)*width_of_font, i*height_of_font + (i + 1)*INTERVAL}};
                    s->polygon(m, 6);
                    s->setColor(RGB(77, 77, 77));
                    s->drawText(INTERVAL, i*height_of_font + (i + 1)*INTERVAL,
                                INTERVAL + (strlen(message[i]) + 2)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + height_of_font, message[i]);
                }
                s->setColor(RGB(160, 160, 160));
                s->selectFont("Lucida Console", 13, 6);
                s->drawText(INTERVAL + (strlen(message[i]) + 2)*width_of_font, i*height_of_font + (i + 1)*INTERVAL,
                            6*INTERVAL + (strlen(message[i]) + 2)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + height_of_font, times_[i]);
            } else if(types[i] == SYSTEM) {
                s->setColor(RGB(213, 213, 213));
                s->setFillColor(RGB(240, 240 ,240));
                s->rectangle(0, height_of_font*i, width, height_of_font*(i + 1));
                s->setColor(RGB(132, 132, 132));
                s->drawText(0, height_of_font*i, width, height_of_font*(i + 1), message[i]);
            }
        }
    } else if(commonType == LIST) {
        if(items_count == 0) {
            s->selectFont("Lucida Console", false, false);
            s->setColor(RGB(159, 159, 159));
            s->rectangle(0, 0, 10, 10);
            s->drawText((x + width)/4, 30, 3*(x + width)/4, 60, "������ ��� ����");
        }
        for(int i = 0; i < items_count; i++) {
            s->setColor(RGB(93, 138, 168));
            s->setFillColor(RGB(240, 248, 255));
            s->rectangle(0, height_of_font*i, width, height_of_font*(i + 1));
            s->drawText(0, height_of_font*i, width, height_of_font*(i + 1), items[i]);
        }
    }
}

void BdMessageList::process(TxEvent* event) {
    if(commonType == CHAT) {
        for(int i = 0; i < count_; i++) {
            POINT pos = event->pos;
            if(types[i] == SENDED) {
                RECT polygon = {x + width - INTERVAL - (strlen(message[i]) + 2)*width_of_font, i*height_of_font + (i + 1)*INTERVAL,
                            x + width - INTERVAL, i*height_of_font + (i + 1)*INTERVAL + height_of_font};
                if(In(pos, polygon) && event->code == 2 && event->type == TxEvent::TX_MOUSE_PRESS) {
                    //��� ����� � �����o� ������������ ������� ���������,
                }

            }
            if(types[i] == RECEIVED) {
                RECT polygon = {x + INTERVAL, i*height_of_font + (i + 1)*INTERVAL,
                            x + INTERVAL + (strlen(message[i]) + 2)*width_of_font, i*height_of_font + (i + 1)*INTERVAL + height_of_font};
                if(In(pos, polygon) && event->code == 2 && event->type == TxEvent::TX_MOUSE_PRESS) {
                    //��� ����� � �����o� ������������ ������� ���������,
                }
            }
        }
    } else if(commonType == LIST) {
        POINT pos = event->pos;
        for(int i = 0; i < items_count; i++) {
            RECT itm = {x, y + height_of_font*i, x + width, y + height_of_font*(i + 1)};
            if(In(pos, itm) && event->type == TxEvent::TX_MOUSE_PRESS && event->code == 1) {
                lastSelectedItem = i;
                for(int j = 0; j < mlListeners.getSize(); j++) {
                    mlListeners.getValue(j)->onSelectedItem(this);
                }
            }
        }
    }
}

bool BdMessageList::multiline(char* line) {
    /*if(strlen(line)*width_of_font > 3*(getWindow()->getWindowX())/5.0) {
        multilinesCount += (strlen(line)*width_of_font)/(3*(getWindow()->getWindowX())/5.0);
        if((strlen(line)*width_of_font)%(3*(getWindow()->getWindowX())/5.0) > 0) {
            multilinesCount++;
        }
        return true;
    }*/
    return false;
}

void BdMessageList::addMess(char* mess, TypeOfMess type) {
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    sprintf(time_, "%d:%d", timeinfo->tm_hour, timeinfo->tm_min);
    if(strlen(mess) > 0) {
        strcpy(times_[count_], time_);
        strcpy(message[count_], mess);
        types[count_] = type;
        count_++;
    }
    if(count_*height_of_font + (count_ + 1)*INTERVAL >= y + height) {
        height += height_of_font + INTERVAL + 1;
    }
}

void BdMessageList::addItem(char* item) {
    if(strlen(item) > 0) {
        strcpy(items[items_count], item);
        items_count++;
    }
    if(items_count*height_of_font >= y + height) {
        height += height_of_font;
    }
}

void BdMessageList::clearTable() {
    if(commonType == CHAT) {
        count_ = 0;
    } else if(commonType == LIST) {
        items_count = 0;
    }
}

void BdMessageList::deleteMess(int index) {
    for (int i = index; i < count_; i++) {
        if(types[i] != SYSTEM) {
            strcpy(message[i], message[i + 1]);
            types[i] = types[i + 1];
        }
    }
    count_--;
    if((height - (height_of_font + INTERVAL)) > startHeight) {
        height -= height_of_font + INTERVAL;
    } else {
        height = startHeight;
    }
}

void BdMessageList::deleteItem(int index) {
    for (int i = index; i < count_; i++) {
        strcpy(items[i], items[i + 1]);
    }
    items_count--;
    if((height - (height_of_font + INTERVAL)) > startHeight) {
        height -= height_of_font + INTERVAL;
    } else {
        height = startHeight;
    }
}

void BdMessageList::deleteItem(char* item) {
    for(int i = 0; i < items_count; i++) {
        if(strcmp(items[i], item)) {
            for (int j = i; j < count_; j++) {
                strcpy(items[j], items[j + 1]);
            }
            items_count--;
            break;
        }
    }
}

void BdMessageList::getItemName(int index, char* array) {
    strcpy(array, items[index]);
}

int BdMessageList::getIndexOfLastSelectedItem() {
    return lastSelectedItem;
}

void BdMessageList::addListener(BdItemListListener* mll) {
    mlListeners.add(mll);
}

void BdMessageList::removeListener(BdItemListListener* mll) {
    mlListeners.removeValue(mll);
}

#include "BdLib.h"

///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////BdMenu/////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
BdMenuItem::BdMenuItem(int _x, int _y, int _width, int _height, char* _itemtext) {
    active = 0;
    x = _x;
    y = _y;
    width = _width;
    height = _height;
    strcpy(itemtext, _itemtext);
    father = tab = NULL;
}

BdMenuItem::BdMenuItem(char* _itemtext) {
    active = 0;
    x = y = width = height = 0;
    father = tab = NULL;
    strcpy(itemtext, _itemtext);
}

BdMenuItem::BdMenuItem() {
    active = 0;
    x = y = width = height = 0;
    father = tab = NULL;
}

char* BdMenuItem::getText() {
    return itemtext;
}

void BdMenuItem::setTab(BdMenu *_tab) {
    tab = _tab;
    tab->father = this;
}

void BdMenuItem::draw(Surface* s) {
    if (tab != NULL) {
        s->setColor(TX_BLACK);
        s->setFillColor(TX_WHITE);
        s->circle(x + width, y + height / 2, 3);
        if (active == 1) {
            tab->draw(s);
        }
    }
}

void BdMenuItem::process(TxEvent *event) {
    RECT rect = {x, y, x + width, y + height};
    switch (event->type) {
        case TxEvent::TX_MOUSE_MOVED: {
            if (In(event->pos, rect)) {
                father->destroy();
                father->active = 1;
                active = 1;
                if (tab != NULL) {
                    tab->active = 1;
                    tab->process(event);
                }
            }
            else {
                if (tab == NULL || tab->active == 0) {
                    active = 0;
                }
                if (tab != NULL) {
                    tab->process(event);
                }
            }
        } break;
    }
}

void BdMenuItem::build() {
    if (tab != NULL) {
        tab->x = x + width - 10;
        tab->y = y + 10;
        tab->height = height;
        tab->width = width;
        tab->build();
    }
}

void BdMenuItem::destroy() {
    active = 0;
    if (tab != NULL) {
        tab->destroy();
    }
}

BdMenu::BdMenu(int _x, int _y, int _width, int _height) {
    father = NULL;
    active = 1;
    x = _x;
    y = _y;
    width = _width;
    height = _height;
}

BdMenu::BdMenu() {
    father = NULL;
    active = 0;
    x = y = width = height = 0;
}

void BdMenu::build() {
    for (int i = 0; i < items.getSize(); ++i){
        BdMenuItem *tmp = items.getValue(i);
        tmp->x = x;
        tmp->y = y + i * height;
        tmp->height = height;
        tmp->width = width;
        tmp->build();
    }
}

int BdMenu::getItemsSize() {
    return items.getSize();
}

void BdMenu::addItem(BdMenuItem *item) {
    item->father = this;
    items.add(item);
}

void BdMenu::draw(Surface* s) {
    if (active) {
        for (int i = 0; i < items.getSize(); ++i) {
            BdMenuItem *tmp = items.getValue(i);
            int dx, dy;
            dx = tmp->x;
            dy = tmp->y;
            s->translate(dx, dy);
            if (tmp->active == 1) {
                s->setFillColor(TX_GRAY);
                s->setColor(TX_BLACK);
                s->rectangle(0, 0, tmp->width, tmp->height);
                s->drawText(0, 0, tmp->width, tmp->height, tmp->getText());
            }
            else {
                s->setFillColor(TX_WHITE);
                s->setColor(TX_BLACK);
                s->rectangle(0, 0, tmp->width, tmp->height);
                s->drawText(0, 0, tmp->width, tmp->height, tmp->getText());
            }
            s->translate(-dx, -dy);
        }
        for (int i = 0; i < items.getSize(); ++i) {
            BdMenuItem *tmp = items.getValue(i);
            tmp->draw(s);
        }
    }
}

void BdMenu::process(TxEvent *event) {
    RECT rect = {x, y, x + width, y + height * items.getSize()};
    if (active) {
        for(int i = 0; i < items.getSize(); ++i) {
            BdMenuItem *tmp = items.getValue(i);
            tmp->process(event);
        }
    }
}

void BdMenu::destroy() {
    active = 0;
    for (int i = 0; i < items.getSize(); ++i) {
        BdMenuItem *tmp = items.getValue(i);
        tmp->destroy();
    }
}

///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////BdWindow///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

BdWindow::BdWindow(int x_, int y_) : container(0, 0, x_, y_, this) {
    x = x_;
    y = y_;
    focusOwner = NULL;
}

BdComponent* BdWindow::getFocusOwner() {
    return focusOwner;
}

void BdWindow::setFocusOwner(BdComponent* focusOwner_) {
    if(focusOwner != focusOwner_) {
        focusOwner = focusOwner_;
        focusOwner_->focusLost();
    } else {
        focusOwner_->focusRecived();
    }
}

void BdWindow::show() {
    DisplaySurface display(0,0);
    surface = &display;
    surface->createWindow(x, y);
    surface->setFillColor(TX_WHITE);
    surface->clear();
    TxEvent event;

    while (!GetAsyncKeyState(VK_ESCAPE)) {
        surface->begin();
        surface->setFillColor(TX_WHITE);
        surface->clear();
        container.draw(surface);
        surface->end();

        event = getNextEvent(25);
        container.fullProcess(&event);
    }
}

void BdWindow::showPopup(BdComponent *popup, int dx, int dy) {
    TxEvent event, nullEvent;
    nullEvent.type = TxEvent::TX_NONE;
    event.type = TxEvent::TX_NONE;
    while (event.type != TxEvent::TX_MOUSE_PRESS) {
        surface->begin();
        surface->setFillColor(TX_WHITE);
        surface->clear();
        container.draw(surface);
        surface->translate(dx, dy);
        popup->draw(surface);
        surface->translate(-dx, -dy);
        surface->end();

        event = getNextEvent(25);
        if (event.type == TxEvent::TX_MOUSE_PRESS && event.code == 2) {
            event.code = 1;
        }
        event.pos.x -= dx;
        event.pos.y -= dy;
        popup->fullProcess(&event);
        event.pos.x -= -dx;
        event.pos.y -= -dy;
        container.fullProcess(&nullEvent);
    }
}

int BdWindow::getWindowX() {
    return x;
}

int BdWindow::getWindowY() {
    return y;
}
///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////BdComponent////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
BdComponent::BdComponent() {
    init(0, 0, 0, 0, true, NULL, NULL);
}

BdComponent::BdComponent(int x_, int y_, int w_, int h_) {
    init(x_, y_, w_, h_, true, NULL, NULL);
}

BdComponent::BdComponent (int x_, int y_, int w_, int h_, bool status_){
    init(x_, y_, w_, h_, status_, NULL, NULL);
}

BdComponent::BdComponent(int x_, int y_, int w_, int h_, BdContainer* container) {
    init(x_, y_, w_, h_, true, container, NULL);
}

BdComponent::BdComponent (int x_, int y_, int w_, int h_, BdWindow* _window) {
    init(x_, y_, w_, h_, true, NULL, _window);
}

void BdComponent::init(int x, int y, int width, int height, bool status, BdContainer* container, BdWindow* window) {
    this->x = x;
    this->y = y;
    this->width = width;
    this->height = height;
    if (container != NULL)
    {
        container->addChild(this); // ���� ��� ��� ����� ��
    }
    else
    {
        this->parent = container;
    }
    this->window = window;
    this->enabled= status;
    this->mouseStatus = NONE;
}

bool BdComponent::isEnabled ()
{
    return enabled;
}

int BdComponent::getMouseStatus ()
{
    return mouseStatus;
}

void BdComponent::setEnabled (bool enabled_)
{
    enabled = enabled_;
}

bool BdComponent::mouseOn(TxEvent* e) {
    return e->pos.x >= x && e->pos.y >= y && e->pos.x <= x + width && e->pos.y <= y + height;
}

void BdComponent::setParent(BdContainer* parent_) {
    parent = parent_;
}

void BdComponent::process(TxEvent* event) {
    if (isEnabled()) {
        if(!(event->pos.x > this->x && event->pos.x < (this->x + this->width) &&
            event->pos.y > this->y && event->pos.y < (this->y + this->height))) {
            mouseStatus = NONE;
        }
        if((event->pos.x > this->x && event->pos.x < (this->x + this->width) &&
            event->pos.y > this->y && event->pos.y < (this->y + this->height))&&(mouseStatus != PRESSED)) {
            mouseStatus = UNDER_CURSOR;
        }
        if((event->type == TxEvent::TX_MOUSE_PRESS)&&(mouseStatus == UNDER_CURSOR)) {
            if (event->code == 1) {
                mouseStatus = PRESSED;
            }
            else if (event->code == 2) {
                showPopup(preparePopup(), event->pos.x, event->pos.y);
            }
        }
        if((event->type == TxEvent::TX_MOUSE_RELEASE)&&(mouseStatus == PRESSED)) {
            for(int i = 0; i < listeners.getSize(); i++) {
                listeners.getValue(i)->onClick(this, event->pos.x, event->pos.y);
            }
            mouseStatus = UNDER_CURSOR;
        }
    }
}

void BdComponent::commonProcess(TxEvent* event) {
    if(event->type == TxEvent::TX_MOUSE_PRESS) {
        if (getWindow() != NULL) {
            if (getWindow()->getFocusOwner () != this) {
                getWindow()->setFocusOwner(this);
            }
        }
    }
}

void BdComponent::fullProcess(TxEvent* event) {
    commonProcess(event);
    process(event);
}

void BdComponent::addListener(BdComponentListener* listener) {
    listeners.add(listener);
}

void BdComponent::removeListener(BdComponentListener* listener) {
    listeners.removeValue(listener);
}

void BdComponent::setPopupComponent(BdComponent *_popupComponent) {
    popupComponent = _popupComponent;
}

BdComponent *BdComponent::preparePopup () {
    return popupComponent;
}

int BdComponent::getXOnScreen() {
    if (parent == NULL) {
        return x;
    }
    return parent->getXOnScreen() + x;
}

int BdComponent::getYOnScreen() {
    if (parent == NULL) {
        return y;
    }
    return parent->getYOnScreen() + y;
}

BdWindow* BdComponent::getWindow() {
    if (window == NULL) {
        if (parent != NULL) {
            return parent->getWindow();
        }
        else {
            return NULL;
        }
    }
    else {
        return window;
    }
}

void BdComponent::showPopup (BdComponent *popup, int dx, int dy) {
    BdWindow *window = getWindow();
    if (window != NULL && popup != NULL) {
        window->showPopup(popup, dx, dy);
    }
}

BdComponent* BdComponent::getParent() {
    return parent;
}

bool BdComponent::isParentOf (BdComponent* child) {
    while (child != NULL) {
        if (child->getParent() == this) {
            return true;
        }
        else {
            if (child->getParent() != NULL) {
                child = child->getParent();
            }
            else {
                return false;
            }
        }
    }
    return false;
}

void BdComponent::focusRecived() {
    for(int i = 0; i < focusListeners.getSize(); i++) {
        focusListeners.getValue(i)->onFocusRecived(this);
    }
}

void BdComponent::focusLost() {
    for(int i = 0; i < focusListeners.getSize(); i++) {
        focusListeners.getValue(i)->onFocusLost(this);
    }
}

void BdComponent::addFocusListener(BdFocusListener* fl) {
    focusListeners.add(fl);
}

void BdComponent::removeFocusListener(BdFocusListener* fl) {
    focusListeners.removeValue(fl);
}

///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////BdButton///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

BdButton::BdButton(int x_, int y_, int width_, int height_, char text_[], bool status_) : BdComponent(x_, y_, width_, height_, status_) {
    strcpy(text, text_);
    color = TX_WHITE;
}

void BdButton::draw(Surface* s) {
    if (isEnabled()) {
        int status = getMouseStatus();
        switch(status) {
            case BdComponent::NONE :
                s->setColor(TX_BLACK);
                s->setFillColor(TX_BLACK);
                s->rectangle (3, 3, width, height);
                s->setFillColor(TX_WHITE);
                s->rectangle (0, 0, width - 3, height - 3);
                s->drawText(0, 0, width - 3, height - 3, text);
                break;
            case BdComponent::UNDER_CURSOR :
                s->setColor(TX_BLACK);
                s->setFillColor(TX_BLACK);
                s->rectangle (3, 3, width, height);
                s->setFillColor(RGB(78, 137, 216));
                s->rectangle (0, 0, width - 3, height - 3);
                s->drawText(0, 0, width - 3, height - 3, text);
                break;
            case BdComponent::PRESSED :
                s->setColor(TX_WHITE);
                s->setFillColor(TX_WHITE);
                s->rectangle (0, 0, width, height);

                s->setColor(TX_BLACK);
                s->setFillColor(RGB(78, 137, 216));
                s->rectangle (3, 3, width, height);
                s->drawText(3, 3, width, height, text);
                break;
        }
    }
    else
    {
        s->setColor(RGB(192, 192, 192));
        s->setFillColor(RGB(112, 128, 144));
        s->rectangle (3, 3, width + 3, height + 3);
        s->setFillColor(RGB(192, 192, 192));
        s->rectangle (0, 0, width, height);
        s->setColor(TX_WHITE);
        s->drawText(0, 0, width, height, text);
    }

}

///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////BdLabel////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

BdLabel::BdLabel(int x_, int y_, int width_, int height_, char text_[]) : BdComponent(x_, y_, width_, height_) {
    strcpy(text, text_);
}

void BdLabel::setText(char text_[]) {
    strcpy(text, text_);
}

void BdLabel::process(TxEvent* event) {
    switch (event-> type) {
        case TxEvent::TX_MOUSE_MOVED:

            break;
        case TxEvent::TX_MOUSE_PRESS: {
                if (event->code == 1){

                }
                else if (event->code == 2){

                }
            } break;
    }
}

void BdLabel::draw(Surface* s) {
    s->setFillColor(TX_WHITE);
    s->setColor(TX_BLACK);
    s->drawText(0, 0, width, height, text, DT_LEFT);
}

///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////BdContainer////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

BdContainer::BdContainer(int x_, int y_, int width_, int height_) : BdComponent(x_, y_, width_, height_) {
    window = NULL;
}

BdContainer::BdContainer(int x_, int y_, int width_, int height_, BdWindow* window_) : BdComponent(x_, y_, width_, height_, window_) {
}

void BdContainer::removeChild(BdComponent* child_) {
    children.removeValue(child_);
    child_->setParent(NULL);
}

void BdContainer::addChild(BdComponent* child_) {
    children.add(child_);
    child_->setParent(this);
}

void BdContainer::draw(Surface* s) {
    for (int i = 0; i < children.getSize(); i++) {
        s->selectFont("Lucida Console", 60, 21);
        int dx, dy;
        dx = (*children.getValue(i)).x;
        dy = (*children.getValue(i)).y;
        s->translate(dx, dy);
        children.getValue(i)->draw(s);
        s->translate(-dx, -dy);
    }
}

void BdContainer::process(TxEvent* event) {
    event->pos.x -= x;
    event->pos.y -= y;
    for (int i = 0; i < children.getSize(); i++) {
        if (isEventForComponent(event, children.getValue(i))) {
            children.getValue(i)->fullProcess(event);
        }
    }
    event->pos.x += x;
    event->pos.y += y;
}

bool BdContainer::isEventForComponent (TxEvent* event, BdComponent* child) {
    if ((event->type == TxEvent::TX_MOUSE_MOVED) || (event->type == TxEvent::TX_NONE) || (event->type == TxEvent::TX_MOUSE_WHEEL))
    {
        return true;
    }
    else
    {
        if ((event->type == TxEvent::TX_KEY_PRESS) || (event->type == TxEvent::TX_CHAR) || (event->type == TxEvent::TX_KEY_RELEASE))
        {
            if ((this->getWindow()->getFocusOwner () == child) || (child -> isParentOf(this->getWindow()->getFocusOwner ())))
            {
                return true;
            }
        }
        else
        {
            RECT rect = {child->x, child->y, child->x + child->width, child->y + child->height};
            POINT pt = event->pos;
            if (In(pt, rect)) {
//                if (this->getWindow() != NULL) {
//                    if ((this->getWindow()->getFocusOwner () != child) && (event->type == TxEvent::TX_MOUSE_PRESS))
//                    {
//                        this->getWindow()->setFocusOwner(child);
//                    }
//                }
                return true;
            }
        }
    }
    return false;
}

///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////BdContainer////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
Surface::Surface(int x_, int y_) {
    x0 = x_;
    y0 = y_;
}

//--------------------------DISPLAY SURFACE-------------------------

DisplaySurface::DisplaySurface(int x_, int y_) : Surface (x_, y_) {

}

void DisplaySurface::line(int x1, int y1, int x2, int y2) {
	txLine(x1 + x0, y1 + y0, x2 + x0, y2 + y0);
}

void DisplaySurface::rectangle(int x1, int y1, int x2, int y2) {
	txRectangle(x1 + x0, y1 + y0, x2 + x0, y2 + y0);
}

void DisplaySurface::setColor(COLORREF color) {
	txSetColor(color);
}

void DisplaySurface::setFillColor(COLORREF color) {
	txSetFillColor(color);
}

void DisplaySurface::setColor(COLORREF color, double thickness) {
	txSetColor(color, thickness);
}

void DisplaySurface::selectFont(const char name[], double x, double y) {
	txSelectFont(name, x, y);
}

void DisplaySurface::textOut(int x, int y, const char text[]) {
	txTextOut(x + x0, y + y0, text);
}

void DisplaySurface::drawText(int x1, int y1, int x2, int y2, const char text[]) {
	txDrawText(x1 + x0, y1 + y0, x2 + x0, y2 + y0, text);
}

void DisplaySurface::drawText(int x1, int y1, int x2, int y2, const char text[], unsigned format) {
	txDrawText(x1 + x0, y1 + y0, x2 + x0, y2 + y0, text, format);
}

void DisplaySurface::circle(int x, int y, int r) {
    txCircle(x + x0, y + y0, r);
}

void DisplaySurface::clear () {
    txClear ();
}
void DisplaySurface::createWindow(int x, int y){
    txCreateWindow (x, y);
}

void DisplaySurface::begin (){
    txBegin ();
}
void DisplaySurface::end () {
    txEnd ();
}

void DisplaySurface::sleep (int ms) {
    txSleep (ms);
}

void DisplaySurface::polygon(POINT points[], int numPoints) {
    for (int i = 0; i < numPoints; ++i) {
        points[i].x += x0;
        points[i].y += y0;
    }
    txPolygon (points, numPoints);
}


void DisplaySurface::drawPartOfComopnent (BdComponent* comp, int x, int y, int width, int height) {
    if (comp == NULL) return;

    HDC memDC = txCreateCompatibleDC (width, height);
    Win32::SetBkMode    (memDC, TRANSPARENT);

    DisplaySurface display(0,0);
    Surface *memSurface = &display;

    HDC& curDC = txDC();
    HDC realDC = curDC;

    txLock();

    curDC = memDC;

    memSurface->setFillColor(TX_WHITE);
    txClear();

    memSurface->selectFont("Lucida Console", 60, 21);
    memSurface->translate(-x, -y);
    comp->draw(memSurface);
    memSurface->translate(x, y);

    bitBlt (realDC, 0, 0, width, height, curDC, 0, 0);

    curDC = realDC;

    txUnlock();
    txDeleteDC (memDC);
}
//---------------------------ROGOZA AGAIN---------------------------
SIZE DisplaySurface:: getTextExtent(const char text[]) {
    return txGetTextExtent(text);
}
int DisplaySurface:: getTextExtentX(const char text[]) {
    return txGetTextExtentX(text);
}
int DisplaySurface:: getTextExtentY(const char text[]) {
    return txGetTextExtentY(text);
}

bool DisplaySurface::bitBlt (HDC dest, double xDest, double yDest, double width, double height, HDC src, double xSrc, double ySrc) {
    txBitBlt (dest, xDest + x0, yDest + y0, width, height, src, xSrc, ySrc);
}
/*HDC DisplaySurface::loadImage (const char filename[])
{
    return txLoadImage (filename);
}*/
//--------------------------SURFACE-------------------------
void Surface::translate(int dx, int dy) {
    x0 += dx;
    y0 += dy;
}

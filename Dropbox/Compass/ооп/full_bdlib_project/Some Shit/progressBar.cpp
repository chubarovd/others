enum ProgressBarType{DETERMINATE, INDETERMINATE};
const int REFRESH_TIME = 25;
class ProgressBar: public BdComponent{
private:
    ProgressBarType type;
    int progress;
	double min;
	double max;
	int percentCount;
	bool performing;
	int roundX;
	int roundStep;
	int time;
public:
	ProgressBar(int x_, int y_, int width_, int height_);
	void draw(Surface* s);
	void process(TxEvent* event);
	void setType(ProgressBarType type);
	void setProgress(int p);
	int getProgress();
	void setMin(int min);
	int getMin();
	void setMax(int max);
	int getMax();
	void setPerforming(bool performing);
	bool getPerforming();
};


ProgressBar::ProgressBar(int x_, int y_, int width_, int height_) : BdComponent (x_, y_, width_, height_) {
    min = 0;
    max = 1;
    percentCount = 0;
    progress = 0;
    performing = false;
    roundStep = 5;
    time = GetTickCount();
}

void ProgressBar::draw(Surface* s){
	s->setColor(TX_BLACK);
	s->line(x, y, x + width, y);
	s->line(x, y, x, y + height);
	s->line(x + width, y, x + width, y + height);
	s->line(x, y + height, x + width, y + height);
	if (this->type == DETERMINATE) {
        s->setFillColor(TX_GREEN);
        s->rectangle(x, y, x + width * percentCount / 100, y + height);
        char str[10];
        sprintf(str, " %d %%", percentCount);
        s->setColor(TX_BLACK);
        s->drawText(x, y, x + width, y + height, str);
	}
	if ((this->type == INDETERMINATE) && (this->getPerforming() == true)) {
        if ((roundX + roundStep > x + width - height / 2) || (roundX + roundStep < x + height / 2)) {
            roundStep *= -1;
        }
        s->setFillColor(TX_GREEN);
        s->circle(roundX, y + height / 2, height / 2);
        if (GetTickCount() - time > REFRESH_TIME) {
            roundX += roundStep;
            time = GetTickCount();
        }
	}
	if ((this->type == INDETERMINATE) && (this->getPerforming() == false)) {
        roundX = x + height / 2;
	}
}

void ProgressBar::process(TxEvent* event) {
    if (this->type == DETERMINATE) {
        percentCount = ((progress - this->min) / (this->max - this->min)) * 100;
    }
}

void ProgressBar::setType(ProgressBarType type) {
    this->type = type;
}

void ProgressBar::setMin(int min) {
    this->min = min;
}

int ProgressBar::getMin() {
return(this->min);
}

void ProgressBar::setMax(int max) {
this->max = max;
}

int ProgressBar::getMax() {
    return(this->max);
}

void ProgressBar::setPerforming(bool performing) {
    this->performing = performing;
}

bool ProgressBar::getPerforming() {
    return(this->performing);
}

void ProgressBar::setProgress(int p) {
    if ((this->type == DETERMINATE) && (p >= min) && (p <= max)) {
        this->progress = p;
    }
}

int ProgressBar::getProgress() {
    return this->progress;
}

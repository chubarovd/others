#include "C:\Users\1\Dropbox\���� ����� ������\bdLib.cpp"
#include "C:\Users\1\Dropbox\���� ����� ������\BdListComponent.cpp"
#include "C:\Users\1\Dropbox\���� ����� ������\BdProgressBar.cpp"
#include "C:\Users\1\Dropbox\���� ����� ������\BdTimer.cpp"
#include "C:\Users\1\Dropbox\���� ����� ������\bdTabSet.cpp"
#include "C:\Users\1\Dropbox\���� ����� ������\BdRadioButton.cpp"

class Form_t : public BdComponentListener, BdListComponentListener, BdTimerListener, BdTabSetListener
{
public:
    BdWindow window;
    BdButton start;
    BdButton stop;
    BdLabel title;
    BdProgressBar progressbar;
    BdTimer timer;
    BdListItem item1;
    BdListItem item2;
    BdListModel model;
    BdListComponent list;

    BdRadioGroup group;
        BdRadioButton rbtField;
        BdRadioButton rbtFielda;

    BdContainer cont;
    BdContainer con;
    BdContainer conta;
    BdTabSet ts;

    void onClick (BdComponent* v, int x, int y);
    void onSelectionChanged (BdListComponent* l);
    void onTick(BdTimer* timer);
    void onTabChanged(BdTabSet* tabSet);

    void show ();

    Form_t ();
};

int main ()
{
    Form_t form;
    form.show();
    return 0;
}

void Form_t::show()
{
    window.show();
}

Form_t::Form_t () : window (1000, 650),
    start (25, 275, 150, 150, "START", true),
    stop (575, 275, 150, 150, "STOP", false),
    title (100, 25, 500, 100, "����� ��� ��������"),
    progressbar (25, 450, 700, 175),
    item1 ("�����������������"),
    item2 ("�������������������"),
    model (), list (25, 150, 700, 100, true),
    ts(0, 0, window.container.width, window.container.height), cont(0, 0, 1000, 600), con(0, 0, 1000, 600), conta(0, 0, 1000, 600),
    rbtFielda(650, 10, 150, 30, "button 1"), rbtField(650, 40, 150, 30, "button 2"), group()
{
    ts.addTab(&cont, "Container 1");
    ts.addTab(&con, "Container 2");
    ts.addTab(&conta, "Container 3");
    window.container.addChild(&ts);

    group.addRadBut(&rbtField);
    group.addRadBut(&rbtFielda);

    con.addChild(&rbtField);
    con.addChild(&rbtFielda);

    model.addItem (item1);
    model.addItem (item2);
    list.setModel(model);

    cont.addChild(&timer);
    cont.addChild(&start);
    cont.addChild(&stop);
    cont.addChild(&title);
    cont.addChild(&list);
    cont.addChild(&progressbar);
    start.addListener(this);
    stop.addListener(this);
    list.addListener(this);

    ts.addTabSetListener(this);

    timer.setPeriodMillis(100);
    timer.addListener(this);
    timer.stop();
    progressbar.setMin(0);
    progressbar.setMax(100);
}

void Form_t::onClick(BdComponent* v, int x, int y)
{
     if (v == &start)
    {
        progressbar.setPerforming(true);
        start.setEnabled(false);
        stop.setEnabled(true);
        list.setEnabled(false);
        timer.start();
    }
    if (v == &stop)
    {
        progressbar.setPerforming(false);
        start.setEnabled(true);
        stop.setEnabled(false);
        list.setEnabled(true);
        timer.stop();
    }
}

void Form_t::onSelectionChanged(BdListComponent* l)
{
    start.setEnabled(true);
    char str [100];
    int itemCount;
    itemCount = l -> getSelectedItem ();
    model.getTextItem(str, itemCount);
    title.setText (str);
    if (str[0] == '�')
    {
        progressbar.setType(INDETERMINATE);
    }
    else
    {
        progressbar.setType(DETERMINATE);
        progressbar.setProgress(progressbar.getMin());
    }
}

void Form_t::onTick(BdTimer* timer)
{
    progressbar.setProgress(progressbar.getProgress() + 1);
    if (progressbar.getProgress() == progressbar.getMax()) {
        timer->stop();
    }
}

void Form_t::onTabChanged(BdTabSet* tabSet) {
    if(tabSet->getSelectedTabIndex() == 1) {
        printf("big bang\n");
    }
}

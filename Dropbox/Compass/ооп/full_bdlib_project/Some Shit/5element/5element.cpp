//#ifdef OS_UNIX
    #include "C:\Users\dchubarov\Dropbox\���� ����� ������\BdLib.cpp"
    #include "C:\Users\dchubarov\Dropbox\���� ����� ������\BdListComponent.cpp"
    #include "C:\Users\dchubarov\Dropbox\���� ����� ������\BdProgressBar.cpp"
    #include "C:\Users\dchubarov\Dropbox\���� ����� ������\BdTimer.cpp"
    #include "C:\Users\dchubarov\Dropbox\���� ����� ������\BdTextField.cpp"
//#else
////    #include "C:\Users\1\Dropbox\���� ����� ������\BdLib.cpp"
////    #include "C:\Users\1\Dropbox\���� ����� ������\BdListComponent.cpp"
////    #include "C:\Users\1\Dropbox\���� ����� ������\BdProgressBar.cpp"
////    #include "C:\Users\1\Dropbox\���� ����� ������\BdTimer.cpp"
////    #include "C:\Users\1\Dropbox\���� ����� ������\BdTextField.cpp"
//#endif

class Game;

class FormRound : public BdComponentListener, BdTimerListener {
private:
    char word[50];
    char word1[50];
    char word2[50];

    char nameFirst[100];
    char nameSecond[100];

    int raund_count = 1;
    char raund_numb[100];

    bool win_or_no_1, win_or_no_2;
    bool clik1 = false;
    bool clik2 = false;
    bool action1 = false;
    bool timer_end = false;
    int number;

    Game* game;
public:
    BdWindow window;

    BdProgressBar time;
    BdTimer timer;

    BdButton first_1;
    BdButton first_2;
    BdButton first_3;
    BdButton second_1;
    BdButton second_2;
    BdButton second_3;
    BdButton again;

    BdLabel name_1;
    BdLabel name_2;
    BdLabel letters_1;
    BdLabel letters_2;
    BdLabel raund;
    BdLabel winner;

    void onClick (BdComponent* v, int x, int y);
    void onTick(BdTimer* timer);

    void prepare(char prepare_word[], char prepare_name1[], char prepare_name2[]);
    void next();
    void setLetter(int player_number);

    FormRound();
};

class FormStart : public BdComponentListener {
private:
    char word[50];
    char array1[50];
    char array2[50];
public:
    BdWindow window;
    FormRound fr;

    BdLabel title;
    BdLabel word_label;
    BdLabel first_name;
    BdLabel second_name;

    BdButton start;

    BdTextField first_text;
    BdTextField second_text;
    BdTextField word_field;

    void onClick (BdComponent* v, int x, int y);

    FormStart();
};

class Game {
protected:
    FormStart fs;
    BdWindow window;
public:
    void run() {
        fs.window.show();
    }

    Game();
};

int main() {
    srand(time(NULL));
    Game gama;
    gama.run();
}

//--------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------
//----------------------Game-----------------------Game-----------------------Game------------------------
//--------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------

Game::Game() : window(500, 300) {

}

//--------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------
//-------------------FormStart-------------------FormStart-------------------FormStart--------------------
//--------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------

FormStart::FormStart () : window(500, 300),
                        title(200, 10, 280, 20, "Welcome to  5.element!"), first_name(10, 70, 140, 25, "������ �����:"), second_name(10, 145, 140, 25, "������ ������:"),
                        word_label(10, 220, 140, 25, "�����:"),
                        first_text(160, 50, 140), second_text(160, 125, 140), word_field(160, 200, 230),
                        start(325, 125, 140, 60, "�����!", true)
                        {
    window.container.addChild(&title);
    window.container.addChild(&first_name);
    window.container.addChild(&second_name);
    window.container.addChild(&word_label);
    window.container.addChild(&first_text);
    window.container.addChild(&second_text);
    window.container.addChild(&word_field);
    window.container.addChild(&start);

    start.addListener(this);
}

void FormStart::onClick(BdComponent* v, int x, int y) {
    if (v == &start && first_text.getSymbolCount() > 0 && second_text.getSymbolCount() > 0 && word_field.getSymbolCount() > 0) {
        first_text.getText(array1);
        second_text.getText(array2);
        word_field.getText(word);

        window.container.removeChild(&title);
        window.container.removeChild(&first_name);
        window.container.removeChild(&second_name);
        window.container.removeChild(&word_label);
        window.container.removeChild(&first_text);
        window.container.removeChild(&second_text);
        window.container.removeChild(&word_field);
        window.container.removeChild(&start);

        start.removeListener(this);

        fr.prepare(word, array1, array2);
        fr.window.show();
    }
}

//--------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------
//-------------------FormRound-------------------FormRound-------------------FormRound--------------------
//--------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------

FormRound::FormRound() : window(500, 300), raund(10, 10, 80, 20, "����� 1"),
                        name_1(10, 70, 150, 50, "first"), name_2(10, 130, 150, 50, "second"),
                        first_1(180, 80, 30, 30, "1", true), first_2(220, 80, 30, 30, "2", true), first_3(260, 80, 30, 30, "3", true),
                        second_1(180, 140, 30, 30, "1", false), second_2(220, 140, 30, 30, "2", false), second_3(260, 140, 30, 30, "3", false),
                        time(70, 200, 360, 40), letters_1(330, 80, 80, 20, word1), letters_2(330, 140, 80, 20, word2),
                        winner(110, 230, 280, 20, ""), again(375, 250, 115, 45, "������", true) {
    number = rand() % 3 + 1;
    window.container.addChild(&raund);
    window.container.addChild(&name_1);
    window.container.addChild(&name_2);
    window.container.addChild(&first_1);
    window.container.addChild(&first_2);
    window.container.addChild(&first_3);
    window.container.addChild(&second_1);
    window.container.addChild(&second_2);
    window.container.addChild(&second_3);
    window.container.addChild(&timer);
    window.container.addChild(&time);

    first_1.addListener(this);
    first_2.addListener(this);
    first_3.addListener(this);
    second_1.addListener(this);
    second_2.addListener(this);
    second_3.addListener(this);

    timer.setPeriodMillis(25);
    timer.addListener(this);

    time.setType(DETERMINATE);
    time.setMin(0);
    time.setMax(100);
    time.setProgress(time.getMin());
    time.setPerforming(true);

    timer.start();
}

void FormRound::onClick(BdComponent* v, int x, int y) {
    if(timer_end == false) {
        if(v == &first_1) {
            clik1 = true;
            if(number == 1) {
                win_or_no_1 = true;
            } else {
                win_or_no_1 = false;
            }
            first_1.setEnabled(false);
            first_2.setEnabled(false);
            first_3.setEnabled(false);
            time.setProgress(time.getMin());
            action1 = true;
        }
        if(v == &first_2) {
            clik1 = true;
            if(number == 2) {
                win_or_no_1 = true;
            } else {
                win_or_no_1 = false;
            }
            first_1.setEnabled(false);
            first_2.setEnabled(false);
            first_3.setEnabled(false);
            time.setProgress(time.getMin());
            action1 = true;
        }
        if(v == &first_3) {
            clik1 = true;
            if(number == 3) {
                win_or_no_1 = true;
            } else {
                win_or_no_1 = false;
            }
            first_1.setEnabled(false);
            first_2.setEnabled(false);
            first_3.setEnabled(false);
            time.setProgress(time.getMin());
            action1 = true;
        }
    }
    if(timer_end == false && action1 == true) {
        second_1.setEnabled(true);
        second_2.setEnabled(true);
        second_3.setEnabled(true);
        if(v == &second_1) {
            clik2 = true;
            if(number == 1) {
                win_or_no_2 = true;
            } else {
                win_or_no_2 = false;
            }
            second_1.setEnabled(false);
            second_2.setEnabled(false);
            second_3.setEnabled(false);
            window.container.removeChild(&time);
        }
        if(v == &second_2) {
            clik2 = true;
            if(number == 2) {
                win_or_no_2 = true;
            } else {
                win_or_no_2 = false;
            }
            second_1.setEnabled(false);
            second_2.setEnabled(false);
            second_3.setEnabled(false);
            window.container.removeChild(&time);
        }
        if(v == &second_3) {
            clik2 = true;
            if(number == 3) {
                win_or_no_2 = true;
            } else {
                win_or_no_2 = false;
            }
            second_1.setEnabled(false);
            second_2.setEnabled(false);
            second_3.setEnabled(false);
            window.container.removeChild(&time);
        }
    }
    if(clik1 == true && clik2 == true) {
        next();
    }
    if(v == &again) {
        again.removeListener(this);
        window.container.removeChild(&again);
        Game game;
        game.run();
    }
}

void FormRound::onTick(BdTimer* timer) {
    time.setProgress(time.getProgress() + 1);
    if (time.getProgress() == time.getMax()) {
        timer->stop();
        if(clik1 == true) {
            win_or_no_2 = true;
            win_or_no_2 = false;
        } else {
            win_or_no_1 = false;
            win_or_no_2 = true;
        }
        second_1.setEnabled(false);
        second_2.setEnabled(false);
        second_3.setEnabled(false);
        window.container.removeChild(&time);
        next();
    } else {
        timer_end = false;
    }
}

void FormRound::prepare(char prepare_word[], char prepare_name1[], char prepare_name2[]) {
    strcpy(word, prepare_word);
    strcpy(nameFirst, prepare_name1);
    strcpy(nameSecond, prepare_name2);
    name_1.setText(nameFirst);
    name_2.setText(nameSecond);

}

void FormRound::setLetter(int player_number) {
    if(player_number == 1) {
        word1[strlen(word1)] = word[strlen(word1)];
    } else {
        word2[strlen(word2)] = word[strlen(word2)];
    }
}

void FormRound::next() {
    if(win_or_no_1 == true && win_or_no_2 == true) {

    } else if(win_or_no_1 == false && win_or_no_2 == true){
        setLetter(1);
    } else if(win_or_no_1 == true && win_or_no_2 == false) {
        setLetter(2);
    } else if(win_or_no_1 == false && win_or_no_2 == false) {
        setLetter(1);
        setLetter(2);
    }
    letters_1.setText(word1);
    letters_2.setText(word2);
    window.container.addChild(&letters_1);
    window.container.addChild(&letters_2);

    number = rand() % 3 + 1;

    char loser[100];

    if(strlen(word1) == strlen(word)) {
        sprintf(loser, "%s lose", nameFirst);
        winner.setText(loser);
    }
    if(strlen(word2) == strlen(word)) {
        sprintf(loser, "%s lose", nameSecond);
        winner.setText(loser);
    }
    if((strlen(word1) == strlen(word)) && (strlen(word2) == strlen(word))) {
        winner.setText("big draw");
    }

    if((strlen(word1) < strlen(word)) && (strlen(word2) < strlen(word))) {
        raund_count++;
        sprintf(raund_numb, "����� %d", raund_count);
        raund.setText(raund_numb);
        printf("%s\n", raund_numb);

        window.container.addChild(&time);
        first_1.setEnabled(true);
        first_2.setEnabled(true);
        first_3.setEnabled(true);

        time.setProgress(time.getMin());
        time.setPerforming(true);
        timer.start();

        clik1 = false;
        clik2 = false;
        win_or_no_1 = false;
        win_or_no_2 = false;

        window.show();
    } else {
        window.container.addChild(&winner);
        window.container.addChild(&again);
        again.addListener(this);
        first_1.setEnabled(false);
        first_2.setEnabled(false);
        first_3.setEnabled(false);
        second_1.setEnabled(false);
        second_2.setEnabled(false);
        second_3.setEnabled(false);
        timer.removeListener(this);
    }
}


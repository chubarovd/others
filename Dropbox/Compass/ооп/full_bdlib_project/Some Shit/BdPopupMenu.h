#ifndef BDPOPUPMENU_H
#define BDPOPUPMENU_H

#include "BdList.h"
#include "BdLib.h"

struct BdPopup;
struct BdPopupItem;

struct BdPopupItem : BdComponent {
private:
    static const int MAXSIZE = 100;
    char ItemText[MAXSIZE];
    BdList<BdPopup> tabs;
public:
    BdPopupItem(int _x, int _y, int _width, int _height, char* _ItemText);
    BdPopupItem(char* _ItemText);

    int getTabsSize();
    char* getText();
    void addTab(BdPopup tab);

    virtual void draw(Surface* s);
    virtual void process(TxEvent* event);
};

struct BdPopup : BdComponent {
private:
    BdList<BdPopupItem> items;
public:
    BdPopup(int _x, int _y, int _weigth, _height);

    int getItemsSize();
    void addItem(BdPopupItem);

    virtual void draw(Surface* s);
    virtual void process(TxEvent* event);
};

#endif // BDPOPUPMENU_H

#include "BdText.h"
//------------------TEXT FIELD------------------------------------------
BdTextField:: BdTextField(int x_, int y_, int width_) : BdComponent (x_, y_, width_, FIELD_HEIGHT) {
	text[0] = 0;
	temp[0] = 0;
	highlight[0] = 0;
    curs.x = x;
    curs.y = y;
	backGroundColor = TX_WHITE;
	lineColor = TX_BLACK;
	highlightColor = TX_GRAY;
	inactiveLineColor = TX_GRAY;

	active = false;
	controlPressed = false;
	highlighting = false;
	highlightingEnded = false;
	symbolCount = 0;
	pos = 0;
	int pos0 = 0;
}
void BdTextField:: getText(char str[]) {
    strcpy(str, text);
}
void BdTextField:: setText(char str[]) {
    symbolCount = 0;
    int i = 0;
    while (!(str[symbolCount] == 0)) {
        text[symbolCount] = str[symbolCount];
        temp[symbolCount] = text[symbolCount];
        symbolCount++;
    }
    symbolCount++;
    pos = symbolCount;
}
int BdTextField:: getSymbolCount() {
    return symbolCount;
}
bool BdTextField:: isLetter(TxEvent* event) {
    return !((event->code == VK_BACK) || (event->code == VK_LEFT) || (event->code == VK_RIGHT) || (event->code == VK_DELETE) || (event->code == VK_CONTROL));
}
void BdTextField:: addTextListener(BdTextListener* listener) {
    listeners.add(listener);
}

void BdTextField:: typeSymbol(TxEvent* event) {
    for(int i = symbolCount; i > pos; i--) {
			text[i] = text[i - 1];
        }
    text[pos] = char(event->code);
    text[symbolCount + 1] = 0;
    //temp[pos] = text[pos];
    //temp[pos + 1] = 0;
    symbolCount++;
    pos++;

    for(int i = 0; i < listeners.getSize(); i++) {
        listeners.getValue(i)->textInserted(this, pos - 1, 1);
    }
}

void BdTextField:: deleteSymbol(TxEvent* event) {
    for(int i = pos - 1; i < symbolCount; i++) {
                        text[i] = text[i + 1];
                        temp[i] = text[i];
                    }
    text[symbolCount - 1] = 0;
    //temp[pos] = 0;
    symbolCount--;
    pos--;

    for(int i = 0; i < listeners.getSize(); i++) {
        listeners.getValue(i)->textDeleted(this, pos + 1, 1);
    }

//    printf("pos = %d\n", pos);
}

bool BdTextField:: onClick(TxEvent* event) {
	return (event->pos.x > x) && (event->pos.x < x + width) && (y < event->pos.y) && (event->pos.y < y + height) && (event->type == TxEvent::TX_MOUSE_PRESS);
}

void BdTextField:: getHighlight() {
    if (pos > pos0) {
            for(int i = 0; i < pos - pos0; i++) {
                highlight[i] = text[pos0 + i];
            }
            highlight[pos - pos0 + 1] = 0;
    } else {
            for(int i = 0; i < pos0 - pos; i++) {
                highlight[i] = text[pos + i];
            }
            highlight[pos0 - pos + 1] = 0;
    }
}

void BdTextField:: deleteHighlight() {
    if (pos0 < pos) {
        for(int i = pos0; i < symbolCount - pos + pos0; i++) {
            text[i] = text[i + pos - pos0];
        }
        symbolCount = symbolCount - pos + pos0;
        text[symbolCount] = 0;
        for(int i = 0; i < listeners.getSize(); i++) {
            listeners.getValue(i)->textDeleted(this, pos0, pos - pos0);
        }
        pos = pos0;
    } else {
        for(int i = pos; i < symbolCount - pos0 + pos; i++) {
            text[i] = text[i + pos0 - pos];
        }
        symbolCount = symbolCount - pos0 + pos;
        text[symbolCount] = 0;
        for(int i = 0; i < listeners.getSize(); i++) {
            listeners.getValue(i)->textDeleted(this, pos, pos0 - pos);
        }
    }
    highlight[0] = 0;
    highlightingEnded = false;
}

void BdTextField:: getCursorPos() {
    pos = 0;
    char temp[50] = "";
    while((getWindow()->surface->getTextExtentX(temp) < txMouseX() - x) && (pos < symbolCount)) {
        pos++;
        temp[pos] = text[pos];
    }
}


void BdTextField:: drawCursor(Surface* s) {
    for (int i = 0; i < pos; i++) {
        temp[i] = text[i];
    }
    temp[pos] = 0;
    curs.h = getWindow()->surface->getTextExtentY("n") - 4;
    curs.x = getWindow()->surface->getTextExtentX(temp);
    s->line(curs.x, 0, curs.x, curs.h);
}

void BdTextField :: draw(Surface* s) {
    s->setFillColor(backGroundColor);
    height = getWindow()->surface->getTextExtentY("n") + 4;
    if(this->getWindow()->getFocusOwner() == this) {
        s->setColor(lineColor);
        s->rectangle(-3, -3, -3 + width, -3 + height);
        s->textOut(0, 0, text);
        drawCursor(s);
	} else {
        s->setColor(inactiveLineColor);
        s->rectangle(-3, -3, -3 + width, -3 + height);
        s->textOut(0, 0, text);
	}
	if (highlighting || highlightingEnded) {
        s->setFillColor(highlightColor);
        char temp[MAX_SIZE];

        for(int i = 0; i < pos0; i++) {
            temp[i] = text[i];
        }
        int highlightBeginning = getWindow()->surface->getTextExtentX(temp);

        for(int i = 0; i < pos; i++) {
            temp[i] = text[i];
        }
        int highlightEnding = getWindow()->surface->getTextExtentX(temp);

        s->rectangle(highlightBeginning, -3, highlightEnding, -3 + height);
	}
}

void BdTextField :: process(TxEvent* event) {
    if(this->getWindow()->getFocusOwner() == this) {
        if (event->type == TxEvent::TX_MOUSE_PRESS) {
            //printf("TX_MOUSE_PRESS\n");
            mousePressed = true;
            highlightingEnded = false;
            getCursorPos();
            if(!highlighting) {
                highlighting = true;
                pos0 = pos;
                //printf("highlihhting started \n mouse pressed \n pos0 = %d\n", pos0);
            }
        }

        if(event->type == TxEvent::TX_MOUSE_RELEASE) {
            //printf("TX_MOUSE_RELEASE\n");
            mousePressed = false;
            highlighting = false;
            highlightingEnded = true;
            //printf("highlighting ended \n mouse released \n");
        }

        if (mousePressed && highlighting) {
            getCursorPos();
            getHighlight();
        }

        if ((event->type == TxEvent:: TX_KEY_PRESS) && (event->code == VK_CONTROL)) {
            controlPressed = true;
            //printf("control \n");
        }

        if ((event->type == TxEvent:: TX_KEY_RELEASE) && (event->code == VK_CONTROL)) {
            controlPressed = false;
        }

        if(event->type == TxEvent::TX_KEY_PRESS) {
            //printf("TX_KEY_PRESS\n");
            if(event->code == VK_BACK) {
                if(highlightingEnded) {
                    deleteHighlight();
                } else if(pos > 0){
                    deleteSymbol(event);
                }
            }
            if(event->code == VK_DELETE) {
                if(highlightingEnded) {
                    deleteHighlight();
                } else if(pos < symbolCount) {
                    pos++;
                    deleteSymbol(event);
                }
            }
            if((event->code == VK_LEFT) && (pos > 0)) {
                //temp[pos] = 0;
                pos--;
                highlightingEnded = false;
//                printf("pos = %d\n", pos);
            }
            if((event->code == VK_RIGHT) && (pos < symbolCount)) {
                //temp[pos] = text[pos];
                //temp[pos + 1] = 0;
                pos++;
                highlightingEnded = false;
//                printf("pos = %d\n", pos);
            }
        }
        if (event->type == TxEvent::TX_CHAR && symbolCount < MAX_SIZE && !(event->code == VK_BACK) && !controlPressed) {
            text[symbolCount] = ' ';
            text[symbolCount + 1] = 0;
            if(getWindow()->surface->getTextExtentX(text) < width) {
                if(highlightingEnded) {
                    deleteHighlight();
                }
                highlighting = false;
                highlightingEnded = false;
                typeSymbol(event);
            }
        }
    }
}

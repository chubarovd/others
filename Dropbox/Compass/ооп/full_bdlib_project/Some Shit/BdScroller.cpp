#include "BdScroller.h"

//-------------------------BdScrollSlider-----------------------
BdScrollSlider::BdScrollSlider () {
    mouseStatus = None;
}

BdScrollSlider::BdScrollSlider (int _x, int _y, int _width, int _height, double _min, double _max, Type _type, BdScrollView *_innerScrollView) : BdComponent(_x, _y, _width, _height) {
    innerScrollView = _innerScrollView;
//
    sliderArgument = 20;
    buttonScroll = 50;
//
    type = _type;
    minCount = _min;
    Count = _min;
    maxCount = _max;


    if (_type == Horisontal) {
        sliderSize = (_width * (_width - 2 * sliderArgument) / (_width + _max));
        if (sliderSize > width - 2 * sliderArgument) {
            sliderSize = width - 2 * sliderArgument;
        }
    }
    else if (_type == Vertical) {
        sliderSize = (_height * (_height - 2 * sliderArgument) / (_height + _max));
        if (sliderSize > height - 2 * sliderArgument) {
            sliderSize = height - 2 * sliderArgument;
        }
    }
    if (sliderSize < 10) {
        sliderSize = 10;
    }
    mouseStatus = None;
}

void BdScrollSlider::draw(Surface *surface) {
    if (type == Horisontal) {
        double useWidth = width - sliderSize - 2 * sliderArgument;
        //scroll//
        surface->setFillColor(RGB(180, 180, 180));
        surface->setColor(RGB(180, 180, 180));
        surface->rectangle(sliderArgument, 0, width - sliderArgument, height);
        if (mouseStatus == scrollOver) {
            surface->setFillColor(RGB(100, 100, 100));
            surface->setColor(RGB(100, 100, 100));
        }
        else if (mouseStatus == scrollClick) {
            surface->setFillColor(RGB(60, 60, 60));
            surface->setColor(RGB(60, 60, 60));
        }
        else {
            surface->setFillColor(RGB(140, 140, 140));
            surface->setColor(RGB(140, 140, 140));
        }
        surface->rectangle(sliderArgument + (useWidth * (Count - minCount)) / (maxCount - minCount), 0,
                           sliderArgument + (useWidth * (Count - minCount)) / (maxCount - minCount) + sliderSize, height);


        //backButton//
        if (mouseStatus == backButtonOver) {
            surface->setFillColor(RGB(120, 120, 120));
            surface->setColor(RGB(120, 120, 120));
            surface->rectangle(0, 0, sliderArgument, sliderArgument);
            surface->setFillColor(TX_BLACK);
            surface->setColor(TX_BLACK);
            POINT p[6] = {{sliderArgument / 4, sliderArgument / 2},
                          {sliderArgument / 2, sliderArgument / 4},
                          {3 * sliderArgument / 4, sliderArgument / 4},
                          {sliderArgument / 2, sliderArgument / 2},
                          {3 * sliderArgument / 4, 3 * sliderArgument / 4},
                          {sliderArgument / 2, 3 * sliderArgument / 4}};
            surface->polygon(p, 6);
        }
        else if (mouseStatus == backButtonClick) {
            surface->setFillColor(RGB(60, 60, 60));
            surface->setColor(RGB(60, 60, 60));
            surface->rectangle(0, 0, sliderArgument, sliderArgument);
            surface->setFillColor(TX_GRAY);
            surface->setColor(TX_GRAY);
            POINT p[6] = {{sliderArgument / 4, sliderArgument / 2},
                          {sliderArgument / 2, sliderArgument / 4},
                          {3 * sliderArgument / 4, sliderArgument / 4},
                          {sliderArgument / 2, sliderArgument / 2},
                          {3 * sliderArgument / 4, 3 * sliderArgument / 4},
                          {sliderArgument / 2, 3 * sliderArgument / 4}};
            surface->polygon(p, 6);
        }
        else {
            surface->setFillColor(RGB(180, 180, 180));
            surface->setColor(RGB(180, 180, 180));
            surface->rectangle(0, 0, sliderArgument, sliderArgument);
            surface->setFillColor(TX_BLACK);
            surface->setColor(TX_BLACK);
            POINT p[6] = {{sliderArgument / 4, sliderArgument / 2},
                          {sliderArgument / 2, sliderArgument / 4},
                          {3 * sliderArgument / 4, sliderArgument / 4},
                          {sliderArgument / 2, sliderArgument / 2},
                          {3 * sliderArgument / 4, 3 * sliderArgument / 4},
                          {sliderArgument / 2, 3 * sliderArgument / 4}};
            surface->polygon(p, 6);
        }

        //nextButton//

        if (mouseStatus == nextButtonOver) {
            surface->setFillColor(RGB(120, 120, 120));
            surface->setColor(RGB(120, 120, 120));
            surface->rectangle(width - sliderArgument, height - sliderArgument, width, height);
            surface->setFillColor(TX_BLACK);
            surface->setColor(TX_BLACK);
            POINT p[6] = {{width - sliderArgument + 3 * sliderArgument / 4, height - sliderArgument + sliderArgument / 2},
                          {width - sliderArgument + sliderArgument / 2,     height - sliderArgument + sliderArgument / 4},
                          {width - sliderArgument + sliderArgument / 4,     height - sliderArgument + sliderArgument / 4},
                          {width - sliderArgument + sliderArgument / 2,     height - sliderArgument + sliderArgument / 2},
                          {width - sliderArgument + sliderArgument / 4,     height - sliderArgument + 3 * sliderArgument / 4},
                          {width - sliderArgument + sliderArgument / 2, height - sliderArgument + 3 * sliderArgument / 4}};
            surface->polygon(p, 6);
        }
        else if (mouseStatus == nextButtonClick) {
            surface->setFillColor(RGB(60, 60, 60));
            surface->setColor(RGB(60, 60, 60));
            surface->rectangle(width - sliderArgument, height - sliderArgument, width, height);
            surface->setFillColor(TX_GRAY);
            surface->setColor(TX_GRAY);
            POINT p[6] = {{width - sliderArgument + 3 * sliderArgument / 4, height - sliderArgument + sliderArgument / 2},
                          {width - sliderArgument + sliderArgument / 2,     height - sliderArgument + sliderArgument / 4},
                          {width - sliderArgument + sliderArgument / 4,     height - sliderArgument + sliderArgument / 4},
                          {width - sliderArgument + sliderArgument / 2,     height - sliderArgument + sliderArgument / 2},
                          {width - sliderArgument + sliderArgument / 4,     height - sliderArgument + 3 * sliderArgument / 4},
                          {width - sliderArgument + sliderArgument / 2, height - sliderArgument + 3 * sliderArgument / 4}};
            surface->polygon(p, 6);
        }
        else {
            surface->setFillColor(RGB(180, 180, 180));
            surface->setColor(RGB(180, 180, 180));
            surface->rectangle(width - sliderArgument, height - sliderArgument, width, height);
            surface->setFillColor(TX_BLACK);
            surface->setColor(TX_BLACK);
            POINT p[6] = {{width - sliderArgument + 3 * sliderArgument / 4, height - sliderArgument + sliderArgument / 2},
                          {width - sliderArgument + sliderArgument / 2,     height - sliderArgument + sliderArgument / 4},
                          {width - sliderArgument + sliderArgument / 4,     height - sliderArgument + sliderArgument / 4},
                          {width - sliderArgument + sliderArgument / 2,     height - sliderArgument + sliderArgument / 2},
                          {width - sliderArgument + sliderArgument / 4,     height - sliderArgument + 3 * sliderArgument / 4},
                          {width - sliderArgument + sliderArgument / 2, height - sliderArgument + 3 * sliderArgument / 4}};
            surface->polygon(p, 6);
        }


    }
    else if (type == Vertical) {
        double useHeight = height - sliderSize - 2 * sliderArgument;

        surface->setFillColor(RGB(180, 180, 180));
        surface->setColor(RGB(180, 180, 180));
        surface->rectangle(0, sliderArgument, width, height - sliderArgument);

        if (mouseStatus == scrollOver) {
            surface->setFillColor(RGB(100, 100, 100));
            surface->setColor(RGB(100, 100, 100));
        }
        else if (mouseStatus == scrollClick) {
            surface->setFillColor(RGB(60, 60, 60));
            surface->setColor(RGB(60, 60, 60));
        }
        else {
            surface->setFillColor(RGB(140, 140, 140));
            surface->setColor(RGB(140, 140, 140));
        }
        surface->rectangle(0, sliderArgument + (useHeight * (Count - minCount)) / (maxCount - minCount),
                           width, sliderArgument + (useHeight * (Count - minCount)) / (maxCount - minCount) + sliderSize);

        //backButton//
        if (mouseStatus == backButtonOver) {
            surface->setFillColor(RGB(120, 120, 120));
            surface->setColor(RGB(120, 120, 120));
            surface->rectangle(0, 0, sliderArgument, sliderArgument);

            surface->setFillColor(TX_BLACK);
            surface->setColor(TX_BLACK);
            POINT p[6] = {{sliderArgument / 2,     sliderArgument / 4},
                          {3 * sliderArgument / 4, sliderArgument / 2},
                          {3 * sliderArgument / 4, 3 * sliderArgument / 4},
                          {sliderArgument / 2,     sliderArgument / 2},
                          {sliderArgument / 4,     3 * sliderArgument / 4},
                          {sliderArgument / 4,     sliderArgument / 2}};
            surface->polygon(p, 6);
        }
        else if (mouseStatus == backButtonClick) {
            surface->setFillColor(RGB(60, 60, 60));
            surface->setColor(RGB(60, 60, 60));
            surface->rectangle(0, 0, sliderArgument, sliderArgument);

            surface->setFillColor(TX_GRAY);
            surface->setColor(TX_GRAY);
            POINT p[6] = {{sliderArgument / 2,     sliderArgument / 4},
                          {3 * sliderArgument / 4, sliderArgument / 2},
                          {3 * sliderArgument / 4, 3 * sliderArgument / 4},
                          {sliderArgument / 2,     sliderArgument / 2},
                          {sliderArgument / 4,     3 * sliderArgument / 4},
                          {sliderArgument / 4,     sliderArgument / 2}};
            surface->polygon(p, 6);
        }
        else {
            surface->setFillColor(RGB(180, 180, 180));
            surface->setColor(RGB(180, 180, 180));
            surface->rectangle(0, 0, sliderArgument, sliderArgument);

            surface->setFillColor(TX_BLACK);
            surface->setColor(TX_BLACK);
            POINT p[6] = {{sliderArgument / 2,     sliderArgument / 4},
                          {3 * sliderArgument / 4, sliderArgument / 2},
                          {3 * sliderArgument / 4, 3 * sliderArgument / 4},
                          {sliderArgument / 2,     sliderArgument / 2},
                          {sliderArgument / 4,     3 * sliderArgument / 4},
                          {sliderArgument / 4,     sliderArgument / 2}};
            surface->polygon(p, 6);
        }

        //nextButton//

        if (mouseStatus == nextButtonOver) {
            surface->setFillColor(RGB(120, 120, 120));
            surface->setColor(RGB(120, 120, 120));
            surface->rectangle(width - sliderArgument, height - sliderArgument, width, height);

            surface->setFillColor(TX_BLACK);
            surface->setColor(TX_BLACK);
            POINT p[6] = {{width - sliderArgument + sliderArgument / 2,     height - sliderArgument + 3 * sliderArgument / 4},
                          {width - sliderArgument + sliderArgument / 4,     height - sliderArgument + sliderArgument / 2},
                          {width - sliderArgument + sliderArgument / 4,     height - sliderArgument + sliderArgument / 4},
                          {width - sliderArgument + sliderArgument / 2,     height - sliderArgument + sliderArgument / 2},
                          {width - sliderArgument + 3 * sliderArgument / 4, height - sliderArgument + sliderArgument / 4},
                          {width - sliderArgument + 3 * sliderArgument / 4, height - sliderArgument + sliderArgument / 2}};
            surface->polygon(p, 6);
        }
        else if (mouseStatus == nextButtonClick) {
            surface->setFillColor(RGB(60, 60, 60));
            surface->setColor(RGB(60, 60, 60));
            surface->rectangle(width - sliderArgument, height - sliderArgument, width, height);

            surface->setFillColor(TX_GRAY);
            surface->setColor(TX_GRAY);
            POINT p[6] = {{width - sliderArgument + sliderArgument / 2,     height - sliderArgument + 3 * sliderArgument / 4},
                          {width - sliderArgument + sliderArgument / 4,     height - sliderArgument + sliderArgument / 2},
                          {width - sliderArgument + sliderArgument / 4,     height - sliderArgument + sliderArgument / 4},
                          {width - sliderArgument + sliderArgument / 2,     height - sliderArgument + sliderArgument / 2},
                          {width - sliderArgument + 3 * sliderArgument / 4, height - sliderArgument + sliderArgument / 4},
                          {width - sliderArgument + 3 * sliderArgument / 4, height - sliderArgument + sliderArgument / 2}};
            surface->polygon(p, 6);
        }
        else {
            surface->setFillColor(RGB(180, 180, 180));
            surface->setColor(RGB(180, 180, 180));
            surface->rectangle(width - sliderArgument, height - sliderArgument, width, height);

            surface->setFillColor(TX_BLACK);
            surface->setColor(TX_BLACK);
            POINT p[6] = {{width - sliderArgument + sliderArgument / 2,     height - sliderArgument + 3 * sliderArgument / 4},
                          {width - sliderArgument + sliderArgument / 4,     height - sliderArgument + sliderArgument / 2},
                          {width - sliderArgument + sliderArgument / 4,     height - sliderArgument + sliderArgument / 4},
                          {width - sliderArgument + sliderArgument / 2,     height - sliderArgument + sliderArgument / 2},
                          {width - sliderArgument + 3 * sliderArgument / 4, height - sliderArgument + sliderArgument / 4},
                          {width - sliderArgument + 3 * sliderArgument / 4, height - sliderArgument + sliderArgument / 2}};
            surface->polygon(p, 6);
        }

    }

}

void BdScrollSlider::process(TxEvent *event) {
    this->refresh(innerScrollView->innerComponent);
    if (this->isEnabled()) {
        RECT scrollRect;
        RECT nextButtonRect;
        RECT backButtonRect;

        if (type == Horisontal) {
            double useWidth = width - sliderSize -  2 * sliderArgument;
            if (mouseStatus == nextButtonClick) {
                int tmp = Count + (((buttonScroll * useWidth) / (maxCount - minCount)) * (maxCount - minCount) - minCount * useWidth) / useWidth;
                Count = (maxCount < tmp) ? maxCount : tmp;
            }
            else if (mouseStatus == backButtonClick) {
                int tmp = Count - (((buttonScroll * useWidth) / (maxCount - minCount)) * (maxCount - minCount) - minCount * useWidth) / useWidth;
                Count = (minCount > tmp) ? minCount : tmp;
            }
            RECT rect = {sliderArgument + x, y - 60, x + width - sliderArgument, y + height + 60};
            if (In(event->pos, rect)) {
                if (mouseStatus == scrollClick) {
                    if (event->code != 1) {
                        mouseStatus = None;
                    }
                    else {
                        double dx = event->pos.x - mouseClickX;
                        Count += (dx * (maxCount - minCount) - minCount * useWidth) / useWidth;
                        mouseClickX = event->pos.x;
                        if (minCount > Count) {
                            mouseClickX += (useWidth * (minCount - Count + minCount)) / (maxCount - minCount);
                            Count = minCount;
                        }
                        else if (maxCount < Count) {
                            mouseClickX -= (useWidth * (Count - maxCount + minCount)) / (maxCount - minCount);
                            Count = maxCount;
                        }
                    }
                }
            }

            scrollRect = {sliderArgument + x + (useWidth * (Count - minCount)) / (maxCount - minCount), y,
                          sliderArgument + x + (useWidth * (Count - minCount)) / (maxCount - minCount) + sliderSize, y + height};


            innerScrollView->setStartX(Count);

        }
        else {
            RECT mouseRc = {0, 0, x + width, y + height};
            if (In(event->pos, mouseRc)) {
                switch (event->type) {
                    case TxEvent::TX_MOUSE_WHEEL: {
                        if (event->code > 0) {
                            mouseStatus = backButtonClick;
                        }
                        else {
                            mouseStatus = nextButtonClick;
                        }
                    } break;;
                }
            }


            double useHeight = height - sliderSize - 2 * sliderArgument;
            RECT rect = {x - 60, sliderArgument + y, x + width + 60, y + height - sliderArgument};
            if (mouseStatus == nextButtonClick) {
                int tmp = Count + (((buttonScroll * useHeight) / (maxCount - minCount)) * (maxCount - minCount) - minCount * useHeight) / useHeight;
                Count = (maxCount < tmp) ? maxCount : tmp;
            }
            else if (mouseStatus == backButtonClick) {
                int tmp = Count - (((buttonScroll * useHeight) / (maxCount - minCount)) * (maxCount - minCount) - minCount * useHeight) / useHeight;
                Count = (minCount > tmp) ? minCount : tmp;
            }
            if (In(event->pos, rect)) {
                if (mouseStatus == scrollClick) {
                    if (event->code != 1) {
                        mouseStatus = None;
                    }
                    else {
                        double dy = event->pos.y - mouseClickY;
                        Count += (dy * (maxCount - minCount) - minCount * useHeight) / useHeight;
                        mouseClickY = event->pos.y;
                        if (minCount > Count) {
                            mouseClickY += (useHeight * (minCount - Count + minCount)) / (maxCount - minCount);
                            Count = minCount;
                        }
                        else if (maxCount < Count) {
                            mouseClickY -= (useHeight * (Count - maxCount + minCount)) / (maxCount - minCount);
                            Count = maxCount;
                        }
                    }
                }
            }

            scrollRect = {x, sliderArgument + y + (useHeight * (Count - minCount)) / (maxCount - minCount),
                          x + width, sliderArgument + y + (useHeight * (Count - minCount)) / (maxCount - minCount) + sliderSize};

            innerScrollView->setStartY(Count);
        }

        backButtonRect = {x, y,
                          x + sliderArgument, y + sliderArgument};

        nextButtonRect = {x + width - sliderArgument, y + height - sliderArgument,
                          x + width, y + height};


        if (In(event->pos, nextButtonRect) && mouseStatus != scrollClick) {
            if (mouseStatus != nextButtonClick) {
                mouseStatus = nextButtonOver;
            }
            switch (event->type) {
                case TxEvent::TX_MOUSE_PRESS : {
                    if (event->code == 1) {
                        mouseStatus = nextButtonClick;
                        mouseClickX = event->pos.x;
                        mouseClickY = event->pos.y;
                    }
                } break;
                case TxEvent::TX_MOUSE_RELEASE : {
                    if (event->code == 1) {
                        mouseStatus = nextButtonOver;
                    }
                } break;
            }
        }
        else if (In(event->pos, backButtonRect) && mouseStatus != scrollClick) {
            if (mouseStatus != backButtonClick) {
                mouseStatus = backButtonOver;
            }
            switch (event->type) {
                case TxEvent::TX_MOUSE_PRESS : {
                    if (event->code == 1) {
                        mouseStatus = backButtonClick;
                        mouseClickX = event->pos.x;
                        mouseClickY = event->pos.y;
                    }
                } break;
                case TxEvent::TX_MOUSE_RELEASE : {
                    if (event->code == 1) {
                        mouseStatus = backButtonOver;
                    }
                } break;
            }
        }
        else if (In(event->pos, scrollRect)) {
            if (mouseStatus != scrollClick) {
                mouseStatus = scrollOver;
            }
            switch (event->type) {
                case TxEvent::TX_MOUSE_PRESS : {
                    if (event->code == 1) {
                        mouseStatus = scrollClick;
                        mouseClickX = event->pos.x;
                        mouseClickY = event->pos.y;
                    }
                } break;
            }
        }
        else {
            if (event->code != 1) {
                mouseStatus = None;
            }
        }
    }
}

double BdScrollSlider::getCount() {
    return Count;
}

void BdScrollSlider::refresh(BdComponent *_innerComponent) {
    if (type == Horisontal) {
        minCount = 0;
        maxCount = _innerComponent->width - width;
        sliderSize = (width * (width - 2 * sliderArgument) / (width + maxCount));
        if (sliderSize > width - 2 * sliderArgument) {
            sliderSize = width - 2 * sliderArgument;
        }
        if (sliderSize < 10) {
            sliderSize = 10;
        }
        if (_innerComponent->width <= width) {
            this->setEnabled(false);
        }
        else {
            this->setEnabled(true);
        }
    }
    else if (type == Vertical) {
        minCount = 0;
        maxCount = _innerComponent->height - height;
        sliderSize = (height * (height - 2 * sliderArgument) / (height + maxCount));
        if (sliderSize > height - 2 * sliderArgument) {
            sliderSize = height - 2 * sliderArgument;
        }
        if (sliderSize < 10) {
            sliderSize = 10;
        }
        if (_innerComponent->height <= height) {
            this->setEnabled(false);
        }
        else {
            this->setEnabled(true);
        }
    }

}

//-------------------------BdScroller--------------------------
BdScrollView::BdScrollView () {}

BdScrollView::BdScrollView(int _x, int _y, int _width, int _height) : BdComponent(_x, _y, _width, _height) {
    startX = 0;
    startY = 0;
    innerComponent = NULL;
}

void BdScrollView::setInnerComponent (BdComponent *_innerComponent) {
    innerComponent = _innerComponent;
}

void BdScrollView::draw(Surface *s) {
    if (innerComponent == NULL) return;

    s->drawPartOfComopnent(innerComponent, startX, startY, width, height);

    s->setFillColor(TX_TRANSPARENT);
    s->setColor(TX_BLACK);
    s->rectangle(0, 0, width, height);
}

void BdScrollView::process(TxEvent *event) {
    if (innerComponent == NULL) return;

    RECT rect = {0, 0, width, height};
    if (In(event->pos, rect)) {
        event->pos.x += startX;
        event->pos.y += startY;
        event->pos.x += innerComponent->x;
        event->pos.y += innerComponent->y;
        innerComponent->process(event);
        event->pos.x -= innerComponent->x;
        event->pos.y -= innerComponent->y;
        event->pos.x -= startX;
        event->pos.y -= startY;
    }
    else {
        TxEvent nullEvent = *event;

        nullEvent.type = TxEvent::TX_NONE;

        nullEvent.pos.x = innerComponent->x + innerComponent->width + 1;
        nullEvent.pos.y = innerComponent->y + innerComponent->height + 1;

        innerComponent->process(&nullEvent);
    }
}

void BdScrollView::setStartX(int _startX) {
    startX = _startX;
}

void BdScrollView::setStartY(int _startY) {
    startY = _startY;
}

//-------------------------------------------BdScroller--------------------------------------

BdScroller::BdScroller (int _x, int _y, int _width, int _height) : BdContainer(_x, _y, _width, _height) {
    int scrollViewWidth = width - ARGUMENT;
    int scrollViewHeight =  height - ARGUMENT;

    scrollView = BdScrollView (0, 0, scrollViewWidth, scrollViewHeight);
    downSlider = BdScrollSlider (0, height - ARGUMENT, width - ARGUMENT, ARGUMENT, 0, 0, BdScrollSlider::Horisontal, &scrollView);
    rightSlider = BdScrollSlider (width - ARGUMENT, 0, ARGUMENT, height - ARGUMENT, 0, 0, BdScrollSlider::Vertical, &scrollView);

    this->addChild(&scrollView);
    this->addChild(&downSlider);
    this->addChild(&rightSlider);
}

void BdScroller::setInnerComponent (BdComponent *_innerComponent) {
    scrollView.setInnerComponent(_innerComponent);

    downSlider = BdScrollSlider (0, height - ARGUMENT, width - ARGUMENT, ARGUMENT, 0, _innerComponent->width - width, BdScrollSlider::Horisontal, &scrollView);
    if (_innerComponent->width <= width) {
        isShowDownSlider = false;
    }
    else {
        isShowDownSlider = true;
    }

    rightSlider = BdScrollSlider (width - ARGUMENT, 0, ARGUMENT, height - ARGUMENT, 0, _innerComponent->height - height, BdScrollSlider::Vertical, &scrollView);
    if (_innerComponent->height <= height) {
        isShowRightSlider = false;
    }
    else {
        isShowRightSlider = true;
    }

    if (isShowDownSlider) {
        downSlider.setEnabled(true);
    }
    else {
        downSlider.setEnabled(false);
    }

    if (isShowRightSlider) {
        rightSlider.setEnabled(true);
    }
    else {
        rightSlider.setEnabled(false);
    }
}

void BdScroller::setStartX(int _startX) {
    scrollView.setStartX (_startX);
}

void BdScroller::setStartY(int _startY) {
    scrollView.setStartY (_startY);
}

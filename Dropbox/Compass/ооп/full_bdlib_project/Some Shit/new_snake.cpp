#include "TXLib.h"

const int MAX_SIZE = 50;
int count_ = 0;
int scores = 0;

class Area {
protected:
    int xSize;
    int ySize;

    COLORREF color;
    COLORREF fillColor;
    bool announcedColor;
public:

    Area() : announcedColor(false) {};

    static const int oneDivision = 30;
    int getXSize();
    int getYSize();

    void setXYSize(int _x, int _y);
    void setColorOfArea(COLORREF _color, COLORREF _fillColor);
    void drawArea();
    void drawScoreTable();
};

class Snake : public Area{
private:
    enum moveDirection {UP, DOWN, LEFT, RIGHT};
    moveDirection napr;

    COLORREF color;
    COLORREF fillColor;
    bool announcedColor;

    int startX;
    int startY;
public:
    int x;
    int y;

    Area numberOfDivision[oneDivision][oneDivision];

    /*Snake(int _x, int _y) : startX(_x), startY(_y), napr(RIGHT) {
        add(startX*oneDivision, startY*oneDivision);
        for(int i = startY; i < startY + 5; i++) {
            add(startX, i);
        }
    }*/

    void draw();
    void process(int x, int y);
    void teleportation();
    void control();
    void move();
    void step();
    bool ateItself();
    bool eating();
    void createApple();
    void add(int x, int y);
    void setStartX(int _x);
    void setStartY(int _y);
};

class Apple : public Snake {
public:
    void draw();
    int randomX();
    int randomY();
};

Snake snake;
Area area;
Apple apple;

Snake body[MAX_SIZE];
Snake direction;

int main() {
    snake.process(1, 5);
}

//================================================================
//==============================Area==============================
//================================================================

int Area::getXSize(){
    return xSize;
}

int Area::getYSize(){
    return ySize;
}

void Area::setXYSize(int _x, int _y){
    xSize = _x;
    ySize = _y;
}

void Area::setColorOfArea(COLORREF _color, COLORREF _fillColor){
    color = _color;
    fillColor - _fillColor;
    announcedColor = true;
}

void Area::drawArea(){
    txCreateWindow(xSize, ySize);
    if(announcedColor == true) {
        txSetColor(color);
        txSetFillColor(fillColor);
    } else {
        txSetColor(TX_BLACK);
        txSetFillColor(TX_WHITE);
    }
    txClear();
    for(int i = 30; i < ySize + 3; i += oneDivision) {
        txLine(i, 0, i, ySize);
    }
    for(int i = 30; i < ySize; i += oneDivision) {
        txLine(0, i, ySize, i);
    }
    drawScoreTable();
}

void Area::drawScoreTable() {
    txSetColor(TX_BLACK, 3);
    txSetFillColor(RGB(230, 230, 230));
    txRectangle(getYSize() + 3, 0, getXSize(), getYSize()/5);
//    char* drawScores;
//    sprintf(drawScores, "your srores %d", scores);
//    txDrawText(getYSize() + 3, 0, getXSize(), getYSize()/5, drawScores);
}

//================================================================
//=============================Snake==============================
//================================================================

void Snake::add(int _x, int _y) {
	body[count_].x = _x;
	body[count_].y = _y;
	count_++;
}

void Snake::draw() {
    txSetColor (RGB (255, 117, 24));
	txSetFillColor (RGB (209, 226, 49));
	for (int i = 0; i < count_; i++){
		txRectangle(body[i].x*oneDivision + 2, body[i].y*oneDivision + 2, body[i].x*oneDivision + oneDivision - 1, body[i].y*oneDivision + oneDivision - 1);
	}
}

void Snake::process(int x, int y) {
    snake.setStartX(x);
    snake.setStartY(y);

    for(int i = startY; i > startY - 4; i--) {
        add(startX, i);
    }
    area.setXYSize(800, 600);
    area.drawArea();

    direction.x = 0;
    direction.y = 1;

    snake.control();
}

void Snake::teleportation() {
    if(body[0].y == -1){
        body[0].y = 30;
    } else if(body[0].y == 30) {
        body[0].y = -1;
    }

    if(body[0].x == 30){
        body[0].x = -1;
    } else if(body[0].x == -1){
        body[0].x = 30;
    }
}

void Snake::control() {
    while(!GetAsyncKeyState(VK_ESCAPE) /*(&& ateItself()*/) {
        if (napr != DOWN && GetAsyncKeyState(VK_UP)) {
            direction.x = 0;
            direction.y = -1;
            napr = UP;
            }

        if (napr != LEFT && GetAsyncKeyState(VK_RIGHT)) {
            direction.x = 1;
            direction.y = 0;
            napr = RIGHT;
            }

        if (napr != UP && GetAsyncKeyState(VK_DOWN)) {
            direction.x = 0;
            direction.y = 1;
            napr = DOWN;
            }

        if (napr != RIGHT && GetAsyncKeyState(VK_LEFT)) {
            direction.x = -1;
            direction.y = 0;
            napr = LEFT;
            }

        move();
        txSleep(50);
    }
}

void Snake::move() {
    step();
//    Proverka(body);
    draw();
    teleportation();
}

void Snake::step() {
    txSetColor (TX_BLACK);
    txSetFillColor (TX_WHITE);
    txRectangle (body[0].x*area.oneDivision, body[0].y*area.oneDivision, body[0].x*area.oneDivision + area.oneDivision + 1, body[0].y*area.oneDivision + area.oneDivision + 1);

    int new_x = body[0].x + direction.x;
    int new_y = body[0].y + direction.y;

//    if(ateItself()) {
//        txSleep(3000);
//    }

    if (new_x == apple.y && new_y == apple.x) {
        eating();
    }

    for (int i = 0; i < count_; i++) {
        body[i].x = body[i + 1].x;
        body[i].y = body[i + 1].y;
    }
    body[0].x += direction.x;
    body[0].y += direction.y;
    txSleep(100);
}

bool Snake::ateItself() {
    for (int i = 1; i < count_; i++) {
        if (body[0].x == body[i].x && body[0].y == body[i].y) {
            return true;
        }
    }
    return false;
}

bool Snake::eating() {

}

void Snake::createApple() {
    apple.draw();
}

void Snake::setStartX(int startX_) {
    startX = startX_;
}

void Snake::setStartY(int startY_) {
    startY = startY_;
}



void Apple::draw() {
    txSetColor (TX_RED);
    txSetFillColor (TX_RED);
    txCircle (area.oneDivision*randomX() + area.oneDivision/2, area.oneDivision*randomY() + area.oneDivision/2, 13);
}

int Apple::randomX() {
    srand(time(NULL));
    int coord;
    coord = rand() % area.getYSize()/oneDivision;
    for(int i = 0; i < count_; i++) {
        if(coord == body[i].x*area.oneDivision) {
            coord = rand() % area.getYSize()/area.oneDivision;
        }
    }
}

int Apple::randomY() {
    srand(time(NULL));
    int coord;
    coord = rand() % area.getYSize()/area.oneDivision;
    for(int i = 0; i < count_; i++) {
        if(coord == body[i].y*area.oneDivision) {
            coord = rand() % area.getYSize()/area.oneDivision;
        }
    }
}

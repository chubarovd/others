#include "BdLib.h"

class BdItemListListener;

class BdMessageList : public BdComponent {
public:
    enum TypeOfMessList {CHAT, LIST};
    TypeOfMessList commonType;
    enum TypeOfMess {RECEIVED, SENDED, SYSTEM};
private:
    BdList<BdItemListListener*> mlListeners;
    static const int SIZE = 100;

    time_t rawtime;
    struct tm * timeinfo;
    char time_[20];
    char times_[SIZE][100];

    static const int INTERVAL = 10;

    double width_of_font;
    double height_of_font;
    double lenght_of_side_before_triangle;

    int startHeight;

    TypeOfMess types[SIZE];

    char message[SIZE][100];
    char items[SIZE][100];
    int count_;
    int items_count;
    bool multiline(char* line);
    int multilinesCount;
    int lastSelectedItem;
public:
    BdMessageList(int x_, int y_, int w_, int h_, TypeOfMessList type);

    void draw(Surface* s);
    void process(TxEvent* event);

    void addMess(char* mess, TypeOfMess type);
    void addItem(char* item);
    void clearTable();
    void deleteMess(int index);
    void deleteItem(int index);
    void deleteItem(char* item);
    void getItemName(int index, char* array);
    int getIndexOfLastSelectedItem();

    void addListener(BdItemListListener* mll);
    void removeListener(BdItemListListener* mll);
};

class BdItemListListener {
public:
    virtual void onSelectedItem(BdMessageList* ml) = 0;
};

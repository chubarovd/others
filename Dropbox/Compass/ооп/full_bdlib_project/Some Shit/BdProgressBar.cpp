#include "BdProgressBar.h"

BdProgressBar::BdProgressBar(int x_, int y_, int width_, int height_) : BdComponent (x_, y_, width_, height_) {
    min = 0;
    max = 1;
    percentCount = 0;
    progress = 0;
    performing = false;
    roundStep = 5;
    time = GetTickCount();
    roundX = height / 2;
}

void BdProgressBar::draw(Surface* s){
	s->setColor(TX_BLACK);
	s->line(0, 0, width, 0);
	s->line(0, 0, 0, height);
	s->line(width, 0, width, height);
	s->line(0, height, width, height);
	if (this->type == DETERMINATE) {
        s->setFillColor(TX_GREEN);
        s->rectangle(0, 0, width * percentCount / 100, height);
        char str[10];
        sprintf(str, " %d %%", percentCount);
        s->setColor(TX_BLACK);
        s->drawText(0, 0, width, height, str);
	}
	if ((this->type == INDETERMINATE) && (this->getPerforming() == true)) {
        if ((roundX + roundStep > 0 + width - height / 2) || (roundX + roundStep < 0 + height / 2)) {
            roundStep *= -1;
        }
        s->setFillColor(TX_GREEN);
        s->circle(roundX, height / 2, height / 2);
        if (GetTickCount() - time > REFRESH_TIME) {
            roundX += roundStep;
            time = GetTickCount();
        }
	}
	if ((this->type == INDETERMINATE) && (this->getPerforming() == false)) {
        roundX = 0 + height / 2;
	}
}

void BdProgressBar::process(TxEvent* event) {
    if (this->type == DETERMINATE) {
        percentCount = ((progress - this->min) / (this->max - this->min)) * 100;
    }
}

void BdProgressBar::setType(BdProgressBarType type) {
    this->type = type;
}

void BdProgressBar::setMin(int min) {
    this->min = min;
}

int BdProgressBar::getMin() {
    return(this->min);
}

void BdProgressBar::setMax(int max) {
    this->max = max;
}

int BdProgressBar::getMax() {
    return(this->max);
}

void BdProgressBar::setPerforming(bool performing) {
    this->performing = performing;
}

bool BdProgressBar::getPerforming() {
    return(this->performing);
}

void BdProgressBar::setProgress(int p) {
    if ((this->type == DETERMINATE) && (p >= min) && (p <= max)) {
        this->progress = p;
    }
}

int BdProgressBar::getProgress() {
    return this->progress;
}

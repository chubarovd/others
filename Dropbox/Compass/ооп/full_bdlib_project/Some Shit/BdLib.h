#ifndef BDLIB_H
#define BDLIB_H
#include "TXLib.h"
#include "BdList.h"

//-----------------------SURFACE--------------------------------------------

class BdComponent;

class Surface {
protected:

public:
    int x0;
	int y0;
    void translate (int dx, int dy);
	virtual void line(int x1, int y1, int x2, int y2) = 0;
	virtual void rectangle(int x1, int y1, int x2, int y2) = 0;
	virtual void setColor(COLORREF color) = 0;
	virtual void setColor(COLORREF color, double thickness) = 0;
	virtual void setFillColor(COLORREF color) = 0;
	virtual void selectFont (const char name[], double x, double y) = 0;
	virtual void textOut(int x, int y, const char text[]) = 0;
	virtual void drawText(int x1, int y1, int x2, int y2, const char text[]) = 0;
	virtual void drawText(int x1, int y1, int x2, int y2, const char text[], unsigned format) = 0;
	virtual void circle(int x, int y, int r) = 0;
	virtual void clear () = 0;
	virtual void createWindow (int x, int y) = 0;
	virtual void begin () = 0;
	virtual void end () = 0;
	virtual void sleep (int ms) = 0;
	virtual void polygon (POINT points[], int numPoints) = 0;

	virtual void drawPartOfComopnent (BdComponent* comp, int x, int y, int width, int height) = 0;
	//-------------------------------------time for Rogoza-------------------------------------------
	virtual SIZE getTextExtent (const char text[]) = 0;
	virtual int getTextExtentX (const char text[]) = 0;
	virtual int getTextExtentY (const char text[]) = 0;
	virtual bool bitBlt (HDC dest, double xDest, double yDest, double width, double height, HDC src, double xSrc, double ySrc) = 0;
	//virtual HDC loadImage (const char filename[]);
	Surface (int x_, int y_);
	Surface();
};

class DisplaySurface: public Surface {
public:
	DisplaySurface(int x_, int y_);
	void line(int x1, int y1, int x2, int y2);
	void rectangle(int x1, int y1, int x2, int y2);
	void setColor(COLORREF color);
	void setColor(COLORREF color, double thickness);
	void setFillColor(COLORREF color);
	void selectFont (const char name[], double x, double y);
	void textOut(int x, int y, const char text[]);
	void drawText(int x1, int y1, int x2, int y2, const char text[]);
	void drawText(int x1, int y1, int x2, int y2, const char text[], unsigned format);
	void circle(int x, int y, int r);
	void clear ();
	void createWindow (int x, int y);
	void begin ();
	void end ();
	void sleep (int ms);
	void polygon (POINT points[], int numPoints);

	void drawPartOfComopnent (BdComponent* comp, int x, int y, int width, int height);
	//-------------------------------------time for Rogoza-------------------------------------------
	SIZE getTextExtent (const char text[]);
    int getTextExtentX (const char text[]);
	int getTextExtentY (const char text[]);
	bool bitBlt (HDC dest, double xDest, double yDest, double width, double height, HDC src, double xSrc, double ySrc);
	//HDC loadImage (const char filename[]);
};

class BdComponent;

class BdComponentListener {
public:
    virtual void onClick (BdComponent* comp, int x, int y) = 0;
};

class BdContainer;
class BdWindow;

class BdFocusListener;

class BdComponent {
protected:
    virtual BdComponent *preparePopup ();
    enum MouseStatus {NONE, UNDER_CURSOR, PRESSED};
    BdList<BdComponentListener*> listeners;
    BdList<BdFocusListener*> focusListeners;
private:
    MouseStatus mouseStatus;
    bool enabled;
    BdComponent *popupComponent;
public:
    int x;
    int y;
    int width;
    int height;

    BdContainer *parent;

    BdWindow *window;

    int getMouseStatus ();

    virtual void draw (Surface* s) = 0;
    virtual void process (TxEvent* event);
    void commonProcess(TxEvent* event);
    void fullProcess(TxEvent* event);

    bool isEnabled ();
    void setEnabled (bool enabled_);

    bool isParentOf (BdComponent* child);

    void addListener (BdComponentListener* listener);
    void removeListener (BdComponentListener* listener);

    BdComponent* getParent ();
    void setParent (BdContainer* parent_);
    bool mouseOn(TxEvent* e);

    void showPopup(BdComponent *_popup, int dx, int dy);
    BdWindow* getWindow();
    void setPopupComponent (BdComponent *_popupComponent);

    void focusRecived();
    void focusLost();
    void addFocusListener(BdFocusListener* fl);
    void removeFocusListener(BdFocusListener* fl);

    int getXOnScreen();
    int getYOnScreen();

    BdComponent();
    BdComponent(int x_, int y_, int w_, int h_);
    BdComponent(int x_, int y_, int w_, int h_, bool status_);
    BdComponent(int x_, int y_, int w_, int h_, BdContainer* container);
    BdComponent(int x_, int y_, int w_, int h_, BdWindow* window);

    void init(int x, int y, int width, int height, bool status, BdContainer* container, BdWindow* window);
};

class BdContainer : public BdComponent {
protected:
    BdList<BdComponent*> children;
public:
    void addChild (BdComponent* child_);
    void removeChild (BdComponent* child_);
    void draw (Surface* s);
    void process (TxEvent* event);
    bool isEventForComponent (TxEvent* event, BdComponent* child);

    BdContainer(int x_, int y_, int width_, int height_);
    BdContainer(int x_, int y_, int width_, int height_, BdWindow* window_);
};

class BdButton : public BdComponent {
    static const int MAX_SIZE = 100;
    char text[MAX_SIZE];
    COLORREF color;
public:

    void draw(Surface* s);

    BdButton(int x_, int y_, int width_, int height_, char text_[], bool status_);
};

class BdLabel : public BdComponent {
    static const int MAX_SIZE = 100;
    char text[MAX_SIZE];
public:
    void draw(Surface* s);
    void setText(char text_[]);
    void process(TxEvent* event);

    BdLabel(int x_, int y_, int width_, int height_, char text_[]);
};

class BdFocusListener {
public:
    virtual void onFocusRecived(BdComponent* c) = 0;
    virtual void onFocusLost(BdComponent* c) = 0;
};

class BdWindow {
    int x;
    int y;
    BdComponent* focusOwner;
public:
    BdContainer container;

    BdComponent* getFocusOwner ();
    void setFocusOwner (BdComponent* focusOwner_);
    void showPopup(BdComponent *popup, int x, int y);
    Surface* surface;
    void show ();

    int getWindowX();
    int getWindowY();

    BdWindow (int x_, int y_);
};

//------------------------------BdMenu-------------------------------//
class BdMenu;

class BdMenuItem : public BdComponent {
private:
    static const int MAXSIZE = 100;
    char itemtext[MAXSIZE];
    BdMenu *tab;
public:
    BdMenuItem(int _x, int _y, int _width, int _height, char* _itemtext);
    BdMenuItem(char* _itemtext);
    BdMenuItem();
    bool active;
    char* getText();
    void setTab(BdMenu *_tab);
    void activate();
    void build();

    void destroy();
    virtual void draw(Surface* s);
    virtual void process(TxEvent* event);

    BdMenu *father;
};

class BdMenu : public BdComponent {
private:
    BdList<BdMenuItem*> items;
public:
    BdMenu(int _x, int _y, int _weigth, int _height);
    BdMenu();
    bool active;

    int getItemsSize();
    void addItem(BdMenuItem*);
    void build();
    void destroy();

    virtual void draw(Surface* s);
    virtual void process(TxEvent* event);

    BdMenuItem *father;
};
#endif

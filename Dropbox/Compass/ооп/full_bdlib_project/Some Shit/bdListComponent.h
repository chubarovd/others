#include "BdLib.h"

class BdListItem
{
private:
static const int MAX_SIZE = 100;
    char itemText [MAX_SIZE];
public:
    void getText (char* str);

    BdListItem ();
    BdListItem (char* text_);
};

class BdListModel
{
private:
    BdList<BdListItem> items;
public:
    void getTextItem (char* str, int itemIndex);
    void addItem (BdListItem item);
    BdList<BdListItem>* getItems ();
    BdListModel ();
};

class BdListComponent;

class BdListComponentListener {
public:
    virtual void onSelectionChanged(BdListComponent* l) = 0;
};

class BdListComponent : public BdComponent {
private:
    static const int ITEM_HEIGHT = 50;
    BdListModel model;
    bool isEventForItem (TxEvent* event, int itemIndex);
    BdList<BdListComponentListener*> listeners;
    int selectedItem;
    int sdvig;
    int firstVisibleItem;
    int countVisibleItem;
public:
    int getSelectedItem ();
    void setSelectedItem(int i);
    void setModel (BdListModel model_);
    void draw(Surface* s);
    void process (TxEvent* event);
    BdListModel* getModel();

    void addListener (BdListComponentListener* listener);
    void removeListener (BdListComponentListener* listener);

    BdListComponent (int x_, int y_, int width_, int height_, bool status_);
};

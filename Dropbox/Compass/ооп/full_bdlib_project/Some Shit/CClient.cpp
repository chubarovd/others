#include "BdLib.cpp"
#include "BdTimer.cpp"
#include "BdTextField.cpp"
#include "BdMessageList.cpp"
#include "BdScroller.cpp"
#include "BdTabSet.cpp"
#include "tglib.h"

bool connect_ = false;
size_t left = 0;

/*class privateChat {
private:

public:
    BdContainer container;
    BdMessageList ML;
    BdMessageList UL;
    BdButton send;
    BdTextField sendText;

    privateChat(BdContainer* cont);
};*/

class Form : public BdComponentListener, BdTimerListener, BdItemListListener, BdTabSetListener {
private:
    static const int SIZE = 100;

    enum messageTypes {SEND_TEXT, GET_USER_LIST, USER_LIST, READY, DISCONNECT, RENAME, RECV_PRIVATE_TEXT, SEND_PRIVATE_TEXT, NONE};
    messageTypes mType;

    TGLPort p;

    char buf[SIZE];
    char ip[SIZE];
    char name[SIZE];

    char userList[SIZE][SIZE];
    int usersOnline;
    bool waitingUserList;
    bool userListReceived;

    bool nameIsCorrect;
    bool privateDialog;
public:
    BdWindow window;

    BdTimer counter;
    BdTextField stext;
    BdTextField iptext;
    BdButton send;
    BdButton connect;
    BdMessageList messList;
    BdScroller sc;
    BdMessageList usList;
    BdScroller scUl;
    BdLabel nameL;
    BdTextField nameText;
    BdLabel nameIncorrect;
    BdTabSet ts;
    BdContainer privateUser;
    /*BdContainer privateUser2;
    BdContainer privateUser3;
    BdContainer privateUser4;
    BdContainer privateUser5;*/
    BdContainer* chatField[5];
    char privateNames[5];
    int privateNamesCount;

    void onClick(BdComponent* v, int x, int y);
    void onTick(BdTimer* timer);
    void onTabChanged(BdTabSet* tabSet);
    void onTabClosed(BdTabSet* tabSet);
    void onSelectedItem(BdMessageList* ml);

    void sendMessage(char* str);
    void sendPrivateMessage(char* str);
    void messageProcessing(char* str);
    void buildUserList(char* list);

    Form();
};

int main() {
    Form form;
    form.window.show();
}

Form::Form() : window (780, 400),
                privateUser(0, 0, 780, 400), /*privateUser2(0, 0, 780, 400), privateUser3(0, 0, 780, 400),
                privateUser4(0, 0, 780, 400), privateUser5(0, 0, 780, 400),*/
                ts(0, 0, 780, 400),
                messList(0, 0, 581, 330, BdMessageList::CHAT),
                sc(0, 0, 600, 330),
                usList(600, 0, 161, 400, BdMessageList::LIST),
                scUl(600, 0, 180, 400),
                iptext(3, 3, 573),
                stext(5, 313, 500),
                nameText(203, 73, 400),
                nameL(0, 70, 200, 70, "UserName:"),
                send(500, 310, 100, 70, "send", false),
                connect(580, 0, 200, 70, "connect", true),
                nameIncorrect(80, 150, 620, 70, "YOU CAN'T USE ; IN USERNAME") {
    privateNamesCount = 0;
    //---private users---
    chatField[privateNamesCount] = &privateUser;
    /*chatField[1] = &privateUser2;
    chatField[2] = &privateUser3;
    chatField[3] = &privateUser4;
    chatField[4] = &privateUser5;
    /*-------------------*/
    sc.setInnerComponent(&messList);
    scUl.setInnerComponent(&usList);
    usList.addListener(this);
    window.container.addChild(&iptext);
    window.container.addChild(&nameText);
    window.container.addChild(&nameL);
    window.container.addChild(&connect);
    connect.addListener(this);
    usersOnline = 0;
    userListReceived = false;
    nameIsCorrect = true;
    privateDialog = false;

    iptext.setText("localhost");

    window.container.addChild(&counter);
    counter.setPeriodMillis(1);
    counter.addListener(this);
    counter.start();
}

void Form::onClick (BdComponent* v, int x, int y) {
    if(v == &connect) {
        if(iptext.getSymbolCount() > 0 && nameText.getSymbolCount() > 0 && nameIsCorrect) {
            iptext.getText(ip);
            window.container.addChild(&ts);
            privateUser.addChild(&sc);
            privateUser.addChild(&scUl);
            ts.addTab(&privateUser, "Common Dialog");
            BdContainer* q = new BdContainer(0, 0, 780, 400);
            ts.addTab(q, "q");
            if(p.connect(ip, 2215, -1)) {
                nameText.getText(name);
                p.setMessRecvTimeout(300);
                p.sendMess(name, strlen(name) + 1, -1);
                connect_ = true;

                privateUser.addChild(&send);
                send.addListener(this);
                privateUser.addChild(&stext);

                window.container.removeChild(&iptext);
                window.container.removeChild(&nameText);
                window.container.removeChild(&nameL);
                connect.removeListener(this);
                window.container.removeChild(&connect);
            }
        }
    }
    if(v == &send) {
        stext.getText(buf);
        sendMessage(buf);
    }
}

void Form::onTick(BdTimer* timer) {
    if(connect_) {
        int messLen = p.recvMess(buf, SIZE, &left);
        if(messLen > 0) {
            buf[messLen] = 0;
            messageProcessing(buf);
        }
    }
    char name_[100];
    nameText.getText(name_);
    for(int i = 0; i <= strlen(name_); i++) {
        if(name_[i] == ';' && nameIsCorrect) {
            privateUser.addChild(&nameIncorrect);
            nameIsCorrect = false;
        } else if(!nameIsCorrect) {
            privateUser.removeChild(&nameIncorrect);
            nameIsCorrect = true;
        }
    }
}

void Form::onTabChanged(BdTabSet* tabSet) {
    char q[SIZE];
    tabSet->getSelectedTabName(q);
    if(strcmp(q, "Common Dialog")) {
        privateDialog = false;
    } else {
        privateDialog = true;
    }
}

void Form::onTabClosed(BdTabSet* tabSet) {
    if(tabSet == &ts) {

    }
}

void Form::onSelectedItem(BdMessageList* ml) {
    if(ml == &usList && privateNamesCount < 5) {
        char privName[SIZE];
        ml->getItemName(ml->getIndexOfLastSelectedItem(), privName);
        BdContainer* cont = new BdContainer(0, 0, 780, 400);
        ts.addTab(cont, "sdfg");
        printf("add <%s> tab\n", privName);
        chatField[privateNamesCount] = cont;
        privateNamesCount++;
        // ���������� ��� addDialod
    }
}

void Form::sendMessage(char* str) {
    char output[SIZE] = {};
    int type = str[0] - '0';
    mType = (messageTypes)type;

    if(userListReceived) {
        sprintf(output, "0%s", str);
        if(p.sendMess(output, strlen(str) + 1, -1)) {
            messList.addMess(str, BdMessageList::SENDED);
        }
    }
}

void Form::sendPrivateMessage(char* str) {
    char receiveName[SIZE];
    char output[SIZE] = {};
    int type = str[0] - '0';
    mType = (messageTypes)type;

    if(userListReceived) {
        sprintf(output, "6%s", str);
        if(p.sendMess(output, strlen(str) + 1, -1)) {
            messList.addMess(str, BdMessageList::SENDED);
        }
    }
}

void Form::messageProcessing(char* str) {
    char input[SIZE] = {};
    int type = str[0] - '0';
    mType = (messageTypes)type;

    for(int i = 1; i < strlen(str); i++) {
        input[i - 1] = str[i];
    }
    switch(mType) {
        case SEND_TEXT :        //complete
            if(userListReceived) {
                messList.addMess(input, BdMessageList::RECEIVED);
            }
        break;
        case GET_USER_LIST :
            send.setEnabled(false);
            userListReceived = false;
        break;
        case USER_LIST :
            printf("USER_LIST < %s >\n", input);
            buildUserList(input);
            send.setEnabled(true);
            userListReceived = true;
        break;
        case DISCONNECT :

        break;
        case RENAME :

        break;
    }
}

void Form::buildUserList(char* list) {
    usList.clearTable();
    char one_name[SIZE] = {};
    char interim[SIZE] = {};
    for(int i = 0; i <= strlen(list); i++){
        if(list[i] != ';') {
            interim[0] = list[i];
            interim[1] = 0;
            strcat(one_name, interim);
        } else if(strlen(one_name) > 0) {
            usList.addItem(one_name);
            strcpy(one_name, "");
        }
    }
}





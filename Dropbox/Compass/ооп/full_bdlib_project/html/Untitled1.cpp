#include "TXLib.h"

using namespace std;

int svn_fv = 0;
int fv_svn = 0;
int tot_svn_fv[100];
int tot_svn_fv_count = 0;
int tot_fv_svn[100];
int tot_fv_svn_count = 0;
double mid_svn_fv = 0.0;
double mid_fv_svn = 0.0;

int main() {
    freopen("input.txt", "r", stdin);
    char symbol;
    while(!feof(stdin)) {
        cin >> symbol;
        if(symbol == '(') {
            cin >> symbol;
            if(symbol == '7') {
                cin >> symbol >> symbol;
                if(symbol == '5') {
                    cin >> symbol;
                    if(symbol != ')') {
                        svn_fv++;
                        cin >> symbol;
                        int frst = symbol - '0';
                        cin >> symbol >> symbol;
                        int scnd = symbol - '0';
                        tot_svn_fv[tot_svn_fv_count] = frst + scnd;
                        tot_svn_fv_count++;
                    }
                }
            }
            if(symbol == '5') {
                cin >> symbol >> symbol;
                if(symbol == '7') {
                    cin >> symbol;
                    if(symbol != ')') {
                        fv_svn++;
                        cin >> symbol;
                        int frst = symbol - '0';
                        cin >> symbol >> symbol;
                        int scnd = symbol - '0';
                        tot_fv_svn[tot_fv_svn_count] = frst + scnd;
                        tot_fv_svn_count++;
                    }
                }
            }
        }
    }
    for(int i = 0; i < tot_svn_fv_count; i++) {
        printf("%d  ", tot_svn_fv[i]);
        mid_svn_fv += (tot_svn_fv[i]/tot_svn_fv_count);
    }
    printf("\n");
    for(int i = 0; i < tot_fv_svn_count; i++) {
        printf("%d  ", tot_fv_svn[i]);
        mid_fv_svn += (tot_fv_svn[i]/tot_fv_svn_count);
    }
    printf("\n");
    printf("(7:5) - %d\n (5:7) - %d\n mid_svn_fv - %.4lf\n mid_fv_svn - %.4lf\n", tot_svn_fv_count, tot_fv_svn_count, mid_svn_fv, mid_fv_svn);
}

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class GameView;

namespace Ui {
class MainWindow;

}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void onHelloClicked();
private:
    Ui::MainWindow *ui;
    GameView *gv;
};

class GameView : public QWidget
{
    Q_OBJECT
public:
    explicit GameView(QWidget *parent = 0);

    virtual void paintEvent(QPaintEvent *);

};


#endif // MAINWINDOW_H

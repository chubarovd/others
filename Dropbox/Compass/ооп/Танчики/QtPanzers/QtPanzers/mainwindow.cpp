#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QWidget>
#include <QPainter>
#include <QBrush>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    gv = new GameView(ui->gameFrame);
    gv->setGeometry(0, 0, ui->gameFrame->width(), ui->gameFrame->height());
    //ui->gameFrame->children().append(&);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onHelloClicked() {
     QMessageBox msgBox;
     char text[30];
     sprintf(text, "Hello %s!", ui->nameEdit->text().toStdString().c_str());
     msgBox.setText(text);
     msgBox.exec();
}


GameView::GameView(QWidget* parent) : QWidget(parent) {
}

void GameView::paintEvent(QPaintEvent* e) {
    QPainter painter(this);

    QPen pen = painter.pen();
    pen.setWidth(10);
    pen.setColor(Qt::blue);
    painter.setPen(pen);

    painter.drawRect(0, 0, rect().width(), rect().height());
    painter.fillRect(30, 30, rect().width() - 60, rect().height() - 60, Qt::red);
}

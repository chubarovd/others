#include "..\BdLibFromSVN\BdLib.cpp"
#include "..\BdLibFromSVN\BdTimer.cpp"
#include "..\BdLibFromSVN\tglib.h"

const int ONE_CELL = 30;
const int TIMEOUT = 10;

size_t left_ = 0;

using namespace std;

class Shot {
public:
    enum Napr {UP, DOWN, RIGHT, LEFT};
    Napr napr;

    int x;
    int y;
    bool inFlight;
    Shot(int x_, int y_, Napr napr_);
    Shot();
};

class Player {
public:
    TGLPort p;
    Player* enemy;
    Shot shot;

    enum Napr {UP, DOWN, RIGHT, LEFT};
    Napr napr;

    int x;
    int y;
    bool abilityToShot;

    int health;

    char name[50];
    COLORREF color;

    void shotMade();
};

class Map {
public:
    char name[50];
    char map[50][50];
};

class Form : public BdTimerListener {
private:
    static const int SIZE = 100;

    enum messageTypes {PRESSED_KEY, COORDS, SHOT, NONE};
    messageTypes mType;

    bool someoneConnect;

    TGLServerPort sp;
    Player pl[SIZE];
    int playersCount;

    char buf[SIZE];

    BdList<Map*> maps;
    static const int selectedMap = 0;
public:
    BdWindow window;

    BdTimer counter;
    BdTimer bulletFlight;

    void onTick(BdTimer* timer);
    void messageProcessing(char* str, int index);
    void sendUserList(int ind);
    void sendMaps(int index);
    void sendData(int index);

    Form();
};

int main() {
    Form form;
    form.window.show();
}

//-------------------------------------------------------------------------------------//
//------------------------------------Shot---------------------------------------------//
//-------------------------------------------------------------------------------------//

Shot::Shot(int x_, int y_, Napr napr_) {
    x = x_;
    y = y_;
    napr = napr_;
    inFlight = true;
}

Shot::Shot() {

}

//-------------------------------------------------------------------------------------//
//------------------------------------Player-------------------------------------------//
//-------------------------------------------------------------------------------------//

void Player::shotMade() {
    shot.x = x;
    shot.y = y;
    shot.napr = (Shot::Napr)napr;
    shot.inFlight = true;
    abilityToShot = false;

    printf("<%s> MADE SHOT\n", name);
    p.sendMess("20", 3);  // add time to sended data
    enemy->p.sendMess("21", 3);
}

//-------------------------------------------------------------------------------------//
//------------------------------------Map----------------------------------------------//
//-------------------------------------------------------------------------------------//



//-------------------------------------------------------------------------------------//
//------------------------------------Form---------------------------------------------//
//-------------------------------------------------------------------------------------//

Form::Form() : window(550, 650) {
    freopen("maps.txt", "r", stdin);
    int count;
    char count_[2];
    cin >> count_;
    count = atoi(count_);
    for(int i = 0; i < count; i++) {
        Map* m = new Map();
        cin >> m->name;
        for(int j = 0; j < 13; j++) {
            for(int k = 0; k < 13; k++) {
                char cell;
                cin >> cell;
                m->map[j][k] = cell;
            }
        }
        maps.add(m);
    }
    fclose(stdin);
    sp.bind("0.0.0.0", 2215);
    window.container.addChild(&counter);
    counter.setPeriodMillis(1);
    counter.addListener(this);
    counter.start();
    window.container.addChild(&bulletFlight);
    bulletFlight.setPeriodMillis(25);
    bulletFlight.addListener(this);
    bulletFlight.start();
    playersCount = 0;
    mType = NONE;
}

void Form::onTick(BdTimer* timer) {
    if(timer == &counter) {
        if(sp.accept(&pl[playersCount].p, 1)) {
            pl[playersCount].p.setRecvTimeout(TIMEOUT);
            pl[playersCount].enemy = NULL;
            int messLen = pl[playersCount].p.recvMess(buf, SIZE, &left_);
            if(messLen > 0) {
                buf[messLen] = 0;
                char interim[2];
                int j;
                for(int i = 0; buf[i] != ';'; i++) {
                    interim[0] = buf[i];
                    interim[1] = 0;
                    strcat(pl[playersCount].name, interim);
                    pl[playersCount].name[i + 1] = 0;
                    j = i + 1;
                }
                if(buf[j + 1] == 'r') {
                    pl[playersCount].color = TX_RED;
                    pl[playersCount].x = 0;
                    pl[playersCount].y = 0;
                    pl[playersCount].napr = Player::DOWN;
                    pl[playersCount].abilityToShot = true;
                    pl[playersCount].health = 100;
                    printf("PLAYER <%s> CONNECTIG INTO %d AND NOW IN RED\n", pl[playersCount].name, playersCount);
                } else {
                    pl[playersCount].color = TX_BLUE;
                    pl[playersCount].x = 12;
                    pl[playersCount].y = 12;
                    pl[playersCount].abilityToShot = true;
                    pl[playersCount].health = 100;
                    printf("PLAYER <%s> CONNECTIG INTO %d AND NOW IN BLUE\n", pl[playersCount].name, playersCount);
                }
            }
            for(int i = 0; i < playersCount; i++) {
                if(pl[i].enemy == NULL && pl[i].color != pl[playersCount].color) {
                    pl[i].enemy = &pl[playersCount];
                    pl[playersCount].enemy = &pl[i];
                    printf("PLAYER <%s> PLAYS WITH <%s>\n", pl[playersCount].name, pl[i].name);
                    break;
                }
            }
            playersCount++;
            sendMaps(playersCount - 1);
        }
        for(int i = 0; i < playersCount && pl[i].enemy != NULL; i++) {
            pl[i].p.setRecvTimeout(TIMEOUT);
            int messLen = pl[i].p.recvMess(buf, SIZE, &left_);
            if(messLen > 0) {
                buf[messLen] = 0;
                messageProcessing(buf, i);
            }

        }
    }
    if(timer == &bulletFlight) {
        for(int i = 0; i < playersCount; i++) {
            if(!pl[i].abilityToShot) {
                if(pl[i].shot.inFlight) {
                    switch(pl[i].shot.napr) {
                    case Shot::UP :
                        pl[i].shot.y -= 0.2;
                        break;
                    case Shot::DOWN :
                        pl[i].shot.y += 0.2;
                        break;
                    case Shot::RIGHT :
                        pl[i].shot.x += 0.2;
                        break;
                    case Shot::LEFT :
                        pl[i].shot.x -= 0.2;
                        break;
                    }
                    POINT pos = {(pl[i].shot.x + 0.5)*ONE_CELL, (pl[i].shot.y + 0.5)*ONE_CELL};
                    RECT rect = {pl[i].enemy->x*ONE_CELL, pl[i].enemy->y*ONE_CELL, (pl[i].enemy->x + 1)*ONE_CELL, (pl[i].enemy->y + 1)*ONE_CELL};
                    if(In(pos, rect)) {
                        pl[i].abilityToShot = true;
                        pl[i].enemy->health -= 25;
                        pl[i].shot.inFlight = false;
                        if(pl[i].enemy->health <= 0) {
                            printf("~~~~~PLAYER <%s> WIN~~~~~\n", pl[i].name);
                            exit(0);
                        }
                    }
                    sendData(i);
                }
            }
        }
    }
}

void Form::messageProcessing(char* str, int index) {
    char input[SIZE] = {};
    int type = str[0] - '0';
    mType = (messageTypes)type;

    for(int i = 1; i < strlen(str); i++) {
        input[i - 1] = str[i];
    }
    printf("TYPE: %d ~~ MESS: %s ~~ FROM <%s>\n", mType, input, pl[index].name);
    switch(mType) {
        case PRESSED_KEY :
            int event_code = atoi(input);
            int x_ = pl[index].x;
            int y_ = pl[index].y;
            if(event_code == VK_UP) {
                if(pl[index].y >= 1 && maps.getValue(selectedMap)->map[x_][y_ - 1] != '1') {
                    pl[index].y--;
                }
                pl[index].napr = Player::UP;
            }
            if(event_code == VK_DOWN) {
                if(pl[index].y <= 11 && maps.getValue(selectedMap)->map[x_][y_ + 1] != '1') {
                    pl[index].y++;
                }
                pl[index].napr = Player::DOWN;
            }
            if(event_code == VK_RIGHT) {
                if(pl[index].x <= 11 && maps.getValue(selectedMap)->map[x_ + 1][y_] != '1') {
                    pl[index].x++;
                }
                pl[index].napr = Player::RIGHT;
            }
            if(event_code == VK_LEFT) {
                if(pl[index].x >= 1 && maps.getValue(selectedMap)->map[x_ - 1][y_] != '1') {
                    pl[index].x--;
                }
                pl[index].napr = Player::LEFT;
            }
            if(event_code == VK_CONTROL && pl[index].abilityToShot) {
                pl[index].shotMade();
            }
            sendData(index);
        break;
    }
}

void Form::sendUserList(int index) {

}

void Form::sendMaps(int index) {
    char send[SIZE];
    sprintf(send, "%d", maps.getSize());
    pl[index].p.sendMess(send, strlen(send));
    for(int a = 0; a < maps.getSize(); a++) {
        sprintf(send, "%s", maps.getValue(a)->name);
        pl[index].p.sendMess(send, strlen(send));
        for(int i = 0; i < 13; i++) {
            sprintf(send, "%s", maps.getValue(a)->map[i]);
            pl[index].p.sendMess(send, strlen(send));
        }
    }
    printf("SEND MAPS TO %s\n", pl[index].name);
}

void Form::sendData(int index) {
    char recvToSender[100];
    char recvToHisEnemy[100];
    if(pl[index].shot.inFlight && pl[index].enemy->shot.inFlight) {
        sprintf(recvToSender,  "1%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;0",
                                pl[index].x, pl[index].y, (int)pl[index].napr, pl[index].shot.x, pl[index].shot.y, pl[index].health,
                                pl[index].enemy->x, pl[index].enemy->y, (int)pl[index].enemy->napr, pl[index].enemy->shot.x, pl[index].enemy->shot.y, pl[index].enemy->health);
        sprintf(recvToHisEnemy, "1%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;0",
                                pl[index].enemy->x, pl[index].enemy->y, (int)pl[index].enemy->napr, pl[index].enemy->shot.x, pl[index].enemy->shot.y, pl[index].enemy->health,
                                pl[index].x, pl[index].y, (int)pl[index].napr, pl[index].shot.x, pl[index].shot.y, pl[index].health);
    } else if(pl[index].shot.inFlight && !pl[index].enemy->shot.inFlight){
        sprintf(recvToSender,  "1%d;%d;%d;%d;%d;%d;%d;%d;%d;-;-;%d;0",
                                pl[index].x, pl[index].y, (int)pl[index].napr, pl[index].shot.x, pl[index].shot.y, pl[index].health,
                                pl[index].enemy->x, pl[index].enemy->y, (int)pl[index].enemy->napr, pl[index].enemy->health);
        sprintf(recvToHisEnemy, "1%d;%d;%d;-;-;%d;%d;%d;%d;%d;%d;%d;0",
                                pl[index].enemy->x, pl[index].enemy->y, (int)pl[index].enemy->napr, pl[index].enemy->health,
                                pl[index].x, pl[index].y, (int)pl[index].napr, pl[index].shot.x, pl[index].shot.y, pl[index].health);
    } else if(!pl[index].shot.inFlight && pl[index].enemy->shot.inFlight) {
        sprintf(recvToSender, "1%d;%d;%d;-;-;%d;%d;%d;%d;%d;%d;%d;0",
                                pl[index].x, pl[index].y, (int)pl[index].napr, pl[index].health,
                                pl[index].enemy->x, pl[index].enemy->y, (int)pl[index].enemy->napr, pl[index].enemy->shot.x, pl[index].enemy->shot.y, pl[index].enemy->health);
        sprintf(recvToHisEnemy, "1%d;%d;%d;%d;%d;%d;%d;%d;%d;-;-;%d;0",
                                pl[index].enemy->x, pl[index].enemy->y, (int)pl[index].enemy->napr, pl[index].enemy->shot.x, pl[index].enemy->shot.y, pl[index].enemy->health,
                                pl[index].x, pl[index].y, (int)pl[index].napr, pl[index].health);
    } else {
        sprintf(recvToSender, "1%d;%d;%d;-;-;%d;%d;%d;%d;-;-;%d;0",
                                pl[index].x, pl[index].y, (int)pl[index].napr, pl[index].health,
                                pl[index].enemy->x, pl[index].enemy->y, (int)pl[index].enemy->napr, pl[index].enemy->health);
        sprintf(recvToHisEnemy, "1%d;%d;%d;-;-;%d;%d;%d;%d;-;-;%d;0",
                                pl[index].enemy->x, pl[index].enemy->y, (int)pl[index].enemy->napr, pl[index].enemy->health,
                                pl[index].x, pl[index].y, (int)pl[index].napr, pl[index].health);
    }
    pl[index].p.sendMess(recvToSender, strlen(recvToSender) + 1);
    pl[index].enemy->p.sendMess(recvToHisEnemy, strlen(recvToHisEnemy) + 1);
    printf("SEND %s TO <%s> AND <%s> TO HIS ENEMY\n", recvToSender, pl[index].name, recvToHisEnemy);
}




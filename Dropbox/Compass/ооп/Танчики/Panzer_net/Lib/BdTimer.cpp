#include "BdTimer.h"

BdTimer::BdTimer() {
}

void BdTimer::start() {
    working = ON;
    time = 0;
}

void BdTimer::stop() {
    working = OFF;
}

void BdTimer::setPeriodMillis(int periodMillis) {
    if (periodMillis > 0) {
        period = periodMillis;
    }
}

void BdTimer::draw(Surface* s) {
}

void BdTimer::process(TxEvent* event) {
    if ((working == ON) && (GetTickCount() - time >= period)) {
            for (int i = 0; i < bdTimerListeners.getSize(); i++)
                {
                    bdTimerListeners.getValue(i)->onTick(this);
                }
            time = GetTickCount();
    }
}

void BdTimer::addListener(BdTimerListener* listener) {
    bdTimerListeners.add(listener);
}

void BdTimer::removeListener(BdTimerListener* listener) {
    bdTimerListeners.removeValue(listener);
}

#include ".\Lib\BdLib.cpp"
#include ".\Lib\BdTimer.cpp"
#include ".\Lib\BdMessageList.cpp"
#include ".\Lib\BdTextField.cpp"
#include ".\Lib\BdRadioButton.cpp"
#include ".\Lib\tglib.h"

const int ONE_CELL = 50;
const int CELLS_ON_AREA = 13;
const int SIZE_ = 100;
const int RECV_TIMEOUT = 10;

class Form;
Form* frm;

TGLPort p;

size_t left_ = 0;
bool connected = false;


using namespace std;

class Map : public BdComponent {
public:
    char title[SIZE_];
    char map[CELLS_ON_AREA][CELLS_ON_AREA];

    Map(char* title_);

    void draw(Surface* s);
    void process(TxEvent* event);
};

class Shot : public BdComponent {
public:
    enum Napr {UP, DOWN, RIGHT, LEFT};
    Napr napr;

    int x;
    int y;
    bool inFlight;

    Shot();

    void draw(Surface* s);
};

class Panzer : public BdComponent {
private:
    bool holdKey;
    int code_;
public:
    Shot shot;

    enum Type {YOU, ENEMY};
    Type type;
    enum Napr {UP, DOWN, RIGHT, LEFT};
    Napr napr;

    int x;
    int y;

    int health;
    COLORREF color;
    char name[50];

    Panzer(int x_, int y_, Napr napr_, Type type_);
    Panzer(Type type_);

    void shotMade();
    void draw(Surface* s);
    void process(TxEvent* event);
};

class Form : public BdComponentListener, BdTimerListener, BdItemListListener, BdRadioButtonListener {
public:
    enum messageTypes {PRESSED_KEY, NONE};
    messageTypes mType;

    char buf[SIZE_];

    bool mapsReceived;
    bool waitingMaps;

    Panzer you;
    Panzer enemy;

    BdWindow window;
    BdTimer counter;
    BdTimer bulletFlight;
    BdMessageList mapsList;
    BdTextField insertName;
    BdTextField insertIp;
    BdButton connect;
    BdLabel nameL;
    BdLabel ipL;
    BdRadioButton red;
    BdRadioButton blue;
    BdRadioGroup selectColor;
    BdLabel yourHP;
    BdLabel enemyHP;

    Form();

    void onClick(BdComponent* v, int x, int y);
    void onTick(BdTimer* timer);
    void onSelectedItem(BdMessageList* ml);
    void onChoosing(BdRadioButton* b);

    void game_draw();
    void sendEvent(TxEvent* event_);
    void recvData();
};

BdList<Map*> maps;
int selectedMap = 0;

int main() {
    Form form;
    frm = &form;
    form.window.show();
}

//-------------------------------------------------------------------------------------//
//------------------------------------Map----------------------------------------------//
//-------------------------------------------------------------------------------------//

Map::Map(char* title_) : BdComponent() {
    strcpy(title, title_);
}

void Map::draw(Surface* s) {
    s->setColor(TX_BLACK);
    s->setFillColor(TX_NULL);
    s->rectangle(0, 0, CELLS_ON_AREA*ONE_CELL, CELLS_ON_AREA*ONE_CELL);
    for(int i = 0; i < CELLS_ON_AREA; i++) {
        for(int j = 0; j < CELLS_ON_AREA; j++) {
            if(map[i][j] == '1') {
                s->setColor(TX_BLACK);
                s->setFillColor(TX_BLACK);
            } else {
                s->setColor(TX_WHITE);
                s->setFillColor(TX_WHITE);
            }
            s->rectangle(i*ONE_CELL, j*ONE_CELL, (i + 1)*ONE_CELL, (j + 1)*ONE_CELL);
        }
    }
}

void Map::process(TxEvent* event) {

}

//-------------------------------------------------------------------------------------//
//------------------------------------Shot---------------------------------------------//
//-------------------------------------------------------------------------------------//

Shot::Shot() : BdComponent() {
    inFlight = false;
}

void Shot::draw(Surface* s) {
    s->setColor(TX_BLACK);
    s->setFillColor(TX_YELLOW);
    s->circle((x + 0.5)*ONE_CELL, (y + 0.5)*ONE_CELL, 0.1*ONE_CELL);
}

//-------------------------------------------------------------------------------------//
//------------------------------------Panzer-------------------------------------------//
//-------------------------------------------------------------------------------------//

Panzer::Panzer(Type type_) : BdComponent(), shot() {
    type = type_;
    health = 100;
    holdKey = false;
}

void Panzer::shotMade() {
    if(!shot.inFlight) {
        getWindow()->container.addChild(&shot);
        shot.inFlight = true;
    }
}

void Panzer::draw(Surface* s) {
    if(health > 0) {
        s->setColor(TX_BLACK);
        s->setFillColor(color);
    } else {
        s->setColor(TX_BLACK);
        s->setFillColor(TX_GRAY);
    }
    switch(napr) {
    case UP :
        s->rectangle(x*ONE_CELL, y*ONE_CELL, (x + 0.2)*ONE_CELL, (y + 1)*ONE_CELL);
        s->rectangle((x + 0.8)*ONE_CELL, y*ONE_CELL, (x + 1)*ONE_CELL, (y + 1)*ONE_CELL);
        s->rectangle((x + 0.8)*ONE_CELL, (y + 0.9)*ONE_CELL, (x + 0.2)*ONE_CELL, (y + 0.2)*ONE_CELL);
        s->rectangle((x + 0.6)*ONE_CELL, (y + 0.6)*ONE_CELL, (x + 0.4)*ONE_CELL, y*ONE_CELL);
        s->circle((x + 0.5)*ONE_CELL, (y + 0.7)*ONE_CELL, 0.3*ONE_CELL);
        break;
    case DOWN :
        s->rectangle(x*ONE_CELL, y*ONE_CELL, (x + 0.2)*ONE_CELL, (y + 1)*ONE_CELL);
        s->rectangle((x + 0.8)*ONE_CELL, y*ONE_CELL, (x + 1)*ONE_CELL, (y + 1)*ONE_CELL);
        s->rectangle((x + 0.2)*ONE_CELL, (y + 0.1)*ONE_CELL, (x + 0.8)*ONE_CELL, (y + 0.8)*ONE_CELL);
        s->rectangle((x + 0.4)*ONE_CELL, (y + 0.4)*ONE_CELL, (x + 0.6)*ONE_CELL, (y + 1)*ONE_CELL);
        s->circle((x + 0.5)*ONE_CELL, (y + 0.3)*ONE_CELL, 0.3*ONE_CELL);
        break;
    case RIGHT :
        s->rectangle(x*ONE_CELL, y*ONE_CELL, (x + 1)*ONE_CELL, (y + 0.2)*ONE_CELL);
        s->rectangle(x*ONE_CELL, (y + 0.8)*ONE_CELL, (x + 1)*ONE_CELL, (y + 1)*ONE_CELL);
        s->rectangle((x + 0.1)*ONE_CELL, (y + 0.2)*ONE_CELL, (x + 0.8)*ONE_CELL, (y + 0.8)*ONE_CELL);
        s->rectangle((x + 0.4)*ONE_CELL, (y + 0.4)*ONE_CELL, (x + 1)*ONE_CELL, (y + 0.6)*ONE_CELL);
        s->circle((x + 0.3)*ONE_CELL, (y + 0.5)*ONE_CELL, 0.3*ONE_CELL);
        break;
    case LEFT :
        s->rectangle(x*ONE_CELL, y*ONE_CELL, (x + 1)*ONE_CELL, (y + 0.2)*ONE_CELL);
        s->rectangle(x*ONE_CELL, (y + 0.8)*ONE_CELL, (x + 1)*ONE_CELL, (y + 1)*ONE_CELL);
        s->rectangle((x + 0.9)*ONE_CELL, (y + 0.8)*ONE_CELL, (x + 0.2)*ONE_CELL, (y + 0.2)*ONE_CELL);
        s->rectangle((x + 0.6)*ONE_CELL, (y + 0.6)*ONE_CELL, x*ONE_CELL, (y + 0.4)*ONE_CELL);
        s->circle((x + 0.7)*ONE_CELL, (y + 0.5)*ONE_CELL, 0.3*ONE_CELL);
        break;
    }
}

void Panzer::process(TxEvent* event) {
    /*if(type == YOU && event->type == TxEvent::TX_KEY_PRESS && !holdKey) {
        frm->sendEvent(event);
        int code_ = event->code;
        *event = getNextEvent(100);
        while(event->type == TxEvent::TX_KEY_PRESS && event->code == code_) {
            *event = getNextEvent(1);
            frm->recvData();
        }
    }*/
    if(type == YOU && event->type == TxEvent::TX_KEY_PRESS && !holdKey) {
        frm->sendEvent(event);
        holdKey = true;
//        code_ = event->code;
    }
    if(type == YOU && event->type == TxEvent::TX_KEY_RELEASE && holdKey) {
        holdKey = false;
    }
}

//-------------------------------------------------------------------------------------//
//-------------------------------------Form--------------------------------------------//
//-------------------------------------------------------------------------------------//

Form::Form() : window(800, 650),
                you(Panzer::YOU), yourHP(650, 0, 150, 30, "you 100"),
                enemy(Panzer::ENEMY), enemyHP(650, 50, 150, 30, "enemy 100"),
                mapsList(650, 0, 150, 650, BdMessageList::LIST),
                insertIp(103, 73, 500, 70), insertName(103, 3, 700, 70),
                connect(600, 70, 200, 70, "connect", true),
                nameL(0, 0, 100, 70, "NAME:"), ipL(0, 70, 100, 70, "IP:"),
                red(250, 150, 300, 50, "RED  color"), blue(250, 200, 300, 50, "BLUE color"), selectColor() {
    window.container.addChild(&insertIp);
    insertIp.setText("localhost");
    window.container.addChild(&insertName);
    window.container.addChild(&nameL);
    window.container.addChild(&ipL);
    window.container.addChild(&connect);
    connect.addListener(this);
    window.container.addChild(&red);
    red.addRBListener(this);
    window.container.addChild(&blue);
    blue.addRBListener(this);
    selectColor.addRadBut(&red);
    selectColor.addRadBut(&blue);

    window.container.addChild(&counter);
    counter.setPeriodMillis(3);
    counter.addListener(this);

    window.container.addChild(&bulletFlight);
    bulletFlight.setPeriodMillis(25);
    bulletFlight.addListener(this);

    you.color = NULL;
    enemy.color = NULL;

    mapsReceived = false;
    waitingMaps = false;
}

void Form::onClick(BdComponent* v, int x, int y) {
    if(v == &connect && !connected && insertIp.getSymbolCount() > 0 && insertName.getSymbolCount() > 0 && you.color != NULL) {
        char ip[10];
        insertIp.getText(ip);
        if(p.connect(ip, 2215, -1)) {
            connected = true;
            p.setMessRecvTimeout(RECV_TIMEOUT);
            insertName.getText(you.name);
            char recv[50];
            if(you.color == TX_RED) {
                sprintf(recv, "%s;r", you.name);
                p.sendMess(recv, strlen(recv) + 1, RECV_TIMEOUT);
                enemy.color = TX_BLUE;
            } else {
                sprintf(recv, "%s;b", you.name);
                p.sendMess(recv, strlen(recv) + 1, RECV_TIMEOUT);
                enemy.color = TX_RED;
            }

            window.container.removeChild(&insertIp);
            window.container.removeChild(&insertName);
            window.container.removeChild(&nameL);
            window.container.removeChild(&ipL);
            window.container.removeChild(&connect);
            window.container.removeChild(&red);
            window.container.removeChild(&blue);

            window.container.addChild(&yourHP);
            window.container.addChild(&enemyHP);

//            window.container.addChild(&mapsList);
            counter.start();
            bulletFlight.start();

            mapsReceived = false;
            waitingMaps = true;
        }
    }
}

void Form::onTick(BdTimer* timer) {
    if(timer == &counter && mapsReceived && connected) {
        window.setFocusOwner(&you);
        recvData();
    }
    if(waitingMaps && connected) {
        int messLen = p.recvMess(buf, SIZE_, &left_);
        if(messLen > 0) {
            buf[messLen] = 0;
            int count = atoi(buf);
            for(int i = 0; i < count; i++) {
                int messLen = p.recvMess(buf, SIZE_, &left_);
                if(messLen > 0) {
                    buf[messLen] = 0;
                    Map* m = new Map(buf);
                    mapsList.addItem(buf);
                    for(int i = 0; i < 13; i++) {
                        int messLen = p.recvMess(buf, SIZE_, &left_);
                        if(messLen > 0) {
                            strcpy(m->map[i], buf);
                        }
                    }
                    maps.add(m);
                }
            }
            recvData();
            window.container.addChild(maps.getValue(selectedMap));
            window.container.addChild(&you);
            window.container.addChild(&enemy);
            mapsReceived = true;
            waitingMaps = false;
        }
    }
}

void Form::onSelectedItem(BdMessageList* ml) {

}

void Form::onChoosing(BdRadioButton* b) {
    if(b == &red) {
        you.color = TX_RED;
    }
    if(b == &blue) {
        you.color = TX_BLUE;
    }
}

void Form::sendEvent(TxEvent* event_) {
    char recv[10];
    sprintf(recv, "0%d", event_->code);
    p.sendMess(recv, strlen(recv) + 1, RECV_TIMEOUT);
}

void Form::recvData() {
    int messLen = p.recvMess(buf, SIZE_, &left_);
    if(messLen > 0 && you.health > 0) {
        char youX[3];
        char youY[3];
        char youBulX[3];
        char youBulY[3];
        char youHp[3];
        char enemyX[3];
        char enemyY[3];
        char enemyBulX[3];
        char enemyBulY[3];
        char enemyHp[3];
        buf[messLen] = 0;
        int i = 0;

        //---------------------------//
        //----------for you----------//
        //---------------------------//

        for(int j = 0; buf[i] != ';'; j++) {
            youX[j] = buf[i];
            youX[j + 1] = 0;
            i++;
        }

        you.x = atoi(youX);

        i++;
        for(int j = 0; buf[i] != ';'; j++) {
            youY[j] = buf[i];
            youY[j + 1] = 0;
            i++;
        }

        you.y = atoi(youY);

        i++;
        switch((Panzer::Napr)(buf[i] - '0')) {
        case Panzer::UP :
            you.napr = Panzer::UP;
            break;
        case Panzer::DOWN :
            you.napr = Panzer::DOWN;
            break;
        case Panzer::RIGHT :
            you.napr = Panzer::RIGHT;
            break;
        case Panzer::LEFT :
            you.napr = Panzer::LEFT;
            break;
        }
        i += 2;
        for(int j = 0; buf[i] != ';'; j++) {
            if(buf[i] == '-') {
                i++;
                window.container.removeChild(&you.shot);
                break;
            } else {
                you.shotMade();
                youBulX[j] = buf[i];
                youBulX[j + 1] = 0;
                i++;
            }
        }
        i++;
        for(int j = 0; buf[i] != ';'; j++) {
            if(buf[i] == '-') {
                i++;
                break;
            } else {
                youBulY[j] = buf[i];
                youBulY[j + 1] = 0;
                i++;
            }
        }

        if(you.shot.inFlight) {
            you.shot.x = atoi(youBulX);
            you.shot.y = atoi(youBulY);
        }

        i++;
        for(int j = 0; buf[i] != ';'; j++) {
            youHp[j] = buf[i];
            youHp[j + 1] = 0;
            i++;
        }

        you.health = atoi(youHp);

        i++;

        sprintf(youHp, " you %d ", you.health);
        yourHP.setText(youHp);

        //---------------------------//
        //---------for enemy---------//
        //---------------------------//
        for(int j = 0; buf[i] != ';'; j++) {
            enemyX[j] = buf[i];
            enemyX[j + 1] = 0;
            i++;
        }

        enemy.x = atoi(enemyX);

        i++;
        for(int j = 0; buf[i] != ';'; j++) {
            enemyY[j] = buf[i];
            enemyY[j - 1] = 0;
            i++;
        }

        enemy.y = atoi(enemyY);

        i++;
        switch((Panzer::Napr)(buf[i] - '0')) {
        case Panzer::UP :
            enemy.napr = Panzer::UP;
            break;
        case Panzer::DOWN :
            enemy.napr = Panzer::DOWN;
            break;
        case Panzer::RIGHT :
            enemy.napr = Panzer::RIGHT;
            break;
        case Panzer::LEFT :
            enemy.napr = Panzer::LEFT;
            break;
        }
        i += 2;
        for(int j = 0; buf[i] != ';'; j++) {
            if(buf[i] == '-') {
                i++;
                window.container.removeChild(&enemy.shot);
                break;
            } else {
                enemy.shotMade();
                enemyBulX[j] = buf[i];
                enemyBulX[j + 1] = 0;
                i++;
            }
        }
        i++;
        for(int j = 0; buf[i] != ';'; j++) {
            if(buf[i] == '-') {
                i++;
                break;
            } else {
                enemyBulY[j] = buf[i];
                enemyBulY[j + 1] = 0;
                i++;
            }
        }

        if(enemy.shot.inFlight) {
            enemy.shot.x = atoi(enemyBulX);
            enemy.shot.y = atoi(enemyBulY);
        }

        i++;
        for(int j = 0; buf[i] != ';'; j++) {
            enemyHp[j] = buf[i];
            enemyHp[j + 1] = 0;
            i++;
        }

        enemy.health = atoi(enemyHp);

        sprintf(enemyHp, "enemy %d", enemy.health);
        enemyHP.setText(enemyHp);
//        printf("%d - %d - %d - %d - %d - %d - %d - %d - %d - %d\n", you.x, you.y, you.shot.x, you.shot.y, you.health, enemy.x, enemy.y, enemy.shot.x, enemy.shot.y, enemy.health);
    }
}









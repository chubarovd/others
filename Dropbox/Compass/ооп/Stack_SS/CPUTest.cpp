/*
 * 
 */
 
#include "TXLib.h"
#define _CPU_DEBUG_MODE_
#include "CPUOper.h"
#include "CPUOper.cpp"
 
T_CPU cpu;

int main()
{
/*
	factorial.cpp
----------------------	
	int factorial(int i)
	{
		int f; f = 1;
		while (n > 1) 
		{
			f *= n;
			n--;
		}
		return f;
	}
*/
	T_Program factorial;
//	int factorial(int n)
//	int f; f = 1;
	factorial.Add(T_Command(T_Command::cdNewInt));     // f
	factorial.Add(T_Command(T_Command::cdPushVar, 0));
	factorial.Add(T_Command(T_Command::cdPush, 1));
	factorial.Add(T_Command(T_Command::cdAssign));
//	label 1: -- 4
//	if (n <= 1) go_to label 2
	factorial.Add(T_Command(T_Command::cdPushVal,-1)); // n
	factorial.Add(T_Command(T_Command::cdPush, 1));
	factorial.Add(T_Command(T_Command::cdOper, T_Oper::opLE));
	factorial.Add(T_Command(T_Command::cdIfGoTo, 14)); // label 2
//	f *= n
	factorial.Add(T_Command(T_Command::cdPushVar, 0)); // f
	factorial.Add(T_Command(T_Command::cdPushVal,-1)); // n
	factorial.Add(T_Command(T_Command::cdOper, T_Oper::opMultiAssign));
//	n--
	factorial.Add(T_Command(T_Command::cdPushVar,-1)); // n
	factorial.Add(T_Command(T_Command::cdOper, T_Oper::opMinusMinus));
//	go_to label 1
	factorial.Add(T_Command(T_Command::cdGoTo, 4));    // label 1
//	label 2: -- 14
//	return
	factorial.Add(T_Command(T_Command::cdPushVal, 0)); // f
	factorial.Add(T_Command(T_Command::cdReturn));
	int func_factorial = cpu.Add(T_Function(factorial, T_Item::tpInt, 1));

/*
	recursive.cpp
----------------------	
	int recursive(int i)
	{
		if (n <= 1) 
		{
			return 1;
		}
		return n * recursive(n - 1);
	}
*/
	T_Program recursive;
//	int recursive(int n)
//	if (n > 1) go_to label 1
	recursive.Add(T_Command(T_Command::cdPushVal,-1)); // n
	recursive.Add(T_Command(T_Command::cdPush, 1));
	recursive.Add(T_Command(T_Command::cdOper, T_Oper::opGT));
	recursive.Add(T_Command(T_Command::cdIfGoTo, 6));  // label 1
//	return 1;
	recursive.Add(T_Command(T_Command::cdPush, 1));
	recursive.Add(T_Command(T_Command::cdReturn));
//	label 1: -- 6
//	return n * recursive(n - 1);
	recursive.Add(T_Command(T_Command::cdPushVal,-1)); // n
	recursive.Add(T_Command(T_Command::cdPushVal,-1)); // n
	recursive.Add(T_Command(T_Command::cdPush, 1));
	recursive.Add(T_Command(T_Command::cdOper, T_Oper::opSub));
	recursive.Add(T_Command(T_Command::cdFunction, 1)); // recursive
	recursive.Add(T_Command(T_Command::cdOper, T_Oper::opMulti));
	recursive.Add(T_Command(T_Command::cdReturn));
	int func_recursive = cpu.Add(T_Function(recursive, T_Item::tpInt, 1));

/*
	array.cpp
----------------------	
	void array(int arr[], int size)
	{
		int i = 0;
		while (i < size) 
		{
			arr[i] = i++;
		}
		return;
	}
*/
		
	T_Program array;	
//	void array(int arr[], int size)
//	int i = 0; 
	array.Add(T_Command(T_Command::cdNewInt));     // int i;
	array.Add(T_Command(T_Command::cdPushVar, 0)); // i = 0; 
	array.Add(T_Command(T_Command::cdPush, 0)); 
	array.Add(T_Command(T_Command::cdAssign)); 
//	label 1: -- 4
//	if (i >= 5) go_to label 2
	array.Add(T_Command(T_Command::cdPushVal,0)); // i
	array.Add(T_Command(T_Command::cdPushVal,-1));// size
	array.Add(T_Command(T_Command::cdOper, T_Oper::opGE));
	array.Add(T_Command(T_Command::cdIfGoTo, 16));
//	arr[i] = i++;
	array.Add(T_Command(T_Command::cdPushVar,-2));// arr
	array.Add(T_Command(T_Command::cdPushVal,0)); // i
	array.Add(T_Command(T_Command::cdIndex));
	array.Add(T_Command(T_Command::cdPushVal,0)); // i++
	array.Add(T_Command(T_Command::cdPushVar,0)); 
	array.Add(T_Command(T_Command::cdOper, T_Oper::opPlusPlus));
	array.Add(T_Command(T_Command::cdAssign)); 
//	go_to label 1
	array.Add(T_Command(T_Command::cdGoTo, 4));
//	label 2: -- 16
//	return
	array.Add(T_Command(T_Command::cdReturn));
	int func_array = cpu.Add(T_Function(array, T_Item::tpVoid, 2));
	
/*
	program.cpp                         |    memory
----------------------	                |   --------
	int i = 5;            // global     |    int i;      - 0
----------------------	                |    int f;      - 1
	void call_factorial(int &f)         |    int r;      - 2
	{                                   |    int arr[5]; - 3
		f = factorial(i); // i - global
	}
----------------------	
	int call_recursive()
	{
		return recursive(4);
	}
----------------------	
	int& xref()
	{ 
		return i;
	}
----------------------	
	int f;
	call_factorial(f);
	int r = call_recursive();
	int arr[5];
	array(arr, 5);
	xref()++;
*/
	T_Program program;	
	
//	void call_factorial(int &f)
	int call_factorial = program.low();
//	f = factorial(i);
	program.Add(T_Command(T_Command::cdPushVar,-1));  // f
	program.Add(T_Command(T_Command::cdGlobVal, 0));  // i - global
	program.Add(T_Command(T_Command::cdFunction, func_factorial)); // factorial
	program.Add(T_Command(T_Command::cdAssign));
	program.Add(T_Command(T_Command::cdReturn));
	int func_call_factorial = cpu.Add(T_Function(program, T_Item::tpVoid, 1, call_factorial));

//	int call_recursive()
	int call_recursive = program.last() + 1;
//	return recursive(4);
	program.Add(T_Command(T_Command::cdPush, 4));
	program.Add(T_Command(T_Command::cdFunction, func_recursive)); // recursive
	program.Add(T_Command(T_Command::cdReturn));
	int func_call_recursive = cpu.Add(T_Function(program, T_Item::tpInt,  0, call_recursive));

	int start_xref = program.last() + 1;
//	int& xref() 
	program.Add(T_Command(T_Command::cdGlobVar, 0));  // i - global
//	return i;
	program.Add(T_Command(T_Command::cdReturn));
	int func_xref = cpu.Add(T_Function(program, T_Item::tpAddress,  0, start_xref));

//	void main()
	int start_main = program.last() + 1;
//	int i = 5;
	program.Add(T_Command(T_Command::cdNewInt));      // i
	program.Add(T_Command(T_Command::cdPushVar, 0));  // i
	program.Add(T_Command(T_Command::cdPush, 5)); 
	program.Add(T_Command(T_Command::cdAssign));
//	int f;
	program.Add(T_Command(T_Command::cdNewInt));      // f
	program.Add(T_Command(T_Command::cdPushVar, 1));  // f
	program.Add(T_Command(T_Command::cdFunction, func_call_factorial)); // call_factorial
//	int r;
	program.Add(T_Command(T_Command::cdNewInt));      // r
	program.Add(T_Command(T_Command::cdPushVar, 2));  // r
	program.Add(T_Command(T_Command::cdFunction, func_call_recursive)); // call_recursive
	program.Add(T_Command(T_Command::cdAssign));
//	int arr[5];
	program.Add(T_Command(T_Command::cdNewInt, 5));  // int arr[5];
//	array(arr, 5);
	program.Add(T_Command(T_Command::cdPushVar, 3)); // arr 
	program.Add(T_Command(T_Command::cdPush, 5));    // size
	program.Add(T_Command(T_Command::cdFunction, func_array)); // array
//	xref()++;
	program.Add(T_Command(T_Command::cdFunction, func_xref));  // xref
	program.Add(T_Command(T_Command::cdOper, T_Oper::opPlusPlus));
	program.Add(T_Command(T_Command::cdPrint));
	program.Add(T_Command(T_Command::cdReturn));

	cpu.Exec(program,start_main);
	cpu.print();
	
	return 0;
}

#ifdef _CPU_DEBUG_MODE
void _debug_return_false_()
{
	static bool debug = true;
	if (debug) 
	{
		debug = false;
		printf("---- debug begin ----\n");
		cpu.print();
		T_OperTable::operation.print();
		printf("---- debug end ----\n");
	}
}
#endif

#include <stdlib.h>
#include <TXLib.h>

struct t_Fig {
	int x; 
	int y;
	t_Fig () : x(0), y(0) {}
	t_Fig (int x_, int y_) { x = x_; y = y_; } 
	t_Fig (const t_Fig& f) : x(f.x), y(f.y) {}
	void draw() {};
	void draw(int x_, int y_) {};
};

struct t_Rectangle:t_Fig {
	int h; int v;
	t_Rectangle() : h(20), v(10) {}
	t_Rectangle (int h_, int v_) { h = h_; v = v_; }
	void draw() { txRectangle(x, y, x + h, y + v); };
	void draw(int x_, int y_) { txRectangle(x_ + x, y_ + y, x_ + x + h, y_ + y + v); };
};

struct t_Circle:t_Fig {
	int r;
	t_Circle() : r(10) {}
	t_Circle (const t_Fig& f, int r_) : t_Fig(f), r(r_) {}
	t_Circle (int x_, int y_, int r_) : t_Fig(x_, y_), r(r_) {}
	t_Circle (const t_Circle& f) : t_Fig(f), r(f.r) {}
	void draw() { txCircle(x, y, r); };
	void draw(int x_, int y_) { txCircle(x_ + x, y_ + y, r); };
};

void dump(void *addr, int size);

t_Fig          k(84, 48);
t_Rectangle    m(100, 50);
t_Circle       p(50, 70, 40);

int main(int argc_, char **argv_){
	txCreateWindow(700, 700);
	
	dump(&k, sizeof(k));
	dump(&m, sizeof(m));
	
	for(;;);
}
/*
void t_Rectangle::draw(int x_, int y_){
	txRectangle(x_, y_, (x_ + h_), (y_ + v_));
}

void t_Circle::draw(int x_, int y_){
	txCircle(x_, y_, r);
}
*/
void dump(void *addr, int size){
	for (int i = 1; i <= size; i++) {
		printf("%02x", ((unsigned char*)addr)[i - 1]);
		if (!(i % 4)) printf(" ");
	}
	printf("\n");
}

/*
 * 
 */
 
#include "TXLib.h"
#include "CPUOper.cpp"

using namespace std;
 
struct T_Source
{
	enum T_Sym { slNone, slDigit, slAlpha, slOper, slSeparator, slSpace, slEol, slError, slEof };
	const char* text;
	int index, length;
	T_Source() : text(NULL), index(0), length(0) {}
	T_Source(const char* t) : text(t), index(0), length(MAX_LENGTH) {}
	T_Sym Sym(int &sym);
	T_Sym GetSym(int &sym);
	int  Char(int pos) const { return text[pos]; }
	int  Pos() const   { return index; }
	void Back()        { if (index && text) index--; }
	void Reset()       { index = 0; }
	void Reset(const char* t){ text = t; length = MAX_LENGTH; index = 0; }

	static const int MAX_LENGTH = 10000000;
}; 

struct T_Scan;
struct T_Lexeme
{
	enum T_Group { grNone, grNumber, grIdent, grOper, grSeparator, grSpace, grError, grEof };
	enum T_Type  { lxNone, lxInteger, lxNumber, lxIdent, 
				   lxAdd, lxSub, lxMulti, lxDiv, 
				   lxPoint, lxComma, lxSemicolon, lxColon,
				   lxSpace, lxEol, 
				   lxError, lxEof };
	T_Group group;
	T_Type  type;
	int pos, len;
	int    value;
	double number;
	T_Lexeme() : group(grNone), type(lxNone), pos(0), len(0) {}
	void print(const T_Scan *scan = NULL) const;
}; 

struct T_Scan
{
	T_Source *source;
	T_Scan(T_Source *s) : source(s) {}
	T_Lexeme::T_Group Lex(T_Lexeme &l);
	T_Source::T_Sym GetSym(int &sym) const { return source->GetSym(sym); }
	int  Char(int pos) const     { return source->Char(pos); }
	int  Pos()  const            { return source->Pos(); }
	void Back() const            { source->Back(); }
}; 

// --------------------------- T_Source ---------------------------
T_Source::T_Sym T_Source::Sym(int &sym)
{
	if (!text || (index >= length) || (!text[index]))
	{
		length = index; sym = 0;
		return slEof;
	}
	sym = text[index];
	if (sym == '\n') return slEol;
	if ((sym == ' ') || (sym == '\t')) return slSpace;
	if ((sym >= '0') && (sym <= '9')) return slDigit;
	if ((sym >= 'a') && (sym <= 'z') || (sym >= 'A') && (sym <= 'Z') || (sym == '_')) return slAlpha;
	if ((sym == '.') || (sym == ',') || (sym == ';') || (sym == ':')) return slSeparator;
	if ((sym == '+') || (sym == '-') || (sym == '*') || (sym == '/')) return slOper;
	return slError;
}
 
T_Source::T_Sym T_Source::GetSym(int &sym)
{
	T_Sym ret = Sym(sym);
	index++;
	return ret;
}

// --------------------------- T_Lexeme ---------------------------
void T_Lexeme::print(const T_Scan *scan) const
{
	printf("group=%02d, type=%02d, pos=%03d, len=%02d", group,  type, pos, len);
	if (type == T_Lexeme::lxInteger) printf(", value=%d", value);
	if (type == T_Lexeme::lxNumber)  printf(", number=%lf", number);
	if (scan && len) 
	{
		printf(", text=");
		for (int i = 0; i < len; i++) printf("%c", scan->Char(pos + i));
	}
	printf("\n");
}

// --------------------------- T_Scan ---------------------------
T_Lexeme::T_Group T_Scan::Lex(T_Lexeme &l)
{
	int sym;
	l.group = T_Lexeme::grError;
	l.type  = T_Lexeme::lxError;
	l.pos = Pos();
	l.len = 0;
	T_Source::T_Sym type = GetSym(sym);
	
	if(type == T_Source::slEof){
		return l.group = l.grEof;
	}
	
	if(type == T_Source::slDigit){
		l.value = 0;
		do{
			l.len++;
			l.value *= 10;
			l.value += sym - '0';
			type = source -> GetSym(sym);
		}while(type == T_Source::slDigit);
		
		if(type != T_Source::slSeparator){
			l.len++;
			l.type = l.lxInteger;
		}else{
			l.len++;
			type = source -> GetSym(sym);
			l.number = l.value;
			double step = 1;
			do{
				l.len++;
				step *= 10;
				l.number += (sym - '0')/step;
				type = source -> GetSym(sym);
			}while(type == T_Source::slDigit);
			l.type = l.lxNumber;
		}
		source -> Back();
		return l.group = l.grNumber;
	}
	
	if(type == T_Source::slOper){
		
		if(sym == '+'){
			l.type = l.lxAdd;
		}
		
		if(sym == '-'){
			l.type = l.lxSub;
		}
		
		if(sym == '*'){
			l.type = l.lxMulti;
		}
		
		if(sym == '/'){
			l.type = l.lxDiv;
		}
		l.len++;
		return l.group = l.grOper;
	}
	
	if(type == T_Source::slAlpha){
		do{
			l.len++;
			type = source -> GetSym(sym);
		}while(type == T_Source::slAlpha);
		source -> Back();
		return l.group = l.grSpace;
	}
	
	//~ if(type == T_Source::slSeparator){
		//~ l.len++;
		//~ return l.group = l.grSeparator;
	//~ }
	
	if(type == T_Source::slSpace){
		do{
			l.len++;
			type = source -> GetSym(sym);
		}while(type == T_Source::slSpace);
		source -> Back();
		return l.group = l.grSpace;
	}
}

// --------------------------- main ---------------------------

//~ T_Source source("55555555555555555555 5.88888888888888888012345678901234");
T_Source source("5.8iiiii=iiii 3-4 1.0+*");
//~ T_Source source("50 70-20 30+*");

void print_source()
{
	int sym;
	while (source.GetSym(sym) != T_Source::slEof)
	{
		printf("%c", sym);
	}
	printf("\n");
}

void calc_source()
{
	T_CPU cpu;
	T_Program program;	
	
	T_Scan   scan(&source);
	T_Lexeme lex;
	T_Lexeme::T_Group group = scan.Lex(lex);
	while (group != T_Lexeme::grEof)
	{
		lex.print(&scan);
		switch (group)
		{
			case T_Lexeme::grNumber : {
				if (lex.type == T_Lexeme::lxInteger) 
				{ 
					program.Add(T_Command(T_Command::cdPush, lex.value));
					program.Add(T_Command(T_Command::cdIntToFloat));
				}
				else if (lex.type == T_Lexeme::lxNumber) 
					program.Add(T_Command(T_Command::cdPushFloat, lex.number));
				} break;
			case T_Lexeme::grOper : {
				T_Oper::T_Name oper = T_Oper::opNone;
				switch (lex.type)
				{
					case T_Lexeme::lxAdd :   oper = T_Oper::opAdd;   break;
					case T_Lexeme::lxSub :   oper = T_Oper::opSub;   break;
					case T_Lexeme::lxMulti : oper = T_Oper::opMulti; break;
					case T_Lexeme::lxDiv :   oper = T_Oper::opDiv;   break;
				}
				program.Add(T_Command(T_Command::cdOper, oper));
				} break;
		}
		group = scan.Lex(lex);
	}
	lex.print(&scan);
		
	program.Add(T_Command(T_Command::cdReturn));
	//program.print();

	cpu.Exec(program,0);
	cpu.print();
}

int main()
{
	print_source();
	
	source.Reset();
	calc_source();
	
	return 0;
}

#ifdef _CPU_DEBUG_MODE
void _debug_return_false_()
{
	static bool debug = true;
	if (debug) 
	{
		debug = false;
		printf("---- debug begin ----\n");
		cpu.print();
		operation.print();
		printf("---- debug end ----\n");
	}
}
#endif

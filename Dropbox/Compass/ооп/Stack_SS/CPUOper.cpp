/*
 * 
 */
 
#include "TXLib.h"
#include "CPUOper.h"

const T_OperTable T_OperTable::operation;
	
const char* T_Command::title[] = {
				  "None", "Push", "Pop", "Oper", "ValOper", "OperVal",  
				  "PushFloat", "FloatToInt", "IntToFloat", 
				  "NewInt", "NewFloat", "PushVar", "PushVal", "GlobVar", "GlobVal", "Assign",
				  "IfGoTo", "ElseGoTo", "GoTo", 
				  "Function", "Print", 
				  "Index", "TwoIndex", "ToVal", 
				  "Return", NULL 
	};
	
// --------------------------- T_Command ---------------------------
void T_Command::print() const 
{ 
	printf("code=%02d, command=%s", code, title[code]);
	if (code == cdPush) printf(", value=%d", operand);
	else if (code == cdPushFloat) printf(", value=%lf", number);
	else if ((code == cdPushVar) || (code == cdPushVal) || (code == cdGlobVar) || (code == cdGlobVal)) printf(", address=%d", operand);
	else if (code == cdPop) printf(", size=%d", operand);
	else if (code == cdFunction) printf(", function=%d", operand);
	else if ((code == cdGoTo) || (code == cdIfGoTo) || (code == cdElseGoTo)) printf(", label=%d", operand);
	else if ((code == cdNewInt) || (code == cdNewFloat)) printf(", size=%d", operand);
	else if ((code == cdIndex) || (code == cdTwoIndex)) printf(", val=%d", operand);
	else if ((code == cdOperVal) || (code == cdOper) || (code == cdValOper)) 
	{ 
		if (T_OperTable::operation.inside(operand))
		{ 
			const T_Oper& oper = T_OperTable::operation.Oper(operand);
			if (oper.title) 
				printf(", oper(%d)=%s", operand, oper.title); 
			else
				printf(", oper=%d", operand);
		} 
		else
			printf(", oper=%d", operand); 
	}
}

// --------------------------- T_Program ---------------------------
void T_Program::Add(const T_Command &command)
{
	commands[count++] = command;
}

void T_Program::print() const
{
	for (int ip = low(); ip <= last(); ip++ ) 
	{
		const T_Command &command = Command(ip);
		printf("ip=%02d, ", ip); command.print(); printf("\n");
	}
	printf("\n");
}

// --------------------------- T_Item ---------------------------
void T_Item::print() const
{
	printf("type=%02d, value= %d, number=%lf\n", type, value, number);
}

int T_Item::compare(const T_Item &funcFloat) const // - -1 '<', 0 - '==', 1 - '>' 
{
	if (isInt())
	{ 
		if (value == funcFloat.value) return 0;
		return (value < funcFloat.value) ? -1 : 1;
	}
	else if (isFloat()) 
	{
		if (number < funcFloat.number) return -1;
		return (number > funcFloat.number) ? 1 : 0;
	} 
	return 0;
}

void T_Item::toInt()
{
	if (isFloat()) 
	{
		type = T_Item::tpInt;
		value = number;
	}
}

void T_Item::toFloat()
{
	if (isInt())
	{ 
		type = T_Item::tpFloat;
		number = value;
	}
}

// --------------------------- T_Stack ---------------------------
void T_Stack::Push(const T_Item &item)
{
	stack[top++] = item;
}

bool T_Stack::Pop(T_Item &item)
{
	if (empty()) return_false;
	item = stack[--top];
	return true;
}

void T_Stack::print() const
{
	for (int i = 0; i < top; i++ ) 
	{
		printf("item=%02d, ", i);
		stack[i].print();
	}
	printf("\n");
}

// --------------------------- T_Oper ---------------------------
void T_Oper::print() const
{
	printf("type=%02d, title=%s\n", type, title ? title : "");
}

// --------------------------- T_OperTable ---------------------------
void T_OperTable::print() const
{
	for (int i = 0; i < count; i++ ) 
	{
		printf("oper=%02d, ", i); table[i].print();
	}
	printf("\n");
}

static double func_FloatMin(double x, double y) { return (x < y) ? x : y; }
static double func_FloatMax(double x, double y) { return (x > y) ? x : y; }
static int func_IntMin(int x, int y)  { return (x < y) ? x : y; }
static int func_IntMax(int x, int y)  { return (x > y) ? x : y; }
static int func_IntSign(int x)        { return (x < 0) ? -1 : ((x > 0) ? 1 : 0); }
static int func_FloatSign(double x)   { return (x < 0) ? -1 : ((x > 0) ? 1 : 0); }
static int func_IntAbs(int x)         { return (x < 0) ? -x : x; }
static double func_FloatAbs(double x) { return (x < 0) ? -x : x; }
static double sqr_float(double x)     { return x * x; }

static void func_IntAddAssign(int &x, int y)    { x += y; }
static void func_IntSubAssign(int &x, int y)    { x -= y; }
static void func_IntMultiAssign(int &x, int y)  { x *= y; }
static void func_IntDivAssign(int &x, int y)    { x /= y; }
static void func_ModAssign(int &x, int y)    { x %= y; }
static void func_AndAssign(int &x, int y)    { x &= y; }
static void func_OrAssign(int &x, int y)     { x |= y; }
static void func_XOrAssign(int &x, int y)    { x ^= y; }
static void func_IntAssign(int &x, int y)    { x = y; }

static void func_FloatAddAssign(double &x, double y)   { x += y; }
static void func_FloatSubAssign(double &x, double y)   { x -= y; }
static void func_FloatMultiAssign(double &x, double y) { x *= y; }
static void func_FloatDivAssign(double &x, double y)   { x /= y; }
static void func_FloatAssign(double &x, double y)      { x = y; }

static int func_Or(int x, int y)             { return x | y; }
static int func_And(int x, int y)            { return x & y; }
static int func_OrOr(int x, int y)           { return x || y; }
static int func_AndAnd(int x, int y)         { return x && y; }
static int func_XOr(int x, int y)            { return x ^ y; }
static int func_Tilda(int x)                 { return ~x; }
static int func_Not(int x)                   { return !x; }

static int func_IntAdd(int x, int y)         { return x + y; }
static int func_IntSub(int x, int y)         { return x - y; }
static int func_IntMulti(int x, int y)       { return x * y; }
static int func_IntDiv(int x, int y)         { return x / y; }
static int func_Mod(int x, int y)            { return x % y; }

static void func_PlusPlus(int &x)            { x++; }
static void func_MinusMinus(int &x)          { x--; }

static double func_FloatAdd(double x, double y)   { return x + y; }
static double func_FloatSub(double x, double y)   { return x - y; }
static double func_FloatMulti(double x, double y) { return x * y; }
static double func_FloatDiv(double x, double y)   { return x / y; }

static int func_IntMinus(int x)                   { return - x; }
static int func_IntPlus(int x)                    { return + x; }
static double func_FloatMinus(double x)           { return - x; }
static double func_FloatPlus(double x)            { return + x; }

static int func_IntEqu(int x, int y)              { return x == y; }
static int func_IntNotEqu(int x, int y)           { return x != y; }
static int func_IntLE(int x, int y)               { return x <= y; }
static int func_IntLT(int x, int y)               { return x <  y; }
static int func_IntGE(int x, int y)               { return x >= y; }
static int func_IntGT(int x, int y)               { return x >  y; }

static int func_FloatEqu(double x, double y)      { return x == y; }
static int func_FloatNotEqu(double x, double y)   { return x != y; }
static int func_FloatLE(double x, double y)       { return x <= y; }
static int func_FloatLT(double x, double y)       { return x <  y; }
static int func_FloatGE(double x, double y)       { return x >= y; }
static int func_FloatGT(double x, double y)       { return x >  y; }

static int func_LShift(int x, int y)              { return x <<  y; } 
static int func_RShift(int x, int y)              { return x >>  y; } 
static void func_LShiftAssign(int& x, int y)      { x <<= y; } 
static void func_RShiftAssign(int& x, int y)      { x >>= y; }

#define ADD_FUNC(NAME, TYPE, FUNC) Add(T_Oper::op##NAME, T_Oper(T_Oper::tf##TYPE,(void*)&FUNC ,NULL,""#NAME""))
#define ADD_OPER(NAME, TYPE) Add(T_Oper::op##NAME, T_Oper(T_Oper::tf##TYPE,(void*)&func_##NAME,NULL,""#NAME""))
#define ADD_JOIN(NAME, TYPE) Add(T_Oper::op##NAME, T_Oper(T_Oper::tf##TYPE,(void*)&func_Int##NAME,(void*)&func_Float##NAME,""#NAME""))

T_OperTable::T_OperTable() : count(T_Oper::_op_end)
{
	ADD_FUNC(Exp,           Float_Float,      exp);
	ADD_FUNC(Log,           Float_Float,      log);
	ADD_FUNC(Log10,         Float_Float,      log10);
	ADD_FUNC(Sin,           Float_Float,      sin);
	ADD_FUNC(Cos,           Float_Float,      cos);	
	ADD_FUNC(Tan,           Float_Float,      tan);
	ADD_FUNC(ASin,          Float_Float,      asin);
	ADD_FUNC(ACos,          Float_Float,      acos);
	ADD_FUNC(ATan,          Float_Float,      atan);
	ADD_FUNC(ATan2,         Float_Float_Float,atan2);	
	ADD_FUNC(Pow,           Float_Float_Float,pow);
	ADD_FUNC(Sqrt,          Float_Float,      sqrt);
	ADD_FUNC(Sqr,           Float_Float,      sqr_float);
	ADD_FUNC(Rand,          Int_Int,          rand);
	
	ADD_JOIN(Abs,           Xxx_Xxx);
	ADD_JOIN(Sign,          Int_Xxx);
	ADD_JOIN(Min,           Xxx_Xxx_Xxx);
	ADD_JOIN(Max,           Xxx_Xxx_Xxx);
	
	ADD_JOIN(Assign,        Void_LXxx_Xxx);
	ADD_JOIN(AddAssign,     Void_LXxx_Xxx);
	ADD_JOIN(SubAssign,     Void_LXxx_Xxx);
	ADD_JOIN(MultiAssign,   Void_LXxx_Xxx); 
	ADD_JOIN(DivAssign,     Void_LXxx_Xxx);
	ADD_OPER(ModAssign,     Void_LInt_Int);
	ADD_OPER(AndAssign,     Void_LInt_Int);
	ADD_OPER(OrAssign,      Void_LInt_Int);
	ADD_OPER(XOrAssign,     Void_LInt_Int);
	
	ADD_OPER(Or,            Int_Int_Int);
	ADD_OPER(And,           Int_Int_Int);
	ADD_OPER(OrOr,          Int_Int_Int);
	ADD_OPER(AndAnd,        Int_Int_Int);
	ADD_OPER(XOr,           Int_Int_Int);
	ADD_OPER(Tilda,         Int_Int);
	ADD_OPER(Not,           Int_Int);
	
	ADD_JOIN(Add,           Xxx_Xxx_Xxx);
	ADD_JOIN(Sub,           Xxx_Xxx_Xxx);
	ADD_JOIN(Multi,         Xxx_Xxx_Xxx);
	ADD_JOIN(Div,           Xxx_Xxx_Xxx);
	ADD_OPER(Mod,           Int_Int_Int);
	
	ADD_OPER(PlusPlus,      Void_LInt);
	ADD_OPER(MinusMinus,    Void_LInt);
	
	ADD_JOIN(Minus,         Xxx_Xxx);
	ADD_JOIN(Plus,          Xxx_Xxx);
	
	ADD_JOIN(Equ,           Int_Xxx_Xxx);
	ADD_JOIN(NotEqu,        Int_Xxx_Xxx);
	ADD_JOIN(LE,            Int_Xxx_Xxx);
	ADD_JOIN(LT,            Int_Xxx_Xxx);
	ADD_JOIN(GE,            Int_Xxx_Xxx);
	ADD_JOIN(GT,            Int_Xxx_Xxx);

	ADD_OPER(LShift,        Int_Int_Int); 
	ADD_OPER(RShift,        Int_Int_Int);
	ADD_OPER(LShiftAssign,  Void_LInt_Int); 
	ADD_OPER(RShiftAssign,  Void_LInt_Int);

}

// --------------------------- T_Function ---------------------------
void T_Function::print() const
{
	printf("result=%02d, param=%d, start=%d\n", result, param, start);
}

// --------------------------- T_FunctionTable ---------------------------
void T_FunctionTable::print() const
{
	for (int i = 0; i < count; i++ ) 
	{
		printf("func=%02d, ", i); table[i].print();
	}
	printf("\n");
}

// --------------------------- T_Frame ---------------------------
void T_Frame::print() const
{
	printf("offset=%d, ip_ret=%d\n", offset, ip_ret);
}

// --------------------------- T_FrameStack ---------------------------
void T_FrameStack::print() const
{
	for (int i = 0; i < top; i++ ) 
	{
		printf("frame=%02d, ", i); stack[i].print();
	}
	printf("\n");
}

// --------------------------- T_CPU ---------------------------
void T_CPU::print() const
{ 
	printf("\nip=%02d\n", ip); stack.print(); frames.print(); functions.print(); 
}  

bool T_CPU::Exec(const T_Oper &oper, int mode)
{
	switch (oper.type)
	{
		case T_Oper::tfNone  : {
			} break;
		case T_Oper::tfInt_Int  : {
			T_Item operand;
			if (!stack.Pop(operand) || !operand.isInt()) return_false;
			int (*f)(int o) = (int(*)(int))oper.func;
			stack.Push(T_Item(f(operand.value)));
			} break;
		case T_Oper::tfInt_Int_Int  : {
			T_Item operand_one, operand_two;
			if (!stack.Pop(operand_two) || !stack.Pop(operand_one)) return_false;
			if (!operand_one.isInt() || !operand_two.isInt()) return_false;
			int (*f)(int o1, int o2) = (int(*)(int,int))oper.func;
			stack.Push(T_Item(f(operand_one.value, operand_two.value)));
			} break;
		case T_Oper::tfInt_Float  : {
			T_Item operand;
			if (!stack.Pop(operand) || !operand.isValue()) return_false;
			int (*f)(double o) = (int(*)(double))oper.func;
			operand.toFloat();
			stack.Push(T_Item(f(operand.number)));
			} break;
		case T_Oper::tfFloat_Float  : {
			T_Item operand;
			if (!stack.Pop(operand) || !operand.isValue()) return_false;
			double (*f)(double o) = (double(*)(double))oper.func;
			operand.toFloat();
			stack.Push(T_Item(f(operand.number)));
			} break;
		case T_Oper::tfFloat_Float_Float  : {
			T_Item operand_one, operand_two;
			if (!stack.Pop(operand_two) || !stack.Pop(operand_one)) return_false;
			if (!operand_one.isValue() || !operand_two.isValue()) return_false;
			double (*f)(double o1, double o2) = (double(*)(double,double))oper.func;
			operand_one.toFloat(); operand_two.toFloat();			
			stack.Push(T_Item(f(operand_one.number, operand_two.number)));
			} break;
		case T_Oper::tfVoid_LInt_Int  : {
			T_Item operand, address;
			if (!stack.Pop(operand) || !operand.isInt() || !stack.Pop(address) || !address.isAddress()) return_false;
			T_Item& variable = stack.Item(address.value);
			if (!variable.isInt()) return_false;
			void (*f)(int& o1, int o2) = (void(*)(int&,int))oper.func;
			f(variable.value, operand.value);
			if (mode < 0) 
			{
				stack.Push(variable);
			}
			} break;
		case T_Oper::tfVoid_LInt  : {
			T_Item address, value;
			if (!stack.Pop(address) || !address.isAddress()) return_false;
			T_Item& variable = stack.Item(address.value);
			if (!variable.isInt()) return_false;
			if (mode > 0) 
			{
				value = variable;
			}
			void (*f)(int& o1) = (void(*)(int&))oper.func;
			f(variable.value);
			if (mode < 0) 
			{
				stack.Push(variable);
			}
			else if (mode > 0) 
			{
				stack.Push(value);
			}
			} break;
		case T_Oper::tfInt_Float_Float  : {
			T_Item operand_one, operand_two;
			if (!stack.Pop(operand_two) || !stack.Pop(operand_one)) return_false;
			if (!operand_one.isValue() || !operand_two.isValue()) return_false;
			int (*f)(double o1, double o2) = (int(*)(double,double))oper.func;
			operand_one.toFloat(); operand_two.toFloat();			
			stack.Push(T_Item(f(operand_one.number, operand_two.number)));
			} break;
		case T_Oper::tfVoid_LFloat_Float : {
			T_Item operand, address;
			if (!stack.Pop(operand) || !operand.isValue() || !stack.Pop(address) || !address.isAddress()) return_false;
			T_Item& variable = stack.Item(address.value);
			if (!variable.isFloat()) return_false;
			void (*f)(double& o1, double o2) = (void(*)(double&,double))oper.func;
			operand.toFloat();
			f(variable.number, operand.number);
			} break;
		case T_Oper::tfXxx_Xxx : {
			T_Item operand;
			if (!stack.Pop(operand)) return_false;
			if (operand.isInt())
			{
				int (*f)(int o) = (int(*)(int))oper.func;
				operand.value = f(operand.value);
			}
			else if (operand.isFloat())
			{
				int (*f)(double o) = (int(*)(double))oper.funcFloat;
				operand.number = f(operand.number);
			} 
			else
				return_false;
			stack.Push(operand);
			} break;
		case T_Oper::tfXxx_Xxx_Xxx : {
			T_Item operand_one, operand_two;
			if (!stack.Pop(operand_two) || !stack.Pop(operand_one)) return_false;
			if (!operand_one.isValue() || !operand_two.isValue()) return_false;
			if (operand_one.isInt() && operand_two.isInt()) 
			{
				int (*f)(int o1, int o2) = (int(*)(int,int))oper.func;
				stack.Push(T_Item(f(operand_one.value, operand_two.value)));
			}
			else
			{	
				operand_one.toFloat(); operand_two.toFloat();			
				double (*f)(double o1, double o2) = (double(*)(double,double))oper.funcFloat;
				stack.Push(T_Item(f(operand_one.number, operand_two.number)));
			}
			} break;
		case T_Oper::tfVoid_LXxx_Xxx : {
			T_Item operand, address;
			if (!stack.Pop(operand) || !operand.isValue() || !stack.Pop(address) || !address.isAddress()) return_false;
			T_Item& variable = stack.Item(address.value);
			if (variable.isInt()) 
			{
				void (*f)(int& o1, int o2) = (void(*)(int&,int))oper.func;
				operand.toInt();
				f(variable.value, operand.value);
			}
			else if (variable.isFloat())
			{
				void (*f)(double& o1, double o2) = (void(*)(double&,double))oper.funcFloat;
				operand.toFloat();
				f(variable.number, operand.number);
			}
			else 
				return_false;
			if (mode < 0) 
			{
				stack.Push(variable);
			}
			} break;
		case T_Oper::tfInt_Xxx : {
			T_Item operand, result(T_Item::tpInt, 0);
			if (!stack.Pop(operand)) return_false;
			if (operand.isInt())
			{
				int (*f)(int o) = (int(*)(int))oper.func;
				result.value = f(operand.value);
			}
			else if (operand.isFloat())
			{
				int (*f)(double o) = (int(*)(double))oper.funcFloat;
				result.value = f(operand.number);
			}
			else 
				return_false;
			stack.Push(result);
			} break;
		case T_Oper::tfInt_Xxx_Xxx : { 
			T_Item operand_one, operand_two;
			if (!stack.Pop(operand_two) || !stack.Pop(operand_one)) return_false;
			if (!operand_one.isValue() || !operand_two.isValue()) return_false;
			if (operand_one.isInt() && operand_two.isInt()) 
			{
				int (*f)(int o1, int o2) = (int(*)(int,int))oper.func;
				stack.Push(T_Item(f(operand_one.value, operand_two.value)));
			}
			else
			{	
				int (*f)(double o1, double o2) = (int(*)(double,double))oper.funcFloat;
				operand_one.toFloat(); operand_two.toFloat();			
				stack.Push(T_Item(f(operand_one.number, operand_two.number)));
			}
			} break;
		default : {
			return_false;
			} break;

	}
	return true;
}

bool T_CPU::Exec(const T_Command &command)
{
	int offset = 0;
	switch (command.code)
	{
		case T_Command::cdPush  : {
			stack.Push(T_Item(command.operand));
			} break;
		case T_Command::cdPushFloat  : {
			stack.Push(T_Item(command.number));
			} break;
		case T_Command::cdPop   : {
			T_Item operand;
			int size = command.operand;
			if (size <= 0) size = 1;
			while (size--) if (!stack.Pop(operand)) return_false;
			} break;
		case T_Command::cdValOper   : {
			if (!T_OperTable::operation.inside(command.operand)) return_false;
			if (!Exec(T_OperTable::operation.Oper(command.operand), +1)) return_false;
			} break;
		case T_Command::cdOper   : {
			if (!T_OperTable::operation.inside(command.operand)) return_false;
			if (!Exec(T_OperTable::operation.Oper(command.operand))) return_false;
			} break;
		case T_Command::cdOperVal   : {
			if (!T_OperTable::operation.inside(command.operand)) return_false;
			if (!Exec(T_OperTable::operation.Oper(command.operand), -1)) return_false;
			} break;
		case T_Command::cdIntToFloat : {
			T_Item operand_one;
			if (!stack.Pop(operand_one)) return_false;
			if (operand_one.isInt())
			{
				operand_one.number = operand_one.value;
				operand_one.type = T_Item::tpFloat;
			}
			else if (operand_one.type != T_Item::tpFloat) 
				return_false;
			stack.Push(operand_one);
			} break;
		case T_Command::cdFloatToInt : {
			T_Item operand_one;
			if (!stack.Pop(operand_one)) return_false;
			if (operand_one.isFloat()) 
			{
				operand_one.value = operand_one.number;
				operand_one.type = T_Item::tpInt;
			}
			else if (operand_one.type != T_Item::tpInt) 
				return_false;
			stack.Push(operand_one);
			} break;
		case T_Command::cdNewInt  : {
			int size = command.operand;
			if (size <= 0) size = 1;
			while (size--) stack.Push(T_Item(0));
			} break;
		case T_Command::cdNewFloat : {
			int size = command.operand;
			if (size <= 0) size = 1;
			while (size--) stack.Push(T_Item(0.0));
			} break;
		case T_Command::cdPushVar  : 
			offset = frames.Top().offset;
		case T_Command::cdGlobVar  : { 
			int address = offset + command.operand;
			if (!stack.inside(address)) return_false;
			const T_Item& item = stack.Item(address);
			if (item.type == T_Item::tpAddress) 
			{
				address = item.value;
			}
			stack.Push(T_Item(T_Item::tpAddress, address));
			} break;
		case T_Command::cdPushVal  :
			offset = frames.Top().offset;
		case T_Command::cdGlobVal  : {
			int address = offset + command.operand;
			if (!stack.inside(address)) return_false;
			T_Item item = stack.Item(address);
			while (item.type == T_Item::tpAddress) 
			{
				item = stack.Item(item.value);
			}
			stack.Push(item);
			} break;
		case T_Command::cdAssign : {
			if (!Exec(T_OperTable::operation.Oper(T_Oper::opAssign))) return_false;
/*			
			T_Item operand, address;
			if (!stack.Pop(operand) || !stack.Pop(address) || !address.isAddress()) return_false;
			T_Item& variable = stack.Item(address.value);
			if (operand.type != variable.type) return_false;
			variable = operand;
*/			
			} break;
		case T_Command::cdIndex : { // Type arr[size]; ... arr[index] ...
			T_Item index;
			if (!stack.Pop(index) || !index.isInt() || stack.empty()) return_false;
			T_Item& address = stack.Top();
			if (!address.isAddress()) return_false;
			address.value += index.value;
			if (command.operand) // var -> val
			{
				T_Item addr; stack.Pop(addr);
				stack.Push(stack.Item(address.value));
			}
			} break;
		case T_Command::cdTwoIndex : { // Type arr[size_one, size_two]; ... arr[index_one, index_two] ...
			T_Item size_one, index_one, index_two;
			if (!stack.Pop(index_two) || !index_two.isInt()) return_false;
			if (!stack.Pop(index_one) || !index_one.isInt()) return_false;
			if (!stack.Pop(size_one)  || !size_one.isInt() || stack.empty()) return_false;
			T_Item& address = stack.Top();
			if (!address.isAddress()) return_false;
			address.value += size_one.value * index_one.value + index_two.value;
			if (command.operand) // var -> val
			{
				T_Item addr; stack.Pop(addr);
				stack.Push(stack.Item(addr.value));
			}
			} break;
		case T_Command::cdToVal : {
			T_Item address;
			if (!stack.Pop(address) || !address.isAddress()) return_false;
			stack.Push(stack.Item(address.value));
			} break;
		default : {
			return_false;
			} break;
	}
	return true;
};

bool T_CPU::Exec(const T_Program &program, int start)
{
	printf("\n");	
//	stack.print();
	frames.Push(T_Frame(stack.top, 0));
	ip = start;
	while (program.inside(ip)) 
	{
		const T_Command &command = program.Command(ip++);
		printf("ip=%02d, ", ip - 1); command.print(); printf("\n");
		switch (command.code)
		{
			case T_Command::cdIfGoTo   : 
			case T_Command::cdElseGoTo : {
				T_Item operand;
				if (!stack.Pop(operand) || !operand.isInt() || !program.inside(command.operand)) return_false;
				if (((command.code == T_Command::cdIfGoTo) && operand.value) || ((command.code == T_Command::cdElseGoTo) && !operand.value)) ip = command.operand;
				} break;
			case T_Command::cdGoTo : {
				if (!program.inside(command.operand)) return_false;
				ip = command.operand;
				} break;
			case T_Command::cdFunction : {
				if (!functions.inside(command.operand)) return_false;
				const T_Function& func = functions.Func(command.operand);
				frames.Top().ip_ret = ip;
				int old_top = stack.top;
				if (!func.body || !Exec(*func.body, func.start)) return_false;
				T_Item result;
				if (func.result != T_Item::tpVoid)
				{
					printf("result: "); stack.Top().print();
					if (!stack.Pop(result) || (result.type != func.result)) return_false;
				}
				int tranc = old_top - func.param; 
				stack.Tranc(tranc);
				if (func.result != T_Item::tpVoid)
				{
					stack.Push(result);
				}
				ip = frames.Top().ip_ret;
				} break;
			case T_Command::cdPrint : {
				stack.print();
				} break;
			case T_Command::cdReturn : {
				ip = program.last() + 1;
				} break;
			default : {
				if (!Exec(command)) return_false;
				} break;
		}
//		stack.print();
	}
//	stack.print();
	T_Frame frame;
	if (!frames.Pop(frame)) return_false;
	return true;
}


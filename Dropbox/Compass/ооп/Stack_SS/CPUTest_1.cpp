/*
 * 
 */
 
#include "TXLib.h"
#define _CPU_DEBUG_MODE_
#include "CPUOper.h"
#include "CPUOper.cpp"
 
T_CPU cpu;

int main()
{
	T_Program program;
	
//	int i;
	program.Add(T_Command(T_Command::cdNewInt));     // i				
	program.Add(T_Command(T_Command::cdPushVar, 0));					
	program.Add(T_Command(T_Command::cdPush, 1));					
	program.Add(T_Command(T_Command::cdAssign));						
	
//	int j, k;
// 		int j
	program.Add(T_Command(T_Command::cdNewInt));     // j				
	program.Add(T_Command(T_Command::cdPushVar, 1));					
	program.Add(T_Command(T_Command::cdPush, 0));						
	program.Add(T_Command(T_Command::cdAssign));	
//		int k	
	program.Add(T_Command(T_Command::cdNewInt));     // k				
	program.Add(T_Command(T_Command::cdPushVar, 2));					
	program.Add(T_Command(T_Command::cdPush, 0));						
	program.Add(T_Command(T_Command::cdAssign));				

	program.Add(T_Command(T_Command::cdPushVar, 1)); // j
	
	program.Add(T_Command(T_Command::cdPushVar, 2)); // k

	//~ program.Add(T_Command(T_Command::cdPushVar, 0)); // i				
	//~ program.Add(T_Command(T_Command::cdOper, T_Oper::opPlusPlus));
					
	program.Add(T_Command(T_Command::cdPushVal, 0));// i				
						
	program.Add(T_Command(T_Command::cdPushVar, 0)); // i				
	program.Add(T_Command(T_Command::cdOper, T_Oper::opPlusPlus));
	program.Add(T_Command(T_Command::cdAssign));
	
	program.Add(T_Command(T_Command::cdPushVal, 2));// k				
	program.Add(T_Command(T_Command::cdAssign));
	
	program.Add(T_Command(T_Command::cdReturn));
	
	cpu.Exec(program);
	cpu.print();
}

#ifdef _CPU_DEBUG_MODE
void _debug_return_false_()
{
	static bool debug = true;
	if (debug) 
	{
		debug = false;
		printf("---- debug begin ----\n");
		cpu.print();
		T_OperTable::operation.print();
		printf("---- debug end ----\n");
	}
}
#endif

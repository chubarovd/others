/*
 * CPU.cpp
 * 
 * Copyright 2013 Sergey Vaganov <SVaganov@svaganov-macbook.employees.compassplus.ru>
 * 
 */
 
#include "TXLib.h"

 
const int PROGRAM_SIZE = 20, MEMORY_SIZE = 20;

struct T_Command
{
	enum T_Code { cdNone, cdPush, cdPop, cdAdd, cdSub, cdMulti, cdDiv, cdMod, cdReturn };
	int code;
	int operand;
	T_Command() : code(cdNone), operand(0) {}
	T_Command(int c) : code(c), operand(0) {}
	T_Command(int c, int o) : code(c), operand(o) {}
};

struct T_Program
{
	T_Command commands[PROGRAM_SIZE];
	int count;
	T_Program() : count(0) {}
	void Add(const T_Command &command);
	int low()  const { return 0; }
	int last() const { return count - 1; }
	const T_Command& Command(int command) const;
};

const T_Command& T_Program::Command(int command) const 
{ 
	return commands[command];
};

struct T_ItemStack
{
	int type;
	int value;
	T_ItemStack() : type(0), value(0) {}
	T_ItemStack(int v) : type(0), value(v) {}
	T_ItemStack(int t, int v) : type(t), value(v) {}
};

struct T_Stack
{
	T_ItemStack stack[MEMORY_SIZE];
	int top;
	T_Stack() : top(0) {}
	bool empty() const { return !top; }
	void Push(const T_ItemStack &item);
	bool Pop(T_ItemStack &item); // if empty return false
	void print();
};

void T_Stack::print()
{
	for (int i = 0; i < top; i++ ) 
	{
		printf("%d %d\n", stack[i].type, stack[i].value);
	}
	printf("\n");
}

struct T_CPU
{
	T_Stack stack;
	void exec(const T_Program &program);
	void exec(const T_Command &command);
	void print() { stack.print(); }
};

void T_CPU::exec(const T_Command &command)
{
	switch (command.code)
	{
		case T_Command::cdPush  : {
			} break;
		case T_Command::cdPop   : {
			} break;
		case T_Command::cdAdd   : {
			T_ItemStack operand_one, operand_two, operand_result;
			stack.Push(operand_result);
			} break;
		case T_Command::cdSub   : {
			} break;
		case T_Command::cdMulti : {
			} break;
		case T_Command::cdDiv   : {
			} break;
		case T_Command::cdMod   : {
			} break;
		case T_Command::cdReturn   : {
			} break;
	}
};

int main()
{
	T_CPU     cpu;
	T_Program program;
	
	program.Add(T_Command(T_Command::cdPush, 5));
	program.Add(T_Command(T_Command::cdPush, 6));
	program.Add(T_Command(T_Command::cdSub));
	
	cpu.exec(program);
	cpu.print();
	
	return 0;
}


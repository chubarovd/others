/*
 * CPUDouble.cpp
 * 
 * Copyright 2013 Sergey Vaganov <SVaganov@svaganov-macbook.employees.compassplus.ru>
 * 
 */
 
#include "TXLib.h"

 
const int PROGRAM_SIZE = 200, MEMORY_SIZE = 20;
static const char* title_command[] = {
				  "None", "Push", "Pop", "Add", "Sub", "Multi", "Div", "Mod", 
				  "PushFloat", "FloatToInt", "IntToFloat", "PushVar", "PushVal", "NewInt", "NewFloat", "Assign",
				  "Not", "Equ", "NotEqu", "LT", "LE", "GT", "GE",
				  "IfGoTo", "ElseGoTo", "GoTo",  
				  "Return" 
	};
struct T_Command
{
	enum T_Code { cdNone, cdPush, cdPop, cdAdd, cdSub, cdMulti, cdDiv, cdMod, 
				  cdPushFloat, cdFloatToInt, cdIntToFloat, cdPushVar, cdPushVal, cdNewInt, cdNewFloat, cdAssign,
				  cdNot, cdEqu, cdNotEqu, cdLT, cdLE, cdGT, cdGE,
				  cdIfGoTo, cdElseGoTo, cdGoTo,  
				  cdReturn };
	int code;
	int    operand;
	double number;
	T_Command() : code(cdNone), operand(0), number(0.0) {}
	T_Command(int c) : code(c), operand(0), number(0.0) {}
	T_Command(int c, int o) : code(c), operand(o), number(0.0) {}
	T_Command(int c, double n) : code(c), operand(0), number(n) {}
	void print() const {	printf("%s", title_command[code]); }
};

struct T_Program
{
	T_Command commands[PROGRAM_SIZE];
	int count;
	T_Program() : count(0) {}
	void Add(const T_Command &command);
	int  low()  const { return 0; }
	int  last() const { return count - 1; }
	bool inside(int command) const { return ((low() <= command) && (command <= last())); }
	const T_Command& Command(int command) const;
};

const T_Command& T_Program::Command(int command) const 
{ 
	return commands[command];
};

struct T_ItemStack
{
	enum T_Type { tpNone, tpInt, tpFloat, tpAddress }; 
	int type;
	int value;
	double number;
	T_ItemStack() : type(tpNone), value(0), number(0.0) {}
	T_ItemStack(int v) : type(tpInt), value(v), number(0.0) {}
	T_ItemStack(int t, int v) : type(t), value(v), number(0.0) {}
	T_ItemStack(double n) : type(tpFloat), value(0), number(n) {}
	int compare(const T_ItemStack &join) const; // - -1 '<', 0 - '==', 1 - '>' 
};

struct T_Stack
{
	T_ItemStack stack[MEMORY_SIZE];
	int top;
	T_Stack() : top(0) {}
	bool empty() const { return !top; }
	void Push(const T_ItemStack &item);
	bool Pop(T_ItemStack &item); // if empty return false
	T_ItemStack& Item(int index) { return stack[index]; }
	T_ItemStack& Top()           { return Item(top - 1); }
	void print();
};

void T_Stack::print()
{
	for (int i = 0; i < top; i++ ) 
	{
		printf("type=%d, value= %d, number=%lf\n", stack[i].type, stack[i].value, stack[i].number);
	}
	printf("\n");
}

struct T_CPU
{
	T_Stack stack;
	int ip;
	bool exec(const T_Program &program);
	bool exec(const T_Command &command);
	int  last()   { return stack.top - 1; }
	void print()  { stack.print(); }
};

bool T_CPU::exec(const T_Command &command)
{
	switch (command.code)
	{
		case T_Command::cdPush  : {			
			T_ItemStack item(command.operand);
			stack.Push(item);			
			} break;
		case T_Command::cdPop   : {
			} break;
		case T_Command::cdAdd   : {
			T_ItemStack operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if (operand_one.type == T_ItemStack::tpInt && operand_two.type == T_ItemStack::tpInt)
			{
				operand_result.value = operand_one.value + operand_two.value;
				operand_result.type = T_ItemStack::tpInt;
			} 
			else 
			{
				operand_result.number = operand_one.number + operand_two.number;
				operand_result.type = T_ItemStack::tpFloat;
			}
			stack.Push(operand_result);
			} break;
		case T_Command::cdSub   : {
			T_ItemStack operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if (operand_one.type == T_ItemStack::tpInt && operand_two.type == T_ItemStack::tpInt)
			{
				operand_result.value = operand_one.value - operand_two.value;
				operand_result.type = T_ItemStack::tpInt;
				
				
			} 
			else 
			{
				operand_result.number = operand_one.number - operand_two.number;
				operand_result.type = T_ItemStack::tpFloat;
			}
			stack.Push(operand_result);
			} break;
		case T_Command::cdMulti : {
			T_ItemStack operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if (operand_one.type == T_ItemStack::tpInt && operand_two.type == T_ItemStack::tpInt)
			{
				operand_result.value = operand_one.value * operand_two.value;
				operand_result.type = T_ItemStack::tpInt;
			} 
			else 
			{
				operand_result.number = operand_one.number * operand_two.number;
				operand_result.type = T_ItemStack::tpFloat;
			}
			stack.Push(operand_result);
			} break;
		case T_Command::cdDiv   : {
			T_ItemStack operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if (operand_one.type == T_ItemStack::tpInt && operand_two.type == T_ItemStack::tpInt)
			{
				operand_result.value = operand_one.value / operand_two.value;
				operand_result.type = T_ItemStack::tpInt;
			} 
			else 
			{
				operand_result.number = operand_one.number / operand_two.number;
				operand_result.type = T_ItemStack::tpFloat;
			}
			stack.Push(operand_result);
			} break;
		case T_Command::cdMod   : {
			T_ItemStack operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if (operand_one.type == T_ItemStack::tpInt && operand_two.type == T_ItemStack::tpInt)
			{
				operand_result.value = operand_one.value % operand_two.value;
				operand_result.type = T_ItemStack::tpInt;
			} 
			else 
			{
				printf(" Error! User is crab! ");
			}
			stack.Push(operand_result);
			} break;
		case T_Command::cdPushFloat   : {
			T_ItemStack item(command.number);
			stack.Push(item);
			} break;
		case T_Command::cdFloatToInt   : {
			T_ItemStack operand_one, operand_result;
			stack.Pop(operand_one);
			operand_result.value = operand_one.number;
			operand_result.type = T_ItemStack::tpInt;
			stack.Push(operand_result);
			} break;
		case T_Command::cdIntToFloat   : {
			T_ItemStack operand_one, operand_result;
			stack.Pop(operand_one);
			operand_result.number = operand_one.value;
			operand_result.type = T_ItemStack::tpFloat;
			stack.Push(operand_result);
			} break;	
		case T_Command::cdPushVar  : {
			stack.Push(T_ItemStack (T_ItemStack::tpAddress, command.operand));
			} break;
		case T_Command::cdPushVal  : {
			stack.Push(stack.Item(command.operand));
			} break;
		case T_Command::cdNewInt  : {
			T_ItemStack item(1, command.operand);
			stack.Push(item);
			} break;
		case T_Command::cdNewFloat  : {
			T_ItemStack item(2, command.number);
			stack.Push(item);
			} break;
		case T_Command::cdAssign  : {
			T_ItemStack operand_one, operand_two;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			stack.Item(operand_one.value) = operand_two;
			} break;
		case T_Command::cdNot   : {
			T_ItemStack operand_one, operand_result;
			stack.Pop(operand_one);
			if(operand_one.type == T_ItemStack::tpInt)
			{
				operand_result.value = !operand_one.value;
			}
			else
			{
				operand_result.value = !operand_one.number;
			}
			operand_result.type = T_ItemStack::tpInt;
			stack.Push(operand_result);
			} break;
		case T_Command::cdEqu   : {
			T_ItemStack operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if(operand_one.type == T_ItemStack::tpInt && operand_two.type == T_ItemStack::tpInt)
			{
				operand_result.value = operand_one.value == operand_two.value;
			}
			else
			{
				operand_result.value = operand_one.number == operand_two.number;
			}
			operand_result.type = T_ItemStack::tpInt;
			stack.Push(operand_result);
			} break;
		case T_Command::cdNotEqu   : {
			T_ItemStack operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if(operand_one.type == T_ItemStack::tpInt && operand_two.type == T_ItemStack::tpInt)
			{
				operand_result.value = operand_one.value != operand_two.value;
			}
			else
			{
				operand_result.value = operand_one.number != operand_two.number;
			}
			stack.Push(operand_result);
			} break;
		case T_Command::cdLT   : {
			T_ItemStack operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if(operand_one.type == T_ItemStack::tpInt && operand_two.type == T_ItemStack::tpInt)
			{
				operand_result.value = operand_one.value < operand_two.value;
			}
			else
			{
				operand_result.value = operand_one.number < operand_two.number;
			}
			operand_result.type = T_ItemStack::tpInt;
			stack.Push(operand_result);
			} break;
		case T_Command::cdLE   : {
			T_ItemStack operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if(operand_one.type == T_ItemStack::tpInt && operand_two.type == T_ItemStack::tpInt)
			{
				operand_result.value = operand_one.value <= operand_two.value;
			}
			else
			{
				operand_result.value = operand_one.number <= operand_two.number;
			}
			operand_result.type = T_ItemStack::tpInt;
			stack.Push(operand_result);
			} break;
		case T_Command::cdGT   : {
			T_ItemStack operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if(operand_one.type == T_ItemStack::tpInt && operand_two.type == T_ItemStack::tpInt)
			{
				operand_result.value = operand_one.value > operand_two.value;
			}
			else
			{
				operand_result.value = operand_one.number > operand_two.number;
			}
			operand_result.type = T_ItemStack::tpInt;
			stack.Push(operand_result);
			} break;
		case T_Command::cdGE   : {
			T_ItemStack operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);			
			if(operand_one.type == T_ItemStack::tpInt && operand_two.type == T_ItemStack::tpInt)
			{
				operand_result.value = operand_one.value >= operand_two.value;
			}
			else
			{
				operand_result.value = operand_one.number >= operand_two.number;
			}
			operand_result.type = T_ItemStack::tpInt;
			stack.Push(operand_result);
			} break;
	}
};


				  

int T_ItemStack::compare(const T_ItemStack &join) const // - -1 '<', 0 - '==', 1 - '>' 
{
	if (type == T_ItemStack::tpInt)
	{ 
		if (value == join.value) return 0;
		return (value < join.value) ? -1 : 1;
	}
	else if (type == T_ItemStack::tpFloat) 
	{
		if (number < join.number) return -1;
		return (number > join.number) ? 1 : 0;
	} 
	return 0;
}

void T_Stack::Push(const T_ItemStack &item)
{
	stack[top++] = item;
}

bool T_Stack::Pop(T_ItemStack &item)
{
	if (empty()) return false;
	item = stack[--top];
	return true;
}

void T_Program::Add(const T_Command &command)
{
	commands[count++] = command;
}

bool T_CPU::exec(const T_Program &program)
{
	ip = program.low();
	while (program.inside(ip)) 
	{
		const T_Command &command = program.Command(ip++);
		printf("ip=%d, code=%d, command=", ip, command.code);
		command.print();
		printf("\n");
		switch (command.code)
		{
			case T_Command::cdReturn : {
				return true;
				} break;
			case T_Command::cdIfGoTo   : 
			case T_Command::cdElseGoTo : {
				T_ItemStack operand;
				if (!stack.Pop(operand) || (operand.type != T_ItemStack::tpInt) || !program.inside(command.operand)) return false;
				if ((command.code == T_Command::cdIfGoTo) && operand.value || (command.code == T_Command::cdElseGoTo) && !operand.value) ip = command.operand;
				} break;
			case T_Command::cdGoTo : {
				if (!program.inside(command.operand)) return false;
				ip = command.operand;
				} break;
			default : {
				bool ret = exec(command);
				if (!ret) return ret;
				} break;
		}
		print();
	}
	return true;
};



int main()
{
	T_CPU     cpu;
	T_Program program;
	
//	!n -- factorial(n)
//	int n; n = 5;
	program.Add(T_Command(T_Command::cdNewInt));    // n
	program.Add(T_Command(T_Command::cdPushVar, 0));
	program.Add(T_Command(T_Command::cdPush, 5));
	program.Add(T_Command(T_Command::cdAssign));
//	int f; f = 1;
	program.Add(T_Command(T_Command::cdNewInt));    // f
	program.Add(T_Command(T_Command::cdPushVar, 1));
	program.Add(T_Command(T_Command::cdPush, 1));
	program.Add(T_Command(T_Command::cdAssign));
//	label 1: -- 8
//	if (n <= 1) go_to label 2
	program.Add(T_Command(T_Command::cdPushVal, 0));
	program.Add(T_Command(T_Command::cdPush, 1));
	program.Add(T_Command(T_Command::cdLE));
	program.Add(T_Command(T_Command::cdIfGoTo, 23));
//	f = f * n
	program.Add(T_Command(T_Command::cdPushVar, 1));
	program.Add(T_Command(T_Command::cdPushVal, 1));
	program.Add(T_Command(T_Command::cdPushVal, 0));
	program.Add(T_Command(T_Command::cdMulti));
	program.Add(T_Command(T_Command::cdAssign));
//	n = n - 1
	program.Add(T_Command(T_Command::cdPushVar, 0));
	program.Add(T_Command(T_Command::cdPushVal, 0));
	program.Add(T_Command(T_Command::cdPush, 1));
	program.Add(T_Command(T_Command::cdSub));
	program.Add(T_Command(T_Command::cdAssign));
//	go_to label 1
	program.Add(T_Command(T_Command::cdGoTo, 8));
//	label 2: -- 23
//	return
	program.Add(T_Command(T_Command::cdReturn));


	cpu.exec(program);
	cpu.print();
	
	return 0;
}


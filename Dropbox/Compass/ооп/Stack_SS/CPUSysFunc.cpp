/*
 * StackMachine.cpp
 * 
 * Copyright 2013 Sergey Vaganov <SVaganov@svaganov-macbook.employees.compassplus.ru>
 * 
 */
 
#include "TXLib.h"

const int PROGRAM_SIZE = 200, MEMORY_SIZE = 20;

static const char* title_command[] = {
				  "None", "Push", "Pop", "Add", "Sub", "Multi", "Div", "Mod", "UnaryMinus",
				  "PushFloat", "FloatToInt", "IntToFloat", "PushVar", "PushVal", "NewInt", "NewFloat", "Assign",
				  "Not", "Equ", "NotEqu", "LT", "LE", "GT", "GE",
				  "IfGoTo", "ElseGoTo", "GoTo", 
				  "Oper", 
				  "Return" 
	};
	
struct T_Command
{
	enum T_Code { cdNone, cdPush, cdPop, cdAdd, cdSub, cdMulti, cdDiv, cdMod, cdUnaryMinus, 
				  cdPushFloat, cdFloatToInt, cdIntToFloat, cdPushVar, cdPushVal, cdNewInt, cdNewFloat, cdAssign,
				  cdNot, cdEqu, cdNotEqu, cdLT, cdLE, cdGT, cdGE,
				  cdIfGoTo, cdElseGoTo, cdGoTo,
				  cdOper,  
				  cdReturn };
	int code;
	int    operand;
	double number;
	T_Command() : code(cdNone), operand(0), number(0.0) {}
	T_Command(int c) : code(c), operand(0), number(0.0) {}
	T_Command(int c, int o) : code(c), operand(o), number(0.0) {}
	T_Command(int c, double n) : code(c), operand(0), number(n) {}
	void print() const;
};

void T_Command::print() const 
{ 
	printf("code=%02d, command=%s", code, title_command[code]);
	if (code == cdPush) printf(", value=%d", operand);
	else if (code == cdPushFloat) printf(", value=%lf", number);
	else if (code == cdPushVar) printf(", address=%d", operand);
	else if (code == cdOper) printf(", function=%d", operand);
	else if ((code == cdGoTo) || (code == cdIfGoTo) || (code == cdElseGoTo)) printf(", label=%d", operand);
}

struct T_Program
{
	T_Command commands[PROGRAM_SIZE];
	int count;
	T_Program() : count(0) {}
	void Add(const T_Command &command);
	int  low()  const { return 0; }
	int  last() const { return count - 1; }
	bool inside(int command) const { return ((low() <= command) && (command <= last())); }
	const T_Command& Command(int command) const;
};

const T_Command& T_Program::Command(int command) const 
{ 
	return commands[command];
};

struct T_Item
{
	enum T_Type { tpVoid, tpInt, tpFloat, tpAddress }; 
	int type;
	int value;
	double number;
	T_Item() : type(tpVoid), value(0), number(0.0) {}
	T_Item(int v) : type(tpInt), value(v), number(0.0) {}
	T_Item(int t, int v) : type(t), value(v), number(0.0) {}
	T_Item(double n) : type(tpFloat), value(0), number(n) {}
	int compare(const T_Item &join) const; // - -1 '<', 0 - '==', 1 - '>' 
};

struct T_Stack
{
	T_Item stack[MEMORY_SIZE];
	int top;
	T_Stack() : top(0) {}
	bool empty() const { return !top; }
	void Push(const T_Item &item);
	bool Pop(T_Item &item); // if empty return false
	T_Item& Item(int index) { return stack[index]; }
	T_Item& Top()           { return Item(top - 1); }
	void print();
};

void T_Stack::print()
{
	for (int i = 0; i < top; i++ ) 
	{
		printf("type=%d, value= %d, number=%lf\n", stack[i].type, stack[i].value, stack[i].number);
	}
	printf("\n");
}

struct T_Oper
{
	enum T_Name { opAbs, opSign, opMin, opMax, opAbsFloat, opSignFloat, opExp, opLog, opLog10, opSin, opCos, opTan, opASin, opACos, opATan, opATan2, opPow, 
				  opMinFloat, opMaxFloat, opSqrt, opSqr, opRand, _op_end 
				};
	enum T_Type { tfNone, tfInt_Int, tfInt_Int_Int, tfInt_Float, tfFloat_Float, tfFloat_Float_Float };
	int type;
	void *func;
	T_Oper() : type(tfNone), func(NULL) {} 
	T_Oper(int t, void *f) : type(t), func(f) {} 
};

struct T_OperTable
{
	T_Oper table[T_Oper::_op_end];
	int count;
	T_OperTable();
	void Add(T_Oper::T_Name name, const T_Oper &oper) { table[name] = oper; }
	bool inside(int oper) const { return (0 <= oper) && (oper < count) && (Oper(oper).type != T_Oper::tfNone); }
	const T_Oper& Oper(int index) const { return table[index]; }
};

static double min_float(double x, double y) { return (x < y) ? x : y; }
static double max_float(double x, double y) { return (x > y) ? x : y; }
static int min_int(int x, int y)  { return (x < y) ? x : y; }
static int max_int(int x, int y)  { return (x > y) ? x : y; }
static int sign_int(int x)        { return (x < 0) ? -1 : ((x > 0) ? 1 : 0); }
static int sign_float(double x)   { return (x < 0) ? -1 : ((x > 0) ? 1 : 0); }
static double abs_float(double x) { return (x < 0) ? -x : x; }
static double sqr_float(double x) { return x * x; }

T_OperTable::T_OperTable() : count(T_Oper::_op_end)
{
	Add(T_Oper::opAbs,      T_Oper(T_Oper::tfInt_Int,     (void*)&abs));
	Add(T_Oper::opSign,     T_Oper(T_Oper::tfInt_Int,     (void*)&sign_int));
	Add(T_Oper::opMin,      T_Oper(T_Oper::tfInt_Int_Int, (void*)&min_int));
	Add(T_Oper::opMax,      T_Oper(T_Oper::tfInt_Int_Int, (void*)&max_int));
	Add(T_Oper::opAbsFloat, T_Oper(T_Oper::tfFloat_Float, (void*)&abs_float));
	Add(T_Oper::opSignFloat,T_Oper(T_Oper::tfInt_Float,   (void*)&sign_float));
	Add(T_Oper::opExp,      T_Oper(T_Oper::tfFloat_Float, (void*)&exp));
	Add(T_Oper::opLog,      T_Oper(T_Oper::tfFloat_Float, (void*)&log));
	Add(T_Oper::opLog10,    T_Oper(T_Oper::tfFloat_Float, (void*)&log10));
	Add(T_Oper::opSin,      T_Oper(T_Oper::tfFloat_Float, (void*)&sin));
	Add(T_Oper::opCos,      T_Oper(T_Oper::tfFloat_Float, (void*)&cos));	
	Add(T_Oper::opTan,      T_Oper(T_Oper::tfFloat_Float, (void*)&tan));
	Add(T_Oper::opASin,     T_Oper(T_Oper::tfFloat_Float, (void*)&asin));
	Add(T_Oper::opACos,     T_Oper(T_Oper::tfFloat_Float, (void*)&acos));
	Add(T_Oper::opATan,     T_Oper(T_Oper::tfFloat_Float, (void*)&atan));
	Add(T_Oper::opATan2,    T_Oper(T_Oper::tfFloat_Float_Float,(void*)&atan2));	
	Add(T_Oper::opPow,      T_Oper(T_Oper::tfFloat_Float_Float,(void*)&pow));
	Add(T_Oper::opMinFloat, T_Oper(T_Oper::tfFloat_Float_Float,(void*)&min_float));
	Add(T_Oper::opMaxFloat, T_Oper(T_Oper::tfFloat_Float_Float,(void*)&max_float));
	Add(T_Oper::opSqrt,     T_Oper(T_Oper::tfFloat_Float, (void*)&sqrt));
	Add(T_Oper::opSqr,      T_Oper(T_Oper::tfFloat_Float, (void*)&sqr_float));
	Add(T_Oper::opRand,     T_Oper(T_Oper::tfInt_Int,     (void*)&rand));
}

struct T_CPU
{
	T_Stack stack;
	T_OperTable operation;
	int ip;
	bool exec(const T_Program &program);
	bool exec(const T_Command &command);
	bool exec(const T_Oper &oper);
	void print()  { stack.print(); }
};

bool T_CPU::exec(const T_Command &command)
{
	switch (command.code)
	{
		case T_Command::cdPush  : {			
			T_Item item(command.operand);
			stack.Push(item);			
			} break;
		case T_Command::cdPop   : {
			} break;
		case T_Command::cdAdd   : {
			T_Item operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if (operand_one.type == T_Item::tpInt && operand_two.type == T_Item::tpInt)
			{
				operand_result.value = operand_one.value + operand_two.value;
				operand_result.type = T_Item::tpInt;
			} 
			else 
			{
				operand_result.number = operand_one.number + operand_two.number;
				operand_result.type = T_Item::tpFloat;
			}
			stack.Push(operand_result);
			} break;
		case T_Command::cdSub   : {
			T_Item operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if (operand_one.type == T_Item::tpInt && operand_two.type == T_Item::tpInt)
			{
				operand_result.value = operand_one.value - operand_two.value;
				operand_result.type = T_Item::tpInt;
				
				
			} 
			else 
			{
				operand_result.number = operand_one.number - operand_two.number;
				operand_result.type = T_Item::tpFloat;
			}
			stack.Push(operand_result);
			} break;
		case T_Command::cdMulti : {
			T_Item operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if (operand_one.type == T_Item::tpInt && operand_two.type == T_Item::tpInt)
			{
				operand_result.value = operand_one.value * operand_two.value;
				operand_result.type = T_Item::tpInt;
			} 
			else 
			{
				operand_result.number = operand_one.number * operand_two.number;
				operand_result.type = T_Item::tpFloat;
			}
			stack.Push(operand_result);
			} break;
		case T_Command::cdDiv   : {
			T_Item operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if (operand_one.type == T_Item::tpInt && operand_two.type == T_Item::tpInt)
			{
				operand_result.value = operand_one.value / operand_two.value;
				operand_result.type = T_Item::tpInt;
			} 
			else 
			{
				operand_result.number = operand_one.number / operand_two.number;
				operand_result.type = T_Item::tpFloat;
			}
			stack.Push(operand_result);
			} break;
		case T_Command::cdMod   : {
			T_Item operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if (operand_one.type == T_Item::tpInt && operand_two.type == T_Item::tpInt)
			{
				operand_result.value = operand_one.value % operand_two.value;
				operand_result.type = T_Item::tpInt;
			} 
			else 
			{
				printf(" Error! User is crab! ");
			}
			stack.Push(operand_result);
			} break;
		case T_Command::cdPushFloat   : {
			T_Item item(command.number);
			stack.Push(item);
			} break;
		case T_Command::cdFloatToInt   : {
			T_Item operand_one, operand_result;
			stack.Pop(operand_one);
			operand_result.value = operand_one.number;
			operand_result.type = T_Item::tpInt;
			stack.Push(operand_result);
			} break;
		case T_Command::cdIntToFloat   : {
			T_Item operand_one, operand_result;
			stack.Pop(operand_one);
			operand_result.number = operand_one.value;
			operand_result.type = T_Item::tpFloat;
			stack.Push(operand_result);
			} break;	
		case T_Command::cdPushVar  : {
			stack.Push(T_Item (T_Item::tpAddress, command.operand));
			} break;
		case T_Command::cdPushVal  : {
			stack.Push(stack.Item(command.operand));
			} break;
		case T_Command::cdNewInt  : {
			T_Item item(1, command.operand);
			stack.Push(item);
			} break;
		case T_Command::cdNewFloat  : {
			T_Item item(2, command.number);
			stack.Push(item);
			} break;
		case T_Command::cdAssign  : {
			T_Item operand_one, operand_two;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			stack.Item(operand_one.value) = operand_two;
			} break;
		case T_Command::cdNot   : {
			T_Item operand_one, operand_result;
			stack.Pop(operand_one);
			if(operand_one.type == T_Item::tpInt)
			{
				operand_result.value = !operand_one.value;
			}
			else
			{
				operand_result.value = !operand_one.number;
			}
			operand_result.type = T_Item::tpInt;
			stack.Push(operand_result);
			} break;
		case T_Command::cdEqu   : {
			T_Item operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if(operand_one.type == T_Item::tpInt && operand_two.type == T_Item::tpInt)
			{
				operand_result.value = operand_one.value == operand_two.value;
			}
			else
			{
				operand_result.value = operand_one.number == operand_two.number;
			}
			operand_result.type = T_Item::tpInt;
			stack.Push(operand_result);
			} break;
		case T_Command::cdNotEqu   : {
			T_Item operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if(operand_one.type == T_Item::tpInt && operand_two.type == T_Item::tpInt)
			{
				operand_result.value = operand_one.value != operand_two.value;
			}
			else
			{
				operand_result.value = operand_one.number != operand_two.number;
			}
			stack.Push(operand_result);
			} break;
		case T_Command::cdLT   : {
			T_Item operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if(operand_one.type == T_Item::tpInt && operand_two.type == T_Item::tpInt)
			{
				operand_result.value = operand_one.value < operand_two.value;
			}
			else
			{
				operand_result.value = operand_one.number < operand_two.number;
			}
			operand_result.type = T_Item::tpInt;
			stack.Push(operand_result);
			} break;
		case T_Command::cdLE   : {
			T_Item operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if(operand_one.type == T_Item::tpInt && operand_two.type == T_Item::tpInt)
			{
				operand_result.value = operand_one.value <= operand_two.value;
			}
			else
			{
				operand_result.value = operand_one.number <= operand_two.number;
			}
			operand_result.type = T_Item::tpInt;
			stack.Push(operand_result);
			} break;
		case T_Command::cdGT   : {
			T_Item operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if(operand_one.type == T_Item::tpInt && operand_two.type == T_Item::tpInt)
			{
				operand_result.value = operand_one.value > operand_two.value;
			}
			else
			{
				operand_result.value = operand_one.number > operand_two.number;
			}
			operand_result.type = T_Item::tpInt;
			stack.Push(operand_result);
			} break;
		case T_Command::cdGE   : {
			T_Item operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);			
			if(operand_one.type == T_Item::tpInt && operand_two.type == T_Item::tpInt)
			{
				operand_result.value = operand_one.value >= operand_two.value;
			}
			else
			{
				operand_result.value = operand_one.number >= operand_two.number;
			}
			operand_result.type = T_Item::tpInt;
			stack.Push(operand_result);
			} break;
		case T_Command::cdOper   : {
			if (!operation.inside(command.operand)) return false;
			exec(operation.Oper(command.operand));
			} break;
		case T_Command::cdReturn   : {
			} break;
	}
	return true;
};


int T_Item::compare(const T_Item &join) const // - -1 '<', 0 - '==', 1 - '>' 
{
	if (type == T_Item::tpInt)
	{ 
		if (value == join.value) return 0;
		return (value < join.value) ? -1 : 1;
	}
	else if (type == T_Item::tpFloat) 
	{
		if (number < join.number) return -1;
		return (number > join.number) ? 1 : 0;
	} 
	return 0;
}

void T_Stack::Push(const T_Item &item)
{
	stack[top++] = item;
}

bool T_Stack::Pop(T_Item &item)
{
	if (empty()) return false;
	item = stack[--top];
	return true;
}

void T_Program::Add(const T_Command &command)
{
	commands[count++] = command;
}

bool T_CPU::exec(const T_Program &program)
{
	ip = program.low();
	while (program.inside(ip)) 
	{
		const T_Command &command = program.Command(ip++);
		printf("ip=%02d, ", ip - 1); command.print(); printf("\n");
		switch (command.code)
		{
			case T_Command::cdReturn : {
				return true;
				} break;
			case T_Command::cdIfGoTo   : 
			case T_Command::cdElseGoTo : {
				T_Item operand;
				if (!stack.Pop(operand) || (operand.type != T_Item::tpInt) || !program.inside(command.operand)) return false;
				if (((command.code == T_Command::cdIfGoTo) && operand.value) || ((command.code == T_Command::cdElseGoTo) && !operand.value)) ip = command.operand;
				} break;
			case T_Command::cdGoTo : {
				if (!program.inside(command.operand)) return false;
				ip = command.operand;
				} break;
			default : {
				bool ret = exec(command);
				if (!ret) return ret;
				} break;
		}
		print();
	}
	return true;
}

bool T_CPU::exec(const T_Oper &oper)
{
	switch (oper.type)
	{
		case T_Oper::tfNone  : {
			} break;
		case T_Oper::tfInt_Int  : {
			T_Item operand, operand_result;
			if (!stack.Pop(operand) || (operand.type != T_Item::tpInt)) return false;
			int (*f)(int o) = (int(*)(int))oper.func;
			operand_result.value = f(operand.value); 
			operand_result.type = T_Item::tpInt;
			stack.Push(operand_result);
			} break;
		case T_Oper::tfInt_Int_Int  : {
			T_Item operand_one, operand_two, operand_result;
			if ((!stack.Pop(operand_two) || (operand_two.type != T_Item::tpInt)) || (!stack.Pop(operand_one) || (operand_one.type != T_Item::tpInt))) return false;
			int (*f)(int o_one, int o_two) = (int(*)(int, int))oper.func;
			operand_result.value = f(operand_two.value, operand_one.value); 
			operand_result.type = T_Item::tpInt;
			stack.Push(operand_result);
			} break;
		case T_Oper::tfInt_Float  : {
			T_Item operand, operand_result;
			if (!stack.Pop(operand) || (operand.type != T_Item::tpFloat)) return false;
			int (*f)(double o) = (int(*)(double))oper.func;
			operand_result.value = f(operand.number); 
			operand_result.type = T_Item::tpInt;
			stack.Push(operand_result);
			} break;
		case T_Oper::tfFloat_Float  : {
			T_Item operand, operand_result;
			if (!stack.Pop(operand) || (operand.type != T_Item::tpFloat)) return false;
			double (*f)(double o) = (double(*)(double))oper.func;
			operand_result.number = f(operand.number); 
			operand_result.type = T_Item::tpFloat;
			stack.Push(operand_result);
			} break;
		case T_Oper::tfFloat_Float_Float  : {
			T_Item operand_one, operand_two, operand_result;
			if ((!stack.Pop(operand_two) || (operand_two.type != T_Item::tpFloat)) || (!stack.Pop(operand_one) || (operand_one.type != T_Item::tpFloat))) return false;
			double (*f)(double o_one, double o_two) = (double(*)(double, double))oper.func;
			operand_result.number = f(operand_two.number, operand_one.number); 
			operand_result.type = T_Item::tpFloat;
			stack.Push(operand_result);
			} break;
	}
	return true;
}

int main()
{
	printf("œ∑†¥π“‘åß∂ƒ©˙∆˚¬…æ«Ω≈ç√∫≤≥÷¡™£¢∞§¶•ªº–Ω¢≈∑∞ḉ§®√¶†∫¥πø“≤π≥“å∑∂†¥øπº“¬˚ø∆©†ƒ®∂∂ƒ©˙");
	T_CPU     cpu;
	T_Program program;
	
//	double s; s = sin(3.14/4);
	program.Add(T_Command(T_Command::cdNewFloat));    // s
	program.Add(T_Command(T_Command::cdPushVar, 0));
	program.Add(T_Command(T_Command::cdPushFloat, M_PI));
	program.Add(T_Command(T_Command::cdPushFloat, 4.0));
	program.Add(T_Command(T_Command::cdDiv));
	program.Add(T_Command(T_Command::cdOper, T_Oper::opSin));
	program.Add(T_Command(T_Command::cdAssign));
//	double c; c = cos(3.14/4);
	program.Add(T_Command(T_Command::cdNewFloat));    // c
	program.Add(T_Command(T_Command::cdPushVar, 1));
	program.Add(T_Command(T_Command::cdPushFloat, M_PI));
	program.Add(T_Command(T_Command::cdPushFloat, 4.0));
	program.Add(T_Command(T_Command::cdDiv));
	program.Add(T_Command(T_Command::cdOper, T_Oper::opCos));
	program.Add(T_Command(T_Command::cdAssign));
//	double s2c2; s2c2 = sgr(s) + sqr(c);
	program.Add(T_Command(T_Command::cdNewFloat));    // s2c2	
	program.Add(T_Command(T_Command::cdPushVar, 2));
	program.Add(T_Command(T_Command::cdPushVal, 0));  // s
	program.Add(T_Command(T_Command::cdOper, T_Oper::opSqr));
	program.Add(T_Command(T_Command::cdPushVal, 1));  // c
	program.Add(T_Command(T_Command::cdOper, T_Oper::opSqr));
	program.Add(T_Command(T_Command::cdAdd));
	program.Add(T_Command(T_Command::cdAssign));

	cpu.exec(program);
	cpu.print();
	
	return 0;
}


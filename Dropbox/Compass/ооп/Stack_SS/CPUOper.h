#ifndef CPU_OPER_H
#define	CPU_OPER_H
/*
 * 
 */
 
#include "TXLib.h"
 
#ifdef _CPU_DEBUG_MODE
	void _debug_return_false_(); 
	#define return_false { _debug_return_false_(); return false; }
#else
	#define return_false return false
#endif
	
const int PROGRAM_SIZE = 200, MEMORY_SIZE = 200, FRAME_SIZE = 20, FUNCTION_TABLE = 30;

struct T_Command
{
	enum T_Code { cdNone, cdPush, cdPop, cdOper, cdValOper, cdOperVal,
				  cdPushFloat, cdFloatToInt, cdIntToFloat, 
				  cdNewInt, cdNewFloat, cdPushVar, cdPushVal, cdGlobVar, cdGlobVal, cdAssign,
				  cdIfGoTo, cdElseGoTo, cdGoTo,
				  cdFunction, cdPrint,
				  cdIndex, cdTwoIndex, cdToVal, 
				  cdReturn
				};
	T_Code code;
	int    operand;
	double number;
	T_Command() : code(cdNone), operand(0), number(0.0) {}
	T_Command(T_Code c) : code(c), operand(0), number(0.0) {}
	T_Command(T_Code c, int o) : code(c), operand(o), number(0.0) {}
	T_Command(T_Code c, double n) : code(c), operand(0), number(n) {}
	void print() const;
	
	static const char* title[];
};

struct T_Program
{
	T_Command commands[PROGRAM_SIZE];
	int count;
	T_Program() : count(0) {}
	void Add(const T_Command &command);
	int  low()  const  { return 0; }
	int  last() const  { return count - 1; }
	bool inside(int command) const { return ((low() <= command) && (command <= last())); }
	const T_Command& Command(int command) const { return commands[command]; }
	T_Command& Item(int command) { return commands[command]; }
	void print() const;
};

struct T_Item
{
	enum T_Type { tpVoid, tpInt, tpFloat, tpAddress, tpValue }; 
	T_Type type;
	int    value;
	double number;
	T_Item() : type(tpVoid), value(0), number(0.0) {}
	T_Item(int v) : type(tpInt), value(v), number(0.0) {}
	T_Item(T_Type t, int v) : type(t), value(v), number(0.0) {}
	T_Item(double n) : type(tpFloat), value(0), number(n) {}
	int compare(const T_Item &funcFloat) const; // - -1 '<', 0 - '==', 1 - '>' 
	bool isInt()   const   { return type == tpInt; }
	bool isFloat() const   { return type == tpFloat; }
	bool isAddress() const { return type == tpAddress; }
	bool isValue() const   { return isInt() || isFloat(); }
	void toInt();
	void toFloat();
	void print() const;
};

struct T_Stack
{
	T_Item stack[MEMORY_SIZE];
	int top;
	T_Stack() : top(0) {}
	bool empty() const { return !top; }
	bool inside(int index) const { return ((0 <= index) && (index < top)); }
	void Push(const T_Item &item);
	bool Pop(T_Item &item); // if empty return_false
	T_Item& Item(int index) { return stack[index]; }
	T_Item& Top()           { return stack[top - 1]; }
	void Tranc(int t)       { top = t; }
	void print() const;
};

struct T_Oper
{
	enum T_Name { opNone, opExp, opLog, opLog10, opSin, opCos, opTan, opASin, opACos, opATan, opATan2, opPow, opSqrt, opSqr, opRand, 
				  opAbs, opSign, opMin, opMax,
				  opOr, opAnd, opOrOr, opAndAnd, opXOr, opTilda, opNot,
				  opAdd, opSub, opMulti, opDiv, opMod, 
				  opAddAssign, opSubAssign, opMultiAssign, opDivAssign, opModAssign, opAndAssign, opOrAssign, opXOrAssign, opAssign,
				  opMinus, opPlus, opPlusPlus, opMinusMinus,
				  opEqu, opNotEqu, opLT, opLE, opGT, opGE,
				  opLShift, opRShift, opLShiftAssign, opRShiftAssign,
				  _op_end 
				};
	enum T_Type { tfNone, 
				  tfInt_Int,     tfInt_Int_Int,       tfVoid_LInt_Int, tfVoid_LInt, 
				  tfFloat_Float, tfFloat_Float_Float, tfVoid_LFloat_Float, 
				  tfInt_Float,   tfInt_Float_Float,
				  tfXxx_Xxx,     tfXxx_Xxx_Xxx,       tfVoid_LXxx_Xxx, tfInt_Xxx, tfInt_Xxx_Xxx 
				};
	T_Type type;
	void  *func;
	void  *funcFloat;
	const char *title;
	T_Oper() : type(tfNone), func(NULL), funcFloat(NULL), title(NULL) {} 
	T_Oper(T_Type t, void *f) : type(t), func(f), funcFloat(NULL), title(NULL) {} 
	T_Oper(T_Type t, void *f, const char *c) : type(t), func(f), funcFloat(NULL), title(c) {} 
	T_Oper(T_Type t, void *f, void *j, const char *c) : type(t), func(f), funcFloat(j), title(c) {} 
	void print() const;
};

struct T_OperTable
{
	T_Oper table[T_Oper::_op_end];
	int count;
	T_OperTable();
	void Add(T_Oper::T_Name name, const T_Oper &oper) { table[name] = oper; }
	bool inside(int oper) const { return (0 <= oper) && (oper < count) && (Oper(oper).type != T_Oper::tfNone); }
	const T_Oper& Oper(int index) const { return table[index]; }
	void print() const;
	
	static const T_OperTable operation;
};

struct T_Function
{
	T_Item::T_Type result;
	int param, start;
	const T_Program* body;
	T_Function() : result(T_Item::tpVoid), param(0), start(0), body(NULL) {}
	T_Function(const T_Program &b, T_Item::T_Type t, int p, int s = 0) : result(t), param(p), start(s), body(&b) {}
	void print() const;
};

struct T_FunctionTable
{
	T_Function table[FUNCTION_TABLE];
	int count;
	T_FunctionTable(): count(0) {}
	int Add(const T_Function &func) { table[count] = func; return count++; }
	bool inside(int func) const { return (0 <= func) && (func < count); }
	const T_Function& Func(int index) const { return table[index]; }
	void print() const;	
};

struct T_Frame
{
	int offset;
	int ip_ret;
	T_Frame() : offset(0), ip_ret(0) {}
	T_Frame(int o, int r) : offset(o), ip_ret(r) {}
	void print() const;
};

struct T_FrameStack
{
	T_Frame stack[FRAME_SIZE];
	int top;
	T_FrameStack() : top(0) {}
	bool empty() const { return !top; }
	void Push(const T_Frame &item) { stack[top++] = item; }
	bool Pop(T_Frame &item)   { if (empty()) return false; item = stack[--top]; return true; }
	T_Frame& Frame(int index) { return stack[index]; }
	T_Frame& Top()            { return Frame(top - 1); }
	void print() const;
};

struct T_CPU
{
	T_Stack         stack;
	T_FrameStack    frames;
	T_FunctionTable functions;
	
	int ip;
	int  Add(const T_Function &func) { return functions.Add(func); }
	bool Exec(const T_Program &program, int start = 0);
	bool Exec(const T_Command &command);
	bool Exec(const T_Oper &oper, int mode = 0); // -1 - prefix, 0 - none, +1 - sufix
	void reset() { stack.top = 0; frames.top = 0; }  
	void print() const;
};

#endif

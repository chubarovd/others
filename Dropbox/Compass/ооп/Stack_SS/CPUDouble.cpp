/*
 * CPUDouble.cpp
 * 
 * Copyright 2013 Sergey Vaganov <SVaganov@svaganov-macbook.employees.compassplus.ru>
 * 
 */
 
#include "TXLib.h"

 
const int PROGRAM_SIZE = 20, MEMORY_SIZE = 20;

struct T_Command
{
	enum T_Code { cdNone, cdPush, cdPop, cdAdd, cdSub, cdMulti, cdDiv, cdMod, cdPushFloat, cdFloatToInt, cdIntToFloat, cdReturn };
	int code;
	int operand;
	double number;
	T_Command() : code(cdNone), operand(0), number(0.0) {}
	T_Command(int c) : code(c), operand(0), number(0.0) {}
	T_Command(int c, int o) : code(c), operand(o), number(0.0) {}
	T_Command(int c, double n) : code(c), operand(0), number(n) {}
};

struct T_Program
{
	T_Command commands[PROGRAM_SIZE];
	int count;
	T_Program() : count(0) {}
	void Add(const T_Command &command);
	int low()  const { return 0; }
	int last() const { return count - 1; }
	const T_Command& Command(int command) const;
};

const T_Command& T_Program::Command(int command) const 
{ 
	return commands[command];
};

struct T_ItemStack
{
	enum T_Type { tpNone, tpInt, tpFloat }; 
	int type;
	int value;
	double number;
	T_ItemStack() : type(tpNone), value(0), number(0.0) {}
	T_ItemStack(int v) : type(tpInt), value(v), number(0.0) {}
	T_ItemStack(int t, int v) : type(t), value(v), number(0.0) {}
	T_ItemStack(double n) : type(tpFloat), value(0), number(n) {}
};

struct T_Stack
{
	T_ItemStack stack[MEMORY_SIZE];
	int top;
	T_Stack() : top(0) {}
	bool empty() const { return !top; }
	void Push(const T_ItemStack &item);
	bool Pop(T_ItemStack &item); // if empty return false
	void print();
};

void T_Stack::print()
{
	for (int i = 0; i < top; i++ ) 
	{
		printf("type=%d, value= %d, number=%lf\n", stack[i].type, stack[i].value, stack[i].number);
	}
	printf("\n");
}

void T_Stack::Push(const T_ItemStack &item)
{
	stack[top] = item;
	top++;
}

bool T_Stack::Pop(T_ItemStack &item)
{
	top--;
	item = stack[top];
}

void T_Program::Add(const T_Command &command)
{
	commands[count] = command;
	count++; 
} 

struct T_CPU
{
	T_Stack stack;
	bool exec(const T_Program &program);
	bool exec(const T_Command &command);
	void print() { stack.print(); }
};

bool T_CPU::exec(const T_Command &command)
{
	switch (command.code)
	{
		case T_Command::cdPush  : {			
			T_ItemStack item(1, command.operand);
			stack.Push(item);			
			} break;
		case T_Command::cdPop   : {
			} break;
		case T_Command::cdAdd   : {
			T_ItemStack operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if (operand_one.type == T_ItemStack::tpInt && operand_two.type == T_ItemStack::tpInt)
			{
				operand_result.value = operand_one.value + operand_two.value;
			} 
			else 
			{
				operand_result.number = operand_one.number + operand_two.number;
			}
			stack.Push(operand_result);
			} break;
		case T_Command::cdSub   : {
			T_ItemStack operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if (operand_one.type == T_ItemStack::tpInt && operand_two.type == T_ItemStack::tpInt)
			{
				operand_result.value = operand_one.value - operand_two.value;
			} 
			else 
			{
				operand_result.number = operand_one.number - operand_two.number;
			}
			stack.Push(operand_result);
			} break;
		case T_Command::cdMulti : {
			T_ItemStack operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if (operand_one.type == T_ItemStack::tpInt && operand_two.type == T_ItemStack::tpInt)
			{
				operand_result.value = operand_one.value * operand_two.value;
			} 
			else 
			{
				operand_result.number = operand_one.number * operand_two.number;
			}
			stack.Push(operand_result);
			} break;
		case T_Command::cdDiv   : {
			T_ItemStack operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if (operand_one.type == T_ItemStack::tpInt && operand_two.type == T_ItemStack::tpInt)
			{
				operand_result.value = operand_one.value / operand_two.value;
			} 
			else 
			{
				operand_result.number = operand_one.number / operand_two.number;
			}
			stack.Push(operand_result);
			} break;
		case T_Command::cdMod   : {
			T_ItemStack operand_one, operand_two, operand_result;
			stack.Pop(operand_two);
			stack.Pop(operand_one);
			if (operand_one.type == T_ItemStack::tpInt && operand_two.type == T_ItemStack::tpInt)
			{
				operand_result.value = operand_one.value % operand_two.value;
			} 
			else 
			{
				printf(" Error! User is crab! ");
			}
			stack.Push(operand_result);
			} break;
		case T_Command::cdPushFloat   : {
			T_ItemStack item(command.number);
			stack.Push(item);
			} break;
		case T_Command::cdFloatToInt   : {
			T_ItemStack operand_one, operand_result;
			stack.Pop(operand_one);
			operand_result.value = operand_one.number;
			operand_result.type = T_ItemStack::tpInt;
			stack.Push(operand_result);
			} break;
		case T_Command::cdIntToFloat   : {
			T_ItemStack operand_one, operand_result;
			stack.Pop(operand_one);
			operand_result.number = operand_one.value;
			operand_result.type = T_ItemStack::tpFloat;
			stack.Push(operand_result);
			} break;	
		case T_Command::cdReturn   : {
			} break;
	}
};

bool T_CPU::exec(const T_Program &program)
{
	for(int i = program.low(); i <= program.last(); i++)
	{
		exec(program.Command(i));
	}
}

int main()
{
	T_CPU     cpu;
	T_Program program;
	
	program.Add(T_Command(T_Command::cdPushFloat, 5.0));
	program.Add(T_Command(T_Command::cdPush, 6));
	program.Add(T_Command(T_Command::cdIntToFloat));
	program.Add(T_Command(T_Command::cdAdd));
	
	program.Add(T_Command(T_Command::cdPush, 1));
	program.Add(T_Command(T_Command::cdPushFloat, 2.5));
	program.Add(T_Command(T_Command::cdFloatToInt));
	program.Add(T_Command(T_Command::cdAdd));
	
	program.Add(T_Command(T_Command::cdIntToFloat));
	program.Add(T_Command(T_Command::cdMulti));
	
	cpu.exec(program);
	cpu.print();
	
	return 0;
}


/*
 * 
 */
 
#include "TXLib.h"
#include "CPUOper.cpp"
 
struct T_Source
{
	enum T_Sym { slNone, slDigit, slAlpha, slOper, slSpace, slEol, slUnknown, slError, slEof };
	const char* text;
	int index, length;
	T_Source() : text(NULL), index(0), length(0) {}
	T_Source(const char* t) : text(t), index(0), length(1000000000) {}
	T_Sym Sym(int &sym);
	T_Sym GetSym(int &sym);
	int  Pos()   { return index; }
	void Back()  { if (index && text) index--; }
	void Reset() { index = 0; }
}; 

struct T_Lexeme
{
	enum T_Group { grNone, grInteger, grNumber, grOper, grSpace, grLine, grError, grEof };
	enum T_Type  { lxNone,  
				   lxAdd, lxSub, lxMulti, lxDiv, 
				   lxSpace, lxEol
				 };
	T_Group group;
	int     type;
	int pos, len;
	int    value;
	double number;
	T_Lexeme() : group(grNone), type(lxNone), pos(0), len(0) {}
	void print();
}; 

struct T_Scan
{
	T_Source *source;
	T_Scan(T_Source *s) : source(s) { source->Reset(); }
	T_Lexeme::T_Group Lex(T_Lexeme &l);
	void Reset() { source->Reset(); }
}; 

// --------------------------- T_Source ---------------------------
T_Source::T_Sym T_Source::Sym(int &sym)
{
	if (!text || (index >= length) || (!text[index]))
	{
		length = index; sym = 0;
		return slEof;
	}
	sym = text[index];
	if (sym == '\n') return slEol;
	if ((sym == ' ') || (sym == '\t')) return slSpace;
	if ((sym >= '0') && (sym <= '9')) return slDigit;
	if ((sym >= 'a') && (sym <= 'z') || (sym >= 'A') && (sym <= 'Z')) return slAlpha;
	if ((sym == '+') || (sym == '-') || (sym == '*') || (sym == '/')) return slOper;
	return slUnknown;
}
 
T_Source::T_Sym T_Source::GetSym(int &sym)
{
	T_Sym ret = Sym(sym);
	index++;
	return ret;
}

// --------------------------- T_Lexeme ---------------------------
void T_Lexeme::print()
{
	printf("group=%02d, type=%02d, pos=%d, len=%d, value=%d, number=%lf\n", group,  type, pos, len, value, number);
}

// --------------------------- T_Scan ---------------------------
T_Lexeme::T_Group T_Scan::Lex(T_Lexeme &l)
{
	T_Source::T_Sym type;
	int sym;
	l.pos = source -> Pos();
	l.len = 0;
	type = source -> GetSym(sym);
	
	if(type == T_Source::slEof){
		return l.group = l.grEof;
	}
	
	if(type == T_Source::slDigit){
		l.value = 0;
		do{
			l.len++;
			l.value *= 10;
			l.value += sym - '0';
			type = source -> GetSym(sym);
		}while(type == T_Source::slDigit);
		source -> Back();
		return l.group = l.grInteger;
	}
	
	if(type == T_Source::slOper){
		
		if(sym == '+'){
			l.type = l.lxAdd;
		}
		
		if(sym == '-'){
			l.type = l.lxSub;
		}
		
		if(sym == '*'){
			l.type = l.lxMulti;
		}
		
		if(sym == '/'){
			l.type = l.lxDiv;
		}
		l.len++;
		return l.group = l.grOper;
	}
	
		if(type == T_Source::slSpace){
		do{
			l.len++;
			type = source -> GetSym(sym);
		}while(type == T_Source::slSpace);
		source -> Back();
		return l.group = l.grSpace;
	}
	
	//return l.group;
}

// --------------------------- main ---------------------------

T_CPU cpu;

T_Source source("50 70-20 30+*");

void print_source()
{
	int sym;
	while(source.GetSym(sym) != T_Source::slEof)
	{
		printf("%c", sym);
	}
	printf("\n");
}

int step = 0;

void calc_source()
{
	T_Program program;	

	T_Scan   scan(&source);
	T_Lexeme lex;
	T_Lexeme::T_Group group = scan.Lex(lex);
	while(group != T_Lexeme::grEof)
	{
		lex.print();
		if (++step > 20) break;
		switch (group)
		{
			case T_Lexeme::grInteger : {
				program.Add(T_Command(T_Command::cdPush, lex.value));
				} break;
			case T_Lexeme::grNumber : {
				program.Add(T_Command(T_Command::cdPushFloat, lex.number));
				} break;
			case T_Lexeme::grOper : {
				T_Oper::T_Name oper;
				switch (lex.type)
				{
					case T_Lexeme::lxAdd :   oper = T_Oper::opAdd;   break;
					case T_Lexeme::lxSub :   oper = T_Oper::opSub;   break;
					case T_Lexeme::lxMulti : oper = T_Oper::opMulti; break;
					case T_Lexeme::lxDiv :   oper = T_Oper::opDiv;   break;
				}
				program.Add(T_Command(T_Command::cdOper, oper));
				} break;
		}
		
		group = scan.Lex(lex);
	}
	
	lex.print();
		
	program.Add(T_Command(T_Command::cdReturn));

	cpu.Exec(program,0);
	cpu.print();
}

int main()
{
	print_source();

	calc_source();
	
	return 0;
}

#ifdef _CPU_DEBUG_MODE
void _debug_return_false_()
{
	static bool debug = true;
	if (debug) 
	{
		debug = false;
		printf("---- debug begin ----\n");
		cpu.print();
		operation.print();
		printf("---- debug end ----\n");
	}
}
#endif

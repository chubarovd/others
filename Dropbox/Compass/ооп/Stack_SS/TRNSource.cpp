/*
 * 
 */
 
#include "TXLib.h"
#include "CPUOper.cpp"
 
struct T_Source
{
	enum T_Sym { slNone, slDigit, slAlpha, slOper, slSpace, slEol, slUnknown, slError, slEof };
	const char* text;
	int index, length;
	T_Source() : text(NULL), index(0), length(0) {}
	T_Source(const char* t) : text(t), index(0), length(1000000000) {}
	T_Sym Sym(int &sym);
	T_Sym GetSym(int &sym);
	void Back()  { if (index && text) index--; }
	void Reset() { index = 0; }
}; 

// --------------------------- T_Source ---------------------------
T_Source::T_Sym T_Source::Sym(int &sym)
{
	if (!text || (index >= length) || (!text[index]))
	{
		length = index; sym = 0;
		return slEof;
	}
	sym = text[index];
	if (sym == '\n') return slEol;
	if ((sym == ' ') || (sym == '\t')) return slSpace;
	if ((sym >= '0') && (sym <= '9')) return slDigit;
	if ((sym >= 'a') && (sym <= 'z') || (sym >= 'A') && (sym <= 'Z')) return slAlpha;
	if ((sym == '+') || (sym == '-') || (sym == '*') || (sym == '/')) return slOper;
	return slUnknown;
}
 
T_Source::T_Sym T_Source::GetSym(int &sym)
{
	T_Sym ret = Sym(sym);
	index++;
	return ret;
}

// --------------------------- main ---------------------------

//T_CPU cpu;

T_Source source("57-23+*");

void print_source()
{
	int sym;
	while (source.GetSym(sym) != T_Source::slEof)
	{
		printf("%c", sym);
	}
	printf("\n");
}

void calc_source()
{
	T_CPU cpu;
	T_Program program;	

	int sym;
	T_Source::T_Sym type = source.GetSym(sym);
	while (type != T_Source::slEof)
	{
		switch (type)
		{
			case T_Source::slDigit : {
				int value = sym - '0';
				program.Add(T_Command(T_Command::cdPush, value));
				} break;
			case T_Source::slOper : {
				T_Oper::T_Name oper;
				switch (sym)
				{
					case '+' : oper = T_Oper::opAdd;   break;
					case '-' : oper = T_Oper::opSub;   break;
					case '*' : oper = T_Oper::opMulti; break;
					case '/' : oper = T_Oper::opDiv;   break;
				}
				program.Add(T_Command(T_Command::cdOper, oper));
				} break;
		}
		type = source.GetSym(sym);
	}
		
	program.Add(T_Command(T_Command::cdReturn));

	cpu.Exec(program,0);
	cpu.print();
}

int main()
{
	print_source();
	
	source.Reset();
	calc_source();
	
	return 0;
}

#ifdef _CPU_DEBUG_MODE
void _debug_return_false_()
{
	static bool debug = true;
	if (debug) 
	{
		debug = false;
		printf("---- debug begin ----\n");
		cpu.print();
		operation.print();
		printf("---- debug end ----\n");
	}
}
#endif

/*
 * 
 */
 
#include "TXLib.h"
#define _CPU_DEBUG_MODE
#include "CPUOper.cpp"
#include "TRNScan.cpp"
 
// --------------------------- T_SourceFile ---------------------------
struct T_SourceFile : T_Source
{
	FILE *file;
	T_SourceFile() : T_Source(), file(NULL) {}
	T_SourceFile(const char* name) : T_Source(name), file(NULL) {}
	bool Open(const char* name = NULL);
	void Close() { if (file) fclose(file); file = NULL; }
	virtual int  Char(int pos) const;

};
 
// --------------------------- T_SourceFile ---------------------------
bool  T_SourceFile::Open(const char* name)
{
	if (name) text = name;
	file = fopen(text, "r");
	return file != NULL;
}
int   T_SourceFile::Char(int pos) const  
{
	fseek(file, pos, SEEK_SET);
	int c = getc(file);
	if (c == EOF) c = 0x00;
	return c; 
}

// --------------------------- T_OperSymbol ---------------------------
struct T_OperSymbol
{
	T_Oper::T_Name   oper;
	T_Symbol::T_Type symbol;
	T_OperSymbol() : oper(T_Oper::opNone), symbol(T_Symbol::lxNone) {}
	T_OperSymbol(T_Symbol::T_Type s, T_Oper::T_Name o) : oper(o), symbol(s) {}
};

struct T_OperSymbolTable
{
	static const int TABLE_LENGTH = 40;
	T_OperSymbol table[TABLE_LENGTH];
	int count;
	T_OperSymbolTable();
	void Add(const T_OperSymbol &sym) { if (count < TABLE_LENGTH) table[count++] = sym; }
	bool inside(int sym) const        { return (0 <= sym) && (sym < count); }
	const T_OperSymbol& Item(int index) const { return table[index]; }
	T_Oper::T_Name Find(T_Symbol::T_Type symbol) const;
	void print() const;
	
	static const T_OperSymbolTable oper_symbol;
}; 

T_Oper::T_Name T_OperSymbolTable::Find(T_Symbol::T_Type symbol) const
{
	for (int i = 0; i <count; i++)
	{
		if (table[i].symbol == symbol) return table[i].oper;
	}
	return T_Oper::opNone;
}

#define OPER_SYMBOL(TYPE, SYMBOL) Add(T_OperSymbol(T_Symbol::lx##TYPE, T_Oper::op##TYPE))
T_OperSymbolTable::T_OperSymbolTable() : count(0)
{
	OPER_SYMBOL(AddAssign,     "+=");
	OPER_SYMBOL(SubAssign,     "-=");
	OPER_SYMBOL(MultiAssign,   "*=");
	OPER_SYMBOL(DivAssign,     "/=");
	OPER_SYMBOL(ModAssign,     "%=");
	OPER_SYMBOL(AndAssign,     "&=");
	OPER_SYMBOL(OrAssign,      "|=");
	OPER_SYMBOL(XOrAssign,     "^=");
	OPER_SYMBOL(OrOr,          "||");
	OPER_SYMBOL(Or,             "|");
	OPER_SYMBOL(AndAnd,        "&&");
	OPER_SYMBOL(And,            "&");
	OPER_SYMBOL(XOr,            "^");
	OPER_SYMBOL(Tilda,          "~");
				   
	OPER_SYMBOL(Multi,          "*"); 
	OPER_SYMBOL(Div,            "/");
	OPER_SYMBOL(Mod,            "%");
				   
	OPER_SYMBOL(PlusPlus,      "++");
	OPER_SYMBOL(Add,            "+");
	OPER_SYMBOL(MinusMinus,    "--");
	OPER_SYMBOL(Sub,            "-");
				   
	OPER_SYMBOL(Equ,           "==");
	OPER_SYMBOL(Assign,         "=");
	OPER_SYMBOL(NotEqu,        "!=");
	OPER_SYMBOL(Not,            "!");
				  
	OPER_SYMBOL(LShiftAssign, "<<=");
	OPER_SYMBOL(LShift,        "<<");
	OPER_SYMBOL(LE,            "<=");
	OPER_SYMBOL(LT,             "<");
	OPER_SYMBOL(RShiftAssign, ">>=");
	OPER_SYMBOL(RShift,        ">>");
	OPER_SYMBOL(GE,            ">=");
	OPER_SYMBOL(GT,             ">");
}

// --------------------------- main ---------------------------
const T_OperSymbolTable T_OperSymbolTable::oper_symbol;

T_CPU cpu;

T_SourceFile source;

void print_source()
{
	int sym;
	while (source.GetSym(sym) != T_Source::slEof)
	{
		printf("%c", sym);
	}
	printf("\n");
}

bool calc_source()
{
	T_Program program;	
	
	T_Scan   scan(&source);
	T_Lexeme lex;
	T_Lexeme::T_Group group = scan.Lex(lex);
	while (group != T_Lexeme::grEof)
	{
		lex.print(&scan);
		if (lex.group == T_Lexeme::grIdent)
		{
			bool ret = scan.Find(lex, T_Command::title);
			if (ret) 
			{
				T_Lexeme space, operand;
				bool sign = false;
				switch (lex.value)
				{
					case T_Command::cdPush  : { 
						if ((scan.Lex(space) == T_Lexeme::grSpace) && (scan.Lex(operand) == T_Lexeme::grInteger) && (scan.Lex(space) == T_Lexeme::grLine)) 
						{
							program.Add(T_Command(T_Command::cdPush, operand.value));
						}
						else 
							return false;
						} break;
					case T_Command::cdPushFloat  : {
						if ((scan.Lex(space) == T_Lexeme::grSpace) && (scan.Lex(operand) == T_Lexeme::grNumber) && (scan.Lex(space) == T_Lexeme::grLine)) 
						{
							program.Add(T_Command(T_Command::cdPushFloat, operand.number));
						}
						else 
							return false;
						} break;
					case T_Command::cdOper   : {
						if ((scan.Lex(space) == T_Lexeme::grSpace)){
							scan.Lex(operand);
							if (operand.group == T_Lexeme::grOper)
							{
								T_Oper::T_Name oper = T_OperSymbolTable::oper_symbol.Find((T_Symbol::T_Type)operand.type);
								if (oper == T_Oper::opNone) return false;
								operand.value = oper;
							}
							else if (operand.group == T_Lexeme::grIdent)
							{
								bool find = false;
								for (int i = 0; i < T_OperTable::operation.count; i++)
								{
									const T_Oper &oper = T_OperTable::operation.Oper(i);
									if (oper.title && scan.Compare(operand, oper.title))
									{
										operand.value = i; find = true; break;
									}
								}
								if (!find) return false;
							}
							else 
								return false;
						    if (scan.Lex(space) == T_Lexeme::grLine) 
							{
								program.Add(T_Command((T_Command::T_Code)lex.value, operand.value));
							}
							else 
								return false;
						}
						else 
							return false;
						} break;
					case T_Command::cdNewInt  : 
					case T_Command::cdNewFloat : {
						int size = 0;
						if (scan.Lex(space) == T_Lexeme::grSpace) {
							if (scan.Lex(operand) == T_Lexeme::grInteger)
							{
								size = operand.value;
								scan.Lex(space);
							}
							else 
								return false;
						}
					    if (space.group == T_Lexeme::grLine) 
						{
							program.Add(T_Command((T_Command::T_Code)lex.value, size));
						}
						else 
							return false;
						} break;
					case T_Command::cdPushVar  : 
					case T_Command::cdPushVal  :
						sign = true;
					case T_Command::cdGlobVar  : 
					case T_Command::cdGlobVal  :
					case T_Command::cdIfGoTo   :
					case T_Command::cdElseGoTo :
					case T_Command::cdGoTo     :
					case T_Command::cdFunction : { 
						if (scan.Lex(space) == T_Lexeme::grSpace)
						{
							scan.Lex(operand);
							int znak = 1;
							if (sign && (operand.group == T_Lexeme::grOper) && (operand.type == T_Symbol::lxSub))
							{
								znak = -1;
								scan.Lex(operand);
							}
							if (operand.group == T_Lexeme::grInteger)
							{
								 operand.value *= znak;
							}
							else 
								return false;
						}
					    if (scan.Lex(space) == T_Lexeme::grLine) 
						{
							program.Add(T_Command((T_Command::T_Code)lex.value, operand.value));
						}
						else 
							return false;
						} break;
					case T_Command::cdPop   : 
					case T_Command::cdIntToFloat : 
					case T_Command::cdFloatToInt : 
					case T_Command::cdAssign : 
					case T_Command::cdIndex  : 
					case T_Command::cdTwoIndex : 
					case T_Command::cdToVal    :
					case T_Command::cdReturn   : {
						if (scan.Lex(space) == T_Lexeme::grLine) 
						{
							program.Add(T_Command((T_Command::T_Code)lex.value));
						}
						else 
							return false;
						} break;
					default : {
						return false;
						} break;
				}
			}
			else // no find ident
				return false;
		}
		else if (lex.group == T_Lexeme::grOper)
		{
			T_Lexeme space;
			T_Oper::T_Name oper = T_OperSymbolTable::oper_symbol.Find((T_Symbol::T_Type)lex.type);
			if (oper == T_Oper::opNone) return false;
		    if (scan.Lex(space) == T_Lexeme::grLine) 
			{
				program.Add(T_Command(T_Command::cdOper, oper));
			}
			else 
				return false;
		}
		else if (lex.group == T_Lexeme::grSpace)
		{
			
		}
		else if (lex.group == T_Lexeme::grLine)
		{
			
		}
		group = scan.Lex(lex);
	}
	
	lex.print(&scan); 
	printf("\n");
	program.print();
		
	cpu.Exec(program,0);
	cpu.print();

	return true;
}

int main()
{
	
	if (!source.Open("commands.txt")) return 1;
	print_source();
	
	calc_source();
	
	return 0;
}

#ifdef _CPU_DEBUG_MODE
void _debug_return_false_()
{
	static bool debug = true;
	if (debug) 
	{
		debug = false;
		printf("---- debug begin ----\n");
		cpu.print();
		T_OperTable::operation.print();
		printf("---- debug end ----\n");
	}
}
#endif


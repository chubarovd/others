/*
 * 
 */
 
#include "TXLib.h"
 
struct T_Source
{
	enum T_Sym { slNone, slDigit, slAlpha, slOper, slSeparator, slSpace, slEol, slUnknown, slError, slEof };
	const char* text;
	int index, length;
	T_Source() : text(NULL), index(0), length(MAX_LENGTH) {}
	T_Source(const char* t) : text(t), index(0), length(MAX_LENGTH) {}
	T_Sym Sym(int &sym);
	T_Sym GetSym(int &sym);
	virtual int Char(int pos) const { return text[pos]; }
	int  Pos() const          { return index; }
	void Back()               { if (index && text) index--; }
	void Reset()              { index = 0; }
	void Reset(const char* t) { text = t; length = MAX_LENGTH; index = 0; }

	static const int MAX_LENGTH = 10000000;
}; 

struct T_Scan;
struct T_Lexeme
{
	enum T_Group { grNone, grInteger, grNumber, grIdent, grOper, grSeparator, grReserv, grSpace, grLine, grError, grEof };
	enum T_Type  { lxNone, lxSpace, lxEol
				 };
	T_Group group;
	int     type;
	int pos, len;
	int    value;
	double number;
	T_Lexeme() : group(grNone), type(lxNone), pos(0), len(0) {}
	void print(const T_Scan *scan = NULL) const;
}; 

struct T_Scan
{
	T_Source *source;
	T_Scan(T_Source *s) : source(s) { source->Reset(); }
	T_Lexeme::T_Group Lex(T_Lexeme &l);
	T_Source::T_Sym GetSym(int &sym) const { return source->GetSym(sym); }
	int  Char(int pos) const     { return source->Char(pos); }
	int  Pos()  const            { return source->Pos(); }
	void Back() const            { source->Back(); }
	void Reset()                 { source->Reset(); }
	bool Find(T_Lexeme &l, const char* table[]) const;
	bool Compare(T_Lexeme &l, const char* title) const;
}; 

struct T_Symbol
{
	enum T_Type  { lxNone, lxAdd, lxSub, lxMulti, lxDiv, lxMod,
				   lxAddAssign, lxSubAssign, lxMultiAssign, lxDivAssign, lxModAssign, lxAndAssign, lxOrAssign, lxXOrAssign, lxAssign,
				   lxOr, lxAnd, lxOrOr, lxAndAnd, lxXOr, lxTilda, lxNot,
				   lxPlusPlus, lxMinusMinus,
				   lxEqu, lxNotEqu, lxLT, lxLE, lxGT, lxGE,
				   lxLShift, lxRShift, lxLShiftAssign, lxRShiftAssign,
				   lxLeftCramp, lxRightCramp, lxLeftBracket, lxRightBracket, lxRightScrape, lxLeftScrape, lxQuestion,
				   lxColonColon, lxSlach, lxPoint, lxComma, lxSemicolon, lxColon, /*lxCommentBegin, lxCommentEnd,*/
				   _lx_end_
				 };
	T_Lexeme::T_Group group;
	T_Type            type;
	const char* text;
	T_Symbol() : group(T_Lexeme::grNone), type(T_Symbol::lxNone), text(NULL) {}
	T_Symbol(T_Lexeme::T_Group g, T_Symbol::T_Type t, const char* s) : group(g), type(t), text(s) {}
	bool Compare(T_Lexeme &lex, T_Scan &scan) const;
}; 

struct T_SymbolTable
{
	T_Symbol table[T_Symbol::_lx_end_];
	int count;
	T_SymbolTable();
	void Add(const T_Symbol &sym) { table[count++] = sym; }
	bool inside(int sym) const    { return (0 <= sym) && (sym < count); }
	const T_Symbol& Symbol(int index) const { return table[index]; }
	bool Find(T_Lexeme &lex, T_Scan &scan) const;
	void print() const;
	
	static const T_SymbolTable symbol_table;
};

// --------------------------- T_Source ---------------------------
T_Source::T_Sym T_Source::Sym(int &sym)
{
	if (!text || (index >= length) || (!Char(index)))
	{
		if (index < length) length = index; 
		sym = 0;
		return slEof;
	}
	sym = Char(index);
	if (sym == '\n') return slEol;
	if ((sym == ' ') || (sym == '\t')) return slSpace;
	if ((sym >= '0') && (sym <= '9'))  return slDigit;
	if ((sym >= 'a') && (sym <= 'z') || (sym >= 'A') && (sym <= 'Z') || (sym == '_')) return slAlpha;
	if ((sym == '.') || (sym == ',') || (sym == ';') || (sym == ':')) return slSeparator;
	if ((sym == '+') || (sym == '-') || (sym == '*') || (sym == '/')) return slOper;
	return slUnknown;
}
 
T_Source::T_Sym T_Source::GetSym(int &sym)
{
	T_Sym ret = Sym(sym);
	index++;
	return ret;
}

// --------------------------- T_Lexeme ---------------------------
void T_Lexeme::print(const T_Scan *scan) const
{
	printf("group=%02d, type=%02d, pos=%03d, len=%02d", group,  type, pos, len);
	if (group == grInteger) printf(", value=%d", value);
	if (group == grNumber)  printf(", number=%lf", number);
	if ((group == grOper) || (group == grSeparator) || (group == grReserv)) printf(", value=%d", value);
	if (scan && len) 
	{
		printf(", text=");
		for (int i = 0; i < len; i++) printf("%c", scan->Char(pos + i));
	}
	printf("\n");
}

// --------------------------- T_Scan ---------------------------
bool T_Scan::Find(T_Lexeme &l, const char* table[]) const
{
	int index = 0;
	const char *text = table[index];
	while (text) { 
		if (Compare(l, text)) { l.value = index; return true; }
		text = table[++index];
	}
	return false;
}

bool T_Scan::Compare(T_Lexeme &l, const char* title) const
{
	int len = 0;
	int pos = l.pos;
	const char* p = title;
	while(p){
		int sym = Char(pos + len);
		if(p[0] != sym) return false;
			p++; 
			len++;
	}
	if(len == l.len) return false;
}

T_Lexeme::T_Group T_Scan::Lex(T_Lexeme &l)
{
	
	int sym;
	l.group = T_Lexeme::grError;
	l.type  = T_Lexeme::grError;
	l.pos = Pos();
	l.len = 0;
	T_Source::T_Sym type = GetSym(sym);
	//~ printf("pos=%d, type=%d, sym=%c\n", Pos(), type, sym);
	
	if(type == T_Source::slEof){
		return l.group = l.grEof;
	}
	
	if(type == T_Source::slDigit){
		l.value = 0;
		do{
			l.len++;
			l.value *= 10;
			l.value += sym - '0';
			type = source -> GetSym(sym);
		}while(type == T_Source::slDigit);
		
		if(type != T_Source::slSeparator){
			l.len++;
			l.group = l.grInteger;
		}else{
			l.len++;
			type = source -> GetSym(sym);
			l.number = l.value;
			double step = 1;
			do{
				l.len++;
				step *= 10;
				l.number += (sym - '0')/step;
				type = source -> GetSym(sym);
			}while(type == T_Source::slDigit);
			l.group = l.grNumber;
		}
		source -> Back();
		//l.type = type;
		return l.group;
	}
	
	if(type == T_Source::slOper){	
		if(sym == '/'){
			int lenght = l.len;
			l.len++;
			type = source -> GetSym(sym);
			if(sym == '*'){
				black_label: ; //~
					do{
						l.len++;
						type = source -> GetSym(sym);
					}while(sym != '*' and type != T_Source::slEof);
					if(sym == '*'){
						l.len++;
						type = source -> GetSym(sym);
						if(sym == '/'){
							l.len++;
							type = source -> GetSym(sym);
							source -> Back();
							return l.group = l.grSpace;
						}else goto black_label;
					}else if(type == T_Source::slEof){
						return l.group = l.grError;
					}
			}else{
				l.len = lenght;
				l.type = T_Symbol::lxDiv;   
			}	
		}
		
		l.group = l.grOper;
		if(T_SymbolTable::symbol_table.Find(l, *this)) return l.group;
		l.len++;
	}
	
	if(type == T_Source::slAlpha){
		do{
			l.len++;
			type = source -> GetSym(sym);
		}while(type == T_Source::slAlpha);
		source -> Back();
		return l.group = l.grSpace;
	}
	
	if(type == T_Source::slSeparator){
		l.len++;
		return l.group = l.grSeparator;
	}
	
	if(type == T_Source::slSpace){
		do{
			l.len++;
			type = source -> GetSym(sym);
		}while(type == T_Source::slSpace);
		source -> Back();
		return l.group = l.grSpace;
	}
}

// --------------------------- T_Symbol ---------------------------
bool T_Symbol::Compare(T_Lexeme &lex, T_Scan &scan) const
{
	int len = 0;
	int pos = lex.pos;
	const char* p = text;
	while(p[0]){
		int sym = scan.Char(pos + len);
		if(p[0] != sym) return false;
		p++; 
		len++;
	}
	if(len == lex.len) return false;
	lex.type = type;
	return true;
	
}

// --------------------------- T_SymbolTable ---------------------------
#define ADD_SYMBOL(GROUP, TYPE, TEXT) Add(T_Symbol(T_Lexeme::gr##GROUP, T_Symbol::lx##TYPE, TEXT))

T_SymbolTable::T_SymbolTable() : count(0)
{

	ADD_SYMBOL(Oper, AddAssign,     "+=");
	ADD_SYMBOL(Oper, SubAssign,     "-=");
	ADD_SYMBOL(Oper, MultiAssign,   "*=");
	ADD_SYMBOL(Oper, DivAssign,     "/=");
	ADD_SYMBOL(Oper, ModAssign,     "%=");
	ADD_SYMBOL(Oper, AndAssign,     "&=");
	ADD_SYMBOL(Oper, OrAssign,      "|=");
	ADD_SYMBOL(Oper, XOrAssign,     "^=");
	ADD_SYMBOL(Oper, OrOr,          "||");
	ADD_SYMBOL(Oper, Or,             "|");
	ADD_SYMBOL(Oper, AndAnd,        "&&");
	ADD_SYMBOL(Oper, And,            "&");
	ADD_SYMBOL(Oper, XOr,            "^");
	ADD_SYMBOL(Oper, Tilda,          "~");
				   
	ADD_SYMBOL(Oper, Multi,          "*"); 
	ADD_SYMBOL(Oper, Div,            "/");
	ADD_SYMBOL(Oper, Mod,            "%");
				   
	ADD_SYMBOL(Oper, PlusPlus,      "++");
	ADD_SYMBOL(Oper, Add,            "+");
	ADD_SYMBOL(Oper, MinusMinus,    "--");
	ADD_SYMBOL(Oper, Sub,            "-");
				   
	ADD_SYMBOL(Oper, Equ,           "==");
	ADD_SYMBOL(Oper, Assign,         "=");
	ADD_SYMBOL(Oper, NotEqu,        "!=");
	ADD_SYMBOL(Oper, Not,            "!");
				  
	ADD_SYMBOL(Oper, LShiftAssign, "<<=");
	ADD_SYMBOL(Oper, LShift,        "<<");
	ADD_SYMBOL(Oper, LE,            "<=");
	ADD_SYMBOL(Oper, LT,             "<");
	ADD_SYMBOL(Oper, RShiftAssign, ">>=");
	ADD_SYMBOL(Oper, RShift,        ">>");
	ADD_SYMBOL(Oper, GE,            ">=");
	ADD_SYMBOL(Oper, GT,             ">");
	
	ADD_SYMBOL(Separator, LeftCramp   ,"{");
	ADD_SYMBOL(Separator, RightCramp  ,"}");
	ADD_SYMBOL(Separator, LeftBracket ,"(");
	ADD_SYMBOL(Separator, RightBracket,")");
	ADD_SYMBOL(Separator, LeftScrape  ,"[");
	ADD_SYMBOL(Separator, RightScrape ,"]");
	ADD_SYMBOL(Separator, Question    ,"?");
	ADD_SYMBOL(Separator, ColonColon ,"::");
	ADD_SYMBOL(Separator, Slach      ,"\\");
	ADD_SYMBOL(Separator, Point,       ".");
	ADD_SYMBOL(Separator, Semicolon,   ";");
	ADD_SYMBOL(Separator, Colon,       ":");
	ADD_SYMBOL(Separator, Comma,       ",");
	
	//~ ADD_SYMBOL(Separator, CommentBegin,"/*");
	//~ ADD_SYMBOL(Separator, CommentBegin,"*/");
}

bool T_SymbolTable::Find(T_Lexeme &lex, T_Scan &scan) const
{
	for (int i = 0; i < count; i++)
	{
		const T_Symbol& symbol = Symbol(i);
		if (symbol.Compare(lex, scan)) { lex.value = i; return true; }
	}
	return false;
}

const T_SymbolTable T_SymbolTable::symbol_table;


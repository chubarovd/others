/*
 * 
 * Copyright 2012 Sergey Vaganov <SVaganov@svaganov-macbook.employees.compassplus.ru>
 * 
 */


#include "TXLib.h"
#include "stdlib.h"

const int W = 1000, H = 800;
struct T_Fig
{
	int x, y;
	T_Fig () : x(0), y(0) {}
	T_Fig (int x_, int y_) { x = x_; y = y_; }   //  konstructor 
	T_Fig (const T_Fig& f) : x(f.x), y(f.y) {}
	virtual void draw() {};
	virtual void draw(int x_, int y_) {};
};

struct T_Fig_Color : T_Fig
{
	COLORREF color;
	T_Fig_Color() : color(TX_BLACK) {}
	T_Fig_Color (const T_Fig& f, COLORREF c) : T_Fig(f), color(c) {}
	T_Fig_Color (int x_, int y_, COLORREF c) : color(c) { x =x_; y = y_; }
	T_Fig_Color (const T_Fig_Color& f) : T_Fig(f), color(f.color) {}
};

struct T_Circle : T_Fig
{
	int r;
	T_Circle() : r(10) {}
	T_Circle (const T_Fig& f, int r_) : T_Fig(f), r(r_) {}
	T_Circle (int x_, int y_, int r_) : T_Fig(x_, y_), r(r_) {}
	T_Circle (const T_Circle& f) : T_Fig(f), r(f.r) {}
	virtual void draw() { txCircle(x, y, r); };
	virtual void draw(int x_, int y_) { txCircle(x_ + x, y_ + y, r); };
};

struct T_Rectangle : T_Fig
{
	int w, h;
	T_Rectangle() : w(20), h(10) {}
	T_Rectangle(int x_, int y_, int w_, int h_) : T_Fig (x_, y_), w(w_), h(h_) {}
	virtual void draw() { txRectangle(x, y, x + w, y + h); };
	virtual void draw(int x_, int y_) { txRectangle(x_ + x, y_ + y, x_ + x + w, y_ + y + h); };
};

struct T_Man : T_Fig, T_Rectangle, T_Circle
{
	T_Man () {}
	T_Man (const T_Fig& f, const T_Rectangle& rec, const T_Circle& c);
	T_Man (int x_, int y_, int w_, int h_, int r_);
	virtual void draw();
};

T_Man::T_Man(const T_Fig& f, const T_Rectangle& rec, const T_Circle& c) : T_Fig(f), T_Rectangle(rec), T_Circle(c) 
{
	T_Circle::y = r; T_Rectangle::y = 2 * r; 
	if (w > 2 * r) {
		T_Circle::x = w / 2; T_Rectangle::x = 0;
	}
	else {
		T_Circle::x = r; T_Rectangle::x = r - w / 2;
	}
 
}

T_Man::T_Man (int x_, int y_, int w_, int h_, int r_) : T_Fig(x_, y_)
{	
	w = w_; h = h_; 
	T_Circle::y = T_Circle::r = r_; T_Rectangle::y = 2 * r;
	if (w > 2 * r) {
		T_Circle::x = w / 2; T_Rectangle::x = 0;
	}
	else {
		T_Circle::x = r; T_Rectangle::x = r - w / 2;
	}
}

void T_Man::draw() 
{ 
	T_Fig *fig = (T_Fig*)(void*)this;
	int x__ = T_Circle::x;
	T_Rectangle::draw((*fig).x, (*fig).y); T_Circle::draw((*fig).x, (*fig).y); 
};

struct T_Mun : T_Fig
{
	T_Rectangle rec; T_Circle cir;
	T_Mun () {}
	T_Mun (const T_Fig& f, const T_Rectangle& rec, const T_Circle& c);
	T_Mun (int x_, int y_, int w_, int h_, int r_);
	virtual void draw();
};

T_Mun::T_Mun(const T_Fig& f, const T_Rectangle& rec_, const T_Circle& cir_) : T_Fig(f), rec(rec_), cir(cir_) 
{
	cir.y = cir.r; rec.y = 2 * cir.r; 
	if (rec.w > 2 * cir.r) {
		cir.x = rec.w / 2; rec.x = 0;
	}
	else {
		cir.x = cir.r; rec.x = cir.r - rec.w / 2;
	}
}

T_Mun::T_Mun (int x_, int y_, int w_, int h_, int r_) : T_Fig(x_, y_)
{	
	rec.w = w_; rec.h = h_; 
	cir.y = cir.r; rec.y = 2 * cir.r; 
	if (rec.w > 2 * cir.r) {
		cir.x = rec.w / 2; rec.x = 0;
	}
	else {
		cir.x = cir.r; rec.x = cir.r - rec.w / 2;
	}
}

void T_Mun::draw() 
{ 
	rec.draw(x, y); cir.draw(x, y); 
};

struct T_This : T_Fig
{
	T_This *t;
	T_This() : t(this) {}
	T_This (int x_, int y_) : T_Fig (x_, y_), t(this) {} 
};

void dump(void *addr, int size)
{
	for (int i = 1; i <= size; i++) {
		printf("%02x", ((unsigned char*)addr)[i - 1]);
		if (!(i % 4)) printf(" ");
	}
	printf("\n");
}

T_Fig fig(10,20);
T_Circle cir(100, 150, 30);
T_Rectangle rec(200, 100, 30, 20);
T_Man man(400, 500, 50, 40, 20);
T_Mun mun(300, 500, 50, 40, 70);

T_This t_global(7, 8);

void draw(T_Fig * f)
{
	f->draw();
}

int main(int argc_, char **argv_)
{
	txCreateWindow(W, H);
	dump(&fig, sizeof(fig));
	dump(&man, sizeof(man));
	
	T_This t_local(7, 8);	
	T_This *t_mem = new T_This(7, 8);
	
	dump(&t_global, sizeof(t_global));
	dump(&t_local, sizeof(t_local));
	dump(t_mem, sizeof(T_This));
	
	T_Man *m = new T_Man(100, 200, 50, 40, 20);
	dump(m, sizeof(T_Man));

	int *iii = new int[10];
	printf("i=%d\n", *iii);

	delete (m);
	delete (t_mem);
	
	man.draw();

	draw(&cir);
	draw(&mun);
	
	draw((T_Fig*)(void*)&cir);
	draw((T_Fig*)(void*)&rec);
	draw((T_Fig*)(void*)&man);
	
	while (GetAsyncKeyState (VK_ESCAPE) >= 0);
	
	return 0;
}


/*
 * 
 */
 
#include "TXLib.h"
#include "CPUOper.cpp"
#include "TRNScan.cpp"
 
// --------------------------- main ---------------------------

T_CPU cpu;

//T_Source source("55555555555555555555 5.88888888888888888012345678901234");
//T_Source source("<< <* 5.8iiiii=iiii 3-4 1.0+*");
//T_Source source("for;forf For+++<<= >>");
T_Source source("1 2 3/*hhjjhlhhlhljk* 4 5 6 7.0******7 6*5*4*3*2*1*-");

void print_source()
{
	int sym;
	while (source.GetSym(sym) != T_Source::slEof)
	{
		printf("%c", sym);
	}
	printf("\n");
}

void calc_source()
{
	T_Program program;	
	
	T_Scan   scan(&source);
	T_Lexeme lex;
	T_Lexeme::T_Group group = scan.Lex(lex);
	while (group != T_Lexeme::grEof)
	{
		lex.print(&scan);
		switch (lex.group)
		{
			case T_Lexeme::grInteger : {
				program.Add(T_Command(T_Command::cdPush, lex.value));
				program.Add(T_Command(T_Command::cdIntToFloat));
				} break;
			case T_Lexeme::grNumber : {
				program.Add(T_Command(T_Command::cdPushFloat, lex.number));
				} break;
			case T_Lexeme::grOper : {
				T_Oper::T_Name oper = T_Oper::opNone;
				switch (lex.type)
				{
					case T_Symbol::lxAdd :   oper = T_Oper::opAdd;   break;
					case T_Symbol::lxSub :   oper = T_Oper::opSub;   break;
					case T_Symbol::lxMulti : oper = T_Oper::opMulti; break;
					case T_Symbol::lxDiv :   oper = T_Oper::opDiv;   break;
				}
				program.Add(T_Command(T_Command::cdOper, oper));
				} break;
		}
		group = scan.Lex(lex);
	}
	lex.print(&scan);
		
	program.Add(T_Command(T_Command::cdReturn));

	cpu.Exec(program,0);
	cpu.print();
}

int main()
{
	print_source();
	
	calc_source();
	
	return 0;
}

#ifdef _CPU_DEBUG_MODE
void _debug_return_false_()
{
	static bool debug = true;
	if (debug) 
	{
		debug = false;
		printf("---- debug begin ----\n");
		cpu.print();
		T_OperTable::operation.print();
		printf("---- debug end ----\n");
	}
}
#endif

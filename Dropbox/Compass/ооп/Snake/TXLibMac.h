/* 
 * File:   TXLibMac.h
 * Author: svaganov
 *
 * Created on 19 March 2012, 13:44
 */
#ifndef TXLIBMAC_H
#define	TXLIBMAC_H

#include <mach-o/dyld.h>

#if (GTK_MAJOR_VERSION == 3)  
#else
#include <gdk/gdkkeysyms.h>
#endif

string WinToUtf(const char* InputText) {
    GError* error = NULL;
    string OutputText = g_convert(InputText, -1, "utf-8", "windows-1251", NULL, NULL, &error);
    if (error) { g_error_free(error); }
    return OutputText;
}

string UtfToWin(const char* InputText) {
    GError* error = NULL;
    string OutputText = g_convert(InputText, -1, "windows-1251", "utf-8", NULL, NULL, &error);
    if (error) { g_error_free(error); }
    return OutputText;
}

int MultiByteToWideChar(
    UINT CodePage,
    DWORD dwFlags,
    LPCSTR lpMultiByteStr,
    int cbMultiByte,
    LPWSTR lpWideCharStr,
    int cchWideChar
) 
{
    gsize bytes_read, bytes_written = 0;  
    GError* error = NULL;
    gchar* OutputText = g_convert(lpMultiByteStr, -1, "utf-8", "windows-1251", &bytes_read, &bytes_written, &error); // G_GNUC_MALLOC
    if (error) { 
        g_error_free(error); bytes_written = 0; 
    } else {
$       strncpy_s ((char*)lpWideCharStr, OutputText, cchWideChar - 1);
    }
    return bytes_written;
}

//HWND _txCanvas_Window;
GtkWidget *drawing_area;
static cairo_surface_t *surface = NULL;
static int surface_width = 500, surface_height = 500;

static const char* argv[] = { __FILE__ };

static struct {
    int argc;
    union {
    const char** argv_const;
          char** argv;
    };
    int window_size_x, window_size_y;
    bool centered;
    int thickness;
    int count_lock;
    COLORREF color, fill_color;
    double color_r, color_g, color_b, color_a;
    double fill_color_r, fill_color_g, fill_color_b, fill_color_a;
    pthread_t thread_info;

} tx_gtk_data = {1, argv, 500, 400, true, 1, 0};

#define __MAC_LOCK_THREAD__
void tx_gdk_enter();
void tx_gdk_leave();

void tx_gdk_enter() {
#ifdef  __MAC_LOCK_THREAD__   
    if (!tx_gtk_data.count_lock)
#endif
        gdk_threads_enter();
    
    while (gtk_events_pending ()) gtk_main_iteration ();

}

void tx_gdk_leave() {
#ifdef  __MAC_LOCK_THREAD__   
    if (!tx_gtk_data.count_lock)
#endif
        gdk_threads_leave();
}

static short __vk_key__[256] = {};
//#define GDK_KEY_Mode_switch   0xff7e
//#define GDK_KEY_script_switch 0xff7e

int GTK2VK(int keyval) {
    int vk = 0;
  
    switch (keyval) {

#undef  GDK_VK(GDK,VK)  
#define GDK_VK(GDK,VK)   case GDK: vk = VK; break; 

            GDK_VK(GDK_KEY_Shift_L   , VK_LSHIFT)
            GDK_VK(GDK_KEY_Shift_R   , VK_RSHIFT)
            GDK_VK(GDK_KEY_Control_L , VK_LCONTROL)
            GDK_VK(GDK_KEY_Control_R , VK_RCONTROL)
            GDK_VK(GDK_KEY_space     , VK_SPACE)
            GDK_VK(GDK_KEY_Prior     , VK_PRIOR)
            GDK_VK(GDK_KEY_Next      , VK_NEXT)
            GDK_VK(GDK_KEY_End       , VK_END)
            GDK_VK(GDK_KEY_Home      , VK_HOME)
            GDK_VK(GDK_KEY_Left      , VK_LEFT)
            GDK_VK(GDK_KEY_Up        , VK_UP)
            GDK_VK(GDK_KEY_Right     , VK_RIGHT)
            GDK_VK(GDK_KEY_Down      , VK_DOWN)
            GDK_VK(GDK_KEY_Escape    , VK_ESCAPE)                    
            GDK_VK(GDK_KEY_BackSpace , VK_BACK)
            GDK_VK(GDK_KEY_Tab       , VK_TAB)
            GDK_VK(GDK_KEY_Num_Lock  , VK_NUMLOCK)
            GDK_VK(GDK_KEY_Clear     , VK_CLEAR)
            GDK_VK(GDK_KEY_Return    , VK_RETURN)
            GDK_VK(GDK_KEY_Menu      , VK_MENU)
            GDK_VK(GDK_KEY_Pause     , VK_PAUSE)
            GDK_VK(GDK_KEY_Insert    , VK_INSERT)
            GDK_VK(GDK_KEY_Delete    , VK_DELETE)
            GDK_VK(GDK_KEY_Help      , VK_HELP)
            GDK_VK(GDK_KEY_Alt_L     , VK_LMENU)
            GDK_VK(GDK_KEY_Alt_R     , VK_RMENU)
                                       
#undef  GDK_VK(GDK,VK) 

        default:
            if ((GDK_KEY_0  <= keyval) && (keyval <= GDK_KEY_9)) {
                vk = keyval;
                break;
            }
            if ((GDK_KEY_A  <= keyval) && (keyval <= GDK_KEY_Z)) {
                vk = keyval;
                break;
            }
            if ((GDK_KEY_a  <= keyval) && (keyval <= GDK_KEY_z)) {
                vk = keyval;
                break;
            }
            if ((GDK_KEY_F1 <= keyval) && (keyval <= GDK_KEY_F24)) {
                vk = (keyval - GDK_KEY_F1) + VK_F1;
                break;
            }
            break;
    }

    return vk;
}

short GetAsyncKeyState(int key) {
    if ((key <= 0) || (256 <= key)) return 0;
    tx_gdk_enter();
    int ret = __vk_key__[key];
    __vk_key__[key] &= 0x7FFF;
    tx_gdk_leave();
    return ret;
}

static gboolean
key_event(GtkWidget *widget, GdkEventKey *event, gpointer data) {
    short press;
    if (event->type == GDK_KEY_PRESS) {
        press = 0x8001;
    } else if (event->type == GDK_KEY_RELEASE) {
        press = 0x0000;
    } else {
        return FALSE;
    }

    int key = GTK2VK(event -> keyval);
    if (key) {
        __vk_key__[key] = press;
    }

    if ((event -> keyval == GDK_KEY_Control_L) || (event -> keyval == GDK_KEY_Control_R)) {
        __vk_key__[VK_CONTROL] = (__vk_key__[VK_LCONTROL] || __vk_key__[VK_RCONTROL]) ? 0x8001 : 0x0000;
    }
    if ((event -> keyval == GDK_KEY_Shift_L) || (event -> keyval == GDK_KEY_Shift_R)) {
        __vk_key__[VK_SHIFT] = (__vk_key__[VK_LSHIFT] || __vk_key__[VK_RSHIFT]) ? 0x8001 : 0x0000;
    }

    return FALSE;
}

namespace {
    namespace TX { // <<<<<<<<< The main code is here, unfold it

//        const char* _txError(const char file[] /*= NULL*/, int line /*= 0*/, const char func[] /*= NULL*/,
//                const char msg [] /*= NULL*/, ...) {
//            return msg;
//        }

        inline bool _txCanvas_OK() {
            $1 return (surface != NULL);
        }

        inline bool txOK() {
            $1 return _txCanvas_OK();
        }

    }
} // namespace TX, anonymous namespace

typedef struct SelectFontStruct {
    const char* name;
    double size, sizeX;
    int bold;
    bool italic, underline, strikeout;
    double angle;
    unsigned ta_align;
};

static SelectFontStruct ConsoleSelectFont = { "Verdana", 10, -1, FW_DONTCARE, false, false, 0, false, TA_LEFT | TA_TOP };

GtkWidget     *console_window;
GtkWidget     *console_text_view;
GtkTextBuffer *console_buffer;
GtkTextIter    console_iter, console_start, console_end;

int ConsoleBackgroundColor = 0;
int ConsoleFontColor = 0;


static void
close_console_window(void){
    gtk_widget_destroy(console_window);
}

void AddConsoleText(const gchar* InputText) {
    gdk_threads_enter();
    static GtkTextTag *t_underline = NULL;
    static GtkTextTag *t_strikeout = NULL;

    
    string FontOption;
    if (ConsoleSelectFont.bold) FontOption = "Bold";
    if (ConsoleSelectFont.italic) FontOption += " Italic";
    if (ConsoleSelectFont.underline) t_underline = gtk_text_buffer_create_tag(console_buffer, NULL, "underline", PANGO_UNDERLINE_SINGLE, NULL);
    if (ConsoleSelectFont.strikeout) t_strikeout = gtk_text_buffer_create_tag(console_buffer, NULL, "strikethrough", TRUE, NULL);
    string FontName = ConsoleSelectFont.name;
    int FontSize = ConsoleSelectFont.size;
    char *FS = g_strdup_printf("%i", FontSize); //converting int to string
    string FontInfo;
    FontInfo += FontName + " " + FontOption + " " + FS;
        free(FS);
    PangoFontDescription *font_desc;
    font_desc = pango_font_description_from_string(FontInfo.c_str());
    
    gtk_widget_modify_font(console_text_view, font_desc);
    pango_font_description_free(font_desc);
    
    gtk_text_buffer_insert_with_tags(console_buffer, &console_iter, InputText, -1, t_underline, t_strikeout, NULL);
    
    gdk_threads_leave();
}

void ShowConsole() {

    GtkWidget *vbox;
    console_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(console_window), "I'm 'Console'");
    gtk_window_set_default_size(GTK_WINDOW(console_window), 800, 480);
    g_signal_connect(G_OBJECT(console_window), "destroy",
            G_CALLBACK(close_console_window),
            NULL);

    vbox = gtk_vbox_new(FALSE, 2);
    gtk_container_add(GTK_CONTAINER(console_window), vbox);

    console_text_view = gtk_text_view_new();
    gtk_box_pack_start(GTK_BOX(vbox), console_text_view, 1, 1, 0);

    console_buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(console_text_view));
    gtk_text_buffer_get_iter_at_offset(console_buffer, &console_iter, 0);
    gtk_widget_show_all(console_window);
}

namespace {
    namespace TX { // <<<<<<<<< The console code is here, unfold it
        
        POINT txGetConsoleFontSize() {
            string FontOption;
            if (ConsoleSelectFont.bold) FontOption = "Bold";
            if (ConsoleSelectFont.italic) FontOption += " Italic";
            string FontName = ConsoleSelectFont.name;
            int FontSize = ConsoleSelectFont.size;
            char *FS = g_strdup_printf("%i", FontSize);
            string FontInfo;
            FontInfo += FontName + " " + FontOption + " " + FS;
            free(FS);
            PangoFontDescription *font_desc;
            font_desc = pango_font_description_from_string(FontInfo.c_str());
            PangoFontMap * fontmap = pango_cairo_font_map_get_default();
            PangoContext *pcontext = pango_font_map_create_context(fontmap);
            PangoFont *font = pango_font_map_load_font(fontmap, pcontext, font_desc);

            PangoFontMetrics *font_metrics = pango_font_get_metrics(font, NULL); // and finally, we got it !

            int font_weight = pango_font_metrics_get_approximate_char_width(font_metrics);
            int font_height = pango_font_metrics_get_ascent(font_metrics) + pango_font_metrics_get_descent(font_metrics);

            font_weight /= PANGO_SCALE;
            font_height /= PANGO_SCALE;
            SIZE size = {font_weight, font_height};
            POINT sizeFont = { size.cx, size.cy };

            return sizeFont;
        }

        POINT txGetConsoleCursorPos() {

            POINT fontSz = txGetConsoleFontSize();
            GtkTextMark * TextMark = gtk_text_buffer_get_insert(console_buffer);
            GtkTextIter CursorIter;
            gtk_text_buffer_get_iter_at_mark(console_buffer, &CursorIter, TextMark); //current textmark's position is in CursorIter now
            int IterOffset = gtk_text_iter_get_offset(&CursorIter);
            int IterLinesOffset = gtk_text_iter_get_line_offset(&CursorIter);
            POINT pos = {IterOffset * fontSz.x, IterLinesOffset * fontSz.y};

            return pos;
        }

        POINT txSetConsoleCursorPos(double x, double y) {
            POINT fontSz = txGetConsoleFontSize();

            GtkTextIter CursorIter;

            gtk_text_buffer_get_iter_at_line_offset(console_buffer, &CursorIter, x / fontSz.x, y / fontSz.y);
            gtk_text_buffer_place_cursor(console_buffer, &CursorIter);

            POINT prev = txGetConsoleCursorPos();

            return prev;
        }

        void color_decimal_to_gdk(unsigned int colour, GdkColor * color) {
            guint16 red, green, blue;
            blue = colour % 256;
            colour = colour / 256;
            green = colour % 256;
            colour = colour / 256;
            red = colour;
            color->red = 256 * red;
            color->green = 256 * green;
            color->blue = 256 * blue;
        }

        static COLORREF ConsoleFontColorTable[] = { TX_BLACK   , TX_BLUE     , TX_GREEN     , TX_CYAN     , TX_RED     , TX_MAGENTA     , TX_YELLOW, TX_GRAY,
                                                    TX_DARKGRAY, TX_LIGHTBLUE, TX_LIGHTGREEN, TX_LIGHTCYAN, TX_LIGHTRED, TX_LIGHTMAGENTA, TX_YELLOW, TX_WHITE 
                                                  };
        
        bool txSetConsoleAttr(WORD colors /*= 0x07*/) {
            gdk_threads_enter();
            //colors = 0x1E; //blue background, yellow font;
            ConsoleBackgroundColor = colors >> 4;
            ConsoleFontColor = colors & 15;

            GdkColor gdkcolor;
            int b_color;
            //setting background color
            b_color = ConsoleFontColorTable[ConsoleBackgroundColor];
            color_decimal_to_gdk(b_color, &gdkcolor);
            gtk_widget_modify_base(console_text_view, GTK_STATE_NORMAL, &gdkcolor);
            
            //settings font color
            b_color = ConsoleFontColorTable[ConsoleFontColor];
            color_decimal_to_gdk(b_color, &gdkcolor);
            gtk_widget_modify_text(console_text_view, GTK_STATE_NORMAL, &gdkcolor);
            
            gdk_threads_leave();
            return true;
        }
        WORD txGetConsoleAttr()
        {
            WORD CA; 
            if (!ConsoleBackgroundColor && !ConsoleFontColor) CA = 0xF0;
            else CA = ConsoleFontColor + ConsoleBackgroundColor*16;
            return CA;
        }

       

    }
} // namespace TX, anonymous namespace


/* Redraw the screen from the surface. Note that the ::draw
 * signal receives a ready-to-be-used cairo_t that is already
 * clipped to only draw the exposed areas of the widget
 */
#if (GTK_MAJOR_VERSION == 3)        
static gboolean
draw_area(GtkWidget *widget, cairo_t *cr, gpointer data) {
    if (surface == NULL) return FALSE;
    cairo_set_source_surface(cr, surface, 0, 0);
    cairo_paint(cr);

    return FALSE;
}

#else
static gboolean 
draw_user (GtkWidget *widget, GdkEvent *event, gpointer user_data) {
    
    if (surface == NULL) return FALSE;
    cairo_t *cr = gdk_cairo_create (gtk_widget_get_window(widget));
    cairo_set_source_surface(cr, surface, 0, 0);
    cairo_paint(cr);
    cairo_destroy(cr);
    
    return FALSE;
}
#endif    

/* Create a new surface of the appropriate size to store our scribbles */
static gboolean
configure_event(GtkWidget *widget, GdkEventConfigure *event, gpointer data) {
    //    if (surface) cairo_surface_destroy (surface);
    if (surface) return false;
    int width = event->width, height = event->height;
    surface = gdk_window_create_similar_surface(gtk_widget_get_window(widget),
            CAIRO_CONTENT_COLOR_ALPHA, width, height);
    //                                       CAIRO_CONTENT_COLOR, gtk_widget_get_allocated_width (widget), gtk_widget_get_allocated_height (widget));
    _txCanvas_BackBuf[0] = surface;
    return FALSE;
}

static gboolean
button_press_release_event(GtkWidget *widget, GdkEventButton *event, gpointer data) {
#if (GTK_MAJOR_VERSION == 3)        
    int buttons = 0;

    if (event->button == 1) {
        buttons = 0x01;
    } else if (event->button == 2) {
        buttons = 0x02;
    } else if (event->button == 3) {
        buttons = 0x04;
    }

    _txMousePos.x = event->x;
    _txMousePos.y = event->y;
    if (event->type = GDK_BUTTON_PRESS) {
        _txMouseButtons |= buttons;
    }
    if (event->type = GDK_BUTTON_RELEASE) {
        _txMouseButtons &= ~buttons;
    }

#else
    int x, y, buttons = 0;
    GdkModifierType state;

    gdk_window_get_pointer(event->window, &x, &y, &state);
    if (state & GDK_BUTTON1_MASK) buttons |= 0x01;
    if (state & GDK_BUTTON2_MASK) buttons |= 0x02;
    if (state & GDK_BUTTON3_MASK) buttons |= 0x04;

    _txMousePos.x = x;
    _txMousePos.y = y;
    _txMouseButtons = buttons;
//    printf("mouse state=%d; x=%d, y=%d\n", state, x, y);
#endif    
//    printf("mouse type=%d; x=%d, y=%d; button=%d\n", event->type, (int)event->x, (int)event->y, event->button);
    return TRUE;
}

static gboolean
motion_notify_event(GtkWidget *widget, GdkEventMotion *event, gpointer data) {
    int x, y, buttons = 0;
    GdkModifierType state;

    gdk_window_get_pointer(event->window, &x, &y, &state);

    if (state & GDK_BUTTON1_MASK) buttons |= 0x01;
    if (state & GDK_BUTTON2_MASK) buttons |= 0x02;
    if (state & GDK_BUTTON3_MASK) buttons |= 0x04;

    _txMousePos.x = x;
    _txMousePos.y = y;
    _txMouseButtons = buttons;
    return TRUE;
}

static void
close_window(void) {
    if (surface) {
        cairo_surface_destroy(surface);
        surface = NULL;
    }
    gtk_main_quit();
    exit(0);
}

void *do_draw(void *ptr) {
    gdk_threads_enter();

    _txCanvas_Window = GTK_WINDOW(gtk_window_new(GTK_WINDOW_TOPLEVEL));
    gtk_window_set_title(GTK_WINDOW(_txCanvas_Window), "");
    if (tx_gtk_data.centered) {
        gtk_window_set_position(GTK_WINDOW(_txCanvas_Window), GTK_WIN_POS_CENTER);
    }

    //    gtk_container_set_border_width (GTK_CONTAINER (_txCanvas_Window), 2);
    gtk_window_set_resizable(GTK_WINDOW(_txCanvas_Window), false);

    GtkWidget *frame = gtk_frame_new(NULL);
    gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
    gtk_container_add(GTK_CONTAINER(_txCanvas_Window), frame);

    drawing_area = gtk_drawing_area_new();
    /* set a minimum size */
    gtk_widget_set_size_request(drawing_area, tx_gtk_data.window_size_x, tx_gtk_data.window_size_y);

    gtk_container_add(GTK_CONTAINER(frame), drawing_area);

    if (_txConsoleMode & SWP_HIDEWINDOW) {
    }
    if (_txConsoleMode & SWP_SHOWWINDOW) {
        g_set_print_handler(AddConsoleText); // text output redirection
        ShowConsole();
    }

    g_signal_connect(_txCanvas_Window, "destroy"             , G_CALLBACK(close_window), NULL);
#if (GTK_MAJOR_VERSION == 3)        
    g_signal_connect(drawing_area,     "draw"                , G_CALLBACK(draw_area), NULL);
#else
    g_signal_connect(drawing_area,     "expose-event"        , G_CALLBACK(draw_user), NULL);
#endif    
    g_signal_connect(drawing_area,     "configure-event"     , G_CALLBACK(configure_event), NULL);
    g_signal_connect(_txCanvas_Window, "key-press-event"     , G_CALLBACK(key_event), NULL);
    g_signal_connect(_txCanvas_Window, "key-release-event"   , G_CALLBACK(key_event), NULL);
    g_signal_connect(drawing_area,     "motion-notify-event" , G_CALLBACK(motion_notify_event), NULL);
    g_signal_connect(drawing_area,     "button-release-event", G_CALLBACK(button_press_release_event), NULL);
    g_signal_connect(drawing_area,     "button-press-event"  , G_CALLBACK(button_press_release_event), NULL);

    gtk_widget_set_events(drawing_area, gtk_widget_get_events(drawing_area)
            | GDK_KEY_PRESS_MASK
            | GDK_KEY_RELEASE_MASK
            | GDK_BUTTON_PRESS_MASK
            | GDK_BUTTON_RELEASE_MASK
            | GDK_POINTER_MOTION_MASK
            | GDK_POINTER_MOTION_HINT_MASK);

    gtk_widget_show_all(GTK_WIDGET(_txCanvas_Window));

    gtk_widget_set_app_paintable(GTK_WIDGET(_txCanvas_Window), TRUE);
    gtk_widget_set_double_buffered(GTK_WIDGET(_txCanvas_Window), FALSE);

    gtk_main();

    gdk_threads_leave();

}

namespace {
    namespace TX { // <<<<<<<<< The main code is here, unfold it

        bool txLock(bool wait /*= true*/) {
            return true;
        }

        //--------------------------------------------------------------------------------------------

        bool txUnlock() {
            return false;
        }

        HWND txCreateWindow(double sizeX, double sizeY, bool centered) {
            tx_gtk_data.window_size_x = sizeX;
            tx_gtk_data.window_size_y = sizeY;
            tx_gtk_data.centered = centered;
            tx_gtk_data.count_lock = 0;
            txSetDefaults();

            if (!g_thread_supported()) {
                g_thread_init(NULL);
            }

            gdk_threads_init();
            gdk_threads_enter();
            gtk_init(&tx_gtk_data.argc, &tx_gtk_data.argv);
            gdk_threads_leave();

            int iret = pthread_create(&tx_gtk_data.thread_info, NULL, do_draw, NULL);
            while (!surface) sleep(0);

            return _txCanvas_Window;
        }

        void tx_gtk_queue_draw_area(int x0_, int y0_, int x1_, int y1_) {
            if (!_txCanvas_OK()) return;
            if (!tx_gtk_data.count_lock) {
                int x0 , y0, x1, y1;
                if (x1_ > x0_) { x0 = x0_; x1 = x1_; } else { x0 = x1_; x1 = x0_; }
                if (y1_ > y0_) { y0 = y0_; y1 = y1_; } else { y0 = y1_; y1 = y0_; }
                int border = tx_gtk_data.thickness;
                int width = x1 - x0 + 1 + 2 * border, height = y1 - y0 + 1 + 2 * border;
                gtk_widget_queue_draw_area(drawing_area, x0 - border, y0 - border, width, height);
            }
        }

        void tx_gtk_queue_draw() {
            if (!_txCanvas_OK()) return;
            if (!tx_gtk_data.count_lock) gtk_widget_queue_draw(drawing_area);
        }

        int txBegin() {
#ifdef  __MAC_LOCK_THREAD__   
            if (_txCanvas_OK() && !tx_gtk_data.count_lock) gdk_threads_enter();
#endif
            return ++tx_gtk_data.count_lock;
        }

        int txEnd() {
            if (tx_gtk_data.count_lock > 0) {
                tx_gtk_data.count_lock--;
                if (_txCanvas_OK() && !tx_gtk_data.count_lock) {
                    gtk_widget_queue_draw(drawing_area);
#ifdef  __MAC_LOCK_THREAD__   
                    gdk_threads_leave();
#endif
                }
            }
            if (surface == NULL) return false;
            return tx_gtk_data.count_lock;
        }

        bool txSleep(double time) {
            dword my_time = time;
            if (_txCanvas_OK() && tx_gtk_data.count_lock) {
#ifndef  __MAC_LOCK_THREAD__   
                gdk_threads_enter();
#endif
                gtk_widget_queue_draw(drawing_area);
                gdk_threads_leave();
                Sleep(my_time ? my_time : 1);
#ifdef  __MAC_LOCK_THREAD__   
                gdk_threads_enter();
#endif
            } else {
                Sleep(my_time);
            }
            return true;
        }

        bool txSetColor(COLORREF color, double thickness) {
            tx_gtk_data.thickness = thickness;
            tx_gtk_data.color = color;
            tx_gtk_data.color_r = ((color & 0x00FF0000) >> 16) / 256.0;
            tx_gtk_data.color_g = ((color & 0x0000FF00) >> 8) / 256.0;
            tx_gtk_data.color_b = (color & 0x000000FF) / 256.0;
            tx_gtk_data.color_a = (color == TX_NULL) ? 0 : 1;
            return true;
        }

        bool txSetFillColor(COLORREF color) {
            tx_gtk_data.fill_color = color;
            tx_gtk_data.fill_color_r = ((color & 0x00FF0000) >> 16) / 256.0;
            tx_gtk_data.fill_color_g = ((color & 0x0000FF00) >> 8) / 256.0;
            tx_gtk_data.fill_color_b = (color & 0x000000FF) / 256.0;
            tx_gtk_data.fill_color_a = (color == TX_NULL) ? 0 : 1;
            return true;
        }

        COLORREF txGetColor()     { return tx_gtk_data.color; }
        COLORREF txGetFillColor() { return tx_gtk_data.fill_color; }
        
        bool txClear() {
            if (!txOK()) return false;
            tx_gdk_enter();
            cairo_t *cr = cairo_create(surface);

            cairo_set_source_rgba(cr, tx_gtk_data.fill_color_r, tx_gtk_data.fill_color_g, tx_gtk_data.fill_color_b, tx_gtk_data.fill_color_a);
            cairo_paint(cr);

            cairo_destroy(cr);
            tx_gtk_queue_draw();
            tx_gdk_leave();
            return true;
        }

        bool tx_gtk_txLine(double x0, double y0, double x1, double y1) {
            cairo_t *cr = cairo_create(surface);

            cairo_set_source_rgba(cr, tx_gtk_data.color_r, tx_gtk_data.color_g, tx_gtk_data.color_b, tx_gtk_data.color_a);
            cairo_set_line_width(cr, tx_gtk_data.thickness);
            cairo_move_to(cr, x0, y0);
            cairo_line_to(cr, x1, y1);
            cairo_stroke(cr);

            cairo_destroy(cr);
            tx_gtk_queue_draw_area(x0, y0, x1, y1);
            return true;
        }

        bool txLine(double x0, double y0, double x1, double y1) {
            if (!txOK()) return false;
            tx_gdk_enter();
            bool ret = tx_gtk_txLine(x0, y0, x1, y1);
            tx_gdk_leave();
            return ret;
        }

        bool tx_gtk_txRectangle(double x0, double y0, double x1, double y1, int mode) {
            cairo_t *cr = cairo_create(surface);

            if (mode & 0x01) {
                cairo_set_line_width(cr, tx_gtk_data.thickness);
                cairo_set_source_rgba(cr, tx_gtk_data.color_r, tx_gtk_data.color_g, tx_gtk_data.color_b, tx_gtk_data.color_a);
            }
            double width = x1 - x0 + 1, height = y1 - y0 + 1;
            cairo_rectangle(cr, x0, y0, width, height);

            if (mode & 0x01) {
                cairo_stroke_preserve(cr);
            }
            if (mode & 0x02) {
                cairo_set_source_rgba(cr, tx_gtk_data.fill_color_r, tx_gtk_data.fill_color_g, tx_gtk_data.fill_color_b, tx_gtk_data.fill_color_a);
                cairo_fill(cr);
            }

            cairo_destroy(cr);
            tx_gtk_queue_draw_area(x0, y0, x1, y1);
            return true;
        }

        bool txRectangle(double x0, double y0, double x1, double y1) {
            if (!txOK()) return false;
            tx_gdk_enter();
            bool ret = tx_gtk_txRectangle(x0, y0, x1, y1, 0x03);
            tx_gdk_leave();
            return ret;
        }

        bool txPolygon(const POINT points[], int numPoints) {
            if (!txOK() || (numPoints < 2)) return false;
            tx_gdk_enter();
            int x_min = points[0].x, x_max = points[0].x, y_min = points[0].y, y_max = points[0].y;

            cairo_t *cr = cairo_create(surface);
            cairo_move_to(cr, points[0].x, points[0].y);
            cairo_set_line_width(cr, tx_gtk_data.thickness);
            cairo_set_source_rgba(cr, tx_gtk_data.color_r, tx_gtk_data.color_g, tx_gtk_data.color_b, tx_gtk_data.color_a);

            for (int i = 1; i < numPoints; i++) {
                if (points[i].x < x_min) x_min = points[i].x;
                if (points[i].x > x_max) x_max = points[i].x;
                if (points[i].y < y_min) y_min = points[i].y;
                if (points[i].y > y_max) y_max = points[i].y;

                cairo_line_to(cr, points[i].x, points[i].y);
            }
            cairo_close_path(cr);
            cairo_stroke_preserve(cr);

            cairo_set_source_rgba(cr, tx_gtk_data.fill_color_r, tx_gtk_data.fill_color_g, tx_gtk_data.fill_color_b, tx_gtk_data.fill_color_a);
            cairo_fill(cr);
            cairo_destroy(cr);

            tx_gtk_queue_draw_area(x_min, y_min, x_max, y_max);
            tx_gdk_leave();
            return true;

        }

        bool _txPie(double x0, double y0, double x1, double y1, double startAngle, double totalAngle, int mode) {
            if (!txOK()) return false;
            tx_gdk_enter();
            cairo_t *cr = cairo_create(surface);

            cairo_set_line_width(cr, tx_gtk_data.thickness);
            cairo_set_source_rgba(cr, tx_gtk_data.color_r, tx_gtk_data.color_g, tx_gtk_data.color_b, tx_gtk_data.color_a);

            double x = (x0 + x1) / 2, y = (y0 + y1) / 2, width = (x1 - x0), height = (y1 - y0);
            double begAngle = startAngle * txPI / 180, endAngle = (startAngle + totalAngle) * txPI / 180;

            cairo_save(cr);
            cairo_translate(cr, x, y);
            cairo_scale(cr, width / 2., height / 2.);
            if (mode & 0x02) {
                cairo_move_to(cr, 1., 0.0);
            }

            cairo_arc(cr, 0., 0., 1., -endAngle, -begAngle);
            if (mode & 0x03) {
                if (mode & 0x01) {
                    cairo_line_to(cr, 0, 0);
                }
                cairo_close_path(cr);
            }
            cairo_restore(cr);

            if (mode & 0x01) {
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, tx_gtk_data.fill_color_r, tx_gtk_data.fill_color_g, tx_gtk_data.fill_color_b, tx_gtk_data.fill_color_a);
                cairo_fill(cr);
            } else {
                cairo_stroke(cr);
            }
            cairo_destroy(cr);
            tx_gtk_queue_draw_area(x0, y0, x1, y1);
            tx_gdk_leave();
            return true;
        }

        bool txArc(double x0, double y0, double x1, double y1, double startAngle, double totalAngle) {
            return _txPie(x0, y0, x1, y1, startAngle, totalAngle, 0x00);
        }

        bool txPie(double x0, double y0, double x1, double y1, double startAngle, double totalAngle) {
            return _txPie(x0, y0, x1, y1, startAngle, totalAngle, 0x01);
        }

        bool txChord(double x0, double y0, double x1, double y1, double startAngle, double totalAngle) {
            return _txPie(x0, y0, x1, y1, startAngle, totalAngle, 0x02);
        }

        bool txEllipse(double x0, double y0, double x1, double y1) {
            return _txPie(x0, y0, x1, y1, 0, 360, (tx_gtk_data.fill_color == TX_NULL) ? 0x00 : 0x01);
        }

        bool txCircle(double x, double y, double r) {
            return txEllipse(x - r, y - r, x + r, y + r);
        }

        SelectFontStruct SelectFont = { "Verdana", 10, -1, FW_DONTCARE, false, false, false, 0, TA_LEFT | TA_TOP };
        /*
         * All encoded in UTF-8
         Fonts, which tested to work with Russian.
         * Georgia, Arial, Tahoma, Verdana, Times, Times New Roman, Comic Sans MS Times, Geneva, Helvetica, Impact, Palatino
        */

        /* Font Weights */

        /*
        #define FW_DONTCARE         0
        #define FW_THIN             100
        #define FW_EXTRALIGHT       200
        #define FW_LIGHT            300
        #define FW_NORMAL           400
        #define FW_MEDIUM           500
        #define FW_SEMIBOLD         600
        #define FW_BOLD             700
        #define FW_EXTRABOLD        800
        #define FW_HEAVY            900
         */

        bool txSelectFont(const char name[], double sizeY, double sizeX, int bold, bool italic, bool underline, bool strikeout, double angle) {
            tx_gdk_enter();

            //    Some fonts cannot be italic, don't forget about it.

            SelectFont.name = name;
            SelectFont.size = sizeY;
            SelectFont.sizeX = sizeX; //nothing here, i didn't find how to set font weight separately.
            SelectFont.bold = bold;
            SelectFont.italic = italic;
            SelectFont.underline = underline;
            SelectFont.strikeout = strikeout;
            SelectFont.angle = angle;

            tx_gdk_leave();
            return true;
        }

        bool _GetTextExtentPoint(cairo_t *cr, LPCTSTR InputStr, LPSIZE size) {

            PangoLayout *layout;
            PangoFontDescription *desc;

            string FontOption;
            string text = WinToUtf(InputStr);
            if (SelectFont.bold) FontOption = "Bold";
            if (SelectFont.italic) FontOption += " Italic";
            string FontName = SelectFont.name;
            int FontSize = SelectFont.size;

            char *FS = g_strdup_printf("%i", FontSize); //converting int to string

            string FontInfo;
            FontInfo += FontName + " " + FontOption + " " + FS;

            layout = pango_cairo_create_layout(cr);
            pango_layout_set_text(layout, text.c_str(), -1);        

            desc = pango_font_description_from_string(FontInfo.c_str());
            pango_layout_set_font_description(layout, desc);

            int text_height;
            int text_weight;
            pango_layout_get_size(layout, &text_weight, &text_height);
            text_weight /= PANGO_SCALE;
            text_height /= PANGO_SCALE;

            size->cx = text_weight;
            size->cy = text_height;
            
            pango_font_description_free(desc);
            g_object_unref(layout);
            return TRUE;

        }

        bool tx_pango_text(const char* InputText, int x_position, int y_position, unsigned format, int position_x, int position_y) {
            // This function must NOT be used separately.

            PangoLayout *layout;
            PangoFontDescription *desc;
            cairo_t *cr = cairo_create(surface);
            string text = WinToUtf(InputText);

            //color detection
            cairo_set_line_width(cr, tx_gtk_data.thickness);
            cairo_set_source_rgba(cr, tx_gtk_data.color_r, tx_gtk_data.color_g, tx_gtk_data.color_b, tx_gtk_data.color_a);

            gboolean Underline = SelectFont.underline, Strikeout = SelectFont.strikeout; 
            string FontName = SelectFont.name; int FontSize = SelectFont.size; 
            string FO; if (SelectFont.bold) FO = "Bold";  if (SelectFont.italic) FO += " Italic"; //if (FO == "") FO = "Normal";
            char *FS = g_strdup_printf("%i", FontSize); //converting int to string

            string font;
            font += FontName + " " + FO + " " + FS;

            layout = pango_cairo_create_layout(cr);
            pango_layout_set_text(layout, text.c_str(), -1);
            //underline and strikeout block
            PangoAttrList * attrs = pango_attr_list_new();

            if (Underline)pango_attr_list_insert(attrs, pango_attr_underline_new(PANGO_UNDERLINE_SINGLE));
            pango_attr_list_insert(attrs, pango_attr_strikethrough_new(Strikeout));
            pango_layout_set_attributes(layout, attrs);


            desc = pango_font_description_from_string(font.c_str());
            pango_layout_set_font_description(layout, desc);

            int text_width, text_height;
            pango_layout_get_size(layout, &text_width, &text_height);
            text_width /= PANGO_SCALE; text_height /= PANGO_SCALE;

            if (format & DT_CENTER) {
                x_position = (x_position + position_x) / 2 - text_width/2; 
            } else if (format & DT_RIGHT) {
                x_position = position_x - text_width;
            } else { // DT_LEFT
                x_position;
            }
            if (format & DT_VCENTER) {
                y_position = (y_position + position_y) / 2 - text_height/2;
            } else if (format & DT_BOTTOM) {
                y_position = position_y - text_height;
            } else { // DT_TOP
                y_position;
            }
            
            cairo_move_to(cr, x_position, y_position);
            pango_cairo_show_layout(cr, layout);
            cairo_destroy(cr);
            pango_font_description_free(desc);
            g_object_unref(layout);
            tx_gtk_queue_draw_area(x_position, y_position, x_position + text_width, y_position + text_height);
            return TRUE;
        }

        bool txTextOut(double x, double y, const char text[]) {
            if (!txOK()) return false;
            tx_gdk_enter();

            cairo_t *cr = cairo_create(surface);
            
            unsigned dt_format = 0;
            unsigned ta_align_h = SelectFont.ta_align & TA_CENTER, ta_align_v = SelectFont.ta_align & TA_BASELINE;
            if (ta_align_h == TA_LEFT) {
                dt_format = DT_LEFT; 
            } else if (ta_align_h == TA_RIGHT) {
                dt_format = DT_RIGHT; 
            } else { // TA_CENTER
                dt_format = DT_CENTER; 
            }
            if (ta_align_v == TA_TOP) {
                dt_format |= DT_TOP; 
            } else if (ta_align_v == TA_BOTTOM) {
                dt_format |= DT_BOTTOM; 
            } else { // TA_BASELINE
                dt_format |= DT_VCENTER; 
            }
            
            tx_pango_text(text, x, y, dt_format, x, y);
            cairo_destroy(cr);
            
            tx_gdk_leave();
            return true;
        }

        bool txDrawText(double x, double y, double x1, double y1, const char text[], unsigned format) {
            if (!txOK()) return false;
            tx_gdk_enter();

            tx_pango_text(text, x, y, format, x1, y1);
            
            tx_gdk_leave();
            return true;
        }
        
        const char* entry_text_input_box = NULL;       
        GtkEntryBuffer *get_entry_buffer()
        {
            static GtkEntryBuffer *entry_buffer = gtk_entry_buffer_new (NULL, -1);
            return entry_buffer;
        }

        void set_input_string()
        {
            static string input_text;
            input_text = UtfToWin(gtk_entry_buffer_get_text (get_entry_buffer()));
            entry_text_input_box = input_text.c_str(); 
        }
        static void enter_callback_input_box(GtkWidget *widget, GtkWidget *window)
        {
            set_input_string();
            gtk_widget_destroy (window);

        }

        static void cancel_callback_input_box(GtkWidget *widget, GtkWidget *window)
        {
            gtk_entry_buffer_set_text (get_entry_buffer(), "", -1);
            enter_callback_input_box(widget, window);
        }

        static gboolean delete_callback_input_box(GtkWidget *widget, GdkEvent *event, GtkWidget *window)
        {
            cancel_callback_input_box(widget, window);
            return TRUE;
        }

#if !defined (__TX_DIALOG_WIN32__)      
        const char* txInputBox (const char* text_, const char* caption_, const char* input_)
        {

            $1  //----------------------------------------------------------------------------------------
                // ���� �� ������� ���������, ���������� ������������ ���� �����-�� �������.
                // txGetModuleFileName() - ��� EXE-�����, �� ������, ���� ���-��� ��������� ������ ��������.
                //----------------------------------------------------------------------------------------

            $   if (!text_)    text_    = "������� ������:";
            $   if (!caption_) caption_ = txGetModuleFileName (false);
            $   if (!input_)   input_   = "";
            
            string text_str    = WinToUtf(text_   ); const char *text    = text_str   .c_str();
            string input_str   = WinToUtf(input_  ); const char *input   = input_str  .c_str();
            string caption_str = WinToUtf(caption_); const char *caption = caption_str.c_str();
        
            if (!txOK()) return false;
            tx_gdk_enter();
            
            const int width_min = 40, DialogFontSize = 15;
            
            GtkWidget *window = gtk_dialog_new();
            gtk_widget_set_size_request(GTK_WIDGET(window), 300, 100);
            gtk_window_set_title(GTK_WINDOW(window), caption);
            gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
            gtk_window_set_modal(GTK_WINDOW(window), true);
            gtk_window_set_resizable(GTK_WINDOW(window), false);
            g_signal_connect(window, "delete_event", GTK_SIGNAL_FUNC(delete_callback_input_box), window);

            GtkWidget *entry = gtk_entry_new_with_buffer(get_entry_buffer());
            g_signal_connect(entry, "activate", G_CALLBACK(enter_callback_input_box), window);
            gtk_entry_set_text(GTK_ENTRY(entry), input);
            gtk_editable_select_region(GTK_EDITABLE(entry), 0, GTK_ENTRY(entry)->text_length);

            GtkWidget *content_area = gtk_dialog_get_content_area(GTK_DIALOG(window));
            gtk_container_add(GTK_CONTAINER(content_area), entry);
            
            GtkWidget *button_ok = gtk_button_new_from_stock(GTK_STOCK_OK);
            gtk_box_pack_start(GTK_BOX(GTK_DIALOG(window)->action_area), button_ok, TRUE, TRUE, 10);
            g_signal_connect(button_ok, "clicked", GTK_SIGNAL_FUNC(enter_callback_input_box),  window);

            GtkWidget *button_cancel = gtk_button_new_from_stock(GTK_STOCK_CANCEL);
            gtk_box_pack_start(GTK_BOX(GTK_DIALOG(window)->action_area), button_cancel, TRUE, TRUE, 10);
            g_signal_connect(button_cancel, "clicked", GTK_SIGNAL_FUNC(cancel_callback_input_box), window);

            GdkWindow *gdkwindow = gtk_widget_get_window(window);
            GString *caption_string = g_string_new(caption_);
            gint width_caption = DialogFontSize * caption_string->len;
            g_string_free(caption_string, TRUE);
            gint width, height;
            gtk_widget_get_size_request(window, &width, &height);
            if (width_caption > (width - width_min)) width = width_caption + width_min;
            gtk_widget_set_size_request(window, width, height);

            gtk_widget_show_all(window);
            entry_text_input_box = NULL;
            gtk_dialog_run (GTK_DIALOG (window));
            tx_gdk_leave();

            $ return entry_text_input_box;
        }

#endif    //   _TX_DIALOG_WIN32__

        GdkPixbuf *create_pixbuf(const gchar * filename)
        {
            GdkPixbuf *pixbuf;
            GError *error = NULL;
            pixbuf = gdk_pixbuf_new_from_file(filename, &error);
            if(!pixbuf) {
                fprintf(stderr, "%s\n", error->message);
                g_error_free(error);
            }
            return pixbuf;
        }

#if !defined (__TX_IMAGE_WIN32__)      
        HDC txLoadImage (const char filename[])
        {
            GdkPixbuf *pixbuf = create_pixbuf(filename); if(!pixbuf) return NULL;
            
            int width = gdk_pixbuf_get_width (pixbuf), height = gdk_pixbuf_get_height (pixbuf);           
            cairo_surface_t *surface_image = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, width, height);
            
            cairo_t *cr = cairo_create (surface_image);
            gdk_cairo_set_source_pixbuf (cr, pixbuf, 0, 0);
            cairo_paint(cr);
            cairo_destroy(cr);  
            g_object_unref(pixbuf);

            return surface_image;
        }
        bool txDeleteDC (HDC dc)
        {
            if (dc) {
                cairo_surface_destroy(dc);
                return true;
            }
            return false;
        }
#endif    //   _TX_IMAGE_WIN32__
        
        bool txSetDefaults()
        {
            txSetColor (TX_BLACK);
            txSetFillColor (TX_WHITE);
            txSelectFont ("Verdana", 10, -1, FW_DONTCARE, false, false, false);
 
        }


    }
} // namespace TX, anonymous namespace


static void
get_pixel(GdkPixbuf *pixbuf, int x, int y, guchar *red, guchar *green, guchar *blue, guchar *alpha)
{
    int width = gdk_pixbuf_get_width(pixbuf), height = gdk_pixbuf_get_height(pixbuf);
    if (!(x >= 0 && x < width) || !(y >= 0 && y < height)) return;

    int rowstride = gdk_pixbuf_get_rowstride (pixbuf);
    gboolean has_alpha = gdk_pixbuf_get_has_alpha (pixbuf);
    guint pixel_offset = has_alpha ? 4 : 3;

    guchar *pixels = gdk_pixbuf_get_pixels(pixbuf);
    guchar *offset = pixels + y * rowstride + x * pixel_offset;

    *red   = *(offset + 0);
    *green = *(offset + 1);
    *blue  = *(offset + 2);

    if (has_alpha) 
        *alpha = *(offset + 3);
    else
        *alpha = 0xFF;
}

static void
put_pixel (GdkPixbuf *pixbuf, int x, int y, guchar red, guchar green, guchar blue, guchar alpha)
{
    int width = gdk_pixbuf_get_width(pixbuf), height = gdk_pixbuf_get_height(pixbuf);
    if (!(x >= 0 && x < width) || !(y >= 0 && y < height)) return;

    int rowstride = gdk_pixbuf_get_rowstride (pixbuf);
    gboolean has_alpha = gdk_pixbuf_get_has_alpha (pixbuf);
    guint pixel_offset = has_alpha ? 4 : 3;

    guchar *pixels = gdk_pixbuf_get_pixels(pixbuf);
    guchar *offset = pixels + y * rowstride + x * pixel_offset;

    *(offset + 0) = red;
    *(offset + 1) = green;
    *(offset + 2) = blue;

    if (has_alpha) 
    {
        *(offset + 3) = alpha;
    }
}

static bool less_point(const POINT &p1, const POINT &p2) { return true; }
struct _tx_mac_flood_t
{
    class T_POINT : public POINT
    {
    public:
        T_POINT() {}
        T_POINT(int x, int y) { this->x = x; this->y = y;}
        bool operator <  (const T_POINT &p) const { return (x < p.x) ? true : (((x == p.x) && (y < p.y)) ? true : false); }        
        bool operator == (const T_POINT &p) const { return (x == p.x) && (y == p.y); }        
    };
    int width, height; int rowstride; gboolean has_alpha, border; guint pixel_offset;
    int min_x, min_y, max_x, max_y;
    guchar border_r, border_g, border_b;
    guchar new_r, new_g, new_b;
    guchar *pixels;
    std::queue<T_POINT> q;
    std::set<T_POINT> war;
    std::set<int> w;
   
    _tx_mac_flood_t(GdkPixbuf *pixbuf, COLORREF color, bool border) 
    {
        width = gdk_pixbuf_get_width(pixbuf); height = gdk_pixbuf_get_height(pixbuf);
        min_x = width + 1; min_y = height + 1; max_x = - 1; max_y = - 1;
        rowstride = gdk_pixbuf_get_rowstride (pixbuf);
        has_alpha = gdk_pixbuf_get_has_alpha (pixbuf);
        pixel_offset = has_alpha ? 4 : 3;
        pixels = gdk_pixbuf_get_pixels(pixbuf);
        this->border = border;

        border_r = (color & 0x00FF0000) >> 16;
        border_g = (color & 0x0000FF00) >> 8;
        border_b = (color & 0x000000FF) ;

        new_r = (tx_gtk_data.fill_color & 0x00FF0000) >> 16;
        new_g = (tx_gtk_data.fill_color & 0x0000FF00) >> 8;
        new_b = (tx_gtk_data.fill_color & 0x000000FF) ;
    }
    int yes_color(int x, int y) const
    {
        guchar *p = pixels + y * rowstride + x * pixel_offset;
        if ((p[0] == new_r) && (p[1] == new_g) && (p[2] == new_b)) return - 1; 
        bool color = (p[0] == border_r) && (p[1] == border_g) && (p[2] == border_b);
        return (color ^ border) ? 1 : 0; 
    }
    bool yes_point(int x, int y) const
    {
        return (x >= 0 && x < width) && (y >= 0 && y < height);
    }
    bool flood_point(int x, int y) 
    {
        if (!yes_point(x, y)) return false;
        int yes = yes_color(x, y);
        if (border) 
        {
            if (yes == 0) return false;
            T_POINT point(x, y);                
            if (std::find(war.begin(), war.end(), point) != war.end()) return false;
            war.insert(point);
        }
        else
        {
            if (yes <= 0) return false;
        }
        if (min_x > x) min_x = x; if (min_y > y) min_y = y;
        if (max_x < x) max_x = x; if (max_y < y) max_y = y;
        T_POINT point(x, y); q.push(point);
        guchar *p = pixels + point.y * rowstride + point.x * pixel_offset;
        p[0] = new_r; p[1] = new_g; p[2] = new_b; 
        return true;            
        
    }
    void flood()
    {
        while ( !q.empty() ) 
        {
            T_POINT point = q.front(); q.pop();
            flood_point(point.x - 1, point.y    );
            flood_point(point.x    , point.y - 1);
            flood_point(point.x + 1, point.y    );
            flood_point(point.x    , point.y + 1);
        }
        
    }
};

namespace Win32 {
    
    COLORREF SetPixel(HDC dc, int x, int y, COLORREF color) {

        if (!txOK()) return false;
        tx_gdk_enter();

        int old_thickness = tx_gtk_data.thickness;
        COLORREF old_color = tx_gtk_data.color, old_fill_color = tx_gtk_data.fill_color;
        txSetColor(color, 0);
        txSetFillColor(color);

        tx_gtk_txRectangle(x, y, x, y, 0x02);

        txSetColor(old_color, old_thickness);
        txSetFillColor(old_fill_color);

        tx_gdk_leave();
        return color;
    }

    COLORREF GetPixel(HDC dc, int x, int y) {
        if (!txOK()) return false;
        tx_gdk_enter();
        COLORREF color = 0;

#if (GTK_MAJOR_VERSION == 3)        
        GdkPixbuf * pixbuf = gdk_pixbuf_get_from_surface(dc, x, y, 1, 1);
#else    
        GdkDrawable *drawable = gtk_widget_get_window(drawing_area);
        GdkPixbuf *pixbuf = gdk_pixbuf_get_from_drawable (NULL, drawable, NULL, x, y, 0, 0, 1, 1);
#endif        
        guchar *pixels = gdk_pixbuf_get_pixels(pixbuf), *p = pixels;
        guchar red = p[0], green = p[1], blue = p[2], alpha;
        color = RGB(red, green, blue);
        g_object_unref(pixbuf);

        tx_gdk_leave();
        return color;
    }

    BOOL GetTextExtentPoint32(HDC dc, LPCTSTR string, int length, LPSIZE size) {
        if (!txOK()) return false;
        tx_gdk_enter();

        cairo_t *cr = cairo_create(surface);

        BOOL ret = TX::_GetTextExtentPoint(cr, string, size);
        cairo_destroy(cr);

        tx_gdk_leave();
        return ret;
    }

    UINT SetTextAlign(HDC dc, UINT mode) {
        UINT old = SelectFont.ta_align;
        SelectFont.ta_align = mode;
        return old;
    }
    
    BOOL GetCurrentConsoleFont (HANDLE console, BOOL maxWnd, CONSOLE_FONT_INFO* curFont)
    {
        return true;    
    }
    
    HWND GetConsoleWindow ()
    {
        return NULL;    
    }

//    int EnumFontFamiliesEx (HDC dc, LPLOGFONT logFont, FONTENUMPROC enumProc, LPARAM lParam, DWORD reserved)
//    {
//        return 0;            
//    }
    
    BOOL PatBlt (HDC dc, int x0, int y0, int width, int height, DWORD rOp)
    {
        return false;
    }

    BOOL BitBlt (HDC dest, int xDest, int yDest, int width, int height,
                 HDC src,  int xSrc,  int ySrc,  DWORD rOp /*= SRCCOPY*/)
    {
        if (!dest || ! src) return false;
        tx_gdk_enter();

        cairo_surface_t *surface_dest = cairo_surface_create_for_rectangle (dest, xDest, yDest, width, height);
        cairo_t *cr = cairo_create (surface_dest);
        cairo_set_source_surface(cr, src, 0, 0);
        cairo_paint(cr);
        cairo_surface_destroy(surface_dest);
        cairo_destroy(cr);    

        if (dest == txDC()) tx_gtk_queue_draw_area(xDest, yDest, xDest + width, yDest + height);

        tx_gdk_leave();
        return true;        
    }
    
#if  defined (__TX_IMAGE_WIN32__)      
//  GdkPixbuf *gdk_pixbuf_new_from_file_at_size (const char *filename, int width, int height, GError **error);
//  HBITMAP image = (HBITMAP) Win32::LoadImage (NULL, filename, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
    HANDLE LoadImage (HINSTANCE inst, LPCTSTR name, UINT type, int sizex, int sizey, UINT mode)
    {
        GdkPixbuf *pixbuf = create_pixbuf(name);
        return pixbuf;
        
    }
#endif    //   _TX_IMAGE_WIN32__
    BOOL _TransparentBlt (HDC dest, int destX, int destY, int destWidth, int destHeight,
                         HDC src,  int srcX,  int srcY,  int srcWidth,  int srcHeight,
                         UINT transparentColor)
    {      
        return BitBlt (dest, destX, destY, destWidth, destHeight, src,  srcX,  srcY);
    }
    BOOL _AlphaBlend (HDC dest, int destX, int destY, int destWidth, int destHeight,
                     HDC src,  int srcX,  int srcY,  int srcWidth,  int srcHeight,
                     BLENDFUNCTION blending)
    {
        return BitBlt (dest, destX, destY, destWidth, destHeight, src,  srcX,  srcY);
    }

    BOOL PlaySound (LPCSTR sound, HMODULE mod, DWORD mode)
    {
        return true;    
    }

    BOOL ExtFloodFill (HDC dc, int x, int y, COLORREF color, UINT type)
    {
        if (!txOK()) return false;
        if ((type != FLOODFILLBORDER) && (type != FLOODFILLSURFACE)) return false;
        txSleep(10);
        tx_gdk_enter();
        bool border = (type == FLOODFILLBORDER);

        int width  = gdk_window_get_width (gtk_widget_get_window(drawing_area));
        int height = gdk_window_get_height(gtk_widget_get_window(drawing_area));
#if (GTK_MAJOR_VERSION == 3)        
        GdkPixbuf * pixbuf = gdk_pixbuf_get_from_surface(dc, 0, 0, width, height);
#else    
        GdkDrawable *drawable = gtk_widget_get_window(drawing_area);
        GdkPixbuf *pixbuf = gdk_pixbuf_get_from_drawable (NULL, drawable, NULL, 0, 0, 0, 0, width, height);
#endif        
        _tx_mac_flood_t flood(pixbuf, color, border);
        bool ret = flood.flood_point(x, y); 
        if (ret) 
        {
            flood.flood(); 
            cairo_t *cr = cairo_create (dc);
            gdk_cairo_set_source_pixbuf (cr, pixbuf, 0, 0);
            cairo_paint(cr);
            cairo_destroy(cr); 
            tx_gtk_queue_draw_area(flood.min_x, flood.min_y, flood.max_x, flood.max_y);
        }
        
        g_object_unref(pixbuf);
        tx_gdk_leave();
        return ret;
        
    }



} // namespace Win32

DWORD GetModuleFileName(HMODULE hModule, LPTSTR lpFilename, DWORD nSize) {
        if (_NSGetExecutablePath(lpFilename, &nSize) !=0) return 0;
        DWORD x = strlen (lpFilename);       
        return x;
    }

bool GetWindowRect(HWND wnd, RECT *r) {
    POINT size = {}, position = {};
    if (!txOK()) return false;
    tx_gdk_enter();
#if (GTK_MAJOR_VERSION == 3)        
    size.x = gtk_widget_get_allocated_width(wnd);
    size.y = gtk_widget_get_allocated_height(wnd);
#else        
    gtk_window_get_size(GTK_WINDOW(wnd), &size.x, &size.y);
#endif        
    gtk_window_get_position(GTK_WINDOW(wnd), &position.x, &position.y);
    tx_gdk_leave();
    r->left = position.x;
    r->top  = position.y;
    r->right  = position.x + size.x;
    r->bottom = position.y + size.y;
    return true;
}

bool GetClientRect(HWND wnd, RECT *r) {
    POINT size = {}, position = {};
    if (!txOK()) return false;
    tx_gdk_enter();
#if (GTK_MAJOR_VERSION == 3)        
    size.x = gtk_widget_get_allocated_width (drawing_area);
    size.y = gtk_widget_get_allocated_height(drawing_area);
#else        
    size.x = gdk_window_get_width (gtk_widget_get_window(drawing_area));
    size.y = gdk_window_get_height(gtk_widget_get_window(drawing_area));
#endif        
    gdk_window_get_origin(gtk_widget_get_window(drawing_area), &position.x, &position.y);
    tx_gdk_leave();
    r->left = position.x;
    r->top  = position.y;
    r->right  = position.x + size.x;
    r->bottom = position.y + size.y;
    return true;
}

bool SetWindowText(HWND wnd, const char *caption) {
    if (!txOK()) return false;
    tx_gdk_enter();
    gtk_window_set_title(GTK_WINDOW(wnd), caption);
    tx_gdk_leave();
    return true;
}

HANDLE GetStdHandle(DWORD nStdHandle)
{
    if (nStdHandle == STD_INPUT_HANDLE ) return NULL;
    if (nStdHandle == STD_OUTPUT_HANDLE) return NULL;
    if (nStdHandle == STD_ERROR_HANDLE ) return NULL;
    return NULL;
}

static gint message_box_response_id = 0;

void message_box__response (GtkDialog *dialog, gint response_id, gpointer user_data)
{
    switch (response_id) {
        case GTK_RESPONSE_NONE   : message_box_response_id = 0; break;
        case GTK_RESPONSE_REJECT : message_box_response_id = IDABORT; break;
        case GTK_RESPONSE_ACCEPT : message_box_response_id = IDTRYAGAIN; break;
        case GTK_RESPONSE_DELETE_EVENT : message_box_response_id = IDTIMEOUT; break;
        case GTK_RESPONSE_OK     : message_box_response_id = IDOK; break;
        case GTK_RESPONSE_CANCEL : message_box_response_id = IDCANCEL; break;
        case GTK_RESPONSE_CLOSE  : message_box_response_id = IDCLOSE; break;
        case GTK_RESPONSE_YES    : message_box_response_id = IDYES; break;
        case GTK_RESPONSE_NO     : message_box_response_id = IDNO; break;
        case GTK_RESPONSE_APPLY  : message_box_response_id = IDRETRY; break;
        case GTK_RESPONSE_HELP   : message_box_response_id = IDHELP; break;
        default                  : message_box_response_id = 0; break;
    } 
}

int MessageBox_(HWND wnd, const char *text, const char *caption, UINT type, gint caption_len)
{
    if (!txOK()) return false;
    tx_gdk_enter();
    
    const int width_min = 40, DialogFontSize = 15;

    UINT type_win = type & MB_TYPEMASK;
    UINT icon_win = type & MB_ICONMASK;
    GtkWidget *dialog, *label, *content_area, *dialog_icon;
    GtkWidget * hbox, *hboxfix,*fixpos;
    hbox = gtk_hbox_new(false, 0);
    hboxfix = gtk_hbox_new(false, 10);
    dialog = gtk_dialog_new();
    content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));
    label = gtk_label_new (text);
    
    //gtk_dialog_add_button format: IconButton, ResponseCode. Null terminated
    switch (type_win) {
        case MB_OK         : gtk_dialog_add_button (GTK_DIALOG(dialog),GTK_STOCK_OK ,GTK_RESPONSE_OK); break;
        case MB_OKCANCEL   : gtk_dialog_add_buttons(GTK_DIALOG(dialog),GTK_STOCK_OK ,GTK_RESPONSE_OK ,GTK_STOCK_CANCEL,GTK_RESPONSE_CANCEL,NULL); break;
        case MB_YESNOCANCEL: gtk_dialog_add_buttons(GTK_DIALOG(dialog),GTK_STOCK_YES,GTK_RESPONSE_YES,GTK_STOCK_NO    ,GTK_RESPONSE_NO    ,GTK_STOCK_CANCEL,GTK_RESPONSE_CANCEL,NULL); break;
        case MB_YESNO      : gtk_dialog_add_buttons(GTK_DIALOG(dialog),GTK_STOCK_YES,GTK_RESPONSE_YES,GTK_STOCK_NO    ,GTK_RESPONSE_NO    ,NULL); break;
        default: gtk_dialog_add_button(GTK_DIALOG(dialog),GTK_STOCK_OK,GTK_RESPONSE_OK); break;
    }

    switch (icon_win) {
        case MB_ICONERROR        : dialog_icon=gtk_image_new_from_stock(GTK_STOCK_DIALOG_ERROR   ,GTK_ICON_SIZE_DIALOG); break;
        case MB_ICONINFORMATION  : dialog_icon=gtk_image_new_from_stock(GTK_STOCK_DIALOG_INFO    ,GTK_ICON_SIZE_DIALOG); break;
        case MB_ICONQUESTION     : dialog_icon=gtk_image_new_from_stock(GTK_STOCK_DIALOG_QUESTION,GTK_ICON_SIZE_DIALOG); break;
        case MB_ICONWARNING      : dialog_icon=gtk_image_new_from_stock(GTK_STOCK_DIALOG_WARNING ,GTK_ICON_SIZE_DIALOG); break;
//      case MB_ICONSTOP         : dialog_icon=gtk_image_new_from_stock(GTK_STOCK_STOP           ,GTK_ICON_SIZE_DIALOG); break; 
    }
    
    fixpos = gtk_fixed_new();
    gtk_container_add (GTK_CONTAINER (content_area), hbox);
    gtk_container_add (GTK_CONTAINER (hbox), fixpos);
    gtk_fixed_put(GTK_FIXED(fixpos), hboxfix, 0, 0);
    gtk_container_add (GTK_CONTAINER (hboxfix), dialog_icon);
    gtk_container_add (GTK_CONTAINER (hboxfix), label);

    gtk_window_set_resizable(GTK_WINDOW(dialog), false);
    gtk_window_set_title (GTK_WINDOW (dialog), caption);
    gtk_window_set_modal (GTK_WINDOW (dialog), true);
    gtk_window_position(GTK_WINDOW(dialog), GTK_WIN_POS_CENTER);
    
    g_signal_connect(dialog, "response", G_CALLBACK(message_box__response), NULL);
    GtkWidget *widget = GTK_WIDGET(dialog);
    gtk_widget_show_all (widget);
    GdkWindow *window = gtk_widget_get_window(widget);
    gint width_caption = DialogFontSize * caption_len;
    gint width = gdk_window_get_width (window), height = gdk_window_get_height(window);
    // if (width_caption > (width - width_min)) width = width_caption + width_min;
    gint min_width_caption = 100;
    if (width_caption < min_width_caption) width_caption=min_width_caption;
    if (width < width_caption+width_min) width = width_caption+width_min;
    gtk_widget_set_size_request(widget, width, height);
    message_box_response_id = 0;
    gtk_dialog_run (GTK_DIALOG (dialog));
    //dialog is a top level widget, so all other will be destroyed automatically.
    gtk_widget_destroy (dialog);
    tx_gdk_leave();
    return message_box_response_id;
}
int MessageBoxW(HWND wnd, LPCWSTR text_, LPCWSTR caption_, UINT type)
{
    return MessageBox_(wnd, (const char *)text_, (const char *)caption_, type, strlen((const char *)caption_));
}
int MessageBox (HWND wnd, LPCTSTR text_, LPCTSTR caption_, UINT type)
{
    string text_str    = WinToUtf(text_   ); const char *text    = text_str   .c_str();
    string caption_str = WinToUtf(caption_); const char *caption = caption_str.c_str();
    return MessageBox_(wnd, text, caption, type, caption_str.length());
}

#endif	/* TXLIBMAC_H */


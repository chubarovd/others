#include "stdio.h"
#include "TXLib.h"

const int razm = 20;
const int maxsizezmei = 50;
int DlinaZmei = 0;
int scores = 0;
int klavishi = 1;
int speed = 150;
int apple_X;
int apple_Y;

struct Napr_t
    {int x; int y;};

struct Zmeya_t
    {int x; int y;};

void Menu ();
void Knopka ();
void Igra ();
void Loading ();
void show ();
void Upravlenie (Zmeya_t body[maxsizezmei], Napr_t Napr[]);
void Eating (Zmeya_t body[maxsizezmei]);
bool Proverka(Zmeya_t body[maxsizezmei]);
bool TochkaInSnake (Zmeya_t body[maxsizezmei], int x, int y);
void Dobavlenie (Zmeya_t body[maxsizezmei], int x, int y);
void DrawZmei (Zmeya_t body[maxsizezmei]);
void Dvizhenie (Zmeya_t body[maxsizezmei], int x, int y);
void DrawApple (Zmeya_t body[maxsizezmei]);
void ScoresTable ();
void Peredvizhenie (Zmeya_t body[maxsizezmei], Napr_t Napr[]);
void Peremeshenie (Zmeya_t body[maxsizezmei]);

int pole[30][30] = {{}};

int main()
{
txCreateWindow(1000, 600);
Menu ();
return 0;
}

void Igra ()
{
txBegin ();

    Loading();
    show();
    txRectangle (600, 0, 1000, 600);
    Zmeya_t body[maxsizezmei] = {{}};
	Napr_t Napr[1] = {{0, 1}};
    Upravlenie(body, Napr);
    Menu();

txEnd ();
}

void show () {
    int v = 0;
    while (v < 30) {
        for (int h = 0; h < 30; h++) {
            if (pole[v][h] > 0) {
                txSetColor (RGB (255, 255, 255));
                txSetFillColor (TX_BLACK);
                txRectangle (razm*v, razm*h, razm*v + razm, razm*h + razm);
                }
            else {
                txSetColor (RGB (255, 255, 255));
                txSetFillColor (RGB (255, 255, 255));
                txRectangle (razm*v, razm*h, razm*v + razm, razm*h + razm);
                }
            }
            v++;
        }
    }

void Loading(){
    txSetFillColor (TX_BLACK);
    txClear();
    txDrawText(200, 100, 800, 300, "Loading...");
    for (int s = 0; s < 1000; s += 20){
        txSetColor (RGB (209, 226, 49));
        txSetFillColor (RGB (209, 226, 49));
        txRectangle (s, 350, s+15, 400);
        txSleep (30);
        }
    }

void Eating (Zmeya_t body[maxsizezmei]){
    Dobavlenie(body, apple_X, apple_Y);
    scores += 10;
    speed += -3;
    while (TochkaInSnake(body, apple_X, apple_Y)) {
        DrawApple(body);
    }
}


void Peremeshenie (Zmeya_t body[maxsizezmei]){
	if (body[DlinaZmei - 1].y == -1){
		body[DlinaZmei - 1].y = 30;
		klavishi = 0;
	}
	else if (body[DlinaZmei - 1].y == 30){
			body[DlinaZmei - 1].y = -1;
			klavishi = 0;
	}

	if (body[DlinaZmei - 1].x == 30){
		body[DlinaZmei - 1].x = -1;
		klavishi = 0;
	}
	else if (body[DlinaZmei - 1].x == -1){
			body[DlinaZmei - 1].x = 30;
			klavishi = 0;
	}
}

void Peredvizhenie (Zmeya_t body[],  Napr_t Napr[]){
    Dvizhenie(body, Napr[0].x, Napr[0].y);
    Proverka(body);
    DrawZmei(body);
    Peremeshenie(body);
}



void Upravlenie (Zmeya_t body[maxsizezmei], Napr_t Napr[]){
	DrawApple (body);

	Dobavlenie(body, 2, 2);
	Dobavlenie(body, 2, 3);
	Dobavlenie(body, 2, 4);
	Dobavlenie(body, 2, 5);
	DrawZmei (body);


	while (!GetAsyncKeyState (VK_ESCAPE) && TochkaInSnake(body, body[DlinaZmei - 1].x, body[DlinaZmei - 1].y)){

		if (klavishi != 1 && GetAsyncKeyState (VK_UP)){
			Napr[0].x = 0;
			Napr[0].y = -1;
			klavishi = 2;
		}

		if (klavishi != 3 && GetAsyncKeyState (VK_RIGHT)){
			Napr[0].x = 1;
			Napr[0].y = 0;
			klavishi = 4;
		}

		if (klavishi != 2 && GetAsyncKeyState (VK_DOWN)){
			Napr[0].x = 0;
			Napr[0].y = 1;
			klavishi = 1;
		}

		if (klavishi != 4 && GetAsyncKeyState (VK_LEFT)){
			Napr[0].x = -1;
			Napr[0].y = 0;
			klavishi = 3;
		}

		Peredvizhenie(body, Napr);

		ScoresTable();
		txSleep (speed);
    }
}

bool Proverka (Zmeya_t body[maxsizezmei]){
bool prov = false;
for (int i; i < (DlinaZmei - 1); i++){
    if (body[DlinaZmei - 1].x == body[i].x && body[DlinaZmei - 1].y == body[i].y){
        txSetFillColor (TX_BLACK);
        txClear();
        txSetColor (RGB (255, 255, 255));
        txDrawText(200, 100, 800, 300, "GAME OVER !!!");
        txSleep (10000);
        prov = true;
        }
    }
return prov;
}

bool TochkaInSnake(Zmeya_t body[maxsizezmei], int x, int y){
    for (int i = 0; i < DlinaZmei; i++){
        if (x == body[i].x && y == body[i].y){
            return true;
        }
    }
    return false;
}

void ScoresTable(){
	txSetColor (RGB (255, 153, 102), 10);
	txSetFillColor (RGB (209, 226, 49));
	txRectangle(605, 0, 1000, 600);
	txSetFillColor (RGB (250, 218, 221));
	txRectangle (700, 100, 900, 200);
	txSetColor (RGB (255, 153, 102), 10);
	txLine (700, 100, 900, 100);
	txLine (900, 100, 900, 200);
	txLine (900, 200, 700, 200);
	txLine (700, 200, 700, 100);

	txSetTextAlign(TA_LEFT | TA_TOP);
	txSetColor(RGB (255, 79, 0));
	txSelectFont("Arial", 20, 15, 0, false, false, false);
	char scores_text[50];
	sprintf(scores_text, "%d", scores);
	txTextOut(720, 120, "scores = ");
	txTextOut(830, 120, scores_text);
}

void DrawApple (Zmeya_t body[maxsizezmei]){
	srand (time(NULL));
	apple_X = rand () % 30;
	apple_Y = rand () % 30;
	txSetColor (RGB (255, 0, 0));
	txSetFillColor (RGB (255, 0, 0));
	txCircle (razm * apple_X + razm / 2, razm * apple_Y + razm / 2, 7);
}

void Dobavlenie (Zmeya_t body[maxsizezmei], int x, int y){
	body[DlinaZmei].x = x;
	body[DlinaZmei].y = y;
	DlinaZmei++;
}

void DrawZmei (Zmeya_t body[maxsizezmei]){
	txSetColor (RGB (255, 117, 24), 3);
	txSetFillColor (RGB (209, 226, 49));
	for (int i = 0; i < DlinaZmei; i++){
		txCircle (body[i].x*razm + razm/2, body[i].y*razm + razm/2, 7);
	}
}

void Dvizhenie (Zmeya_t body[maxsizezmei], int x, int y){
	txSetColor (RGB (255, 255, 255));
	txSetFillColor (RGB (255, 255, 255));
	txRectangle (body[0].x*razm, body[0].y*razm, body[0].x*razm + razm, body[0].y*razm + razm);

	int new_x = body[DlinaZmei - 1].x + x;
	int new_y = body[DlinaZmei - 1].y + y;

	if(TochkaInSnake(body, new_x, new_y)) {
		txSleep(3000);
	}

	if (new_x == apple_X && new_y == apple_Y) {
		Eating(body);
	}

	for (int i = 0; i < DlinaZmei - 1; i++)
		{
		body[i] = body[i + 1];
		}
	body[DlinaZmei - 1].x += x;
	body[DlinaZmei - 1].y += y;
}

void Menu (){
    while (!txMouseButtons()){
	txSetFillColor (TX_BLACK);
	txClear ();

	txSetTextAlign(TA_LEFT | TA_TOP);

	txSelectFont ("Comic Sans MS", 100, 100, false, false, false);
	txSetColor (TX_YELLOW);
	txTextOut (600, 25, "menu");

	txSelectFont ("Arial", 30, 70, 50, false, false, false);

	RECT NewGame = {50, 100, 450, 200};
	RECT Exit = {50, 400, 450, 500};
	RECT Records = {50, 250, 450, 350};

	txSetColor (TX_ORANGE, 5);
	txSetFillColor (TX_CYAN);
	txRectangle (NewGame.left + 25, NewGame.top + 25, NewGame.right - 25, NewGame.bottom - 25);
	txSetColor (TX_ORANGE);
	txTextOut (NewGame.left + 25, NewGame.top + 25, "new game");

	txSetColor (TX_ORANGE, 5);
	txSetFillColor (TX_CYAN);
	txRectangle (Records.left + 25, Records.top + 25, Records.right - 25, Records.bottom - 25);
	txSetColor (TX_ORANGE);
	txTextOut (Records.left + 25, Records.top + 25, "records");

	txSetColor (TX_ORANGE, 5);
	txSetFillColor (TX_CYAN);
	txRectangle (Exit.left + 25, Exit.top + 25, Exit.right - 25, Exit.bottom - 25);
	txSetColor (TX_ORANGE);
	txTextOut (Exit.left + 25, Exit.top + 25, "exit");

		for (;;){
			txBegin();
			if (In (txMousePos(), NewGame)){
				txSetColor (TX_ORANGE, 5);
				txSetFillColor (TX_CYAN);
				txRectangle (NewGame.left, NewGame.top, NewGame.right, NewGame.bottom);
				txSetColor (TX_ORANGE);
				txTextOut (NewGame.left + 25, NewGame.top + 25, "new game");

				  if (txMouseButtons())
					  {
					  txSetColor (TX_ORANGE, 5);
					  txSetFillColor (TX_CYAN);
					  txRectangle (NewGame.left, NewGame.top, NewGame.right, NewGame.bottom);

					  txSetColor (TX_ORANGE);
					  txTextOut (NewGame.left + 25, NewGame.top + 25, "new game");

					  Igra ();

					  break;
				  }
			}
			else{
			txSetColor (TX_BLACK, 10);
			txSetFillColor (TX_BLACK);
			txRectangle (NewGame.left, NewGame.top, NewGame.right, NewGame.bottom);
			txSetColor (TX_ORANGE);
			txTextOut (NewGame.left + 25, NewGame.top + 25, "new game");

			txSetColor (TX_ORANGE, 5);
			txSetFillColor (TX_CYAN);
			txRectangle (NewGame.left + 25, NewGame.top + 25, NewGame.right - 25, NewGame.bottom - 25);
			txSetColor (TX_ORANGE);
			txTextOut (NewGame.left + 25, NewGame.top + 25, "new game");
			}

			if (In (txMousePos(), Records)){
				txSetColor (TX_ORANGE, 5);
				txSetFillColor (TX_CYAN);
				txRectangle (Records.left, Records.top, Records.right, Records.bottom);
				txSetColor (TX_ORANGE);
				txTextOut (Records.left + 25, Records.top + 25, "records");

				if (txMouseButtons()){
				txSetColor (TX_ORANGE, 5);
				txSetFillColor (TX_CYAN);
				txRectangle (Records.left, Records.top, Records.right, Records.bottom);
				txSetColor (TX_ORANGE);
				txTextOut (Records.left + 25, Records.top + 25, "records");

				txSetColor (TX_ORANGE);
				txTextOut (Records.left + 25, Records.top + 25, "records");

				break;
				}
			}
			else{
			txSetColor (TX_BLACK, 10);
			txSetFillColor (TX_BLACK);
			txRectangle (Records.left, Records.top, Records.right, Records.bottom);
			txSetColor (TX_ORANGE);
			txTextOut (Records.left + 25, Records.top + 25, "records");

			txSetColor (TX_ORANGE, 5);
			txSetFillColor (TX_CYAN);
			txRectangle (Records.left + 25, Records.top + 25, Records.right - 25, Records.bottom - 25);
			txSetColor (TX_ORANGE);
			txTextOut (Records.left + 25, Records.top + 25, "records");
			}

			if (In (txMousePos(), Exit)){
				txSetColor (TX_ORANGE, 5);
				txSetFillColor (TX_CYAN);
				txRectangle (Exit.left, Exit.top, Exit.right, Exit.bottom);
				txSetColor (TX_ORANGE);
				txTextOut (Exit.left + 25, Exit.top + 25, "exit");

				if (txMouseButtons()){
					txSetColor (TX_ORANGE, 5);
					txSetFillColor (TX_CYAN);
					txRectangle (Exit.left, Exit.top, Exit.right, Exit.bottom);
					txSetColor (TX_ORANGE);
					txTextOut (Exit.left + 25, Exit.top + 25, "exit");

					txSetColor (TX_ORANGE);
					txTextOut (Exit.left + 25, Exit.top + 25, "exit");

					exit(-1);

					break;
				}
			}
			else{
			txSetColor (TX_BLACK, 10);
			txSetFillColor (TX_BLACK);
			txRectangle (Exit.left, Exit.top, Exit.right, Exit.bottom);
			txSetColor (TX_ORANGE);
			txTextOut (Exit.left + 25, Exit.top + 25, "exit");

			txSetColor (TX_ORANGE, 5);
			txSetFillColor (TX_CYAN);
			txRectangle (Exit.left + 25, Exit.top + 25, Exit.right - 25, Exit.bottom - 25);
			txSetColor (TX_ORANGE);
			txTextOut (Exit.left + 25, Exit.top + 25, "exit");
			}
		txEnd();
		}
	}
}

void Knopka (){

txSetTextAlign(TA_LEFT | TA_TOP);
txSelectFont ("Arial Rounded MT Bold", 100, 100, false, false, false);

RECT exit = {750, 350, 850, 400};

txSetColor (TX_ORANGE, 5);
txSetFillColor (TX_CYAN);
txRectangle (exit.left + 25, exit.top + 25, exit.right - 25, exit.bottom - 25);
txSetColor (TX_WHITE);
txTextOut (exit.left + 25, exit.top + 25, "exit");

for (;;){
    txBegin();
	if (In (txMousePos(), exit)){
	txSetColor (TX_ORANGE, 5);
	txSetFillColor (TX_CYAN);
	txRectangle (exit.left, exit.top, exit.right, exit.bottom);
	txSetColor (TX_ORANGE);
	txTextOut (exit.left + 25, exit.top + 25, "exit");

		if (txMouseButtons()){
			txSetColor (TX_ORANGE, 5);
			txSetFillColor (TX_CYAN);
			txRectangle (exit.left, exit.top, exit.right, exit.bottom);

			txSetColor (TX_ORANGE);
			txTextOut (exit.left + 25, exit.top + 25, "exit");

			Igra ();

			break;
			}
		}
		else{
		txSetColor (TX_BLACK, 10);
		txSetFillColor (TX_BLACK);
		txRectangle (exit.left, exit.top, exit.right, exit.bottom);
		txSetColor (TX_ORANGE);
		txTextOut (exit.left + 25, exit.top + 25, "exit");

		txSetColor (TX_ORANGE, 5);
		txSetFillColor (TX_CYAN);
		txRectangle (exit.left + 25, exit.top + 25, exit.right - 25, exit.bottom - 25);
		txSetColor (TX_ORANGE);
		txTextOut (exit.left + 25, exit.top + 25, "exit");
		}
		txEnd();
	}
}

